import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/widgets/dropdown/dropdown_item.dart';

class DropDownMenu<T> extends StatefulWidget {
  final double width;
  final Widget button;
  final Widget head;
  final List<DropdownItem> items;
  final Function(T e) onChanged;

  DropDownMenu(
      {Key key,
      this.width,
      this.head,
      @required this.button,
      @required this.items,
      @required this.onChanged})
      : super(key: key);

  @override
  _DropDownMenuState createState() => _DropDownMenuState<T>();
}

class _DropDownMenuState<T> extends State<DropDownMenu<T>> {
  GlobalKey _buttonKey;

  @override
  void initState() {
    super.initState();

    _buttonKey = new GlobalKey();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        final value = await _showDropDownItems();
        if (value != null) {
          widget.onChanged(value);
        }
      },
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(21.4),
        ),
        // color: Colors.red,
        // width: ScreenUtil().setWidth(29.57),
        child: Row(
          children: <Widget>[
            SizedBox(
              key: _buttonKey,
              height: ScreenUtil().setHeight(16.01),
              child: widget.button,
            ),
          ],
        ),
      ),
    );
  }

  Future<T> _showDropDownItems() async {
    final renderBox = _buttonKey.currentContext.findRenderObject() as RenderBox;
    final offset = renderBox.localToGlobal(Offset.zero);
    final size = renderBox.size;

    return await showGeneralDialog<T>(
      context: context,
      transitionDuration: Duration(milliseconds: 200),
      barrierDismissible: true,
      barrierLabel: '',
      pageBuilder: (BuildContext context, Animation<double> animation1,
          Animation<double> animation2) {
        return _DropdownItemsDialog(
          position: offset,
          size: size,
          width: widget.width,
          head: widget.head,
          items: widget.items,
          animation: animation1,
        );
      },
    );
  }
}

class _DropdownItemsDialog extends StatefulWidget {
  final Offset position;
  final Size size;
  final double width;
  final Widget head;
  final List<DropdownItem> items;
  final Animation<double> animation;

  _DropdownItemsDialog(
      {this.position,
      this.size,
      this.width,
      @required this.head,
      @required this.items,
      @required this.animation});

  @override
  __DropdownItemsDialogState createState() => __DropdownItemsDialogState();
}

class __DropdownItemsDialogState extends State<_DropdownItemsDialog>
    with TickerProviderStateMixin {
  double _animationValue;

  @override
  void initState() {
    super.initState();

    _animationValue = 0.0;

    widget.animation.addListener(() {
      if (mounted) {
        setState(() {
          _animationValue = widget.animation.value;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final padding = EdgeInsets.only(
      top: widget.head == null
          ? ScreenUtil().setHeight(33.3)
          : ScreenUtil().setHeight(19),
      bottom: ScreenUtil().setHeight(33.3),
    );

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                color: Colors.transparent,
              ),
            ),
          ),
          Positioned(
            top: widget.position == null ? 0 : widget.position.dy - padding.top,
            // left: widget.position == null
            //     ? 0
            //     : widget.position.dx,
            right: widget.position == null ||
                    Directionality.of(context) == TextDirection.rtl
                ? null
                : MediaQuery.of(context).size.width -
                    widget.position.dx -
                    widget.size.width -
                    ScreenUtil().setWidth(20.9),
            left: widget.position == null ||
                    Directionality.of(context) == TextDirection.ltr
                ? null
                : widget.position.dx -
                    ScreenUtil().setWidth(20.9),
            child: Container(
              width: widget.width,
              height: _animationValue == 0 ? 0 : null,
              padding: padding,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(7),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.16),
                    blurRadius: 40,
                  ),
                ],
              ),
              child: Column(
                children: <Widget>[
                  if (widget.head != null) ...[
                    InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(20.9),
                        ),
                        child: widget.head,
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(29)),
                  ],
                  SizeTransition(
                    axis: Axis.vertical,
                    sizeFactor: widget.animation,
                    child: Column(
                      children: <Widget>[
                        for (var i = 0; i < widget.items.length; i++)
                          Builder(
                            builder: (BuildContext context) {
                              final item = widget.items[i];

                              return InkWell(
                                onTap: () {
                                  Navigator.of(context).pop(item.value);
                                },
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(20.9),
                                      ),
                                      child: item.child,
                                    ),
                                    if (i < widget.items.length - 1) ...[
                                      SizedBox(
                                          height: ScreenUtil().setHeight(7)),
                                      Container(
                                        height: 0.58,
                                        // width: 100,
                                        color:
                                            Palette.slateGrey.withOpacity(0.08),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(7)),
                                    ],
                                  ],
                                ),
                              );
                            },
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
