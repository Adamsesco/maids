import 'package:flutter/material.dart';

class DropdownItem<T> extends StatelessWidget {
  final T value;
  final Widget child;

  DropdownItem({@required this.value, @required this.child});

  @override
  Widget build(BuildContext context) {
    return child;
  }
}
