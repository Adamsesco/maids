import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_maids/app/widgets/hm_cupertino_time_picker/hm_cupertino_time_picker.dart';
import 'package:intl/intl.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';

class HMTimePicker extends StatefulWidget {
  final DateTime initialDateTime;
  final Duration duration;

  HMTimePicker(
      {Key key, @required this.initialDateTime, @required this.duration})
      : super(key: key);

  @override
  _HMTimePickerState createState() => _HMTimePickerState();
}

class _HMTimePickerState extends State<HMTimePicker> {
  DateTime _selectedDateTime;

  @override
  void initState() {
    super.initState();

    _selectedDateTime = widget.initialDateTime;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(18.5),
        vertical: ScreenUtil().setHeight(17),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(9),
          bottomRight: Radius.circular(9),
        ),
        // color: Palette.cornflowerBlue,
        gradient: LinearGradient(
          colors: [
            Palette.royalBlue,
            Palette.cornflowerBlue3,
            Palette.royalBlue,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  DateFormat('EEEE').format(widget.initialDateTime),
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(15),
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                        // height: 0.28,
                      ),
                ),
                Text(
                  DateFormat('d MMMM y').format(widget.initialDateTime),
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(15),
                        fontWeight: FontWeight.w300,
                        color: Colors.white,
                        // height: 0.28,
                      ),
                ),
                Text(
                  DateFormat('HH:mm').format(_selectedDateTime) +
                      ' - ' +
                      DateFormat('HH:mm')
                          .format(_selectedDateTime.add(widget.duration)),
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(15),
                        fontWeight: FontWeight.w300,
                        color: Colors.white,
                        // height: 0.28,
                      ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: CupertinoTheme(
              data: CupertinoThemeData(
                textTheme: CupertinoTextThemeData(
                  dateTimePickerTextStyle:
                      Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(20),
                            fontWeight: FontWeight.w800,
                            color: Colors.white,
                          ),
                ),
              ),
              child: HMCupertinoTimePicker(
                initialDateTime: widget.initialDateTime,
                mode: CupertinoDatePickerMode.time,
                onDateTimeChanged: (DateTime dateTime) {
                  setState(() {
                    _selectedDateTime = dateTime;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
