import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/chat_screen/chat_screen.dart';
import 'package:home_maids/app/screens/company_orders_screen/widgets/decline_dialog.dart';
import 'package:home_maids/app/screens/reviews_screen/reviews_screen.dart';
import 'package:home_maids/app/stores/application_store.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/message.dart';
import 'package:home_maids/app/stores/order.dart';
import 'package:home_maids/app/types/order_status.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:home_maids/app/utils/order_utils.dart';
import 'package:home_maids/app/utils/payment_method_utils.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/app/widgets/feedback_dialog/feedback_dialog.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ExpandibleOrderItem extends StatefulWidget {
  final Order order;

  ExpandibleOrderItem({Key key, @required this.order}) : super(key: key);

  @override
  _ExpandibleOrderItemState createState() => _ExpandibleOrderItemState();
}

class _ExpandibleOrderItemState extends State<ExpandibleOrderItem>
    with TickerProviderStateMixin {
  User _me;
  AnimationController _animationController;
  Animation<double> _animation;
  bool _isExpanded;

  @override
  void initState() {
    super.initState();

    final client = Provider.of<Client>(context, listen: false);
    final company = Provider.of<Company>(context, listen: false);
    if (client != null && client.id != null) {
      _me = client;
    } else {
      _me = company;
    }

    _isExpanded = false;

    _animationController = AnimationController(
        duration: const Duration(milliseconds: 200), vsync: this);
    _animation = Tween(begin: 0.0, end: 1.0).animate(_animationController)
      ..addListener(() {
        if (_animation.value == 0.0 &&
            _animationController.status == AnimationStatus.dismissed &&
            _isExpanded) {
          _isExpanded = false;
        }
        setState(() {});
      });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appStore = Provider.of<ApplicationStore>(context, listen: false);

    final client = widget.order.client;
    final company = widget.order.company;
    final enabled = widget.order.status != OrderStatus.declined;
    final orderNumber = widget.order.number ?? 0;
    final orderNumberStr =
        List.generate(6 - orderNumber.toString().length, (_) => '0').join() +
            orderNumber.toString();

    final orderDetailsShort = Row(
      children: <Widget>[
        SizedBox(width: ScreenUtil().setWidth(19)),
        SizedBox(
          width: ScreenUtil().setWidth(151),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                _me.role == UserRole.company
                    ? "${client.firstName} ${client.lastName}"
                    : company.name,
                style: Theme.of(context).textTheme.subtitle.copyWith(
                      fontSize: ScreenUtil().setSp(14),
                      letterSpacing: 0,
                      color: enabled ? Palette.royalBlue : Palette.cadetBlue,
                    ),
              ),
              if (_me.role == UserRole.company)
                Text(
                  widget.order.address.address,
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(10),
                        fontWeight: FontWeight.w400,
                        letterSpacing: 0.50,
                        color: enabled ? Palette.fiord : Palette.cadetBlue,
                      ),
                ),
              if (_me.role == UserRole.client)
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      PageRouteBuilder(
                        pageBuilder: (context, animation, secondaryAnimation) =>
                            ReviewsScreen(company: company),
                        transitionsBuilder:
                            (context, animation, secondaryAnimation, child) {
                          return child;
                        },
                      ),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: ScreenUtil().setHeight(4),
                    ),
                    child: Row(
                      children: <Widget>[
                        RatingBar(
                          initialRating: company.rating ?? 0,
                          ignoreGestures: true,
                          allowHalfRating: true,
                          onRatingUpdate: (_) {},
                          itemSize: ScreenUtil().setHeight(11.4),
                          itemPadding: EdgeInsets.symmetric(
                            horizontal: 2,
                          ),
                          ratingWidget: RatingWidget(
                            full: Image(
                              image: AssetImage('assets/images/star-full.png'),
                            ),
                            half: Image(
                              image: AssetImage('assets/images/star-half.png'),
                            ),
                            empty: Image(
                              image: AssetImage('assets/images/star-empty.png'),
                            ),
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(12)),
                        Text(
                          "${company.reviews == null ? 0 : company.reviews.length} ${S.of(context).Reviews}",
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(10),
                                color: Palette.orangePeel,
                              ),
                        ),
                      ],
                    ),
                  ),
                ),
              SizedBox(height: ScreenUtil().setHeight(15)),
              Text(
                DateFormat('HH:mm').format(widget.order.date) +
                    ' - ' +
                    DateFormat('HH:mm')
                        .format(widget.order.date.add(widget.order.duration)),
                style: Theme.of(context).textTheme.display1.copyWith(
                      fontSize: ScreenUtil().setSp(10),
                      fontWeight: FontWeight.w300,
                      color: enabled ? Palette.fiord : Palette.cadetBlue,
                    ),
              ),
              Text(
                "${S.of(context).ORDER_N} $orderNumberStr",
                style: Theme.of(context).textTheme.display1.copyWith(
                      fontSize: ScreenUtil().setSp(9),
                      color: enabled ? Palette.royalBlue : Palette.cadetBlue,
                    ),
              ),
            ],
          ),
        ),
        SizedBox(width: ScreenUtil().setWidth(30)),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              DateFormat('dd').format(widget.order.date),
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(24),
                    color: enabled ? Palette.royalBlue : Palette.cadetBlue,
                    fontWeight: FontWeight.w800,
                  ),
            ),
            Text(
              DateFormat('MMM').format(widget.order.date).toUpperCase(),
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(15),
                    color: enabled ? Palette.royalBlue : Palette.cadetBlue,
                    letterSpacing: -0.10,
                    fontWeight: FontWeight.w300,
                    height: 0.8,
                  ),
            ),
          ],
        ),
      ],
    );

    final header = orderDetailsShort;

    return InkWell(
      onTap: enabled
          ? () {
              setState(() {
                if (!_isExpanded) {
                  _isExpanded = true;
                  _animationController.forward();
                } else {
                  _animationController.reverse();
                }
              });
            }
          : null,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(8),
          vertical: ScreenUtil().setHeight(7),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          gradient: LinearGradient(
            colors: [
              !enabled ? Color(0xFFF2F2F2) : getOrderColor(widget.order.status),
              !enabled ? Color(0xFFF2F2F2) : Colors.white,
            ],
            stops: [0.02, 0.02],
          ),
          boxShadow: enabled
              ? [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.11),
                    blurRadius: 40,
                  ),
                ]
              : null,
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(15),
                right: ScreenUtil().setWidth(22.6),
                bottom: ScreenUtil().setHeight(15),
                left: ScreenUtil().setWidth(21),
              ),
              child: Row(
                children: <Widget>[
                  Container(
                    foregroundDecoration: !enabled
                        ? BoxDecoration(
                            color: Color(0xFF000000),
                            backgroundBlendMode: BlendMode.saturation)
                        : null,
                    child: Avatar(
                      _me.role == UserRole.company
                          ? client.avatarUrl
                          : company.avatarUrl,
                      width: ScreenUtil().setWidth(81),
                      height: ScreenUtil().setWidth(81),
                      hasShadow: widget.order.status != OrderStatus.declined,
                    ),
                  ),
                  header,
                ],
              ),
            ),
            if (_isExpanded)
              SizeTransition(
                axis: Axis.vertical,
                sizeFactor: _animation,
                child: Padding(
                  padding: EdgeInsets.only(
                    top: ScreenUtil().setHeight(13),
                    right: ScreenUtil().setWidth(32),
                    bottom: ScreenUtil().setHeight(23.9),
                    left: ScreenUtil().setWidth(21),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "${S.of(context).ORDER_N} $orderNumberStr ",
                              style:
                                  Theme.of(context).textTheme.display1.copyWith(
                                        fontSize: ScreenUtil().setSp(11),
                                        fontWeight: FontWeight.w600,
                                        letterSpacing: 0,
                                        color: Palette.fiord,
                                      ),
                            ),
                            TextSpan(
                              text: "( " +
                                  DateFormat('dd MMMM y')
                                      .format(widget.order.date)
                                      .toUpperCase() +
                                  ' AT ' +
                                  DateFormat('HH:mm')
                                      .format(widget.order.date) +
                                  ' )',
                              style:
                                  Theme.of(context).textTheme.display1.copyWith(
                                        fontSize: ScreenUtil().setSp(11),
                                        fontWeight: FontWeight.w300,
                                        letterSpacing: 0,
                                        color: Palette.fiord,
                                      ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "${S.of(context).ORDERED_SERVICES}: ",
                            style:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(11),
                                      fontWeight: FontWeight.w300,
                                      letterSpacing: 0,
                                      color: Palette.fiord,
                                    ),
                          ),
                          Expanded(
                            child: Text(
                              widget.order.services
                                      ?.map((service) => service
                                          .name[appStore.locale.languageCode])
                                      ?.fold('', (curr, previous) {
                                    String separator;
                                    if (curr == null || curr.isEmpty) {
                                      separator = '';
                                    } else {
                                      separator = ', ';
                                    }
                                    return "$previous$separator$curr";
                                  }) ??
                                  '',
                              style:
                                  Theme.of(context).textTheme.display1.copyWith(
                                        fontSize: ScreenUtil().setSp(11),
                                        fontWeight: FontWeight.w300,
                                        letterSpacing: 0,
                                        color: Palette.fiord,
                                      ),
                            ),
                          ),
                        ],
                      ),
                      Text(
                        "${S.of(context).DATE}: " +
                            DateFormat('dd MMMM y').format(widget.order.date),
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(11),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0,
                              color: Palette.fiord,
                            ),
                      ),
                      Text(
                        "${S.of(context).TIME}: " +
                            DateFormat('HH:mm').format(widget.order.date) +
                            ' - ' +
                            DateFormat('HH:mm').format(
                                widget.order.date.add(widget.order.duration)),
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(11),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0,
                              color: Palette.fiord,
                            ),
                      ),
                      Text(
                        "${S.of(context).N_OF_MAIDS}: ${widget.order.maidsCount}",
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(11),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0,
                              color: Palette.fiord,
                            ),
                      ),
                      Text(
                        "${S.of(context).DURATION}: ${widget.order.duration.inHours} ${S.of(context).HOURS}",
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(11),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0,
                              color: Palette.fiord,
                            ),
                      ),
                      Text(
                        "${S.of(context).PAYMENT_METHOD}: ${paymentMethodToString(widget.order.paymentMethod)}",
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(11),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0,
                              color: Palette.fiord,
                            ),
                      ),
                      Text(
                        "${S.of(context).PRICE}: ${widget.order.price}",
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(11),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0,
                              color: Palette.fiord,
                            ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(10)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          _TalkTo(
                            client: _me.role == UserRole.company
                                ? widget.order.client
                                : null,
                            company: _me.role == UserRole.client
                                ? widget.order.company
                                : null,
                          ),
                          _OrderAction(
                            widget.order,
                            onOrderUpdated: () => setState(() {}),
                          ),
                        ],
                      ),
                      if (widget.order.status == OrderStatus.pending) ...[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            // _TalkToClient(widget.order.client.firstName),
                            if (_me.role == UserRole.company)
                              _DeclineOrder(
                                widget.order,
                                onDecline: () {
                                  setState(() {
                                    if (widget.order.status ==
                                        OrderStatus.declined) {
                                      _isExpanded = false;
                                    }
                                  });
                                },
                              ),
                          ],
                        ),
                      ],
                    ],
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

class _LeaveFeedback extends StatelessWidget {
  final Order order;
  final Function onFeedback;

  _LeaveFeedback(this.order, {this.onFeedback});

  Future<void> _leaveFeedback(BuildContext context, Order order) async {
    await showGeneralDialog(
      context: context,
      transitionDuration: Duration(milliseconds: 200),
      barrierDismissible: true,
      barrierLabel: '',
      pageBuilder: (BuildContext context, Animation<double> animation1,
          Animation<double> animation2) {
        return FeedbackDialog(order: order);
      },
    );

    if (onFeedback != null) {
      onFeedback();
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _leaveFeedback(context, order);
      },
      child: Row(
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/feedback-icon.png'),
            height: ScreenUtil().setHeight(18.89),
          ),
          SizedBox(width: ScreenUtil().setWidth(9.4)),
          Text(
            S.of(context).Leave_a_feedback,
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontSize: ScreenUtil().setSp(10),
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0,
                  color: Palette.royalBlue,
                ),
          ),
        ],
      ),
    );
  }
}

class _DeclineOrder extends StatelessWidget {
  final Order order;
  final Function onDecline;

  _DeclineOrder(this.order, {this.onDecline});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _declineOrder(context, order),
      child: Row(
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/Component 4 – 2.png'),
            height: ScreenUtil().setHeight(7.61),
          ),
          SizedBox(width: ScreenUtil().setWidth(7.7)),
          Text(
            S.of(context).Decline_this_order,
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontSize: ScreenUtil().setSp(9),
                  fontWeight: FontWeight.w600,
                  letterSpacing: 0,
                  color: Colors.red,
                ),
          ),
        ],
      ),
    );
  }

  Future<void> _declineOrder(BuildContext context, Order order) async {
    await showGeneralDialog(
      context: context,
      transitionDuration: Duration(milliseconds: 200),
      barrierDismissible: true,
      barrierLabel: '',
      pageBuilder: (BuildContext context, Animation<double> animation1,
          Animation<double> animation2) {
        return DeclineDialog(order: order);
      },
    );

    if (onDecline != null) {
      onDecline();
    }
  }
}

class _TalkTo extends StatelessWidget {
  final Client client;
  final Company company;

  _TalkTo({this.client, this.company});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        User me;
        User other;

        final meCompany = Provider.of<Company>(context, listen: false);
        final meClient = Provider.of<Client>(context, listen: false);
        if (meCompany != null && meCompany.id != null) {
          me = meCompany;
          other = client;
        } else {
          me = meClient;
          other = company;
        }

        final message = Message()
          ..setData(
            sender: me,
            recipient: other,
            isArchived: false,
            isRead: false,
          );

        Navigator.of(context).push(
          PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) =>
                ChatScreen(message: message),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return child;
            },
          ),
        );
      },
      child: Row(
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/noun_chat_881563.png'),
            height: ScreenUtil().setHeight(14.06),
          ),
          SizedBox(width: ScreenUtil().setWidth(8.9)),
          Builder(builder: (context) {
            final text = client == null ? S.of(context).us : client.firstName;
            return Text(
              "${S.of(context).Talk_to} $text",
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(9),
                    fontWeight: FontWeight.w700,
                    letterSpacing: 0,
                    color: Palette.royalBlue,
                  ),
            );
          }),
        ],
      ),
    );
  }
}

class _OrderAction extends StatefulWidget {
  final Order order;
  final Function onOrderUpdated;

  _OrderAction(this.order, {Key key, @required this.onOrderUpdated})
      : super(key: key);

  @override
  __OrderActionState createState() => __OrderActionState();
}

class __OrderActionState extends State<_OrderAction> {
  bool _isLoading;

  @override
  void initState() {
    super.initState();

    _isLoading = false;
  }

  Future<void> _updateOrderStatus(OrderStatus status) async {
    final statusCode = orderStatusCode(status);

    setState(() {
      _isLoading = true;
    });

    final formData = FormData.fromMap({
      'orderid': int.tryParse(widget.order.id),
      'status': statusCode,
    });

    final request = await Dio().post<String>(
        'http://washy-car.com/homemaids/updateOrder',
        data: formData);
    final response = request.data;
    if (request == null ||
        request.statusCode != 200 ||
        response == null ||
        response.isEmpty) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(S.of(context).An_error_happened),
          content: Text(S
              .of(context)
              .An_error_happened_when_trying_to_update_the_status_of_this_order),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );

      return;
    }

    setState(() {
      widget.order.status = status;
      widget.onOrderUpdated();
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final client = Provider.of<Client>(context, listen: false);

    final actionTexts = <OrderStatus, String>{
      OrderStatus.pending: S.of(context).TAKE_THIS_ORDER,
      OrderStatus.inProgress: S.of(context).ORDER_IN_PROGRESS,
      OrderStatus.delayed: S.of(context).ORDER_DELAYED,
      OrderStatus.done: S.of(context).ORDER_COMPLETED,
      OrderStatus.confirmed: S.of(context).ORDER_COMPLETED,
      OrderStatus.cancelled: S.of(context).ORDER_CANCELED,
      OrderStatus.declined: S.of(context).ORDER_DECLINED,
    };

    final actionIcons = <OrderStatus, Widget>{
      OrderStatus.pending: RotatedBox(
        quarterTurns: 2,
        child: Image(
          image: AssetImage('assets/images/noun_Arrow_2335767.png'),
          height: ScreenUtil().setHeight(13.36),
          color: getOrderColor(widget.order.status),
          matchTextDirection: true,
        ),
      ),
      OrderStatus.inProgress: Container(),
      OrderStatus.delayed: Container(),
      OrderStatus.done: Image(
        image: AssetImage('assets/images/Component 3 – 2.png'),
        height: ScreenUtil().setHeight(21),
        color: getOrderColor(widget.order.status),
        matchTextDirection: true,
      ),
      OrderStatus.confirmed: Image(
        image: AssetImage('assets/images/Component 3 – 2.png'),
        height: ScreenUtil().setHeight(21),
        color: getOrderColor(widget.order.status),
        matchTextDirection: true,
      ),
      OrderStatus.cancelled: Image(
        image: AssetImage('assets/images/Component 4 – 2.png'),
        height: ScreenUtil().setHeight(21),
        color: getOrderColor(widget.order.status),
        matchTextDirection: true,
      ),
      OrderStatus.declined: Image(
        image: AssetImage('assets/images/Component 4 – 2.png'),
        height: ScreenUtil().setHeight(21),
        color: getOrderColor(widget.order.status),
        matchTextDirection: true,
      ),
    };

    final button = InkWell(
      onTap: () {
        if (widget.order.status == OrderStatus.pending) {
          _updateOrderStatus(OrderStatus.inProgress);
        }
      },
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(20),
          vertical: ScreenUtil().setHeight(15.4),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Palette.cornflowerBlue,
        ),
        child: _isLoading
            ? CupertinoActivityIndicator()
            : Text(
                actionTexts[widget.order.status],
                style: Theme.of(context).textTheme.subtitle.copyWith(
                      fontSize: ScreenUtil().setSp(12),
                      letterSpacing: 0,
                      color: Colors.white,
                    ),
              ),
      ),
    );

    final text = Row(
      children: <Widget>[
        Text(
          actionTexts[widget.order.status],
          textAlign: TextAlign.end,
          style: Theme.of(context).textTheme.subtitle.copyWith(
                fontSize: ScreenUtil().setSp(16),
                letterSpacing: 0,
                color: getOrderColor(widget.order.status),
              ),
        ),
        SizedBox(width: ScreenUtil().setWidth(13.1)),
        actionIcons[widget.order.status],
      ],
    );

    if (client != null && client.id != null) {
      if (widget.order.status == OrderStatus.done ||
          widget.order.status == OrderStatus.complete ||
          widget.order.status == OrderStatus.confirmed) {
        return _LeaveFeedback(widget.order);
      } else {
        return Container();
      }
    }
    return widget.order.status == OrderStatus.pending ? button : text;
  }
}
