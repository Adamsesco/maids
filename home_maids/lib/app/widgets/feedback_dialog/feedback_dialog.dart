import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/order.dart';
import 'package:home_maids/app/widgets/hmbutton/hmbutton.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class FeedbackDialog extends StatefulWidget {
  final Order order;

  FeedbackDialog({Key key, @required this.order}) : super(key: key);

  @override
  _FeedbackDialogState createState() => _FeedbackDialogState();
}

class _FeedbackDialogState extends State<FeedbackDialog> {
  double _rating = 3.0;
  bool _isNegativeFeedback = false;
  String _review;
  bool _isLoading = false;
  bool _isUpdated = false;

  Future<void> _submit() async {
    if (_isNegativeFeedback && (_review == null || _review.isEmpty)) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(S.of(context).Error),
          content: Text(S
              .of(context)
              .You_should_write_a_review_if_you_want_to_leave_a_negative_feedback),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );

      return;
    }

    setState(() {
      _isLoading = true;
    });

    final client = Provider.of<Client>(context, listen: false);
    final formData = FormData.fromMap({
      'userId': client.id,
      'companyId': widget.order.company.id,
      'orderId': widget.order.id,
      if (_review != null) 'Review': _review,
      'Rate': _isNegativeFeedback ? 0 : _rating,
    });

    final response = await Dio().post<String>(
        'http://www.washy-car.com/homemaids/api/addReview',
        data: formData);
    if (response == null ||
        response.statusCode != 200 ||
        response.data == null ||
        response.data.isEmpty) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(S.of(context).Error),
          content: Text(S
              .of(context)
              .An_unexpected_error_happened_Please_try_submitting_your_review_again),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });

      return;
    }

    setState(() {
      _isLoading = false;
      _isUpdated = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned.fill(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 20, sigmaY: 20),
              child: Container(
                color: Colors.white.withOpacity(0.2),
              ),
            ),
          ),
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(142),
                right: ScreenUtil().setWidth(24),
                left: ScreenUtil().setWidth(24),
              ),
              padding: EdgeInsetsDirectional.only(
                start: ScreenUtil().setWidth(18),
                end: ScreenUtil().setWidth(18),
                bottom: ScreenUtil().setHeight(16.3),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.16),
                    blurRadius: 56,
                  ),
                ],
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: ScreenUtil().setHeight(21),
                          ),
                          child: Image(
                            image: AssetImage('assets/images/close-icon.png'),
                            color: Palette.fiord,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(28.0 - 21.0)),
                  Text(
                    S.of(context).LEAVE_A_FEEDBACK,
                    style: Theme.of(context).textTheme.subtitle.copyWith(
                          fontSize: ScreenUtil().setSp(17),
                          fontWeight: FontWeight.w800,
                          letterSpacing: -0.30,
                        ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(10.6)),
                  RatingBar(
                    initialRating: _rating,
                    ignoreGestures: _isNegativeFeedback,
                    allowHalfRating: true,
                    onRatingUpdate: (double value) {
                      setState(() {
                        _rating = value;
                      });
                    },
                    itemSize: ScreenUtil().setHeight(23),
                    itemPadding: EdgeInsets.symmetric(
                      horizontal: 2,
                    ),
                    ratingWidget: RatingWidget(
                      full: Image(
                        image: _isNegativeFeedback
                            ? AssetImage('assets/images/star-empty.png')
                            : AssetImage('assets/images/star-full.png'),
                        color: _isNegativeFeedback ? Palette.cadetBlue : null,
                      ),
                      half: Image(
                        image: _isNegativeFeedback
                            ? AssetImage('assets/images/star-empty.png')
                            : AssetImage('assets/images/star-half.png'),
                        color: _isNegativeFeedback ? Palette.cadetBlue : null,
                      ),
                      empty: Image(
                        image: AssetImage('assets/images/star-empty.png'),
                        color: Palette.cadetBlue,
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(36.7)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        S.of(context).NEGATIVE_FEEDBACK,
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(11),
                              fontWeight: FontWeight.w800,
                              letterSpacing: 0.30,
                              color: _isNegativeFeedback
                                  ? Colors.red
                                  : Palette.cadetBlue,
                            ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(29.4)),
                      Transform.scale(
                        scale: 0.9,
                        child: CupertinoSwitch(
                          value: _isNegativeFeedback,
                          onChanged: (bool value) {
                            setState(() {
                              _isNegativeFeedback = value;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(32)),
                  DottedBorder(
                    borderType: BorderType.RRect,
                    color: Palette.cadetBlue,
                    radius: Radius.circular(5),
                    strokeWidth: 0.3,
                    child: Container(
                      width: MediaQuery.of(context).size.width -
                          ScreenUtil().setWidth(18.0 * 2),
                      // height: ScreenUtil().setHeight(220.13),
                      padding: EdgeInsetsDirectional.only(
                        top: ScreenUtil().setHeight(44),
                        bottom: ScreenUtil().setHeight(16),
                        start: ScreenUtil().setWidth(24),
                        end: ScreenUtil().setWidth(24),
                      ),
                      color: Color(0xFFFDFDFD),
                      child: Column(
                        children: <Widget>[
                          TextField(
                            onChanged: (String value) {
                              setState(() {
                                _review = value;
                              });
                            },
                            maxLines: 10,
                            maxLength: 250,
                            maxLengthEnforced: true,
                            buildCounter: (BuildContext context,
                                    {int currentLength,
                                    int maxLength,
                                    bool isFocused}) =>
                                Container(),
                            style:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(11),
                                      letterSpacing: 0.30,
                                      color: Palette.fiord,
                                    ),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.zero,
                              border: InputBorder.none,
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(10)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Builder(
                                builder: (context) {
                                  final textLength = _review?.length ?? 0;

                                  return Text(
                                    "$textLength/250",
                                    style: Theme.of(context)
                                        .textTheme
                                        .display1
                                        .copyWith(
                                          fontSize: ScreenUtil().setSp(10),
                                          letterSpacing: 0.30,
                                          color: textLength == 250
                                              ? Colors.red
                                              : Palette.cadetBlue,
                                        ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(22.9)),
                  Align(
                    alignment: AlignmentDirectional.bottomEnd,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        HMButton(
                          onTap: () {
                            _submit();
                          },
                          hasUpdateIcon: _isLoading,
                          isLoading: _isLoading,
                          done: _isUpdated,
                          text: Text(
                            S.of(context).SUBMIT,
                            style:
                                Theme.of(context).textTheme.subtitle.copyWith(
                                      fontSize: ScreenUtil().setSp(13),
                                      letterSpacing: 0,
                                      color: Palette.cornflowerBlue,
                                    ),
                          ),
                          doneText: Text(
                            S.of(context).SUBMITTED,
                            style:
                                Theme.of(context).textTheme.subtitle.copyWith(
                                      fontSize: ScreenUtil().setSp(13),
                                      letterSpacing: 0,
                                      color: Palette.cornflowerBlue,
                                    ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
