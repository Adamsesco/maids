import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';

class HMButton extends StatelessWidget {
  final bool isLoading;
  final bool done;
  final Widget text;
  final Widget doneText;
  final Function onTap;
  final bool hasUpdateIcon;
  final bool hasAnimations;

  HMButton({
    this.isLoading = false,
    this.done = false,
    @required this.text,
    @required this.doneText,
    this.onTap,
    this.hasUpdateIcon = true,
    this.hasAnimations = true,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(17),
        ),
        // width: ScreenUtil().setWidth(136.14),
        height: ScreenUtil().setHeight(45.68),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
          border: Border.all(
            color: Palette.cornflowerBlue,
          ),
        ),
        child: Row(
          children: <Widget>[
            if (!done) text,
            if (done) doneText,
            if (hasUpdateIcon && !done) ...[
              SizedBox(width: ScreenUtil().setWidth(17.9)),
              Builder(
                builder: (BuildContext context) {
                  final spinner = Image(
                    image: AssetImage('assets/images/update-icon.png'),
                    width: ScreenUtil().setWidth(18.27),
                  );

                  if (!isLoading) {
                    return spinner;
                  }

                  return ControlledAnimation(
                    playback: Playback.LOOP,
                    duration: Duration(milliseconds: 700),
                    tween: Tween<double>(begin: 0.0, end: 1.0),
                    builder: (context, animation) {
                      return Transform.rotate(
                        angle: 2 * pi * animation,
                        child: spinner,
                      );
                    },
                  );
                },
              ),
            ],
            if (done) ...[
              SizedBox(width: ScreenUtil().setWidth(17.9)),
              ControlledAnimation(
                duration: Duration(milliseconds: 400),
                tween: Tween<double>(begin: 0.0, end: 1.0),
                builder: (context, animation) {
                  return Stack(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/Component 3 – 2.png'),
                        width: ScreenUtil().setWidth(20.11),
                        color: Palette.cornflowerBlue,
                      ),
                      Positioned(
                        right: 0,
                        height: ScreenUtil().setWidth(20.11),
                        width: ScreenUtil().setWidth(20.11) *
                            (hasAnimations ? (1.0 - animation) : 0),
                        child: Container(
                          color: Colors.white,
                        ),
                      ),
                    ],
                  );
                },
              ),
            ],
          ],
        ),
      ),
    );
  }
}
