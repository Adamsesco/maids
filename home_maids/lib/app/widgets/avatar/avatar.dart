import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Avatar extends StatelessWidget {
  final double width;
  final double height;
  final String avatarUrl;
  final File avatarFile;
  final bool hasShadow;

  Avatar(this.avatarUrl,
      {this.avatarFile,
      @required this.width,
      @required this.height,
      this.hasShadow = true});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(13),
        color: Colors.white,
        image: DecorationImage(
          image: avatarFile == null
              ? avatarUrl == null
                  ? AssetImage('assets/images/noun_Alert_18418.png')
                  : NetworkImage(avatarUrl)
              : FileImage(avatarFile),
          fit: BoxFit.cover,
        ),
        boxShadow: hasShadow
            ? [
                BoxShadow(
                  color: Colors.black.withOpacity(0.16),
                  offset: Offset(0, ScreenUtil().setHeight(3)),
                  blurRadius: 6,
                ),
              ]
            : null,
      ),
    );
  }
}
