import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/client_dashboard_screen/client_dashboard_screen.dart';
import 'package:home_maids/app/screens/client_profile_edit_screen/client_profile_edit_screen.dart';
import 'package:home_maids/app/screens/notifications_screen/notifications_screen.dart';
import 'package:home_maids/app/screens/services_selection_screen/services_selection_screen.dart';

class ClientNavigationBar extends StatelessWidget {
  final int currentIndex;

  ClientNavigationBar({this.currentIndex});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Palette.royalBlue,
      type: BottomNavigationBarType.fixed,
      currentIndex: currentIndex,
      onTap: (int index) {
        if (currentIndex == index) return;

        switch (index) {
          case 0:
            Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    ClientDashboardScreen(),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return child;
                },
              ),
            );
            break;

          case 1:
            Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    ServicesSelectionScreen(),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return child;
                },
              ),
            );
            break;

          case 2:
            Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    NotificationsScreen(),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return child;
                },
              ),
            );
            break;

          case 3:
            Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    ClientProfileEditScreen(),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return child;
                },
              ),
            );
            break;
        }
      },
      items: [
        BottomNavigationBarItem(
          title: Container(),
          icon: Image(
            image: AssetImage('assets/images/orders-list-icon-inactive.png'),
            height: ScreenUtil().setHeight(23.26),
            fit: BoxFit.fitHeight,
          ),
          activeIcon: Image(
            image: AssetImage('assets/images/orders-list-icon-active.png'),
            height: ScreenUtil().setHeight(23.26),
            fit: BoxFit.fitHeight,
          ),
        ),
        BottomNavigationBarItem(
          title: Container(),
          icon: Image(
            image: AssetImage('assets/images/new-order-icon-inactive.png'),
            height: ScreenUtil().setHeight(30),
            fit: BoxFit.fitHeight,
          ),
          activeIcon: Image(
            image: AssetImage('assets/images/new-order-icon-active.png'),
            height: ScreenUtil().setHeight(30),
            fit: BoxFit.fitHeight,
          ),
        ),
        BottomNavigationBarItem(
          title: Container(),
          icon: Image(
            image: AssetImage('assets/images/notifications-icon-inactive.png'),
            height: ScreenUtil().setHeight(26.08),
            fit: BoxFit.fitHeight,
          ),
          activeIcon: Image(
            image: AssetImage('assets/images/notifications-icon-active.png'),
            height: ScreenUtil().setHeight(26.08),
            fit: BoxFit.fitHeight,
          ),
        ),
        BottomNavigationBarItem(
          title: Container(),
          icon: Image(
            image: AssetImage('assets/images/profile-icon-inactive.png'),
            height: ScreenUtil().setHeight(25.92),
            fit: BoxFit.fitHeight,
          ),
          activeIcon: Image(
            image: AssetImage('assets/images/profile-icon-active.png'),
            height: ScreenUtil().setHeight(24.03),
            fit: BoxFit.fitHeight,
          ),
        ),
      ],
    );
  }
}
