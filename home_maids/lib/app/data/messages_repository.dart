import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/data/users_repository.dart';
import 'package:home_maids/app/stores/message.dart';

class MessagesRepository {
  static MessagesRepository instance;
  final CollectionReference _collection;

  factory MessagesRepository() {
    if (instance == null) {
      instance = MessagesRepository._();
    }
    return instance;
  }

  MessagesRepository._()
      : _collection = Firestore.instance.collection('messages');

  Future<Message> getOne(String id) async {
    final document = await _collection.document(id).get();
    if (document == null || !document.exists) {
      return null;
    }

    final data = document.data;

    final usersRepo = UsersRepository();
    final sender = await usersRepo.getOne(data['sender_id']);
    if (sender == null) {
      return null;
    }
    final recipient = await usersRepo.getOne(data['recipient_id']);
    if (recipient == null) {
      return null;
    }

    final message = new Message();
    message.setData(
      id: document.documentID,
      sender: sender,
      recipient: recipient,
      date: (data['date'] as Timestamp).toDate(),
      content: data['content'] as String,
      isRead: data['is_read'] as bool,
      isArchived: data['is_archived'] as bool,
    );

    return message;
  }

  Future<List<Message>> get(String senderId,
      {bool includeArchived = false}) async {
    Query query;
    if (includeArchived) {
      query = _collection.where('sender_id', isEqualTo: senderId);
    } else {
      query = _collection
          .where('sender_id', isEqualTo: senderId)
          .where('is_archived', isEqualTo: false);
    }

    final snapshot = await query.getDocuments();
    if (snapshot == null || snapshot.documents.isEmpty) {
      return null;
    }

    final usersRepo = UsersRepository();

    final messages = <Message>[];
    await Future.forEach(snapshot.documents, (DocumentSnapshot document) async {
      final data = document.data;

      final sender = await usersRepo.getOne(data['sender_id']);
      if (sender == null) {
        return;
      }
      final recipient = await usersRepo.getOne(data['recipient_id']);
      if (recipient == null) {
        return;
      }

      final message = new Message();
      message.setData(
        id: document.documentID,
        sender: sender,
        recipient: recipient,
        date: (data['date'] as Timestamp).toDate(),
        content: data['content'] as String,
        isRead: data['is_read'] as bool,
        isArchived: data['is_archived'] as bool,
      );
      messages.add(message);
    });

    return messages;
  }

  Future<Message> add(Message message) async {
    final data = message.toMap();

    final document = await _collection.add(data);
    if (document == null) return null;

    final newMessage = new Message();
    newMessage.setData(
      id: document.documentID,
      sender: message.sender,
      recipient: message.recipient,
      date: message.date,
      content: message.content,
      isArchived: message.isArchived,
    );
    return newMessage;
  }
}
