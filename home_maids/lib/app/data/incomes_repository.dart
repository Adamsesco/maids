import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:home_maids/app/stores/incomes.dart';
import 'package:home_maids/app/types/income.dart';
import 'package:home_maids/app/types/incomes_type.dart';
import 'package:intl/intl.dart';

class IncomesRepository {
  static IncomesRepository instance;

  factory IncomesRepository() {
    if (instance == null) {
      instance = IncomesRepository._();
    }
    return instance;
  }

  IncomesRepository._();

  Future<List<Income>> get(String companyId, IncomesType type) async {
    String typeStr;
    switch (type) {
      case IncomesType.month:
        typeStr = 'M';
        break;
      case IncomesType.year:
        typeStr = 'Y';
        break;
    }

    final request = await Dio().get<String>(
        "http://washy-car.com/homemaids/api/getBalance/$companyId/$typeStr");
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return [];
    }

    final responseJson = jsonDecode(request.data);
    if (responseJson['data'] == null) {
      return [];
    }

    final months = [
      'JAN',
      'FEB',
      'MAR',
      'APR',
      'MAY',
      'JUN',
      'JUL',
      'AUG',
      'SEP',
      'OCT',
      'NOV',
      'DEC',
    ];
    final days = [
      'SUN',
      'MON',
      'TUE',
      'WED',
      'THU',
      'FRI',
      'SAT',
    ];

    final data = (responseJson['data'].map((i) {
      DateTime date;
      String label;
      switch (type) {
        case IncomesType.month:
          final month = int.tryParse(i['month']) ?? 1;
          final year = int.tryParse(i['year']) ?? DateTime.now().year;
          date = DateTime(year, month);
          label = months[month - 1];
          break;
        case IncomesType.year:
          final year = int.tryParse(i['date']) ?? DateTime.now().year;
          date = DateTime(year);
          label = i['date'];
          break;
      }

      final amount = double.tryParse(i['total']) ?? 0.0;

      return Income(date, amount, label: label);
    })?.toList() as List<dynamic>)
        ?.cast<Income>();

    if (data != null && data.isNotEmpty) {
      data.sort((i1, i2) => i1.date.compareTo(i2.date));
    }

    return data;
  }
}
