import 'dart:convert';

import 'package:home_maids/app/stores/review.dart';
import 'package:home_maids/app/stores/service.dart';
import 'package:home_maids/app/types/order_status.dart';
import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/data/clients_repository.dart';
import 'package:home_maids/app/data/companies_repository.dart';
import 'package:home_maids/app/data/services_repository.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/order.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/payment_method.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:home_maids/app/utils/order_utils.dart';

class OrdersRepository {
  static OrdersRepository instance;

  factory OrdersRepository() {
    if (instance == null) {
      instance = OrdersRepository._();
    }
    return instance;
  }

  OrdersRepository._();

  Future<List<Order>> get(User user,
      {int page = 1, int itemsPerPage = 5, int status}) async {
    final orders = <Order>[];

    switch (user.role) {
      case UserRole.company:
        final company = user as Company;
        final token = company.token;

        final statusComponent = status == null ? '' : "status=$status";
        final itemsPerPageComponent =
            status == null ? itemsPerPage.toString() : '';
        final response = await http.get(
            "http://washy-car.com/homemaids//api/orders/$token/$statusComponent/$page/$itemsPerPageComponent");
        if (response == null ||
            response.statusCode != 200 ||
            response.body == null ||
            response.body.isEmpty) {
          return orders;
        }

        final json = jsonDecode(response.body);
        final data = json['data'] as Map<String, dynamic>;
        data.forEach((String key, value) {
          if (int.tryParse(key) == null) {
            return;
          }

          final orderJson = value as Map<String, dynamic>;

          final client = new Client()
            ..setData(
              id: orderJson['user_id'],
              firstName: orderJson['first_name'],
              lastName: orderJson['last_name'],
              avatarUrl: orderJson['avatar'],
              phoneNumber: orderJson['phone'],
            );

          final services = (orderJson['services'] as Map<String, dynamic>)
              ?.map((String key, value) {
                final serviceData = value as Map<String, dynamic>;
                final service = new Service()
                  ..setData(
                    id: serviceData['service_id']?.toString(),
                    name: {
                      'en': serviceData['name'] as String,
                      'ar': serviceData['name_arabic'] as String,
                    },
                    duration: Duration(hours: serviceData['hours'] ?? 0),
                    maidsCount: serviceData['num_of_maids'] ?? 0,
                  );

                return MapEntry(key, service);
              })
              ?.values
              ?.toList();

          final orderStatus = getOrderStatus(int.tryParse(orderJson['status']));

          final order = new Order()
            ..setData(
              id: orderJson['order_id'] as String,
              client: client,
              address: Address(orderJson['address'] as String, ''),
              requiresMaterials:
                  orderJson['is_material_required'] == '0' ? true : false,
              price: double.tryParse(orderJson['price']) ?? 0.0,
              services: services,
              dateCreated: DateTime.parse(orderJson['date_create']),
              date: DateTime.parse(orderJson['date_request']),
              number: int.tryParse(orderJson['order_number']) ?? 0,
              maidsCount: services?.fold(0,
                      (int acc, Service service) => acc + service.maidsCount) ??
                  0,
              duration: services?.fold(
                      Duration(),
                      (Duration acc, Service service) => Duration(
                          hours: acc.inHours + service.duration.inHours)) ??
                  Duration(),
              status: orderStatus,
            );

          orders.add(order);
        });

        break;

      case UserRole.anonymous:
        // TODO: Handle this case.
        break;
      case UserRole.client:
        final client = user as Client;
        final token = client.token;

        final statusComponent = status == null ? '' : "status=$status";
        final itemsPerPageComponent =
            status == null ? itemsPerPage.toString() : '';
        final response = await http.get(
            "http://washy-car.com/homemaids//api/orders/$token/$statusComponent/$page/$itemsPerPageComponent");
        if (response == null ||
            response.statusCode != 200 ||
            response.body == null ||
            response.body.isEmpty) {
          return orders;
        }

        final json = jsonDecode(response.body);
        final data = json['data'] as Map<String, dynamic>;
        data.forEach((String key, value) {
          if (int.tryParse(key) == null) {
            return;
          }

          final orderJson = value as Map<String, dynamic>;

          final company = Company()
            ..setData(
              id: orderJson['company_id'] as String,
              name: orderJson['company_name'] as String,
              avatarUrl: orderJson['avatar'] as String,
              phoneNumber: orderJson['phone'] as String,
              reviews:
                  List.generate(orderJson['reviews'] ?? 0, (_) => Review()),
              rating: orderJson['rates'],
            );

          final services = (orderJson['services'] as Map<String, dynamic>)
              ?.map((String key, value) {
                final serviceData = value as Map<String, dynamic>;
                final service = new Service()
                  ..setData(
                    id: serviceData['service_id']?.toString(),
                    name: {
                      'en': serviceData['name'] as String,
                      'ar': serviceData['name_arabic'] as String,
                    },
                    duration: Duration(hours: serviceData['hours'] ?? 0),
                    maidsCount: serviceData['num_of_maids'] ?? 0,
                  );

                return MapEntry(key, service);
              })
              ?.values
              ?.toList();

          final orderStatus = getOrderStatus(int.tryParse(orderJson['status']));

          final order = new Order()
            ..setData(
              id: orderJson['order_id'],
              company: company,
              address: Address(orderJson['address'] as String, ''),
              requiresMaterials:
                  orderJson['is_material_required'] == '0' ? true : false,
              price: double.tryParse(orderJson['price']) ?? 0.0,
              services: services,
              dateCreated: DateTime.parse(orderJson['date_create']),
              date: DateTime.parse(orderJson['date_request']),
              number: int.tryParse(orderJson['order_number']) ?? 0,
              maidsCount: services?.fold(0,
                      (int acc, Service service) => acc + service.maidsCount) ??
                  0,
              duration: services?.fold(
                      Duration(),
                      (Duration acc, Service service) => Duration(
                          hours: acc.inHours + service.duration.inHours)) ??
                  Duration(),
              status: orderStatus,
            );

          orders.add(order);
        });

        break;
    }

    return orders;
  }

// Future<List<Order>> get({String companyId, String clientId}) async {
//   final orders = <Order>[];

//   // String userId;
//   // String field;
//   // if (companyId != null) {
//   //   userId = companyId;
//   //   field = 'company_id';
//   // } else if (clientId != null) {
//   //   userId = clientId;
//   //   field = 'client_id';
//   // } else {
//   //   return null;
//   // }

//   // final ordersDocuments =
//   //     await _collection.where(field, isEqualTo: userId).getDocuments();
//   // if (ordersDocuments == null ||
//   //     ordersDocuments.documents == null ||
//   //     ordersDocuments.documents.isEmpty) {
//   //   return null;
//   // }

//   // await Future.forEach(ordersDocuments.documents,
//   //     (DocumentSnapshot orderDocument) async {
//   //   final data = orderDocument.data;

//   //   if (data['client_id'] == null || data['company_id'] == null) {
//   //     return null;
//   //   }

//   //   final clientsRepo = ClientsRepository();
//   //   final companiesRepo = CompaniesRepository();
//   //   final orderClientId = data['client_id'] as String;
//   //   final orderCompanyId = data['company_id'] as String;

//   //   Client client;
//   //   if (clientId == null) {
//   //     client = await clientsRepo.getOne(id: orderClientId);
//   //     if (client == null) {
//   //       return null;
//   //     }
//   //   }

//   //   Company company;
//   //   if (companyId == null) {
//   //     final company = await companiesRepo.getOne(id: orderCompanyId);
//   //     if (company == null) {
//   //       return null;
//   //     }
//   //   }

//   //   PaymentMethod paymentMethod;
//   //   switch (data['payment_method']) {
//   //     case 'cash':
//   //       paymentMethod = PaymentMethod.cash;
//   //       break;

//   //     case 'visa':
//   //       paymentMethod = PaymentMethod.visa;
//   //       break;

//   //     case 'paypal':
//   //       paymentMethod = PaymentMethod.paypal;
//   //       break;
//   //   }

//   //   final servicesRepo = ServicesRepository();
//   //   final servicesIds = (data['services'] as List<dynamic>).cast<String>();
//   //   final services = await servicesRepo.get(servicesIds);

//   //   final status = getOrderStatus(data['status'] as int);

//   //   final order = new Order();
//   //   order.setData(
//   //     id: orderDocument.documentID,
//   //     number: data['number'] as int,
//   //     client: client,
//   //     company: company,
//   //     address:
//   //         new Address(data['address']['address'], data['address']['region']),
//   //     date: (data['date'] as Timestamp).toDate(),
//   //     dateCreated: (data['date_created'] as Timestamp).toDate(),
//   //     duration: Duration(minutes: (data['duration'] as int)),
//   //     maidsCount: data['maids_count'] as int,
//   //     price: (data['price'] as num).toDouble(),
//   //     requiresMaterials: data['requires_materials'] as bool,
//   //     paymentMethod: paymentMethod,
//   //     services: services,
//   //     status: status,
//   //   );
//   //   orders.add(order);
//   // });

//   return orders;
// }

}
