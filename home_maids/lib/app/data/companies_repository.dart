import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/data/incomes_repository.dart';
import 'package:home_maids/app/data/reviews_repository.dart';
import 'package:home_maids/app/data/services_repository.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/types/address.dart';

class CompaniesRepository {
  static CompaniesRepository instance;
  final CollectionReference _collection;

  factory CompaniesRepository() {
    if (instance == null) {
      instance = CompaniesRepository._();
    }
    return instance;
  }

  CompaniesRepository._()
      : _collection = Firestore.instance.collection('users');

//  Future<Company> getOne({String id, String email}) async {
//    DocumentSnapshot companyDocument;
//
//    if (id != null) {
//      companyDocument = await _collection.document(id).get();
//    } else if (email != null) {
//      final companyDocuments = await _collection
//          .where('role', isEqualTo: 'company')
//          .where('email', isEqualTo: email)
//          .getDocuments();
//
//      if (companyDocuments == null || companyDocuments.documents.isEmpty) {
//        return null;
//      }
//
//      companyDocument = companyDocuments.documents.first;
//    } else {
//      return null;
//    }
//
//    final data = companyDocument.data;
//
//    final servicesRepo = ServicesRepository();
//    final servicesIds = (data['services'] as List<dynamic>).cast<String>();
//    final services = await servicesRepo.get(servicesIds);
//
//    final reviewsRepo = ReviewsRepository();
//    final reviews = await reviewsRepo.get(companyDocument.documentID);
//
//    final incomesRepo = IncomesRepository();
//    final incomes = await incomesRepo.getOne(
//        companyDocument.documentID, DateTime.now().year);
//
//    final company = new Company();
//    company.setData(
//      id: companyDocument.documentID,
//      name: data['name'] as String,
//      email: data['email'] as String,
//      phoneNumber: data['phone_number'] as String,
//      description: data['description'] as String,
//      avatarUrl: data['avatar_url'] as String,
//      bannerUrl: data['banner_url'] as String,
//      location: data['location'] as GeoPoint,
//      address: data['address'] == null
//          ? null
//          : new Address(data['address']['address'] as String,
//              data['address']['region'] as String),
//      rating: (data['rating'] as num)?.toDouble(),
//      reviews: reviews,
//      currency: data['currency'] as String,
//      hourlyPrice: (data['hourly_price'] as num)?.toDouble(),
//      services: services,
//      isAvailable: data['is_available'] as bool,
//      incomes: incomes,
//      dateCreated: (data['date_created'] as Timestamp)?.toDate(),
//      jobsDone: (data['jobs_done'] as int) ?? 0,
//    );
//    return company;
//  }

  Future<List<Company>> get() async {
    final companies = <Company>[];

    return companies;
  }

  Future<Company> add(Company company) async {
    final data = company.toMap();
    if (company.id != null) {
      await _collection.document(company.id).setData(data);
    } else {
      final document = await _collection.add(data);
      if (document == null) return null;
      company.setData(id: document.documentID);
    }

    final newCompany = new Company();
    newCompany.setData(
      id: company.id,
      name: company.name,
      email: company.email,
      description: company.description,
      avatarUrl: company.avatarUrl,
      bannerUrl: company.bannerUrl,
      location: company.location,
      address: company.address,
      rating: company.rating,
      currency: company.currency,
      hourlyPrice: company.hourlyPrice,
      services: company.services,
      isAvailable: company.isAvailable,
    );
    return newCompany;
  }
}
