import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/signup_method.dart';

class ClientsRepository {
  static ClientsRepository instance;
  final CollectionReference _collection;

  factory ClientsRepository() {
    if (instance == null) {
      instance = ClientsRepository._();
    }
    return instance;
  }

  ClientsRepository._() : _collection = Firestore.instance.collection('users');

  Future<Client> getOne({String id, String email}) async {
    DocumentSnapshot clientDocument;

    if (id != null) {
      clientDocument = await _collection.document(id).get();
    } else if (email != null) {
      final clientDocuments = await _collection
          .where('role', isEqualTo: 'client')
          .where('email', isEqualTo: email)
          .getDocuments();

      if (clientDocuments == null || clientDocuments.documents.isEmpty) {
        return null;
      }

      clientDocument = clientDocuments.documents.first;
    } else {
      return null;
    }

    final data = clientDocument.data;

    SignupMethod signupMethod;
    switch (data['signup_method']) {
      case 'email':
        signupMethod = SignupMethod.email;
        break;

      case 'facebook':
        signupMethod = SignupMethod.facebook;
        break;
    }

    final addresses = <Address>[];
    if (data['addresses'] != null) {
      (data['addresses'] as List<dynamic>).forEach((a) {
        final address =
            new Address(a['address'] as String, a['region'] as String);
        addresses.add(address);
      });
    }

    final client = new Client();
    client.setData(
      id: clientDocument.documentID,
      firstName: data['first_name'] as String,
      lastName: data['last_name'] as String,
      email: data['email'] as String,
      signupMethod: signupMethod,
      phoneNumber: data['phone_number'] as String,
      addresses: addresses,
      avatarUrl: data['avatar_url'] as String,
      dateCreated: (data['date_created'] == null)
          ? null
          : (data['date_created'] as Timestamp).toDate(),
    );
    return client;
  }

  Future<List<Client>> get() async {
    final clients = <Client>[];

    return clients;
  }

  Future<Client> add(Client client) async {
    final data = client.toMap();
    if (client.id != null) {
      await _collection.document(client.id).setData(data);
    } else {
      final document = await _collection.add(data);
      if (document == null) return null;
      client.setData(id: document.documentID);
    }

    final newClient = new Client();
    newClient.setData(
      id: client.id,
      firstName: client.firstName,
      lastName: client.lastName,
      email: client.email,
      signupMethod: client.signupMethod,
      phoneNumber: client.phoneNumber,
      addresses: client.addresses,
      avatarUrl: client.avatarUrl,
    );
    return newClient;
  }
}
