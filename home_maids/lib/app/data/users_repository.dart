import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/user.dart';

class UsersRepository {
  static UsersRepository instance;
  final CollectionReference _collection;

  factory UsersRepository() {
    if (instance == null) {
      instance = UsersRepository._();
    }
    return instance;
  }

  UsersRepository._() : _collection = Firestore.instance.collection('users');

  Future<User> getOne(String id) async {
    final document = await _collection.document(id).get();
    if (document == null || !document.exists) {
      return null;
    }

    final data = document.data;
    User user;
    switch (data['role']) {
      case 'company':
        final company = new Company();
        company.setData(
          id: document.documentID,
          name: data['name'] as String,
          email: data['email'] as String,
          description: data['description'] as String,
          avatarUrl: data['avatar_url'] as String,
          bannerUrl: data['banner_url'] as String,
          location: data['location'] as GeoPoint,
          address: data['address'] == null
              ? null
              : new Address(data['address']['address'] as String,
                  data['address']['region'] as String),
          rating: (data['rating'] as num).toDouble(),
          currency: data['currency'] as String,
          hourlyPrice: (data['hourly_price'] as num).toDouble(),
          isAvailable: data['is_available'] as bool,
        );
        user = company;
        break;

      case 'client':
        final client = new Client();
        client.setData(
          id: document.documentID,
          firstName: data['first_name'] as String,
          lastName: data['last_name'] as String,
          email: data['email'] as String,
          phoneNumber: data['phone_number'] as String,
          avatarUrl: data['avatar_url'] as String,
          dateCreated: (data['date_created'] == null)
              ? null
              : (data['date_created'] as Timestamp).toDate(),
        );
        user = client;
        break;
    }

    return user;
  }
}
