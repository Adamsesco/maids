import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/stores/service.dart';

class ServicesRepository {
  static ServicesRepository instance;
  final CollectionReference _collection;

  factory ServicesRepository() {
    if (instance == null) {
      instance = ServicesRepository._();
    }
    return instance;
  }

  ServicesRepository._()
      : _collection = Firestore.instance.collection('services');

  Future<List<Service>> get(List<String> ids) async {
    final services = <Service>[];

    if (ids == null || ids.isEmpty) {
      return services;
    }

    for (var id in ids) {
      final serviceDocument = await _collection.document(id).get();
      if (serviceDocument == null || !serviceDocument.exists) {
        continue;
      }

      final service = new Service();
      service.setData(
        id: serviceDocument.documentID,
        name: (serviceDocument.data['name'] as Map<dynamic, dynamic>)
            .cast<String, String>(),
      );

      services.add(service);
    }

    return services;
  }

  Future<Service> add(Service service) async {
    final data = service.toMap();

    final document = await _collection.add(data);
    if (document == null) return null;

    final newService = new Service();
    newService.setData(
      id: document.documentID,
      name: service.name,
    );
    return newService;
  }
}
