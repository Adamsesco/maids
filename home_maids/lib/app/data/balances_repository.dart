import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/stores/balance.dart';

class BalancesRepository {
  static BalancesRepository instance;
  final CollectionReference _collection;

  factory BalancesRepository() {
    if (instance == null) {
      instance = BalancesRepository._();
    }
    return instance;
  }

  BalancesRepository._()
      : _collection = Firestore.instance.collection('balances');

  Future<Balance> getOne(String companyId) async {
    final snapshot = await _collection
        .where('company_id', isEqualTo: companyId)
        .getDocuments();
    if (snapshot == null || snapshot.documents.isEmpty) {
      return null;
    }

    final balanceDocument = snapshot.documents.first;
    if (balanceDocument == null || !balanceDocument.exists) {
      return null;
    }

    final data = balanceDocument.data;

    final balance = new Balance();
    balance.setData(
      id: balanceDocument.documentID,
      value: (data['value'] as num).toDouble(),
    );

    return balance;
  }
}
