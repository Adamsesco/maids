import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/data/clients_repository.dart';
import 'package:home_maids/app/data/companies_repository.dart';
import 'package:home_maids/app/stores/review.dart';

class ReviewsRepository {
  static ReviewsRepository instance;
  final CollectionReference _collection;

  factory ReviewsRepository() {
    if (instance == null) {
      instance = ReviewsRepository._();
    }
    return instance;
  }

  ReviewsRepository._()
      : _collection = Firestore.instance.collection('reviews');

//  Future<Review> getOne(String id) async {
//    final document = await _collection.document(id).get();
//    if (document == null || !document.exists) {
//      return null;
//    }
//
//    final data = document.data;
//
//    final clientsRepo = ClientsRepository();
//    final author = await clientsRepo.getOne(id: data['author_id']);
//    if (author == null) {
//      return null;
//    }
//
//    final companiesRepo = CompaniesRepository();
//    final company = await companiesRepo.getOne(id: data['company_id']);
//    if (company == null) {
//      return null;
//    }
//
//    final review = new Review();
//    review.setData(
//      id: document.documentID,
//      author: author,
//      company: company,
//      date: (data['date'] as Timestamp).toDate(),
//      rating: (data['rating'] as num).toDouble(),
//      content: data['content'] as String,
//      isNegative: data['is_negative'] as bool,
//    );
//
//    return review;
//  }

//  Future<List<Review>> get(String companyId) async {
//    final snapshot = await _collection
//        .where('company_id', isEqualTo: companyId)
//        .getDocuments();
//    if (snapshot == null || snapshot.documents.isEmpty) {
//      return null;
//    }
//
//    final clientsRepo = ClientsRepository();
//    final companiesRepo = CompaniesRepository();
//
//    final reviews = <Review>[];
//    await Future.forEach(snapshot.documents, (DocumentSnapshot document) async {
//      final data = document.data;
//
//      final author = await clientsRepo.getOne(id: data['author_id']);
//      if (author == null) {
//        return null;
//      }
//
//      final company = await companiesRepo.getOne(id: data['company_id']);
//      if (company == null) {
//        return null;
//      }
//
//      final review = new Review();
//      review.setData(
//        id: document.documentID,
//        author: author,
//        company: company,
//        date: (data['date'] as Timestamp).toDate(),
//        rating: (data['rating'] as num).toDouble(),
//        content: data['content'] as String,
//        isNegative: data['is_negative'] as bool,
//      );
//      reviews.add(review);
//    });
//
//    return reviews;
//  }

  Future<Review> add(Review review) async {
    final data = review.toMap();

    final document = await _collection.add(data);
    if (document == null) return null;

    final newReview = new Review();
    newReview.setData(
      id: document.documentID,
      author: review.author,
      company: review.company,
      date: review.date,
      rating: review.rating,
      content: review.content,
      isNegative: review.isNegative,
    );
    return newReview;
  }
}
