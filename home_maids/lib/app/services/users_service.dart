import 'dart:async';
import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hive/hive.dart';
import 'package:home_maids/app/data/incomes_repository.dart';
import 'package:home_maids/app/data/orders_repository.dart';
import 'package:home_maids/app/screens/client_dashboard_screen/client_dashboard_screen.dart';
import 'package:home_maids/app/screens/company_dashboard_screen/company_dashboard_screen.dart';
import 'package:home_maids/app/screens/login_screen/login_screen.dart';
import 'package:home_maids/app/stores/incomes_store.dart';
import 'package:home_maids/app/stores/message.dart';
import 'package:home_maids/app/stores/messages_store.dart';
import 'package:home_maids/app/stores/orders_store.dart';
import 'package:home_maids/app/stores/review.dart';
import 'package:home_maids/app/stores/service.dart';
import 'package:home_maids/app/types/incomes_type.dart';
import 'package:http/http.dart' as http;
import 'package:home_maids/app/data/companies_repository.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/signup_method.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';

class UsersService {
  static User currentUser;

  Future<Box<Map<dynamic, dynamic>>> _userDbFuture;
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: ['email', 'profile'],
  );

  UsersService() {
    _userDbFuture = Hive.openBox('user');
  }

  Widget performLogin(User user, Company companyStore, Client clientStore,
      OrdersStore ordersStore, MessagesStore messagesStore,
      [IncomesStore incomesStore]) {
    Firestore.instance
        .collection('messages')
        .where('participants_ids', arrayContains: user.id)
        .snapshots()
        .listen((data) {
      final messages = data.documents.map((ds) {
        final senderId = ds.data['sender_id'];
        final recipientId = ds.data['recipient_id'];
        User sender;
        User recipient;
        bool isArchived;

        if (senderId == user.id) {
          sender = user;
          isArchived = ds.data['has_sender_archived'];

          switch (user.role) {
            case UserRole.anonymous:
              // TODO: Handle this case.
              break;
            case UserRole.client:
              recipient = Company()
                ..setData(
                  id: recipientId,
                  name: ds.data['recipient_name'],
                  avatarUrl: ds.data['recipient_avatar_url'],
                  phoneNumber: ds.data['recipient_phone_number'],
                );
              break;
            case UserRole.company:
              recipient = Client()
                ..setData(
                  id: recipientId,
                  firstName: ds.data['recipient_first_name'],
                  lastName: ds.data['recipient_last_name'],
                  avatarUrl: ds.data['recipient_avatar_url'],
                  phoneNumber: ds.data['recipient_phone_number'],
                );
              break;
          }
        } else {
          recipient = user;
          isArchived = ds.data['has_recipient_archived'];

          switch (user.role) {
            case UserRole.anonymous:
              // TODO: Handle this case.
              break;
            case UserRole.client:
              sender = Company()
                ..setData(
                  id: senderId,
                  name: ds.data['sender_name'],
                  avatarUrl: ds.data['sender_avatar_url'],
                  phoneNumber: ds.data['sender_phone_number'],
                );
              break;
            case UserRole.company:
              sender = Client()
                ..setData(
                  id: senderId,
                  firstName: ds.data['sender_first_name'],
                  lastName: ds.data['sender_last_name'],
                  avatarUrl: ds.data['sender_avatar_url'],
                  phoneNumber: ds.data['sender_phone_number'],
                );
              break;
          }
        }

        final message = Message()
          ..setData(
            id: ds.documentID,
            date: (ds.data['date'] as Timestamp).toDate(),
            sender: sender,
            recipient: recipient,
            content: ds.data['content'],
            isArchived: isArchived,
            isRead: sender.id == user.id
                ? ds.data['has_sender_read']
                : ds.data['has_recipient_read'],
          );

        return message;
      });

      messages?.forEach((message) => messagesStore.addMessage(message));
    });

    switch (user.role) {
      case UserRole.anonymous:
        // TODO: Handle this case.
        break;
      case UserRole.client:
        final client = user as Client;
        print(client.token);
        clientStore.copyFrom(client);

        final ordersRepo = OrdersRepository();
        final orders = ordersRepo.get(user);
        ordersStore.setOrders(orders);

        return ClientDashboardScreen();
        break;

      case UserRole.company:
        final company = user as Company;
        companyStore.copyFrom(company);

        final ordersRepo = OrdersRepository();
        final orders = ordersRepo.get(user);
        ordersStore.setOrders(orders);

        if (incomesStore != null) {
          final incomesRepo = IncomesRepository();
          incomesStore.monthlyIncomes =
              incomesRepo.get(company.id, IncomesType.month);
          incomesStore.yearlyIncomes =
              incomesRepo.get(company.id, IncomesType.year);
        }

        return CompanyDashboardScreen();
        break;
    }

    return LoginScreen();
  }

  Future<User> getByToken(String token) async {
    print("Get by token: $token");
    final request =
        await http.get("http://washy-car.com/homemaids/api/getProfile/$token");
    if (request == null ||
        request.statusCode != 200 ||
        request.body == null ||
        request.body.isEmpty) {
      return null;
    }

    print("Response: ${request.body}");

    final userJson = jsonDecode(request.body);
    final status = userJson['status'];
    final code = userJson['code'];
    if (status != 200 || code != 1) {
      return null;
    }

    final data = userJson['data'];
    final role = data['role'];
    switch (role) {
      case "customer":
        final client = new Client();
        client.setData(
          id: data['id'] as String,
          email: data['email'] as String,
          phoneNumber: data['phone'] as String,
          avatarUrl: data['avatar'] as String,
          firstName: data['first_name'] as String,
          lastName: data['last_name'] as String,
          addresses: [Address(data['address'] ?? '', '')],
          signupMethod: SignupMethod.email,
          dateCreated: DateTime.parse(data['date_created']) ?? DateTime.now(),
          completedOrdersCount: data['done_orders'] ?? 0,
          canceledOrdersCount: data['cancel_orders'] ?? 0,
          token: data['token'] as String,
        );

        final clientData = client.toMap();
        final userDb = await _userDbFuture;
        await userDb.delete('user');
        userDb.put('user', clientData);

        return client;
        break;

      case "company":
        final company = new Company();

        List<Service> services;
        final servicesDecoded = jsonDecode(data['services'] ?? '[]');
        if (servicesDecoded != null && servicesDecoded is List<dynamic>) {
          services = jsonDecode(data['services'])
              ?.map((service) => Service()
                ..setData(
                  id: service['id'],
                  name: {
                    'en': service['name'],
                    'ar': service['name_arabic'],
                  },
                  price: double.tryParse(service['price']) ?? 0.0,
                  oldPrice: double.tryParse(service['old_price']),
                  isEnabled: service['is_enabled'] == '0' ? false : true,
                ))
              ?.toList()
              ?.cast<Service>();
        } else {
          services = [];
        }

        company.setData(
          id: data['id'] as String,
          avatarUrl: data['avatar'] as String,
          email: data['email'] as String,
          name: data['company_name'] as String,
          currency: data['currency'] as String,
          dateCreated:
              DateTime.parse(data['date_created'] as String) ?? DateTime.now(),
          description: data['description'] as String,
          phoneNumber: data['phone'] as String,
          services: services,
          isAvailable: data['availability'] == '0' ? true : false,
          rating: data['rates'] as double,
          reviews: List<Review>.generate(data['reveiws'] ?? 0, (_) => Review()),
          jobsDone: data['done_orders'] ?? 0,
          address: Address(data['address'] ?? '', ''),
          token: data['token'] as String,
        );

        final companyData = company.toMap();
        final userDb = await _userDbFuture;
        await userDb.delete('user');
        userDb.put('user', companyData);

        return company;
        break;

      default:
        return null;
    }
  }

  Future<User> signinWithGoogle(String fcmToken) async {
    User user;

    try {
      final account = await _googleSignIn.signIn();
      final token = await account.authentication;
      final idToken = token.idToken;

      final request = await http.get(
          "http://washy-car.com/homemaids/api/auth?code=$idToken&device_token=$fcmToken");
      if (request.statusCode == 200) {
        final responseJson = jsonDecode(request.body);
        if (responseJson != null) {
          final status = responseJson['status'];
          if (status != null && status == 200) {
            final code = responseJson['code'];
            if (code != null && code == 1) {
              final data = responseJson['data'];

              switch (data['role'] as String) {
                case "customer":
                  final client = new Client();
                  client.setData(
                    id: data['id'] as String,
                    email: data['email'] as String,
                    phoneNumber: data['phone'] as String,
                    avatarUrl: data['avatar'] as String,
                    firstName: data['first_name'] as String,
                    lastName: data['last_name'] as String,
                    addresses: [Address(data['address'] ?? '', '')],
                    signupMethod: SignupMethod.email,
                    dateCreated:
                        DateTime.parse(data['date_created']) ?? DateTime.now(),
                    completedOrdersCount: data['done_orders'] ?? 0,
                    canceledOrdersCount: data['cancel_orders'] ?? 0,
                    token: data['token'] as String,
                  );

                  final clientData = client.toMap();
                  final userDb = await _userDbFuture;
                  await userDb.delete('user');
                  userDb.put('user', clientData);

                  return client;
                  break;

                case "company":
                  final company = new Company();

                  final services = jsonDecode(data['services'])
                      ?.map((service) => Service()
                        ..setData(
                          id: service['id'],
                          name: {
                            'en': service['name'],
                            'ar': service['name_arabic'],
                          },
                          price: double.tryParse(service['price']) ?? 0.0,
                          oldPrice: double.tryParse(service['old_price']),
                          isEnabled:
                              service['is_enabled'] == '0' ? false : true,
                        ))
                      ?.toList()
                      ?.cast<Service>();

                  company.setData(
                    id: data['id'] as String,
                    avatarUrl: data['avatar'] as String,
                    email: data['email'] as String,
                    name: data['company_name'] as String,
                    currency: data['currency'] as String,
                    dateCreated:
                        DateTime.parse(data['date_created'] as String) ??
                            DateTime.now(),
                    description: data['description'] as String,
                    phoneNumber: data['phone'] as String,
                    services: services,
                    isAvailable: data['availability'] == '0' ? true : false,
                    rating: data['rates'] as double,
                    reviews: List<Review>.generate(
                        data['reveiws'] ?? 0, (_) => Review()),
                    jobsDone: data['done_orders'] ?? 0,
                    address: Address(data['address'] ?? '', ''),
                    token: data['token'] as String,
                  );

                  final companyData = company.toMap();
                  final userDb = await _userDbFuture;
                  await userDb.delete('user');
                  userDb.put('user', companyData);

                  return company;
                  break;

                default:
                  return null;
              }
            }
          }
        }
      }

      // debugPrint(idToken);
      // print(idToken.substring(0, idToken.length ~/ 3));
      // print(idToken.substring(
      //     idToken.length ~/ 3, idToken.length ~/ 3 + idToken.length ~/ 3));
      // print(idToken.substring(idToken.length ~/ 3 + idToken.length ~/ 3));

      // final authHeaders = await account.authHeaders;
      // final accessToken =
      //     authHeaders['Authorization'].replaceFirst('Bearer ', '');
      // await http.get("http://192.168.8.102:1234/?data=$accessToken");
      // print(authHeaders);
    } catch (error) {
      print(error);
    }

    return null;
  }

  Future<User> signInWithEmail(
      String email, String password, String fcmToken) async {
    final request =
        await http.post('http://washy-car.com/homemaids/api/login', body: {
      'username': email,
      'password': password,
      if (fcmToken != null) 'device_token': fcmToken,
    });

    if (request.statusCode == 200) {
      final responseJson = jsonDecode(request.body);
      if (responseJson != null) {
        final status = responseJson['status'];
        if (status != null && status == 200) {
          final code = responseJson['code'];
          if (code != null && code == 1) {
            final data = responseJson['data'];

            switch (data['role'] as String) {
              case "customer":
                final client = new Client();
                client.setData(
                  id: data['id'] as String,
                  email: data['email'] as String,
                  phoneNumber: data['phone'] as String,
                  avatarUrl: data['avatar'] as String,
                  firstName: data['first_name'] as String,
                  lastName: data['last_name'] as String,
                  addresses: [Address(data['address'] ?? '', '')],
                  signupMethod: SignupMethod.email,
                  dateCreated:
                      DateTime.parse(data['date_created']) ?? DateTime.now(),
                  completedOrdersCount: data['done_orders'] ?? 0,
                  canceledOrdersCount: data['cancel_orders'] ?? 0,
                  token: data['token'] as String,
                );

                final clientData = client.toMap();
                final userDb = await _userDbFuture;
                await userDb.delete('user');
                userDb.put('user', clientData);

                return client;
                break;

              case "company":
                final company = new Company();

                final services = jsonDecode(data['services'])
                    ?.map((service) => Service()
                      ..setData(
                        id: service['id'],
                        name: {
                          'en': service['name'],
                          'ar': service['name_arabic'],
                        },
                        price: double.tryParse(service['price']) ?? 0.0,
                        oldPrice: double.tryParse(service['old_price']),
                        isEnabled: service['is_enabled'] == '0' ? false : true,
                      ))
                    ?.toList()
                    ?.cast<Service>();

                company.setData(
                  id: data['id'] as String,
                  avatarUrl: data['avatar'] as String,
                  email: data['email'] as String,
                  name: data['company_name'] as String,
                  currency: data['currency'] as String,
                  dateCreated: DateTime.parse(data['date_created'] as String) ??
                      DateTime.now(),
                  description: data['description'] as String,
                  phoneNumber: data['phone'] as String,
                  services: services,
                  isAvailable: data['availability'] == '0' ? true : false,
                  rating: data['rates'] as double,
                  reviews: List<Review>.generate(
                      data['reveiws'] ?? 0, (_) => Review()),
                  jobsDone: data['done_orders'] ?? 0,
                  address: Address(data['address'], ''),
                  token: data['token'] as String,
                );

                final companyData = company.toMap();
                final userDb = await _userDbFuture;
                await userDb.delete('user');
                userDb.put('user', companyData);

                return company;
                break;

              default:
                return null;
            }
          }
        }
      }
    }

    return null;
  }

  Future<User> getGoogleUser() async {
    try {
      final isSignedIn = await _googleSignIn.isSignedIn();
      if (isSignedIn) {
        await _googleSignIn.disconnect();
      }
      final account = await _googleSignIn.signIn();
      final token = await account.authentication;
      final idToken = token.idToken;

      final request = await http
          .get("https://oauth2.googleapis.com/tokeninfo?id_token=$idToken");
      if (request == null ||
          request.statusCode != 200 ||
          request.body == null ||
          request.body.isEmpty) {
        return null;
      }

      final response = request.body;
      final responseJson = jsonDecode(response);
      final firstName = responseJson['given_name'] as String;
      final lastName = responseJson['family_name'] as String;
      final email = responseJson['email'] as String;

      final client = Client()
        ..setData(
          firstName: firstName,
          lastName: lastName,
          email: email,
          token: idToken,
        );

      return client;
      // debugPrint(idToken);
      // print(idToken.substring(0, idToken.length ~/ 3));
      // print(idToken.substring(
      //     idToken.length ~/ 3, idToken.length ~/ 3 + idToken.length ~/ 3));
      // print(idToken.substring(idToken.length ~/ 3 + idToken.length ~/ 3));

      // final authHeaders = await account.authHeaders;
      // final accessToken =
      //     authHeaders['Authorization'].replaceFirst('Bearer ', '');
      // await http.get("http://192.168.8.102:1234/?data=$accessToken");
      // print(authHeaders);
    } catch (error) {
      print(error);
    }

    return null;
  }

  Future<User> signUpWithEmail(User user, String password) async {
    User newUser;

    switch (user.role) {
      case UserRole.anonymous:
        newUser = null;
        break;
      case UserRole.client:
        final client = user as Client;

        final formData = FormData.fromMap({
          'email': client.email,
          'password': password,
          'first_name': client.firstName,
          'last_name': client.lastName,
          'phone': client.phoneNumber,
        });

        final request = await Dio().post<String>(
            'http://washy-car.com/homemaids/api/signup',
            data: formData);
        final response = request.data;
        print(response);

        if (request.statusCode != 200 || response == null || response.isEmpty) {
          return null;
        }

        final responseJson = jsonDecode(response);
        // if (responseJson['statue'] != 200) {
        //   return null;
        // }

        final userId = responseJson['id'] as String;
        final token = responseJson['login_token'] as String;
        client.id = userId;
        client.token = token;
        client.dateCreated = DateTime.now();

        final clientData = client.toMap();
        final userDb = await _userDbFuture;
        await userDb.delete('user');
        userDb.put('user', clientData);

        newUser = client;
        break;
      case UserRole.company:
        final companiesRepo = CompaniesRepository();
        final company = await companiesRepo.add(newUser);
        newUser = company;
        break;
    }
    currentUser = newUser;

    return newUser;
  }

  Future<User> getFromCache() async {
    User user;

    final userDb = await _userDbFuture;
    final userData = userDb.get('user');

    if (userData == null) {
      return null;
    }

    switch (userData['role']) {
      case 'company':
        final company = Company().fromMap(userData);
        user = company;
        break;

      case 'client':
        final client = Client().fromMap(userData);
        user = client;
        break;
    }

    return user;
  }

  Future<User> getCurrentUser() async {
    if (currentUser != null) {
      return currentUser;
    }

    // final firebaseUser = await _firebaseAuth.currentUser();
    // if (firebaseUser == null ||
    //     firebaseUser.uid == null ||
    //     firebaseUser.uid.isEmpty) {
    //   return null;
    // }

    // final user = await _getUser(firebaseUser);
    // currentUser = user;

    // return user;
  }

  Future<void> signOut() async {
    currentUser = null;

    final userDb = await _userDbFuture;
    await userDb.delete('user');

    final isSignedIn = await _googleSignIn.isSignedIn();
    if (isSignedIn) {
      await _googleSignIn.disconnect();
    }
  }

// Future<User> _getUser(FirebaseUser firebaseUser) async {
//   if (firebaseUser == null ||
//       firebaseUser.uid == null ||
//       firebaseUser.uid.isEmpty) {
//     return null;
//   }

//   // final userDocument = await _collection.document(firebaseUser.uid).get();
//   // if (userDocument == null || !userDocument.exists) {
//   //   return null;
//   // }

//   User user;
//   // final userData = userDocument.data;
//   // switch (userData['role']) {
//   //   case 'client':
//   //     final addresses = <Address>[];
//   //     if (userData['addresses'] != null) {
//   //       (userData['addresses'] as List<dynamic>).forEach((a) {
//   //         final address =
//   //             new Address(a['address'] as String, a['region'] as String);
//   //         addresses.add(address);
//   //       });
//   //     }

//   //     final client = new Client();
//   //     client.setData(
//   //       id: firebaseUser.uid,
//   //       email: userData['email'] as String,
//   //       phoneNumber: userData['phone_number'] as String,
//   //       firstName: userData['first_name'] as String,
//   //       lastName: userData['last_name'] as String,
//   //       avatarUrl: userData['avatar_url'] as String,
//   //       addresses: addresses,
//   //       signupMethod: SignupMethod.email,
//   //       dateCreated: userData['date_created'] == null
//   //           ? null
//   //           : (userData['date_created'] as Timestamp).toDate(),
//   //     );

//   //     user = client;
//   //     break;

//   //   case 'company':
//   //     final servicesRepo = ServicesRepository();
//   //     final servicesIds = userData['services'] == null
//   //         ? null
//   //         : (userData['services'] as List<dynamic>).cast<String>();
//   //     final services = await servicesRepo.get(servicesIds);

//   //     final reviewsRepo = ReviewsRepository();
//   //     final reviews = await reviewsRepo.get(firebaseUser.uid);

//   //     final company = new Company();
//   //     company.setData(
//   //       id: firebaseUser.uid,
//   //       name: userData['name'] as String,
//   //       email: userData['email'] as String,
//   //       phoneNumber: userData['phone_number'] as String,
//   //       description: userData['description'] as String,
//   //       avatarUrl: userData['avatar_url'] as String,
//   //       bannerUrl: userData['banner_url'] as String,
//   //       location: userData['location'] as GeoPoint,
//   //       address: userData['address'] == null
//   //           ? null
//   //           : new Address(userData['address']['address'] as String,
//   //               userData['address']['region'] as String),
//   //       rating: (userData['rating'] as num)?.toDouble(),
//   //       reviews: reviews,
//   //       currency: userData['currency'] as String,
//   //       hourlyPrice: (userData['hourly_price'] as num)?.toDouble(),
//   //       services: services,
//   //       isAvailable: userData['is_available'] as bool,
//   //       dateCreated: (userData['date_created'] as Timestamp)?.toDate(),
//   //       jobsDone: (userData['jobs_done'] as int) ?? 0,
//   //     );

//   //     user = company;
//   //     break;

//   //   default:
//   //     user = null;
//   // }

//   return user;
// }
}
