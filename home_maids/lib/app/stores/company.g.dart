// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Company on _Company, Store {
  final _$idAtom = Atom(name: '_Company.id');

  @override
  String get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$nameAtom = Atom(name: '_Company.name');

  @override
  String get name {
    _$nameAtom.context.enforceReadPolicy(_$nameAtom);
    _$nameAtom.reportObserved();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.context.conditionallyRunInAction(() {
      super.name = value;
      _$nameAtom.reportChanged();
    }, _$nameAtom, name: '${_$nameAtom.name}_set');
  }

  final _$emailAtom = Atom(name: '_Company.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$phoneNumberAtom = Atom(name: '_Company.phoneNumber');

  @override
  String get phoneNumber {
    _$phoneNumberAtom.context.enforceReadPolicy(_$phoneNumberAtom);
    _$phoneNumberAtom.reportObserved();
    return super.phoneNumber;
  }

  @override
  set phoneNumber(String value) {
    _$phoneNumberAtom.context.conditionallyRunInAction(() {
      super.phoneNumber = value;
      _$phoneNumberAtom.reportChanged();
    }, _$phoneNumberAtom, name: '${_$phoneNumberAtom.name}_set');
  }

  final _$descriptionAtom = Atom(name: '_Company.description');

  @override
  String get description {
    _$descriptionAtom.context.enforceReadPolicy(_$descriptionAtom);
    _$descriptionAtom.reportObserved();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.context.conditionallyRunInAction(() {
      super.description = value;
      _$descriptionAtom.reportChanged();
    }, _$descriptionAtom, name: '${_$descriptionAtom.name}_set');
  }

  final _$avatarUrlAtom = Atom(name: '_Company.avatarUrl');

  @override
  String get avatarUrl {
    _$avatarUrlAtom.context.enforceReadPolicy(_$avatarUrlAtom);
    _$avatarUrlAtom.reportObserved();
    return super.avatarUrl;
  }

  @override
  set avatarUrl(String value) {
    _$avatarUrlAtom.context.conditionallyRunInAction(() {
      super.avatarUrl = value;
      _$avatarUrlAtom.reportChanged();
    }, _$avatarUrlAtom, name: '${_$avatarUrlAtom.name}_set');
  }

  final _$bannerUrlAtom = Atom(name: '_Company.bannerUrl');

  @override
  String get bannerUrl {
    _$bannerUrlAtom.context.enforceReadPolicy(_$bannerUrlAtom);
    _$bannerUrlAtom.reportObserved();
    return super.bannerUrl;
  }

  @override
  set bannerUrl(String value) {
    _$bannerUrlAtom.context.conditionallyRunInAction(() {
      super.bannerUrl = value;
      _$bannerUrlAtom.reportChanged();
    }, _$bannerUrlAtom, name: '${_$bannerUrlAtom.name}_set');
  }

  final _$locationAtom = Atom(name: '_Company.location');

  @override
  GeoPoint get location {
    _$locationAtom.context.enforceReadPolicy(_$locationAtom);
    _$locationAtom.reportObserved();
    return super.location;
  }

  @override
  set location(GeoPoint value) {
    _$locationAtom.context.conditionallyRunInAction(() {
      super.location = value;
      _$locationAtom.reportChanged();
    }, _$locationAtom, name: '${_$locationAtom.name}_set');
  }

  final _$addressAtom = Atom(name: '_Company.address');

  @override
  Address get address {
    _$addressAtom.context.enforceReadPolicy(_$addressAtom);
    _$addressAtom.reportObserved();
    return super.address;
  }

  @override
  set address(Address value) {
    _$addressAtom.context.conditionallyRunInAction(() {
      super.address = value;
      _$addressAtom.reportChanged();
    }, _$addressAtom, name: '${_$addressAtom.name}_set');
  }

  final _$ratingAtom = Atom(name: '_Company.rating');

  @override
  double get rating {
    _$ratingAtom.context.enforceReadPolicy(_$ratingAtom);
    _$ratingAtom.reportObserved();
    return super.rating;
  }

  @override
  set rating(double value) {
    _$ratingAtom.context.conditionallyRunInAction(() {
      super.rating = value;
      _$ratingAtom.reportChanged();
    }, _$ratingAtom, name: '${_$ratingAtom.name}_set');
  }

  final _$reviewsAtom = Atom(name: '_Company.reviews');

  @override
  List<Review> get reviews {
    _$reviewsAtom.context.enforceReadPolicy(_$reviewsAtom);
    _$reviewsAtom.reportObserved();
    return super.reviews;
  }

  @override
  set reviews(List<Review> value) {
    _$reviewsAtom.context.conditionallyRunInAction(() {
      super.reviews = value;
      _$reviewsAtom.reportChanged();
    }, _$reviewsAtom, name: '${_$reviewsAtom.name}_set');
  }

  final _$currencyAtom = Atom(name: '_Company.currency');

  @override
  String get currency {
    _$currencyAtom.context.enforceReadPolicy(_$currencyAtom);
    _$currencyAtom.reportObserved();
    return super.currency;
  }

  @override
  set currency(String value) {
    _$currencyAtom.context.conditionallyRunInAction(() {
      super.currency = value;
      _$currencyAtom.reportChanged();
    }, _$currencyAtom, name: '${_$currencyAtom.name}_set');
  }

  final _$hourlyPriceAtom = Atom(name: '_Company.hourlyPrice');

  @override
  double get hourlyPrice {
    _$hourlyPriceAtom.context.enforceReadPolicy(_$hourlyPriceAtom);
    _$hourlyPriceAtom.reportObserved();
    return super.hourlyPrice;
  }

  @override
  set hourlyPrice(double value) {
    _$hourlyPriceAtom.context.conditionallyRunInAction(() {
      super.hourlyPrice = value;
      _$hourlyPriceAtom.reportChanged();
    }, _$hourlyPriceAtom, name: '${_$hourlyPriceAtom.name}_set');
  }

  final _$isAvailableAtom = Atom(name: '_Company.isAvailable');

  @override
  bool get isAvailable {
    _$isAvailableAtom.context.enforceReadPolicy(_$isAvailableAtom);
    _$isAvailableAtom.reportObserved();
    return super.isAvailable;
  }

  @override
  set isAvailable(bool value) {
    _$isAvailableAtom.context.conditionallyRunInAction(() {
      super.isAvailable = value;
      _$isAvailableAtom.reportChanged();
    }, _$isAvailableAtom, name: '${_$isAvailableAtom.name}_set');
  }

  final _$signupMethodAtom = Atom(name: '_Company.signupMethod');

  @override
  SignupMethod get signupMethod {
    _$signupMethodAtom.context.enforceReadPolicy(_$signupMethodAtom);
    _$signupMethodAtom.reportObserved();
    return super.signupMethod;
  }

  @override
  set signupMethod(SignupMethod value) {
    _$signupMethodAtom.context.conditionallyRunInAction(() {
      super.signupMethod = value;
      _$signupMethodAtom.reportChanged();
    }, _$signupMethodAtom, name: '${_$signupMethodAtom.name}_set');
  }

  final _$incomesAtom = Atom(name: '_Company.incomes');

  @override
  Incomes get incomes {
    _$incomesAtom.context.enforceReadPolicy(_$incomesAtom);
    _$incomesAtom.reportObserved();
    return super.incomes;
  }

  @override
  set incomes(Incomes value) {
    _$incomesAtom.context.conditionallyRunInAction(() {
      super.incomes = value;
      _$incomesAtom.reportChanged();
    }, _$incomesAtom, name: '${_$incomesAtom.name}_set');
  }

  final _$dateCreatedAtom = Atom(name: '_Company.dateCreated');

  @override
  DateTime get dateCreated {
    _$dateCreatedAtom.context.enforceReadPolicy(_$dateCreatedAtom);
    _$dateCreatedAtom.reportObserved();
    return super.dateCreated;
  }

  @override
  set dateCreated(DateTime value) {
    _$dateCreatedAtom.context.conditionallyRunInAction(() {
      super.dateCreated = value;
      _$dateCreatedAtom.reportChanged();
    }, _$dateCreatedAtom, name: '${_$dateCreatedAtom.name}_set');
  }

  final _$jobsDoneAtom = Atom(name: '_Company.jobsDone');

  @override
  int get jobsDone {
    _$jobsDoneAtom.context.enforceReadPolicy(_$jobsDoneAtom);
    _$jobsDoneAtom.reportObserved();
    return super.jobsDone;
  }

  @override
  set jobsDone(int value) {
    _$jobsDoneAtom.context.conditionallyRunInAction(() {
      super.jobsDone = value;
      _$jobsDoneAtom.reportChanged();
    }, _$jobsDoneAtom, name: '${_$jobsDoneAtom.name}_set');
  }

  final _$tokenAtom = Atom(name: '_Company.token');

  @override
  String get token {
    _$tokenAtom.context.enforceReadPolicy(_$tokenAtom);
    _$tokenAtom.reportObserved();
    return super.token;
  }

  @override
  set token(String value) {
    _$tokenAtom.context.conditionallyRunInAction(() {
      super.token = value;
      _$tokenAtom.reportChanged();
    }, _$tokenAtom, name: '${_$tokenAtom.name}_set');
  }

  final _$fcmTokenAtom = Atom(name: '_Company.fcmToken');

  @override
  Future<String> get fcmToken {
    _$fcmTokenAtom.context.enforceReadPolicy(_$fcmTokenAtom);
    _$fcmTokenAtom.reportObserved();
    return super.fcmToken;
  }

  @override
  set fcmToken(Future<String> value) {
    _$fcmTokenAtom.context.conditionallyRunInAction(() {
      super.fcmToken = value;
      _$fcmTokenAtom.reportChanged();
    }, _$fcmTokenAtom, name: '${_$fcmTokenAtom.name}_set');
  }

  final _$_CompanyActionController = ActionController(name: '_Company');

  @override
  void copyFrom(Company other) {
    final _$actionInfo = _$_CompanyActionController.startAction();
    try {
      return super.copyFrom(other);
    } finally {
      _$_CompanyActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setData(
      {String id,
      String name,
      String email,
      String phoneNumber,
      String description,
      String avatarUrl,
      String bannerUrl,
      GeoPoint location,
      Address address,
      double rating,
      List<Review> reviews,
      String currency,
      double hourlyPrice,
      List<Service> services,
      bool isAvailable,
      SignupMethod signupMethod,
      Incomes incomes,
      DateTime dateCreated,
      int jobsDone,
      String token,
      Future<String> fcmToken}) {
    final _$actionInfo = _$_CompanyActionController.startAction();
    try {
      return super.setData(
          id: id,
          name: name,
          email: email,
          phoneNumber: phoneNumber,
          description: description,
          avatarUrl: avatarUrl,
          bannerUrl: bannerUrl,
          location: location,
          address: address,
          rating: rating,
          reviews: reviews,
          currency: currency,
          hourlyPrice: hourlyPrice,
          services: services,
          isAvailable: isAvailable,
          signupMethod: signupMethod,
          incomes: incomes,
          dateCreated: dateCreated,
          jobsDone: jobsDone,
          token: token,
          fcmToken: fcmToken);
    } finally {
      _$_CompanyActionController.endAction(_$actionInfo);
    }
  }
}
