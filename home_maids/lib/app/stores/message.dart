import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:mobx/mobx.dart';

part 'message.g.dart';

class Message = _Message with _$Message;

abstract class _Message with Store {
  @observable
  String id;

  @observable
  User sender;

  @observable
  User recipient;

  @observable
  DateTime date;

  @observable
  String content;

  @observable
  bool isRead;

  @observable
  bool isArchived;

  @action
  void setData({
    String id,
    User sender,
    User recipient,
    DateTime date,
    String content,
    bool isRead,
    bool isArchived,
  }) {
    if (id != null) this.id = id;
    if (sender != null) this.sender = sender;
    if (recipient != null) this.recipient = recipient;
    if (date != null) this.date = date;
    if (content != null) this.content = content;
    if (isRead != null) this.isRead = isRead;
    if (isArchived != null) this.isArchived = isArchived;
  }

  Map<String, dynamic> toMap() {
    return {
      'sender_id': sender.id,
      'recipient_id': recipient.id,
      'date': Timestamp.fromDate(date),
      'content': content,
      'is_read': isRead,
      'is_archived': isArchived,
    };
  }
}
