import 'package:home_maids/app/stores/order.dart';
import 'package:mobx/mobx.dart';

part 'orders_store.g.dart';

class OrdersStore = _OrdersStore with _$OrdersStore;

abstract class _OrdersStore with Store {
  @observable
  Future<List<Order>> orders;

  @action
  void setOrders(Future<List<Order>> orders) {
    this.orders = orders;
  }
}
