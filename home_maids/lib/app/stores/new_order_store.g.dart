// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_order_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NewOrderStore on _NewOrderStore, Store {
  final _$orderAtom = Atom(name: '_NewOrderStore.order');

  @override
  Order get order {
    _$orderAtom.context.enforceReadPolicy(_$orderAtom);
    _$orderAtom.reportObserved();
    return super.order;
  }

  @override
  set order(Order value) {
    _$orderAtom.context.conditionallyRunInAction(() {
      super.order = value;
      _$orderAtom.reportChanged();
    }, _$orderAtom, name: '${_$orderAtom.name}_set');
  }

  final _$_NewOrderStoreActionController =
      ActionController(name: '_NewOrderStore');

  @override
  void setOrder(Order order) {
    final _$actionInfo = _$_NewOrderStoreActionController.startAction();
    try {
      return super.setOrder(order);
    } finally {
      _$_NewOrderStoreActionController.endAction(_$actionInfo);
    }
  }
}
