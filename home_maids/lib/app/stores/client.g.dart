// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Client on _Client, Store {
  final _$idAtom = Atom(name: '_Client.id');

  @override
  String get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$firstNameAtom = Atom(name: '_Client.firstName');

  @override
  String get firstName {
    _$firstNameAtom.context.enforceReadPolicy(_$firstNameAtom);
    _$firstNameAtom.reportObserved();
    return super.firstName;
  }

  @override
  set firstName(String value) {
    _$firstNameAtom.context.conditionallyRunInAction(() {
      super.firstName = value;
      _$firstNameAtom.reportChanged();
    }, _$firstNameAtom, name: '${_$firstNameAtom.name}_set');
  }

  final _$lastNameAtom = Atom(name: '_Client.lastName');

  @override
  String get lastName {
    _$lastNameAtom.context.enforceReadPolicy(_$lastNameAtom);
    _$lastNameAtom.reportObserved();
    return super.lastName;
  }

  @override
  set lastName(String value) {
    _$lastNameAtom.context.conditionallyRunInAction(() {
      super.lastName = value;
      _$lastNameAtom.reportChanged();
    }, _$lastNameAtom, name: '${_$lastNameAtom.name}_set');
  }

  final _$emailAtom = Atom(name: '_Client.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$signupMethodAtom = Atom(name: '_Client.signupMethod');

  @override
  SignupMethod get signupMethod {
    _$signupMethodAtom.context.enforceReadPolicy(_$signupMethodAtom);
    _$signupMethodAtom.reportObserved();
    return super.signupMethod;
  }

  @override
  set signupMethod(SignupMethod value) {
    _$signupMethodAtom.context.conditionallyRunInAction(() {
      super.signupMethod = value;
      _$signupMethodAtom.reportChanged();
    }, _$signupMethodAtom, name: '${_$signupMethodAtom.name}_set');
  }

  final _$phoneNumberAtom = Atom(name: '_Client.phoneNumber');

  @override
  String get phoneNumber {
    _$phoneNumberAtom.context.enforceReadPolicy(_$phoneNumberAtom);
    _$phoneNumberAtom.reportObserved();
    return super.phoneNumber;
  }

  @override
  set phoneNumber(String value) {
    _$phoneNumberAtom.context.conditionallyRunInAction(() {
      super.phoneNumber = value;
      _$phoneNumberAtom.reportChanged();
    }, _$phoneNumberAtom, name: '${_$phoneNumberAtom.name}_set');
  }

  final _$addressesAtom = Atom(name: '_Client.addresses');

  @override
  List<Address> get addresses {
    _$addressesAtom.context.enforceReadPolicy(_$addressesAtom);
    _$addressesAtom.reportObserved();
    return super.addresses;
  }

  @override
  set addresses(List<Address> value) {
    _$addressesAtom.context.conditionallyRunInAction(() {
      super.addresses = value;
      _$addressesAtom.reportChanged();
    }, _$addressesAtom, name: '${_$addressesAtom.name}_set');
  }

  final _$avatarUrlAtom = Atom(name: '_Client.avatarUrl');

  @override
  String get avatarUrl {
    _$avatarUrlAtom.context.enforceReadPolicy(_$avatarUrlAtom);
    _$avatarUrlAtom.reportObserved();
    return super.avatarUrl;
  }

  @override
  set avatarUrl(String value) {
    _$avatarUrlAtom.context.conditionallyRunInAction(() {
      super.avatarUrl = value;
      _$avatarUrlAtom.reportChanged();
    }, _$avatarUrlAtom, name: '${_$avatarUrlAtom.name}_set');
  }

  final _$dateCreatedAtom = Atom(name: '_Client.dateCreated');

  @override
  DateTime get dateCreated {
    _$dateCreatedAtom.context.enforceReadPolicy(_$dateCreatedAtom);
    _$dateCreatedAtom.reportObserved();
    return super.dateCreated;
  }

  @override
  set dateCreated(DateTime value) {
    _$dateCreatedAtom.context.conditionallyRunInAction(() {
      super.dateCreated = value;
      _$dateCreatedAtom.reportChanged();
    }, _$dateCreatedAtom, name: '${_$dateCreatedAtom.name}_set');
  }

  final _$completedOrdersCountAtom = Atom(name: '_Client.completedOrdersCount');

  @override
  int get completedOrdersCount {
    _$completedOrdersCountAtom.context
        .enforceReadPolicy(_$completedOrdersCountAtom);
    _$completedOrdersCountAtom.reportObserved();
    return super.completedOrdersCount;
  }

  @override
  set completedOrdersCount(int value) {
    _$completedOrdersCountAtom.context.conditionallyRunInAction(() {
      super.completedOrdersCount = value;
      _$completedOrdersCountAtom.reportChanged();
    }, _$completedOrdersCountAtom,
        name: '${_$completedOrdersCountAtom.name}_set');
  }

  final _$canceledOrdersCountAtom = Atom(name: '_Client.canceledOrdersCount');

  @override
  int get canceledOrdersCount {
    _$canceledOrdersCountAtom.context
        .enforceReadPolicy(_$canceledOrdersCountAtom);
    _$canceledOrdersCountAtom.reportObserved();
    return super.canceledOrdersCount;
  }

  @override
  set canceledOrdersCount(int value) {
    _$canceledOrdersCountAtom.context.conditionallyRunInAction(() {
      super.canceledOrdersCount = value;
      _$canceledOrdersCountAtom.reportChanged();
    }, _$canceledOrdersCountAtom,
        name: '${_$canceledOrdersCountAtom.name}_set');
  }

  final _$tokenAtom = Atom(name: '_Client.token');

  @override
  String get token {
    _$tokenAtom.context.enforceReadPolicy(_$tokenAtom);
    _$tokenAtom.reportObserved();
    return super.token;
  }

  @override
  set token(String value) {
    _$tokenAtom.context.conditionallyRunInAction(() {
      super.token = value;
      _$tokenAtom.reportChanged();
    }, _$tokenAtom, name: '${_$tokenAtom.name}_set');
  }

  final _$fcmTokenAtom = Atom(name: '_Client.fcmToken');

  @override
  String get fcmToken {
    _$fcmTokenAtom.context.enforceReadPolicy(_$fcmTokenAtom);
    _$fcmTokenAtom.reportObserved();
    return super.fcmToken;
  }

  @override
  set fcmToken(String value) {
    _$fcmTokenAtom.context.conditionallyRunInAction(() {
      super.fcmToken = value;
      _$fcmTokenAtom.reportChanged();
    }, _$fcmTokenAtom, name: '${_$fcmTokenAtom.name}_set');
  }

  final _$_ClientActionController = ActionController(name: '_Client');

  @override
  void setData(
      {String id,
      String firstName,
      String lastName,
      String email,
      SignupMethod signupMethod,
      String phoneNumber,
      List<Address> addresses,
      String avatarUrl,
      DateTime dateCreated,
      int completedOrdersCount,
      int canceledOrdersCount,
      String token,
      String fcmToken}) {
    final _$actionInfo = _$_ClientActionController.startAction();
    try {
      return super.setData(
          id: id,
          firstName: firstName,
          lastName: lastName,
          email: email,
          signupMethod: signupMethod,
          phoneNumber: phoneNumber,
          addresses: addresses,
          avatarUrl: avatarUrl,
          dateCreated: dateCreated,
          completedOrdersCount: completedOrdersCount,
          canceledOrdersCount: canceledOrdersCount,
          token: token,
          fcmToken: fcmToken);
    } finally {
      _$_ClientActionController.endAction(_$actionInfo);
    }
  }

  @override
  void copyFrom(Client other) {
    final _$actionInfo = _$_ClientActionController.startAction();
    try {
      return super.copyFrom(other);
    } finally {
      _$_ClientActionController.endAction(_$actionInfo);
    }
  }
}
