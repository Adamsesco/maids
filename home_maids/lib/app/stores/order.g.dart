// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Order on _Order, Store {
  final _$isNewAtom = Atom(name: '_Order.isNew');

  @override
  bool get isNew {
    _$isNewAtom.context.enforceReadPolicy(_$isNewAtom);
    _$isNewAtom.reportObserved();
    return super.isNew;
  }

  @override
  set isNew(bool value) {
    _$isNewAtom.context.conditionallyRunInAction(() {
      super.isNew = value;
      _$isNewAtom.reportChanged();
    }, _$isNewAtom, name: '${_$isNewAtom.name}_set');
  }

  final _$idAtom = Atom(name: '_Order.id');

  @override
  String get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$numberAtom = Atom(name: '_Order.number');

  @override
  int get number {
    _$numberAtom.context.enforceReadPolicy(_$numberAtom);
    _$numberAtom.reportObserved();
    return super.number;
  }

  @override
  set number(int value) {
    _$numberAtom.context.conditionallyRunInAction(() {
      super.number = value;
      _$numberAtom.reportChanged();
    }, _$numberAtom, name: '${_$numberAtom.name}_set');
  }

  final _$clientAtom = Atom(name: '_Order.client');

  @override
  Client get client {
    _$clientAtom.context.enforceReadPolicy(_$clientAtom);
    _$clientAtom.reportObserved();
    return super.client;
  }

  @override
  set client(Client value) {
    _$clientAtom.context.conditionallyRunInAction(() {
      super.client = value;
      _$clientAtom.reportChanged();
    }, _$clientAtom, name: '${_$clientAtom.name}_set');
  }

  final _$companyAtom = Atom(name: '_Order.company');

  @override
  Company get company {
    _$companyAtom.context.enforceReadPolicy(_$companyAtom);
    _$companyAtom.reportObserved();
    return super.company;
  }

  @override
  set company(Company value) {
    _$companyAtom.context.conditionallyRunInAction(() {
      super.company = value;
      _$companyAtom.reportChanged();
    }, _$companyAtom, name: '${_$companyAtom.name}_set');
  }

  final _$addressAtom = Atom(name: '_Order.address');

  @override
  Address get address {
    _$addressAtom.context.enforceReadPolicy(_$addressAtom);
    _$addressAtom.reportObserved();
    return super.address;
  }

  @override
  set address(Address value) {
    _$addressAtom.context.conditionallyRunInAction(() {
      super.address = value;
      _$addressAtom.reportChanged();
    }, _$addressAtom, name: '${_$addressAtom.name}_set');
  }

  final _$servicesAtom = Atom(name: '_Order.services');

  @override
  List<Service> get services {
    _$servicesAtom.context.enforceReadPolicy(_$servicesAtom);
    _$servicesAtom.reportObserved();
    return super.services;
  }

  @override
  set services(List<Service> value) {
    _$servicesAtom.context.conditionallyRunInAction(() {
      super.services = value;
      _$servicesAtom.reportChanged();
    }, _$servicesAtom, name: '${_$servicesAtom.name}_set');
  }

  final _$servicesDataAtom = Atom(name: '_Order.servicesData');

  @override
  List<ServiceData> get servicesData {
    _$servicesDataAtom.context.enforceReadPolicy(_$servicesDataAtom);
    _$servicesDataAtom.reportObserved();
    return super.servicesData;
  }

  @override
  set servicesData(List<ServiceData> value) {
    _$servicesDataAtom.context.conditionallyRunInAction(() {
      super.servicesData = value;
      _$servicesDataAtom.reportChanged();
    }, _$servicesDataAtom, name: '${_$servicesDataAtom.name}_set');
  }

  final _$durationAtom = Atom(name: '_Order.duration');

  @override
  Duration get duration {
    _$durationAtom.context.enforceReadPolicy(_$durationAtom);
    _$durationAtom.reportObserved();
    return super.duration;
  }

  @override
  set duration(Duration value) {
    _$durationAtom.context.conditionallyRunInAction(() {
      super.duration = value;
      _$durationAtom.reportChanged();
    }, _$durationAtom, name: '${_$durationAtom.name}_set');
  }

  final _$maidsCountAtom = Atom(name: '_Order.maidsCount');

  @override
  int get maidsCount {
    _$maidsCountAtom.context.enforceReadPolicy(_$maidsCountAtom);
    _$maidsCountAtom.reportObserved();
    return super.maidsCount;
  }

  @override
  set maidsCount(int value) {
    _$maidsCountAtom.context.conditionallyRunInAction(() {
      super.maidsCount = value;
      _$maidsCountAtom.reportChanged();
    }, _$maidsCountAtom, name: '${_$maidsCountAtom.name}_set');
  }

  final _$requiresMaterialsAtom = Atom(name: '_Order.requiresMaterials');

  @override
  bool get requiresMaterials {
    _$requiresMaterialsAtom.context.enforceReadPolicy(_$requiresMaterialsAtom);
    _$requiresMaterialsAtom.reportObserved();
    return super.requiresMaterials;
  }

  @override
  set requiresMaterials(bool value) {
    _$requiresMaterialsAtom.context.conditionallyRunInAction(() {
      super.requiresMaterials = value;
      _$requiresMaterialsAtom.reportChanged();
    }, _$requiresMaterialsAtom, name: '${_$requiresMaterialsAtom.name}_set');
  }

  final _$dateAtom = Atom(name: '_Order.date');

  @override
  DateTime get date {
    _$dateAtom.context.enforceReadPolicy(_$dateAtom);
    _$dateAtom.reportObserved();
    return super.date;
  }

  @override
  set date(DateTime value) {
    _$dateAtom.context.conditionallyRunInAction(() {
      super.date = value;
      _$dateAtom.reportChanged();
    }, _$dateAtom, name: '${_$dateAtom.name}_set');
  }

  final _$priceAtom = Atom(name: '_Order.price');

  @override
  double get price {
    _$priceAtom.context.enforceReadPolicy(_$priceAtom);
    _$priceAtom.reportObserved();
    return super.price;
  }

  @override
  set price(double value) {
    _$priceAtom.context.conditionallyRunInAction(() {
      super.price = value;
      _$priceAtom.reportChanged();
    }, _$priceAtom, name: '${_$priceAtom.name}_set');
  }

  final _$dateCreatedAtom = Atom(name: '_Order.dateCreated');

  @override
  DateTime get dateCreated {
    _$dateCreatedAtom.context.enforceReadPolicy(_$dateCreatedAtom);
    _$dateCreatedAtom.reportObserved();
    return super.dateCreated;
  }

  @override
  set dateCreated(DateTime value) {
    _$dateCreatedAtom.context.conditionallyRunInAction(() {
      super.dateCreated = value;
      _$dateCreatedAtom.reportChanged();
    }, _$dateCreatedAtom, name: '${_$dateCreatedAtom.name}_set');
  }

  final _$statusAtom = Atom(name: '_Order.status');

  @override
  OrderStatus get status {
    _$statusAtom.context.enforceReadPolicy(_$statusAtom);
    _$statusAtom.reportObserved();
    return super.status;
  }

  @override
  set status(OrderStatus value) {
    _$statusAtom.context.conditionallyRunInAction(() {
      super.status = value;
      _$statusAtom.reportChanged();
    }, _$statusAtom, name: '${_$statusAtom.name}_set');
  }

  final _$paymentMethodAtom = Atom(name: '_Order.paymentMethod');

  @override
  PaymentMethod get paymentMethod {
    _$paymentMethodAtom.context.enforceReadPolicy(_$paymentMethodAtom);
    _$paymentMethodAtom.reportObserved();
    return super.paymentMethod;
  }

  @override
  set paymentMethod(PaymentMethod value) {
    _$paymentMethodAtom.context.conditionallyRunInAction(() {
      super.paymentMethod = value;
      _$paymentMethodAtom.reportChanged();
    }, _$paymentMethodAtom, name: '${_$paymentMethodAtom.name}_set');
  }

  final _$_OrderActionController = ActionController(name: '_Order');

  @override
  void setData(
      {bool isNew,
      String id,
      int number,
      Client client,
      Company company,
      Address address,
      List<Service> services,
      List<ServiceData> servicesData,
      Duration duration,
      int maidsCount,
      bool requiresMaterials,
      DateTime date,
      double price,
      DateTime dateCreated,
      OrderStatus status,
      PaymentMethod paymentMethod}) {
    final _$actionInfo = _$_OrderActionController.startAction();
    try {
      return super.setData(
          isNew: isNew,
          id: id,
          number: number,
          client: client,
          company: company,
          address: address,
          services: services,
          servicesData: servicesData,
          duration: duration,
          maidsCount: maidsCount,
          requiresMaterials: requiresMaterials,
          date: date,
          price: price,
          dateCreated: dateCreated,
          status: status,
          paymentMethod: paymentMethod);
    } finally {
      _$_OrderActionController.endAction(_$actionInfo);
    }
  }
}
