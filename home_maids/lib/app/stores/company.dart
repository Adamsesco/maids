import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/stores/incomes.dart';
import 'package:home_maids/app/stores/review.dart';
import 'package:home_maids/app/stores/service.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/signup_method.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:mobx/mobx.dart';

part 'company.g.dart';

class Company = _Company with _$Company;

abstract class _Company extends User with Store {
  @override
  UserRole get role => UserRole.company;

  @observable
  @override
  String id;

  @observable
  String name;

  @observable
  @override
  String email;

  @observable
  String phoneNumber;

  @observable
  String description;

  @observable
  String avatarUrl;

  @observable
  String bannerUrl;

  @observable
  GeoPoint location;

  @observable
  Address address;

  @observable
  double rating;

  @observable
  List<Review> reviews;

  @observable
  String currency;

  @observable
  double hourlyPrice;

  List<Service> services;

  @observable
  bool isAvailable;

  @observable
  @override
  SignupMethod signupMethod;

  @observable
  Incomes incomes;

  @observable
  DateTime dateCreated;

  @observable
  int jobsDone;

  @observable
  String token;

  @observable
  Future<String> fcmToken;

  @action
  void copyFrom(Company other) {
    id = other.id;
    name = other.name;
    email = other.email;
    phoneNumber = other.phoneNumber;
    description = other.description;
    avatarUrl = other.avatarUrl;
    bannerUrl = other.bannerUrl;
    location = other.location;
    address = other.address;
    rating = other.rating;
    reviews = other.reviews;
    currency = other.currency;
    hourlyPrice = other.hourlyPrice;
    services = other.services;
    isAvailable = other.isAvailable;
    signupMethod = other.signupMethod;
    incomes = other.incomes;
    dateCreated = other.dateCreated;
    jobsDone = other.jobsDone;
    token = other.token;
    fcmToken = other.fcmToken;
  }

  @action
  void setData({
    String id,
    String name,
    String email,
    String phoneNumber,
    String description,
    String avatarUrl,
    String bannerUrl,
    GeoPoint location,
    Address address,
    double rating,
    List<Review> reviews,
    String currency,
    double hourlyPrice,
    List<Service> services,
    bool isAvailable,
    SignupMethod signupMethod,
    Incomes incomes,
    DateTime dateCreated,
    int jobsDone,
    String token,
    Future<String> fcmToken,
  }) {
    if (id != null) this.id = id;
    if (name != null) this.name = name;
    if (email != null) this.email = email;
    if (phoneNumber != null) this.phoneNumber = phoneNumber;
    if (description != null) this.description = description;
    if (avatarUrl != null) this.avatarUrl = avatarUrl;
    if (bannerUrl != null) this.bannerUrl = bannerUrl;
    if (location != null) this.location = location;
    if (address != null) this.address = address;
    if (rating != null) this.rating = rating;
    if (reviews != null) this.reviews = reviews;
    if (currency != null) this.currency = currency;
    if (hourlyPrice != null) this.hourlyPrice = hourlyPrice;
    if (services != null) this.services = services;
    if (isAvailable != null) this.isAvailable = isAvailable;
    if (signupMethod != null) this.signupMethod = signupMethod;
    if (incomes != null) this.incomes = incomes;
    if (dateCreated != null) this.dateCreated = dateCreated;
    if (jobsDone != null) this.jobsDone = jobsDone;
    if (token != null) this.token = token;
    if (fcmToken != null) this.fcmToken = fcmToken;
  }

  @override
  Map<String, dynamic> toMap() {
    String signupMethod;
    switch (this.signupMethod) {
      case SignupMethod.email:
        signupMethod = 'email';
        break;

      case SignupMethod.facebook:
        signupMethod = 'facebook';
        break;

      case SignupMethod.google:
        signupMethod = 'google';
        break;
    }

    return <String, dynamic>{
      'id': id,
      'name': name,
      'email': email,
      'phone_number': phoneNumber,
      'description': description,
      'avatar_url': avatarUrl,
      'banner_url': bannerUrl,
      'location': location,
      if (address != null) 'address': address.toMap(),
      'rating': rating,
      'currency': currency,
      'hourly_price': hourlyPrice,
      if (services != null)
        'services': services.map((service) => service.id).toList(),
      'is_available': isAvailable,
      'signup_method': signupMethod,
      if (dateCreated != null) 'date_created': dateCreated.toString(),
      'jobs_done': jobsDone,
      'token': token,
      'role': 'company',
    };
  }

  @override
  User fromMap(Map<dynamic, dynamic> map) {
    SignupMethod signupMethod;
    switch (map['signup_method']) {
      case 'email':
        signupMethod = SignupMethod.email;
        break;

      case 'google':
        signupMethod = SignupMethod.google;
        break;

      case 'facebook':
        signupMethod = SignupMethod.facebook;
        break;
    }

    Address address =
        Address(map['address']['address'], map['address']['region']);

    final company = Company()
      ..setData(
        id: map['id'],
        name: map['name'],
        email: map['email'],
        phoneNumber: map['phone_number'],
        description: map['description'],
        avatarUrl: map['avatar_url'],
        address: address,
        rating: map['rating'],
        currency: map['currency'],
        hourlyPrice: map['hourly_price'],
        isAvailable: map['is_available'],
        signupMethod: signupMethod,
        dateCreated: DateTime.parse(map['date_created']),
        jobsDone: map['jobs_done'],
        token: map['token'],
      );

    return company;
  }
}
