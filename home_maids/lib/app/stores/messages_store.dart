import 'package:home_maids/app/stores/message.dart';
import 'package:mobx/mobx.dart';

part 'messages_store.g.dart';

class MessagesStore = _MessagesStore with _$MessagesStore;

abstract class _MessagesStore with Store {
  @observable
  int unreadCount = 0;

  @observable
  List<Message> messages = [];

  @action
  void readOne() {
    if (unreadCount > 0) {
      unreadCount -= 1;
    }
  }

  @action
  void addMessage(Message message) {
    final exists =
        messages.where((m) => m.id == message.id)?.isNotEmpty ?? false;

    if (!exists) {
      messages.add(message);
      messages.sort((m1, m2) => m1.date.compareTo(m2.date));

      if (!message.isRead) {
        unreadCount += 1;
      }
    }
  }
}
