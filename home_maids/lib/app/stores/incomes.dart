import 'package:mobx/mobx.dart';

part 'incomes.g.dart';

class Incomes = _Incomes with _$Incomes;

abstract class _Incomes with Store {
  @observable
  String id;

  @observable
  int year;

  @observable
  Map<DateTime, double> dailyIncomes;

  @action
  void setData({
    String id,
    int year,
    Map<DateTime, double> dailyIncomes,
  }) {
    if (id != null) this.id = id;
    if (year != null) this.year = year;
    if (dailyIncomes != null) this.dailyIncomes = dailyIncomes;
  }
}
