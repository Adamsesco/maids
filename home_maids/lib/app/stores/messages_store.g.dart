// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'messages_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MessagesStore on _MessagesStore, Store {
  final _$unreadCountAtom = Atom(name: '_MessagesStore.unreadCount');

  @override
  int get unreadCount {
    _$unreadCountAtom.context.enforceReadPolicy(_$unreadCountAtom);
    _$unreadCountAtom.reportObserved();
    return super.unreadCount;
  }

  @override
  set unreadCount(int value) {
    _$unreadCountAtom.context.conditionallyRunInAction(() {
      super.unreadCount = value;
      _$unreadCountAtom.reportChanged();
    }, _$unreadCountAtom, name: '${_$unreadCountAtom.name}_set');
  }

  final _$messagesAtom = Atom(name: '_MessagesStore.messages');

  @override
  List<Message> get messages {
    _$messagesAtom.context.enforceReadPolicy(_$messagesAtom);
    _$messagesAtom.reportObserved();
    return super.messages;
  }

  @override
  set messages(List<Message> value) {
    _$messagesAtom.context.conditionallyRunInAction(() {
      super.messages = value;
      _$messagesAtom.reportChanged();
    }, _$messagesAtom, name: '${_$messagesAtom.name}_set');
  }

  final _$_MessagesStoreActionController =
      ActionController(name: '_MessagesStore');

  @override
  void readOne() {
    final _$actionInfo = _$_MessagesStoreActionController.startAction();
    try {
      return super.readOne();
    } finally {
      _$_MessagesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addMessage(Message message) {
    final _$actionInfo = _$_MessagesStoreActionController.startAction();
    try {
      return super.addMessage(message);
    } finally {
      _$_MessagesStoreActionController.endAction(_$actionInfo);
    }
  }
}
