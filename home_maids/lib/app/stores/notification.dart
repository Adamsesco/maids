import 'package:home_maids/app/types/notification_status.dart';
import 'package:home_maids/app/types/notification_type.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:mobx/mobx.dart';

import 'order.dart';

part 'notification.g.dart';

class Notification = _Notification with _$Notification;

abstract class _Notification with Store {
  @observable
  String id;

  @observable
  String title;

  @observable
  String body;

  @observable
  DateTime date;

  @observable
  String firstName;

  @observable
  String lastName;

  @observable
  String avatarUrl;

  // @observable
  // DateTime date;

  // @observable
  // NotificationType type;

  // @observable
  // User triggerer;

  // @observable
  // Order order;

  @observable
  NotificationStatus status = NotificationStatus.unread;

  @action
  void setData({
    String id,
    String title,
    String body,
    DateTime date,
    String firstName,
    String lastName,
    String avatarUrl,
    // DateTime date,
    NotificationType type,
    // User triggerer,
    // Order order,
    NotificationStatus status,
  }) {
    if (id != null) this.id = id;
    if (title != null) this.title = title;
    if (body != null) this.body = body;
    if (date != null) this.date = date;
    if (firstName != null) this.firstName = firstName;
    if (lastName != null) this.lastName = lastName;
    if (avatarUrl != null) this.avatarUrl = avatarUrl;
    // if (date != null) this.date = date;
    // if (type != null) this.type = type;
    // if (triggerer != null) this.triggerer = triggerer;
    // if (order != null) this.order = order;
    if (status != null) this.status = status;
  }

  @override
  String toString() {
    return "id: $id\ntitle: $title\nbody:$body\nfirstName:$firstName\nlastName:$lastName";
  }
}
