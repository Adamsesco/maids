// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incomes.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Incomes on _Incomes, Store {
  final _$idAtom = Atom(name: '_Incomes.id');

  @override
  String get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$yearAtom = Atom(name: '_Incomes.year');

  @override
  int get year {
    _$yearAtom.context.enforceReadPolicy(_$yearAtom);
    _$yearAtom.reportObserved();
    return super.year;
  }

  @override
  set year(int value) {
    _$yearAtom.context.conditionallyRunInAction(() {
      super.year = value;
      _$yearAtom.reportChanged();
    }, _$yearAtom, name: '${_$yearAtom.name}_set');
  }

  final _$dailyIncomesAtom = Atom(name: '_Incomes.dailyIncomes');

  @override
  Map<DateTime, double> get dailyIncomes {
    _$dailyIncomesAtom.context.enforceReadPolicy(_$dailyIncomesAtom);
    _$dailyIncomesAtom.reportObserved();
    return super.dailyIncomes;
  }

  @override
  set dailyIncomes(Map<DateTime, double> value) {
    _$dailyIncomesAtom.context.conditionallyRunInAction(() {
      super.dailyIncomes = value;
      _$dailyIncomesAtom.reportChanged();
    }, _$dailyIncomesAtom, name: '${_$dailyIncomesAtom.name}_set');
  }

  final _$_IncomesActionController = ActionController(name: '_Incomes');

  @override
  void setData({String id, int year, Map<DateTime, double> dailyIncomes}) {
    final _$actionInfo = _$_IncomesActionController.startAction();
    try {
      return super.setData(id: id, year: year, dailyIncomes: dailyIncomes);
    } finally {
      _$_IncomesActionController.endAction(_$actionInfo);
    }
  }
}
