import 'package:home_maids/app/stores/notification.dart';
import 'package:home_maids/app/types/notification_status.dart';
import 'package:mobx/mobx.dart';

part 'notifications_store.g.dart';

class NotificationsStore = _NotificationsStore with _$NotificationsStore;

abstract class _NotificationsStore with Store {
  @observable
  int unreadCount = 0;

  @observable
  List<Notification> notifications = [];

  @action
  void readOne() {
    if (unreadCount > 0) {
      unreadCount -= 1;
    }
  }

  @action
  void addNotification(Notification notification) {
    notifications.add(notification);
    notifications.sort((n1, n2) => n1.date.compareTo(n2.date));

    if (notification.status == NotificationStatus.unread) {
      unreadCount += 1;
    }
  }
}
