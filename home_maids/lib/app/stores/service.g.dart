// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Service on _Service, Store {
  final _$idAtom = Atom(name: '_Service.id');

  @override
  String get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$nameAtom = Atom(name: '_Service.name');

  @override
  Map<String, String> get name {
    _$nameAtom.context.enforceReadPolicy(_$nameAtom);
    _$nameAtom.reportObserved();
    return super.name;
  }

  @override
  set name(Map<String, String> value) {
    _$nameAtom.context.conditionallyRunInAction(() {
      super.name = value;
      _$nameAtom.reportChanged();
    }, _$nameAtom, name: '${_$nameAtom.name}_set');
  }

  final _$iconUrlAtom = Atom(name: '_Service.iconUrl');

  @override
  String get iconUrl {
    _$iconUrlAtom.context.enforceReadPolicy(_$iconUrlAtom);
    _$iconUrlAtom.reportObserved();
    return super.iconUrl;
  }

  @override
  set iconUrl(String value) {
    _$iconUrlAtom.context.conditionallyRunInAction(() {
      super.iconUrl = value;
      _$iconUrlAtom.reportChanged();
    }, _$iconUrlAtom, name: '${_$iconUrlAtom.name}_set');
  }

  final _$imageUrlAtom = Atom(name: '_Service.imageUrl');

  @override
  String get imageUrl {
    _$imageUrlAtom.context.enforceReadPolicy(_$imageUrlAtom);
    _$imageUrlAtom.reportObserved();
    return super.imageUrl;
  }

  @override
  set imageUrl(String value) {
    _$imageUrlAtom.context.conditionallyRunInAction(() {
      super.imageUrl = value;
      _$imageUrlAtom.reportChanged();
    }, _$imageUrlAtom, name: '${_$imageUrlAtom.name}_set');
  }

  final _$priceAtom = Atom(name: '_Service.price');

  @override
  double get price {
    _$priceAtom.context.enforceReadPolicy(_$priceAtom);
    _$priceAtom.reportObserved();
    return super.price;
  }

  @override
  set price(double value) {
    _$priceAtom.context.conditionallyRunInAction(() {
      super.price = value;
      _$priceAtom.reportChanged();
    }, _$priceAtom, name: '${_$priceAtom.name}_set');
  }

  final _$oldPriceAtom = Atom(name: '_Service.oldPrice');

  @override
  double get oldPrice {
    _$oldPriceAtom.context.enforceReadPolicy(_$oldPriceAtom);
    _$oldPriceAtom.reportObserved();
    return super.oldPrice;
  }

  @override
  set oldPrice(double value) {
    _$oldPriceAtom.context.conditionallyRunInAction(() {
      super.oldPrice = value;
      _$oldPriceAtom.reportChanged();
    }, _$oldPriceAtom, name: '${_$oldPriceAtom.name}_set');
  }

  final _$isEnabledAtom = Atom(name: '_Service.isEnabled');

  @override
  bool get isEnabled {
    _$isEnabledAtom.context.enforceReadPolicy(_$isEnabledAtom);
    _$isEnabledAtom.reportObserved();
    return super.isEnabled;
  }

  @override
  set isEnabled(bool value) {
    _$isEnabledAtom.context.conditionallyRunInAction(() {
      super.isEnabled = value;
      _$isEnabledAtom.reportChanged();
    }, _$isEnabledAtom, name: '${_$isEnabledAtom.name}_set');
  }

  final _$durationAtom = Atom(name: '_Service.duration');

  @override
  Duration get duration {
    _$durationAtom.context.enforceReadPolicy(_$durationAtom);
    _$durationAtom.reportObserved();
    return super.duration;
  }

  @override
  set duration(Duration value) {
    _$durationAtom.context.conditionallyRunInAction(() {
      super.duration = value;
      _$durationAtom.reportChanged();
    }, _$durationAtom, name: '${_$durationAtom.name}_set');
  }

  final _$maidsCountAtom = Atom(name: '_Service.maidsCount');

  @override
  int get maidsCount {
    _$maidsCountAtom.context.enforceReadPolicy(_$maidsCountAtom);
    _$maidsCountAtom.reportObserved();
    return super.maidsCount;
  }

  @override
  set maidsCount(int value) {
    _$maidsCountAtom.context.conditionallyRunInAction(() {
      super.maidsCount = value;
      _$maidsCountAtom.reportChanged();
    }, _$maidsCountAtom, name: '${_$maidsCountAtom.name}_set');
  }

  final _$_ServiceActionController = ActionController(name: '_Service');

  @override
  void setData(
      {String id,
      Map<String, String> name,
      String iconUrl,
      String imageUrl,
      double price,
      double oldPrice,
      bool isEnabled,
      Duration duration,
      int maidsCount}) {
    final _$actionInfo = _$_ServiceActionController.startAction();
    try {
      return super.setData(
          id: id,
          name: name,
          iconUrl: iconUrl,
          imageUrl: imageUrl,
          price: price,
          oldPrice: oldPrice,
          isEnabled: isEnabled,
          duration: duration,
          maidsCount: maidsCount);
    } finally {
      _$_ServiceActionController.endAction(_$actionInfo);
    }
  }
}
