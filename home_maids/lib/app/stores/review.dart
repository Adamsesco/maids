import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';

import 'client.dart';
import 'company.dart';

part 'review.g.dart';

class Review = _Review with _$Review;

abstract class _Review with Store {
  @observable
  String id;

  @observable
  Client author;

  @observable
  Company company;

  @observable
  DateTime date;

  @observable
  double rating;

  @observable
  String content;

  @observable
  bool isNegative;

  @action
  void setData({
    String id,
    Client author,
    Company company,
    DateTime date,
    double rating,
    String content,
    bool isNegative,
  }) {
    if (id != null) this.id = id;
    if (author != null) this.author = author;
    if (company != null) this.company = company;
    if (date != null) this.date = date;
    if (rating != null) this.rating = rating;
    if (content != null) this.content = content;
    if (isNegative != null) this.isNegative = isNegative;
  }

  Map<String, dynamic> toMap() {
    return {
      'author_id': author.id,
      'company_id': company.id,
      'date': Timestamp.fromDate(date),
      'rating': rating,
      'content': content,
      'is_negative': isNegative,
    };
  }
}
