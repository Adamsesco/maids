import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/signup_method.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:mobx/mobx.dart';

part 'client.g.dart';

class Client = _Client with _$Client;

abstract class _Client extends User with Store {
  @override
  UserRole get role => UserRole.client;

  @observable
  @override
  String id;

  @observable
  String firstName;

  @observable
  String lastName;

  @observable
  @override
  String email;

  @observable
  SignupMethod signupMethod;

  @observable
  String phoneNumber;

  @observable
  List<Address> addresses;

  @observable
  String avatarUrl;

  @observable
  DateTime dateCreated;

  @observable
  int completedOrdersCount;

  @observable
  int canceledOrdersCount;

  @observable
  String token;

  @observable
  String fcmToken;

  @action
  void setData({
    String id,
    String firstName,
    String lastName,
    String email,
    SignupMethod signupMethod,
    String phoneNumber,
    List<Address> addresses,
    String avatarUrl,
    DateTime dateCreated,
    int completedOrdersCount,
    int canceledOrdersCount,
    String token,
    String fcmToken,
  }) {
    if (id != null) this.id = id;
    if (firstName != null) this.firstName = firstName;
    if (lastName != null) this.lastName = lastName;
    if (email != null) this.email = email;
    if (signupMethod != null) this.signupMethod = signupMethod;
    if (phoneNumber != null) this.phoneNumber = phoneNumber;
    if (addresses != null) this.addresses = addresses;
    if (avatarUrl != null) this.avatarUrl = avatarUrl;
    if (dateCreated != null) this.dateCreated = dateCreated;
    if (completedOrdersCount != null)
      this.completedOrdersCount = completedOrdersCount;
    if (canceledOrdersCount != null)
      this.canceledOrdersCount = canceledOrdersCount;
    if (token != null) this.token = token;
    if (fcmToken != null) this.fcmToken = fcmToken;
  }

  @action
  void copyFrom(Client other) {
    id = other.id;
    firstName = other.firstName;
    lastName = other.lastName;
    email = other.email;
    signupMethod = other.signupMethod;
    phoneNumber = other.phoneNumber;
    addresses = other.addresses;
    avatarUrl = other.avatarUrl;
    dateCreated = other.dateCreated;
    completedOrdersCount = other.completedOrdersCount;
    canceledOrdersCount = other.canceledOrdersCount;
    token = other.token;
    fcmToken = other.fcmToken;
  }

  @override
  Map<String, dynamic> toMap() {
    String signupMethod;
    switch (this.signupMethod) {
      case SignupMethod.email:
        signupMethod = 'email';
        break;

      case SignupMethod.facebook:
        signupMethod = 'facebook';
        break;

      case SignupMethod.google:
        signupMethod = 'google';
        break;
    }

    return <String, dynamic>{
      'id': id,
      'first_name': firstName,
      'last_name': lastName,
      'email': email,
      'signup_method': signupMethod,
      'phone_number': phoneNumber,
      'addresses': addresses
          .map((address) =>
              {'address': address.address, 'region': address.region})
          .toList(),
      'avatar_url': avatarUrl,
      if (dateCreated != null) 'date_created': dateCreated.toString(),
      'completed_orders': completedOrdersCount,
      'canceled_orders': canceledOrdersCount,
      'token': token,
      'role': 'client',
    };
  }

  @override
  User fromMap(Map<dynamic, dynamic> map) {
    SignupMethod signupMethod;
    switch (map['signup_method']) {
      case 'email':
        signupMethod = SignupMethod.email;
        break;

      case 'google':
        signupMethod = SignupMethod.google;
        break;

      case 'facebook':
        signupMethod = SignupMethod.facebook;
        break;
    }

    List<Address> addresses;
    addresses = map['addresses']
        .map((address) => Address(address['address'], address['region']))
        .toList()
        .cast<Address>();

    final client = Client()
      ..setData(
        id: map['id'],
        firstName: map['first_name'],
        lastName: map['last_name'],
        email: map['email'],
        signupMethod: signupMethod,
        phoneNumber: map['phone_number'],
        addresses: addresses,
        avatarUrl: map['avatar_url'],
        dateCreated: DateTime.parse(map['date_created']),
        completedOrdersCount: map['completed_orders'],
        canceledOrdersCount: map['canceled_orders'],
        token: map['token'],
      );

    return client;
  }
}
