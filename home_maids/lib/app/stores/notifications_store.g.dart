// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notifications_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NotificationsStore on _NotificationsStore, Store {
  final _$unreadCountAtom = Atom(name: '_NotificationsStore.unreadCount');

  @override
  int get unreadCount {
    _$unreadCountAtom.context.enforceReadPolicy(_$unreadCountAtom);
    _$unreadCountAtom.reportObserved();
    return super.unreadCount;
  }

  @override
  set unreadCount(int value) {
    _$unreadCountAtom.context.conditionallyRunInAction(() {
      super.unreadCount = value;
      _$unreadCountAtom.reportChanged();
    }, _$unreadCountAtom, name: '${_$unreadCountAtom.name}_set');
  }

  final _$notificationsAtom = Atom(name: '_NotificationsStore.notifications');

  @override
  List<Notification> get notifications {
    _$notificationsAtom.context.enforceReadPolicy(_$notificationsAtom);
    _$notificationsAtom.reportObserved();
    return super.notifications;
  }

  @override
  set notifications(List<Notification> value) {
    _$notificationsAtom.context.conditionallyRunInAction(() {
      super.notifications = value;
      _$notificationsAtom.reportChanged();
    }, _$notificationsAtom, name: '${_$notificationsAtom.name}_set');
  }

  final _$_NotificationsStoreActionController =
      ActionController(name: '_NotificationsStore');

  @override
  void readOne() {
    final _$actionInfo = _$_NotificationsStoreActionController.startAction();
    try {
      return super.readOne();
    } finally {
      _$_NotificationsStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addNotification(Notification notification) {
    final _$actionInfo = _$_NotificationsStoreActionController.startAction();
    try {
      return super.addNotification(notification);
    } finally {
      _$_NotificationsStoreActionController.endAction(_$actionInfo);
    }
  }
}
