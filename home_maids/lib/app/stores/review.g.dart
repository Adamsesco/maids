// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Review on _Review, Store {
  final _$idAtom = Atom(name: '_Review.id');

  @override
  String get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$authorAtom = Atom(name: '_Review.author');

  @override
  Client get author {
    _$authorAtom.context.enforceReadPolicy(_$authorAtom);
    _$authorAtom.reportObserved();
    return super.author;
  }

  @override
  set author(Client value) {
    _$authorAtom.context.conditionallyRunInAction(() {
      super.author = value;
      _$authorAtom.reportChanged();
    }, _$authorAtom, name: '${_$authorAtom.name}_set');
  }

  final _$companyAtom = Atom(name: '_Review.company');

  @override
  Company get company {
    _$companyAtom.context.enforceReadPolicy(_$companyAtom);
    _$companyAtom.reportObserved();
    return super.company;
  }

  @override
  set company(Company value) {
    _$companyAtom.context.conditionallyRunInAction(() {
      super.company = value;
      _$companyAtom.reportChanged();
    }, _$companyAtom, name: '${_$companyAtom.name}_set');
  }

  final _$dateAtom = Atom(name: '_Review.date');

  @override
  DateTime get date {
    _$dateAtom.context.enforceReadPolicy(_$dateAtom);
    _$dateAtom.reportObserved();
    return super.date;
  }

  @override
  set date(DateTime value) {
    _$dateAtom.context.conditionallyRunInAction(() {
      super.date = value;
      _$dateAtom.reportChanged();
    }, _$dateAtom, name: '${_$dateAtom.name}_set');
  }

  final _$ratingAtom = Atom(name: '_Review.rating');

  @override
  double get rating {
    _$ratingAtom.context.enforceReadPolicy(_$ratingAtom);
    _$ratingAtom.reportObserved();
    return super.rating;
  }

  @override
  set rating(double value) {
    _$ratingAtom.context.conditionallyRunInAction(() {
      super.rating = value;
      _$ratingAtom.reportChanged();
    }, _$ratingAtom, name: '${_$ratingAtom.name}_set');
  }

  final _$contentAtom = Atom(name: '_Review.content');

  @override
  String get content {
    _$contentAtom.context.enforceReadPolicy(_$contentAtom);
    _$contentAtom.reportObserved();
    return super.content;
  }

  @override
  set content(String value) {
    _$contentAtom.context.conditionallyRunInAction(() {
      super.content = value;
      _$contentAtom.reportChanged();
    }, _$contentAtom, name: '${_$contentAtom.name}_set');
  }

  final _$isNegativeAtom = Atom(name: '_Review.isNegative');

  @override
  bool get isNegative {
    _$isNegativeAtom.context.enforceReadPolicy(_$isNegativeAtom);
    _$isNegativeAtom.reportObserved();
    return super.isNegative;
  }

  @override
  set isNegative(bool value) {
    _$isNegativeAtom.context.conditionallyRunInAction(() {
      super.isNegative = value;
      _$isNegativeAtom.reportChanged();
    }, _$isNegativeAtom, name: '${_$isNegativeAtom.name}_set');
  }

  final _$_ReviewActionController = ActionController(name: '_Review');

  @override
  void setData(
      {String id,
      Client author,
      Company company,
      DateTime date,
      double rating,
      String content,
      bool isNegative}) {
    final _$actionInfo = _$_ReviewActionController.startAction();
    try {
      return super.setData(
          id: id,
          author: author,
          company: company,
          date: date,
          rating: rating,
          content: content,
          isNegative: isNegative);
    } finally {
      _$_ReviewActionController.endAction(_$actionInfo);
    }
  }
}
