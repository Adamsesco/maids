import 'dart:ui';

import 'package:hive/hive.dart';
import 'package:mobx/mobx.dart';

part 'application_store.g.dart';

class ApplicationStore = _ApplicationStore with _$ApplicationStore;

abstract class _ApplicationStore with Store {
  @observable
  Locale locale;

  @action
  void setLocale(Locale locale) {
    this.locale = locale;

    final dataDb = Hive.box('data');
    dataDb.put('language', locale.languageCode);
  }
}
