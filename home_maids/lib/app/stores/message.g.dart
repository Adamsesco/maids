// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Message on _Message, Store {
  final _$idAtom = Atom(name: '_Message.id');

  @override
  String get id {
    _$idAtom.context.enforceReadPolicy(_$idAtom);
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.conditionallyRunInAction(() {
      super.id = value;
      _$idAtom.reportChanged();
    }, _$idAtom, name: '${_$idAtom.name}_set');
  }

  final _$senderAtom = Atom(name: '_Message.sender');

  @override
  User get sender {
    _$senderAtom.context.enforceReadPolicy(_$senderAtom);
    _$senderAtom.reportObserved();
    return super.sender;
  }

  @override
  set sender(User value) {
    _$senderAtom.context.conditionallyRunInAction(() {
      super.sender = value;
      _$senderAtom.reportChanged();
    }, _$senderAtom, name: '${_$senderAtom.name}_set');
  }

  final _$recipientAtom = Atom(name: '_Message.recipient');

  @override
  User get recipient {
    _$recipientAtom.context.enforceReadPolicy(_$recipientAtom);
    _$recipientAtom.reportObserved();
    return super.recipient;
  }

  @override
  set recipient(User value) {
    _$recipientAtom.context.conditionallyRunInAction(() {
      super.recipient = value;
      _$recipientAtom.reportChanged();
    }, _$recipientAtom, name: '${_$recipientAtom.name}_set');
  }

  final _$dateAtom = Atom(name: '_Message.date');

  @override
  DateTime get date {
    _$dateAtom.context.enforceReadPolicy(_$dateAtom);
    _$dateAtom.reportObserved();
    return super.date;
  }

  @override
  set date(DateTime value) {
    _$dateAtom.context.conditionallyRunInAction(() {
      super.date = value;
      _$dateAtom.reportChanged();
    }, _$dateAtom, name: '${_$dateAtom.name}_set');
  }

  final _$contentAtom = Atom(name: '_Message.content');

  @override
  String get content {
    _$contentAtom.context.enforceReadPolicy(_$contentAtom);
    _$contentAtom.reportObserved();
    return super.content;
  }

  @override
  set content(String value) {
    _$contentAtom.context.conditionallyRunInAction(() {
      super.content = value;
      _$contentAtom.reportChanged();
    }, _$contentAtom, name: '${_$contentAtom.name}_set');
  }

  final _$isReadAtom = Atom(name: '_Message.isRead');

  @override
  bool get isRead {
    _$isReadAtom.context.enforceReadPolicy(_$isReadAtom);
    _$isReadAtom.reportObserved();
    return super.isRead;
  }

  @override
  set isRead(bool value) {
    _$isReadAtom.context.conditionallyRunInAction(() {
      super.isRead = value;
      _$isReadAtom.reportChanged();
    }, _$isReadAtom, name: '${_$isReadAtom.name}_set');
  }

  final _$isArchivedAtom = Atom(name: '_Message.isArchived');

  @override
  bool get isArchived {
    _$isArchivedAtom.context.enforceReadPolicy(_$isArchivedAtom);
    _$isArchivedAtom.reportObserved();
    return super.isArchived;
  }

  @override
  set isArchived(bool value) {
    _$isArchivedAtom.context.conditionallyRunInAction(() {
      super.isArchived = value;
      _$isArchivedAtom.reportChanged();
    }, _$isArchivedAtom, name: '${_$isArchivedAtom.name}_set');
  }

  final _$_MessageActionController = ActionController(name: '_Message');

  @override
  void setData(
      {String id,
      User sender,
      User recipient,
      DateTime date,
      String content,
      bool isRead,
      bool isArchived}) {
    final _$actionInfo = _$_MessageActionController.startAction();
    try {
      return super.setData(
          id: id,
          sender: sender,
          recipient: recipient,
          date: date,
          content: content,
          isRead: isRead,
          isArchived: isArchived);
    } finally {
      _$_MessageActionController.endAction(_$actionInfo);
    }
  }
}
