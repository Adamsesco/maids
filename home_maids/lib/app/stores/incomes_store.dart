import 'package:home_maids/app/types/income.dart';
import 'package:mobx/mobx.dart';

part 'incomes_store.g.dart';

class IncomesStore = _IncomesStore with _$IncomesStore;

abstract class _IncomesStore with Store {
  @observable
  Future<List<Income>> monthlyIncomes;

  @observable
  Future<List<Income>> yearlyIncomes;
}