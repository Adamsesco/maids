import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/service.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/order_status.dart';
import 'package:home_maids/app/types/payment_method.dart';
import 'package:home_maids/app/types/service_data.dart';
import 'package:home_maids/app/utils/order_utils.dart';
import 'package:mobx/mobx.dart';

part 'order.g.dart';

class Order = _Order with _$Order;

abstract class _Order with Store {
  @observable
  bool isNew = false;

  @observable
  String id;

  @observable
  int number;

  @observable
  Client client;

  @observable
  Company company;

  @observable
  Address address;

  @observable
  List<Service> services;

  @observable
  List<ServiceData> servicesData;

  @observable
  Duration duration;

  @observable
  int maidsCount;

  @observable
  bool requiresMaterials;

  @observable
  DateTime date;

  @observable
  double price;

  @observable
  DateTime dateCreated;

  @observable
  OrderStatus status;

  @observable
  PaymentMethod paymentMethod;

  @action
  void setData({
    bool isNew,
    String id,
    int number,
    Client client,
    Company company,
    Address address,
    List<Service> services,
    List<ServiceData> servicesData,
    Duration duration,
    int maidsCount,
    bool requiresMaterials,
    DateTime date,
    double price,
    DateTime dateCreated,
    OrderStatus status,
    PaymentMethod paymentMethod,
  }) {
    if (isNew != null) this.isNew = isNew;
    if (id != null) this.id = id;
    if (number != null) this.number = number;
    if (client != null) this.client = client;
    if (company != null) this.company = company;
    if (address != null) this.address = address;
    if (services != null) this.services = services;
    if (servicesData != null) this.servicesData = servicesData;
    if (duration != null) this.duration = duration;
    if (maidsCount != null) this.maidsCount = maidsCount;
    if (requiresMaterials != null) this.requiresMaterials = requiresMaterials;
    if (date != null) this.date = date;
    if (price != null) this.price = price;
    if (dateCreated != null) this.dateCreated = dateCreated;
    if (status != null) this.status = status;
    if (paymentMethod != null) this.paymentMethod = paymentMethod;
  }

  Map<String, dynamic> toMap() {
    List<String> servicesIds;
    if (this.services != null) {
      servicesIds = this.services.map((service) => service.id).toList();
    }

    final orderStatus = orderStatusCode(this.status);

    String paymentMethod;
    if (this.paymentMethod != null) {
      switch (this.paymentMethod) {
        case PaymentMethod.cash:
          paymentMethod = 'cash';
          break;
        case PaymentMethod.visa:
          paymentMethod = 'visa';
          break;
        case PaymentMethod.paypal:
          paymentMethod = 'paypal';
          break;
      }
    }

    return <String, dynamic>{
      'client_id': client.id,
      'company_id': company.id,
      if (address != null) 'address': address.toMap(),
      if (servicesIds != null) 'services': servicesIds,
      'duration': duration.inMinutes,
      'maids_count': maidsCount,
      'requires_materials': requiresMaterials,
      if (date != null) 'date': Timestamp.fromDate(date),
      'price': price,
      if (dateCreated != null) 'date_created': Timestamp.fromDate(dateCreated),
      if (orderStatus != null) 'status': orderStatus,
      if (paymentMethod != null) 'payment_method': paymentMethod,
    };
  }
}
