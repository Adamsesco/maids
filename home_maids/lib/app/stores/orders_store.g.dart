// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orders_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$OrdersStore on _OrdersStore, Store {
  final _$ordersAtom = Atom(name: '_OrdersStore.orders');

  @override
  Future<List<Order>> get orders {
    _$ordersAtom.context.enforceReadPolicy(_$ordersAtom);
    _$ordersAtom.reportObserved();
    return super.orders;
  }

  @override
  set orders(Future<List<Order>> value) {
    _$ordersAtom.context.conditionallyRunInAction(() {
      super.orders = value;
      _$ordersAtom.reportChanged();
    }, _$ordersAtom, name: '${_$ordersAtom.name}_set');
  }

  final _$_OrdersStoreActionController = ActionController(name: '_OrdersStore');

  @override
  void setOrders(Future<List<Order>> orders) {
    final _$actionInfo = _$_OrdersStoreActionController.startAction();
    try {
      return super.setOrders(orders);
    } finally {
      _$_OrdersStoreActionController.endAction(_$actionInfo);
    }
  }
}
