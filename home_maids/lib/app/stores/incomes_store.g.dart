// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incomes_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$IncomesStore on _IncomesStore, Store {
  final _$monthlyIncomesAtom = Atom(name: '_IncomesStore.monthlyIncomes');

  @override
  Future<List<Income>> get monthlyIncomes {
    _$monthlyIncomesAtom.context.enforceReadPolicy(_$monthlyIncomesAtom);
    _$monthlyIncomesAtom.reportObserved();
    return super.monthlyIncomes;
  }

  @override
  set monthlyIncomes(Future<List<Income>> value) {
    _$monthlyIncomesAtom.context.conditionallyRunInAction(() {
      super.monthlyIncomes = value;
      _$monthlyIncomesAtom.reportChanged();
    }, _$monthlyIncomesAtom, name: '${_$monthlyIncomesAtom.name}_set');
  }

  final _$yearlyIncomesAtom = Atom(name: '_IncomesStore.yearlyIncomes');

  @override
  Future<List<Income>> get yearlyIncomes {
    _$yearlyIncomesAtom.context.enforceReadPolicy(_$yearlyIncomesAtom);
    _$yearlyIncomesAtom.reportObserved();
    return super.yearlyIncomes;
  }

  @override
  set yearlyIncomes(Future<List<Income>> value) {
    _$yearlyIncomesAtom.context.conditionallyRunInAction(() {
      super.yearlyIncomes = value;
      _$yearlyIncomesAtom.reportChanged();
    }, _$yearlyIncomesAtom, name: '${_$yearlyIncomesAtom.name}_set');
  }
}
