import 'package:mobx/mobx.dart';

part 'service.g.dart';

class Service = _Service with _$Service;

abstract class _Service with Store {
  @observable
  String id;

  @observable
  Map<String, String> name;

  @observable
  String iconUrl;

  @observable
  String imageUrl;

  @observable
  double price;

  @observable
  double oldPrice;

  @observable
  bool isEnabled = false;

  @observable
  Duration duration;

  @observable
  int maidsCount;

  @action
  void setData({
    String id,
    Map<String, String> name,
    String iconUrl,
    String imageUrl,
    double price,
    double oldPrice,
    bool isEnabled,
    Duration duration,
    int maidsCount,
  }) {
    if (id != null) this.id = id;
    if (name != null) this.name = name;
    if (iconUrl != null) this.iconUrl = iconUrl;
    if (imageUrl != null) this.imageUrl = imageUrl;
    if (price != null) this.price = price;
    if (oldPrice != null) this.oldPrice = oldPrice;
    if (isEnabled != null) this.isEnabled = isEnabled;
    if (duration != null) this.duration = duration;
    if (maidsCount != null) this.maidsCount = maidsCount;
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
    };
  }
}
