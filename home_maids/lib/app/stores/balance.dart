import 'package:mobx/mobx.dart';

part 'balance.g.dart';

class Balance = _Balance with _$Balance;

abstract class _Balance with Store {
  @observable
  String id;

  @observable
  double value;

  @action
  void setData({String id, double value}) {
    if (id != null) this.id = id;
    if (value != null) this.value = value;
  }
}
