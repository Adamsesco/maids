import 'package:home_maids/app/stores/order.dart';
import 'package:mobx/mobx.dart';

part 'new_order_store.g.dart';

class NewOrderStore = _NewOrderStore with _$NewOrderStore;

abstract class _NewOrderStore with Store {
  @observable
  Order order;

  @action
  void setOrder(Order order) {
    this.order = order;
  }
}
