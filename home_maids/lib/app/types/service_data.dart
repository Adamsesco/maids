import 'package:home_maids/app/stores/service.dart';

class ServiceData {
  Service service;
  bool selected;
  int hours;
  int maidsCount;
  bool requiresMaterials;

  ServiceData({
    this.service,
    this.selected,
    this.hours,
    this.maidsCount,
    this.requiresMaterials,
  });
}
