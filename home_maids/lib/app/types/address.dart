class Address {
  final String address;
  final String region;

  Address(this.address, this.region);

  Map<String, String> toMap() {
    return <String, String>{
      'address': address,
      'region': region,
    };
  }
}
