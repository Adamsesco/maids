import 'package:home_maids/app/types/declining_reasons.dart';

class DecliningReason {
  final DecliningReasons reason;
  final String details;

  DecliningReason(this.reason, {this.details});
}
