enum NotificationType {
  orderCreated,
  orderCanceled,
  orderReminder,
}
