import 'package:home_maids/app/types/signup_method.dart';
import 'package:home_maids/app/types/user_role.dart';

abstract class User {
  UserRole get role => UserRole.anonymous;
  String id;
  String email;
  SignupMethod signupMethod;

  Map<String, dynamic> toMap();
  User fromMap(Map<dynamic, dynamic> map);
}
