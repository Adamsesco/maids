enum OrderStatus {
  all,
  pending,
  inProgress,
  delayed,
  done,
  confirmed,
  cancelled,
  declined,
  unpaid,
  complete,
}
