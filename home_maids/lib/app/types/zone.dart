import 'package:home_maids/app/types/location.dart';

class Zone {
  final String id;
  final Map<String, String> name;
  final Location location;

  Zone({this.id, this.name, this.location});
}

class City {
  final String id;
  final Map<String, String> name;
  final List<Zone> zones;

  City({this.id, this.name, this.zones});
}
