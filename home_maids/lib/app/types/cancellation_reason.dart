import 'package:home_maids/app/types/cancellation_reasons.dart';

class CancellationReason {
  final CancellationReasons reason;

  CancellationReason(this.reason);
}
