class Income {
  final DateTime date;
  final double amount;
  final String label;

  Income(this.date, this.amount, {this.label = ''});
}
