import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/signup_method.dart';

class SignupData {
  String firstName;
  String lastName;
  String email;
  String phoneNumber;
  String password;
  Address address;
  String idToken;
  SignupMethod signupMethod;

  String toString() {
    return "First name: $firstName\nLast name: $lastName\nEmail: $email\nPhone number: $phoneNumber\nPassword: $password\nAddress: ${address?.address}";
  }
}
