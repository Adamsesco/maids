import 'package:home_maids/app/types/payment_method.dart';

String paymentMethodToString(PaymentMethod pm) {
  switch (pm) {
    case PaymentMethod.cash:
      return 'CASH';
    case PaymentMethod.visa:
      return 'VISA';
    case PaymentMethod.paypal:
      return 'PAYPAL';
  }

  return '';
}
