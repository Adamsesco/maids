import 'package:flutter/material.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/types/order_status.dart';

Color getOrderColor(OrderStatus orderStatus) {
  assert(orderStatus != null);

  switch (orderStatus) {
    case OrderStatus.all:
      return Colors.black;
    case OrderStatus.pending:
      return Palette.cornflowerBlue;
    case OrderStatus.inProgress:
      return Palette.royalBlue;
    case OrderStatus.done:
      return Palette.malachite;
    case OrderStatus.confirmed:
      return Palette.malachite;
    case OrderStatus.cancelled:
      return Palette.cadetBlue;
    case OrderStatus.delayed:
      return Colors.red;
    case OrderStatus.declined:
      return Palette.mercury;
    case OrderStatus.unpaid:
      return Palette.amber;
      break;
    case OrderStatus.complete:
      return Palette.malachite2;
      break;
  }

  return Palette.royalBlue;
}

OrderStatus getOrderStatus(int statusCode) {
  switch (statusCode) {
    case 0:
      return OrderStatus.pending;

    case 1:
      return OrderStatus.delayed;

    case 2:
      return OrderStatus.inProgress;

    case 3:
      return OrderStatus.done;

    case 4:
      return OrderStatus.confirmed;

    case 5:
      return OrderStatus.cancelled;

    case 6:
      return OrderStatus.declined;

    case 7:
      return OrderStatus.unpaid;

    case 8:
      return OrderStatus.complete;
  }

  return OrderStatus.pending;
}

int orderStatusCode(OrderStatus status) {
  switch (status) {
    case OrderStatus.all:
      return null;
    case OrderStatus.pending:
      return 0;
    case OrderStatus.delayed:
      return 1;
    case OrderStatus.inProgress:
      return 2;
    case OrderStatus.done:
      return 3;
    case OrderStatus.confirmed:
      return 4;
    case OrderStatus.cancelled:
      return 5;
    case OrderStatus.declined:
      return 6;
    case OrderStatus.unpaid:
      return 7;
      break;
    case OrderStatus.complete:
      return 8;
      break;
  }

  return 0;
}
