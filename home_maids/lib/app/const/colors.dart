import 'package:flutter/painting.dart';

class Palette {
  static const brightTurquoise = const Color(0xFF00FDDD);
  static const athensGray = const Color(0xFFF5F7F9);
  static const alto = const Color(0xFFD1D1D1);
  static const supernova = const Color(0xFFFFCF07);
  static const cornflowerBlue = const Color(0xFF8F51F4);
  static const blackHaze = const Color(0xFFF6F7F7);
  static const malachite = const Color(0xFF00D329);
  static const cadetBlue = const Color(0xFFB4B9C5);
  static const fiord = const Color(0xFF454D66);
  static const alabaster = const Color(0xFFF7F7F7);
  static const red = const Color(0xFFFF0000);
  static const royalBlue = const Color(0xFF724EE8);
  static const azureRadiance = const Color(0xFF1777F1);
  static const cornflowerBlue2 = const Color(0xFF62A0EF);
  static const periwinkleGray = const Color(0xFFD2D9EA);
  static const heliotrope = const Color(0xFFAA55FF);
  static const royalBlue2 = const Color(0xFF584BDD);
  static const tundora = const Color(0xFF464646);
  static const slateGrey = const Color(0xFF6C7B8A);
  static const heliotrope2 = const Color(0xFFB98FFD);
  static const trendyPink = const Color(0xFF785480);
  static const malachite2 = const Color(0xFF09D621);
  static const mercury = const Color(0xFFE8E8E8);
  static const grape = const Color(0xFF4C1F5C);
  static const orangePeel = const Color(0xFFFF9D00);
  static const salem = const Color(0xFF059F34);
  static const silver = const Color(0xFFC1C1C1);
  static const mercury2 = const Color(0xFFE9E9E9);
  static const blackSqueeze = const Color(0xFFF0F4F9);
  static const cornflowerBlue3 = const Color(0xFF8250EF);
  static const amber = const Color(0xFFFFC400);
  static const selago = const Color(0xFFF2F6FE);
  static const perfume = const Color(0xFFD8C5F7);
}
