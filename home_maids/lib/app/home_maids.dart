import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hive/hive.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/language_screen/language_screen.dart';
import 'package:home_maids/app/screens/login_screen/login_screen.dart';
import 'package:home_maids/app/services/users_service.dart';
import 'package:home_maids/app/stores/application_store.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/incomes_store.dart';
import 'package:home_maids/app/stores/messages_store.dart';
import 'package:home_maids/app/stores/new_order_store.dart';
import 'package:home_maids/app/stores/notifications_store.dart';
import 'package:home_maids/app/stores/notification.dart' as notif;
import 'package:home_maids/app/stores/orders_store.dart';
import 'package:home_maids/app/types/notification_status.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:home_maids/app/widgets/rounded_rectangle_slider_thumb_shape.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class HomeMaids extends StatefulWidget {
  @override
  _HomeMaidsState createState() => _HomeMaidsState();
}

class _HomeMaidsState extends State<HomeMaids> {
  StreamController<ThemeData> _themeController = StreamController();
  Locale _previousLocale;
  Box<Map<dynamic, dynamic>> _notificationsDb;
  bool _builtTheme = false;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  ApplicationStore _app;
  Company _company;
  Client _client;
  OrdersStore _orders;
  NotificationsStore _notifications;
  MessagesStore _messages;
  NewOrderStore _newOrder;
  IncomesStore _incomes;
  Completer _databaseCompleter = Completer();
  Future<Widget> _nextScreen;

  @override
  void initState() {
    super.initState();

    _initDatabase();

    _app = ApplicationStore();
    _company = Company();
    _client = Client();
    _orders = OrdersStore();
    _notifications = NotificationsStore();
    _messages = MessagesStore();
    _newOrder = NewOrderStore();
    _incomes = IncomesStore();
    _nextScreen = _getNextScreen();

    _firebaseCloudMessagingListeners();
  }

  @override
  void dispose() {
    _themeController.close();
    super.dispose();
  }

  Future<Widget> _getNextScreen() async {
    await _databaseCompleter.future;

    final dataDb = await Hive.openBox('data');
    final launchTimes = dataDb.get('launch_times') as int ?? 0;
    dataDb.put('launch_times', launchTimes + 1);

    if (launchTimes == 0) {
      return LanguageScreen();
    }

    final language = dataDb.get('language');
    if (language == null) {
      return LanguageScreen();
    }
    _app.setLocale(Locale(language));
    if (mounted) {
      setState(() {});
    }

    return await _signinUserFromCache();
  }

  Future<void> _initDatabase() async {
    final dir = await getApplicationDocumentsDirectory();
    Hive.init("${dir.path}/database");
    _databaseCompleter.complete();
  }

  Future<Widget> _signinUserFromCache() async {
    await _databaseCompleter.future;

    final usersService = UsersService();
    final cachedUser = await usersService.getFromCache();
    if (cachedUser != null) {
      String token;
      switch (cachedUser.role) {
        case UserRole.anonymous:
          // TODO: Handle this case.
          break;
        case UserRole.client:
          final client = cachedUser as Client;
          token = client.token;
          break;
        case UserRole.company:
          final company = cachedUser as Company;
          token = company.token;
          break;
      }
      final user = await usersService.getByToken(token);
      final screen = usersService.performLogin(
          user ?? cachedUser, _company, _client, _orders, _messages, _incomes);
      return screen;
    }

    return LoginScreen();
  }

  Future<void> _initNotificationsDb() async {
    await _databaseCompleter.future;

    _notificationsDb = await Hive.openBox('notifications');

    _notificationsDb.keys.forEach((key) {
      final data = _notificationsDb.get(key);
      final notification = notif.Notification();
      notification.setData(
        id: DateTime.now().toString(),
        title: data['notification']['title'],
        body: data['notification']['body'],
        date: data['data']['date'] == null
            ? DateTime.now()
            : DateTime.parse(data['data']['date']),
        firstName: data['data']['first_name'],
        lastName: data['data']['last_name'],
        avatarUrl: data['data']['avatar'] ??
            'https://ca.slack-edge.com/THK6F225U-UH91W88AD-g63de2e09c56-72',
        status: NotificationStatus.unread,
      );

      print(notification);

      if (notification.status == NotificationStatus.read) {
        _notificationsDb.delete(key);
      } else {
        _notifications.addNotification(notification);
      }
    });
  }

  Future<void> _addNotification(Map<String, dynamic> data) async {
    final notification = notif.Notification();
    notification.setData(
      id: DateTime.now().toString(),
      title: data['notification']['title'],
      body: data['notification']['body'],
      date: data['data']['date'] == null
          ? DateTime.now()
          : DateTime.parse(data['data']['date']),
      firstName: data['data']['first_name'],
      lastName: data['data']['last_name'],
      avatarUrl: data['data']['avatar'] ??
          'https://ca.slack-edge.com/THK6F225U-UH91W88AD-g63de2e09c56-72',
      status: NotificationStatus.unread,
    );
    _notifications.addNotification(notification);
    await _notificationsDb.put(DateTime.now().toString(), data);
  }

  void _firebaseCloudMessagingListeners() async {
    if (Platform.isIOS) _iOSPermission();

    final fcmTokenFuture = _firebaseMessaging.getToken();
    _company.fcmToken = fcmTokenFuture;

    await _initNotificationsDb();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        await _addNotification(message);
      },
      onResume: (Map<String, dynamic> message) async {
        await _addNotification(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        await _addNotification(message);
      },
    );
  }

  void _iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<ApplicationStore>(
          create: (_) => _app,
        ),
        Provider<Company>(
          create: (_) => _company,
        ),
        Provider<Client>(
          create: (_) => _client,
        ),
        Provider<OrdersStore>(
          create: (_) => _orders,
        ),
        Provider<NotificationsStore>(
          create: (_) => _notifications,
        ),
        Provider<MessagesStore>(
          create: (_) => _messages,
        ),
        Provider<NewOrderStore>(
          create: (_) => _newOrder,
        ),
        Provider<IncomesStore>(
          create: (_) => _incomes,
        ),
      ],
      child: Observer(builder: (context) {
        return StreamBuilder<ThemeData>(
          stream: _themeController.stream,
          builder: (context, snapshot) {
            return MaterialApp(
              title: 'TrustMaids',
              debugShowCheckedModeBanner: false,
              theme: snapshot.data,
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              locale: _app.locale,
              home: Builder(
                builder: (BuildContext context) {
                  ScreenUtil.instance = ScreenUtil(
                      width: 375, height: 812, allowFontScaling: true)
                    ..init(context);

                  if (!_builtTheme) {
                    _themeController.add(_buildThemeData(context));
                    _builtTheme = true;
                  }

                  final locale = _app.locale;
                  if (locale != _previousLocale) {
                    print("Set locale");
                    _previousLocale = locale;
                    _themeController.add(_buildThemeData(context));
                  }

                  return FutureBuilder<Widget>(
                    future: _nextScreen,
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                        case ConnectionState.waiting:
                        case ConnectionState.active:
                          return Container(
                            color: Colors.white,
                            alignment: Alignment.center,
                            child: CupertinoActivityIndicator(),
                          );
                        case ConnectionState.done:
                          if (snapshot.hasError || snapshot.data == null) {
                            return LoginScreen();
                          }

                          final nextScreen = snapshot.data;
                          return nextScreen;
                      }

                      return Container();
                    },
                  );
                },
              ),
            );
          },
        );
      }),
    );
  }

  ThemeData _buildThemeData(BuildContext context) {
    print("Language code: ${_app?.locale?.languageCode}");
    final fontFamily =
        _app?.locale?.languageCode == 'ar' ? 'Droid Arabic Kufi' : 'SF UI Text';

    return ThemeData(
      primaryColor: Palette.royalBlue,
      backgroundColor: Colors.white,
      textTheme: TextTheme(
        button: TextStyle(
          fontFamily: fontFamily,
          fontSize: ScreenUtil().setSp(13),
          fontWeight: FontWeight.bold,
        ),
        headline: TextStyle(
          fontFamily: fontFamily,
          fontSize: ScreenUtil().setSp(19),
          fontWeight: FontWeight.w800,
          color: Colors.white.withOpacity(0.99),
        ),
        subtitle: TextStyle(
          fontFamily: fontFamily,
          fontSize: ScreenUtil().setSp(17),
          fontWeight: FontWeight.w800,
          letterSpacing: -0.30,
          color: Palette.royalBlue,
        ),
        display1: TextStyle(
          fontFamily: fontFamily,
          fontSize: ScreenUtil().setSp(12),
          fontWeight: FontWeight.w500,
          color: Palette.tundora,
        ),
        display3: TextStyle(
          fontFamily: fontFamily,
          fontSize: ScreenUtil().setSp(12),
          fontWeight: FontWeight.w500,
          letterSpacing: 0.52,
          color: Palette.royalBlue,
        ),
        display4: TextStyle(
          fontFamily: fontFamily,
          fontSize: ScreenUtil().setSp(15),
          fontWeight: FontWeight.bold,
          letterSpacing: 1.10,
          color: Colors.black,
        ),
      ),
      appBarTheme: AppBarTheme(
        color: Palette.royalBlue,
        elevation: 0,
      ),
      sliderTheme: SliderThemeData(
        activeTrackColor: Palette.royalBlue,
        inactiveTrackColor: Palette.mercury2,
        activeTickMarkColor: Colors.transparent,
        inactiveTickMarkColor: Colors.transparent,
        trackHeight: ScreenUtil().setHeight(6),
        thumbColor: Palette.cornflowerBlue,
        thumbShape: RoundedRectangleSliderThumbShape(),
      ),
    );
  }
}
