import 'package:dio/dio.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/company_profile_screen/company_profile_screen.dart';
import 'package:home_maids/app/screens/security_screen/security_screen.dart';
import 'package:home_maids/app/screens/services_screen/services_screen.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  Company _company;
  bool _isAvailable;

  @override
  void initState() {
    super.initState();

    _company = Provider.of<Company>(context, listen: false);
    _isAvailable = _company.isAvailable;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        leading: Container(),
        centerTitle: true,
        title: Text(
          S.of(context).SETTINGS,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      bottomNavigationBar: CompanyNavigationBar(currentIndex: 2),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(
          top: ScreenUtil().setHeight(28),
        ),
        child: Column(
          children: <Widget>[
            _SettingItem(
              text: S.of(context).Profile,
              screen: CompanyProfileScreen(),
            ),
            SizedBox(height: ScreenUtil().setHeight(3)),
            _SettingItem(
              text: S.of(context).Service_pricing,
              screen: ServicesScreen(),
            ),
            SizedBox(height: ScreenUtil().setHeight(3)),
            _SettingItem(
              text: S.of(context).Security,
              screen: SecurityScreen(),
            ),
            SizedBox(height: ScreenUtil().setHeight(196)),
            SizedBox(
              width: ScreenUtil().setWidth(280.73),
              child: Row(
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/images/noun_Alert_18418.png'),
                    height: ScreenUtil().setHeight(55),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(23.1)),
                  Expanded(
                    child: Text(
                      S.of(context).Please_make_sure_to_disable_your_availability,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(9.6),
                          ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(53)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  S.of(context).Availability,
                  style: Theme.of(context).textTheme.subtitle.copyWith(
                        fontSize: ScreenUtil().setSp(14),
                        letterSpacing: 0,
                      ),
                ),
                SizedBox(width: ScreenUtil().setWidth(21.4)),
                Transform.scale(
                  scale: 0.8,
                  child: CupertinoSwitch(
                    value: _isAvailable,
                    onChanged: (bool value) {
                      setState(() {
                        _isAvailable = value;
                        _company.setData(isAvailable: _isAvailable);

                        final availabilityInt = _isAvailable ? 0 : 1;

                        final formData = FormData.fromMap({
                          'token': _company.token,
                          'availability': availabilityInt,
                        });
                        Dio().post(
                            'http://washy-car.com/homemaids/api/availability',
                            data: formData);
                      });
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _SettingItem extends StatelessWidget {
  final String text;
  final Widget screen;

  _SettingItem({@required this.text, @required this.screen});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) =>
                this.screen,
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return child;
            },
          ),
        );
      },
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      child: Container(
        color: Palette.blackHaze,
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(52),
          vertical: ScreenUtil().setHeight(18),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              text,
              style: Theme.of(context).textTheme.subtitle.copyWith(
                    fontWeight: FontWeight.w300,
                    letterSpacing: 0,
                    color: Palette.fiord,
                  ),
            ),
            RotatedBox(
              quarterTurns: 2,
              child: Image(
                image: AssetImage('assets/images/noun_Arrow_2335767.png'),
                height: ScreenUtil().setHeight(13.4),
                color: Palette.fiord,
                matchTextDirection: true,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
