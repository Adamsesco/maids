import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/login_screen/login_screen.dart';
import 'package:home_maids/generated/i18n.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  PageController _pageController = PageController();
  int _page = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Spacer(),
          _ActionButton(isLastPage: _page == 2),
          Spacer(),
          Expanded(
            flex: 14,
            child: PageView(
              controller: _pageController,
              onPageChanged: (int page) {
                if (_page != page) {
                  setState(() {
                    _page = page;
                  });
                }
              },
              children: <Widget>[
                _BestCompaniesPage(),
                _TrustWorthyPage(),
                _ConvenientPage(),
              ],
            ),
          ),
          Spacer(),
          _Indicators(
            count: 3,
            current: _page,
            onTap: (int index) {
              setState(() {
                _page = index;
              });
              _pageController.animateToPage(index,
                  duration: Duration(milliseconds: 200), curve: Curves.linear);
            },
          ),
          Spacer(),
        ],
      ),
    );
  }
}

class _ActionButton extends StatelessWidget {
  final bool isLastPage;

  _ActionButton({this.isLastPage = false}) : super();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => LoginScreen(),
              ),
            );
          },
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(41),
              vertical: ScreenUtil().setHeight(6),
            ),
            child: Text(
              isLastPage ? S.of(context).GET_STARTED : S.of(context).SKIP,
              style: TextStyle(
                fontFamily: 'SF Compact Display',
                fontSize: ScreenUtil().setSp(16),
                fontWeight: FontWeight.bold,
                letterSpacing: 0,
                color: Palette.royalBlue,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

typedef void _IndicatorCallback(int index);

class _Indicators extends StatelessWidget {
  final int count;
  final int current;
  final _IndicatorCallback onTap;

  _Indicators({this.count = 0, this.current = 0, this.onTap}) : super();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        for (var i = 0; i < count; i++) ...[
          InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              if (onTap != null) {
                onTap(i);
              }
            },
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: ScreenUtil().setHeight(6),
              ),
              child: _PageIndicator(isCurrent: i == current),
            ),
          ),
          if (i < count - 1) SizedBox(width: ScreenUtil().setWidth(10)),
        ],
      ],
    );
  }
}

class _PageIndicator extends StatelessWidget {
  final bool isCurrent;

  _PageIndicator({this.isCurrent = false}) : super();

  @override
  Widget build(BuildContext context) {
    final defaultWidth = ScreenUtil().setWidth(19);
    final currentWidth = ScreenUtil().setWidth(39);

    final defaultColor = Palette.perfume;
    final currentColor = Palette.cornflowerBlue;

    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      width: isCurrent ? currentWidth : defaultWidth,
      height: ScreenUtil().setHeight(10),
      decoration: BoxDecoration(
        color: isCurrent ? currentColor : defaultColor,
        borderRadius: BorderRadius.circular(7),
      ),
    );
  }
}

class _BestCompaniesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/best-companies.png'),
            width: ScreenUtil().setWidth(336.03),
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(
              start: ScreenUtil().setWidth(51),
              end: ScreenUtil().setWidth(33),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  S.of(context).Best_companies,
                  style: TextStyle(
                    fontFamily: 'SF Compact Rounded',
                    fontSize: ScreenUtil().setSp(34),
                    fontWeight: FontWeight.bold,
                    color: Palette.fiord,
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(23)),
                RichText(
                  text: TextSpan(
                    style: TextStyle(
                      fontFamily: 'SF Pro Display',
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.w300,
                      color: Palette.fiord,
                    ),
                    text: S
                        .of(context)
                        .We_make_sure_to_thoroughly_study_the_demand,
                    children: [
                      TextSpan(
                        style: TextStyle(fontWeight: FontWeight.bold),
                        text: S.of(context).TrustMaids,
                      ),
                      TextSpan(
                        text: S
                            .of(context)
                            .which_must_be_the_best_and_most_efficient_in_its_region,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _TrustWorthyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/trustworthy.png'),
            width: ScreenUtil().setWidth(319.57),
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(
              start: ScreenUtil().setWidth(51),
              end: ScreenUtil().setWidth(33),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  S.of(context).Trustworthy,
                  style: TextStyle(
                    fontFamily: 'SF Compact Rounded',
                    fontSize: ScreenUtil().setSp(34),
                    fontWeight: FontWeight.bold,
                    color: Palette.fiord,
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(23)),
                RichText(
                  text: TextSpan(
                    style: TextStyle(
                      fontFamily: 'SF Pro Display',
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.w300,
                      color: Palette.fiord,
                    ),
                    text: S.of(context).We_all_know_when_it_comes_to_trust,
                    children: [
                      TextSpan(
                        style: TextStyle(fontWeight: FontWeight.bold),
                        text: S.of(context).TrustMaids,
                      ),
                      TextSpan(
                        text: S.of(context).we_have_the_most_trustworthy_maids,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _ConvenientPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/convenient.png'),
            width: ScreenUtil().setWidth(312),
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(
              start: ScreenUtil().setWidth(51),
              end: ScreenUtil().setWidth(33),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  S.of(context).It_is_convenient,
                  style: TextStyle(
                    fontFamily: 'SF Compact Rounded',
                    fontSize: ScreenUtil().setSp(34),
                    fontWeight: FontWeight.bold,
                    color: Palette.fiord,
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(23)),
                RichText(
                  text: TextSpan(
                    style: TextStyle(
                      fontFamily: 'SF Pro Display',
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.w300,
                      color: Palette.fiord,
                    ),
                    text:
                        S.of(context).Choose_the_service_that_suits_your_budget,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
