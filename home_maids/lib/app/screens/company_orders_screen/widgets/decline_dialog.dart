import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/order.dart';
import 'package:home_maids/app/types/order_status.dart';
import 'package:home_maids/app/widgets/hmbutton/hmbutton.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class DeclineDialog extends StatefulWidget {
  final Order order;

  DeclineDialog({@required this.order, Key key}) : super(key: key);

  @override
  _DeclineDialogState createState() => _DeclineDialogState();
}

class _DeclineDialogState extends State<DeclineDialog> {
  Company _company;
  bool _isViolationSelected = false;
  String _reason;
  bool _isLoading = false;
  bool _isUpdated = false;

  @override
  void initState() {
    super.initState();

    _company = Provider.of<Company>(context, listen: false);
  }

  Future<void> _submit() async {
    setState(() {
      _isLoading = true;
    });

    final formData = FormData.fromMap({
      'token': _company.token,
      'order_id': int.tryParse(widget.order.id),
      'decline_reason': _isViolationSelected ? 1 : 0,
      'decline_message': _reason,
    });

    final request = await Dio().post<String>(
        'http://washy-car.com/homemaids//api/orders/decline',
        data: formData);

    widget.order.status = OrderStatus.declined;

    setState(() {
      _isLoading = false;
      _isUpdated = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 20, sigmaY: 20),
              child: Container(
                color: Colors.white.withOpacity(0.2),
              ),
            ),
          ),
          Positioned.fill(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(161)),
                  Container(
                    width: ScreenUtil().setWidth(329),
                    padding: EdgeInsetsDirectional.only(
                      start: ScreenUtil().setWidth(26),
                      end: ScreenUtil().setWidth(26),
                      top: ScreenUtil().setHeight(22.0 - 10.0),
                      bottom: ScreenUtil().setHeight(25),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(19),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.16),
                          blurRadius: 40,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: AlignmentDirectional.topEnd,
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Image(
                                image:
                                    AssetImage('assets/images/close-icon.png'),
                              ),
                            ),
                          ),
                        ),
                        Text(
                          S.of(context).DECLINE_REASON,
                          style: Theme.of(context).textTheme.subtitle.copyWith(
                                fontSize: ScreenUtil().setSp(17),
                                fontWeight: FontWeight.w800,
                                letterSpacing: -0.30,
                              ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(21)),
                        InkWell(
                          onTap: () {
                            setState(() {
                              _isViolationSelected = !_isViolationSelected;
                            });
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(25),
                              vertical: ScreenUtil().setHeight(11),
                            ),
                            decoration: BoxDecoration(
                              color: Palette.selago,
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                width: 0.2,
                                color: Palette.silver,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  S.of(context).DUE_TO_VIOLATION_OF_A_POLICY,
                                  style: Theme.of(context)
                                      .textTheme
                                      .display1
                                      .copyWith(
                                        fontSize: ScreenUtil().setSp(11),
                                        letterSpacing: -0.30,
                                        color: Palette.fiord,
                                      ),
                                ),
                                Visibility(
                                  visible: _isViolationSelected,
                                  maintainSize: true,
                                  maintainAnimation: true,
                                  maintainState: true,
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/Component 3 – 2.png'),
                                    matchTextDirection: true,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(31)),
                        Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(25),
                            vertical: ScreenUtil().setHeight(14),
                          ),
                          decoration: BoxDecoration(
                            color: Palette.blackHaze,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              width: 0.2,
                              color: Palette.silver,
                            ),
                          ),
                          child: TextField(
                            maxLines: 6,
                            style:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(11),
                                      letterSpacing: -0.30,
                                      color: Palette.fiord,
                                    ),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.zero,
                              hintText: S.of(context).Other_reason,
                              hintStyle:
                                  Theme.of(context).textTheme.display1.copyWith(
                                        fontSize: ScreenUtil().setSp(11),
                                        color: Palette.cadetBlue,
                                      ),
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(26)),
                        Align(
                          alignment: AlignmentDirectional.bottomEnd,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              HMButton(
                                onTap: () {
                                  _submit();
                                },
                                hasUpdateIcon: _isLoading,
                                isLoading: _isLoading,
                                done: _isUpdated,
                                text: Text(
                                  S.of(context).SUBMIT,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle
                                      .copyWith(
                                        fontSize: ScreenUtil().setSp(13),
                                        letterSpacing: 0,
                                        color: Palette.cornflowerBlue,
                                      ),
                                ),
                                doneText: Text(
                                  S.of(context).SUBMITTED,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle
                                      .copyWith(
                                        fontSize: ScreenUtil().setSp(13),
                                        letterSpacing: 0,
                                        color: Palette.cornflowerBlue,
                                      ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
