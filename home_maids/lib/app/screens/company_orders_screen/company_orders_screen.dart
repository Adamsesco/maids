import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/data/orders_repository.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/order.dart';
import 'package:home_maids/app/stores/orders_store.dart';
import 'package:home_maids/app/types/order_status.dart';
import 'package:home_maids/app/types/sort.dart';
import 'package:home_maids/app/utils/order_utils.dart';
import 'package:home_maids/app/widgets/dropdown/dropdown_item.dart';
import 'package:home_maids/app/widgets/dropdown/dropdown_menu.dart';
import 'package:home_maids/app/widgets/expandible_order_item/expandible_order_item.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class CompanyOrdersScreen extends StatefulWidget {
  @override
  _CompanyOrdersScreenState createState() => _CompanyOrdersScreenState();
}

class _CompanyOrdersScreenState extends State<CompanyOrdersScreen> {
  OrdersStore _ordersStore;
  OrderStatus _orderStatus = OrderStatus.all;
  int _page = 1;
  ScrollController _scrollController = ScrollController();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    _ordersStore = Provider.of<OrdersStore>(context, listen: false);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _getMoreOrders(_ordersStore);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();

    super.dispose();
  }

  Future<void> _getMoreOrders(OrdersStore ordersStore) async {
    if (_isLoading) {
      return;
    }

    setState(() {
      _isLoading = true;
    });

    _page += 1;

    final company = Provider.of<Company>(context, listen: false);

    final newOrders = await OrdersRepository()
        .get(company, page: _page, status: orderStatusCode(_orderStatus));
    if (newOrders == null || newOrders.isEmpty) {
      setState(() {
        _isLoading = false;
      });

      return;
    }

    final orders = await ordersStore.orders;
    if (orders.isEmpty || _orderStatus == OrderStatus.all) {
      orders.addAll(newOrders);
    }

    setState(() {
      _isLoading = false;
      ordersStore.setOrders(ordersStore.orders);
    });
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: Palette.royalBlue,
      brightness: Brightness.dark,
      centerTitle: true,
      leading: InkWell(
        onTap: () => Navigator.of(context).pop(),
        child: Image(
          image: AssetImage('assets/images/noun_Arrow_2335767.png'),
          height: ScreenUtil().setHeight(19.27),
          matchTextDirection: true,
        ),
      ),
      title: Text(
        S.of(context).ORDERS,
        style: Theme.of(context).textTheme.headline,
      ),
      actions: <Widget>[
        DropDownMenu<OrderStatus>(
          onChanged: (status) async {
            final orders = await _ordersStore.orders;
            setState(() {
              orders.clear();
              _page = 0;
              _orderStatus = status;
              _getMoreOrders(_ordersStore);
            });
          },
          width: ScreenUtil().setWidth(203),
          button: Image(
            image: AssetImage('assets/images/Component 5 – 2.png'),
            height: ScreenUtil().setHeight(16.01),
            color: Colors.white,
            matchTextDirection: true,
          ),
          head: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Image(
                image: AssetImage('assets/images/Component 5 – 2.png'),
                height: ScreenUtil().setHeight(16.01),
                color: Palette.royalBlue,
                matchTextDirection: true,
              ),
            ],
          ),
          items: [
            DropdownItem<OrderStatus>(
              value: OrderStatus.all,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.cornflowerBlue,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Latest_first,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.all) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
            DropdownItem<OrderStatus>(
              value: OrderStatus.delayed,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.red,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Delayed,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.delayed) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
            DropdownItem<OrderStatus>(
              value: OrderStatus.cancelled,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.cadetBlue,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Canceled,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.cancelled) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
            DropdownItem<OrderStatus>(
              value: OrderStatus.declined,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.mercury,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Declined,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.declined) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
            DropdownItem<OrderStatus>(
              value: OrderStatus.done,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.malachite2,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Complete,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.done) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
          ],
        ),
      ],
      bottom: PreferredSize(
        preferredSize: Size(MediaQuery.of(context).size.width, 4),
        child: Column(
          children: <Widget>[
            Consumer<OrdersStore>(
              builder: (BuildContext context, OrdersStore ordersStore,
                      Widget child) =>
                  FutureBuilder(
                future: ordersStore.orders,
                builder: (BuildContext context,
                    AsyncSnapshot<List<Order>> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                    case ConnectionState.active:
                      return SizedBox(
                        height: 4,
                        child: LinearProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Palette.brightTurquoise),
                        ),
                      );
                    case ConnectionState.none:
                    case ConnectionState.done:
                      return SizedBox(height: 4);
                  }

                  return SizedBox(height: 4);
                },
              ),
            ),
          ],
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar,
      bottomNavigationBar: CompanyNavigationBar(currentIndex: 0),
      body: Container(
        child: Consumer<OrdersStore>(
          builder:
              (BuildContext context, OrdersStore ordersStore, Widget child) =>
                  FutureBuilder(
            future: ordersStore.orders,
            builder:
                (BuildContext context, AsyncSnapshot<List<Order>> snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                case ConnectionState.active:
                  return CupertinoActivityIndicator();
                case ConnectionState.none:
                  return Container();
                case ConnectionState.done:
                  final orders = snapshot.data;
                  if (orders == null) {
                    return Container();
                  }

                  return ListView.builder(
                    controller: _scrollController,
                    padding: EdgeInsets.symmetric(
                      vertical: ScreenUtil().setHeight(7),
                    ),
                    itemCount: orders.length + (_isLoading ? 1 : 0),
                    itemBuilder: (BuildContext context, int index) {
                      if (index < orders.length) {
                        final order = orders[index];
                        return ExpandibleOrderItem(order: order);
                      }

                      return Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(8),
                        ),
                        child: CupertinoActivityIndicator(),
                      );
                    },
                  );
              }

              return Container();
            },
          ),
        ),
      ),
    );
  }
}
