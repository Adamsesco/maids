import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/login_screen/login_screen.dart';
import 'package:home_maids/app/widgets/hmbutton/hmbutton.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  PageController _pageController;
  int _page = 0;

  @override
  void initState() {
    super.initState();

    _pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        brightness: Brightness.light,
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            if (_page == 0) {
              Navigator.of(context).pop();
            } else {
              _pageController.previousPage(
                  duration: Duration(milliseconds: 200), curve: Curves.linear);
            }
          },
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            color: Palette.royalBlue,
          ),
        ),
        title: Image(
          image: AssetImage('assets/images/logo.png'),
          height: ScreenUtil().setHeight(28),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(32),
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: ScreenUtil().setHeight(24)),
            Row(
              children: <Widget>[
                Text(
                  'CHANGE PASSWORD',
                  style: Theme.of(context).textTheme.subtitle,
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(50)),
            Expanded(
              child: PageView(
                onPageChanged: (int page) => _page = page,
                controller: _pageController,
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  _EmailPage(
                    onSubmit: () {
                      _pageController.nextPage(
                          duration: Duration(milliseconds: 200),
                          curve: Curves.linear);
                    },
                  ),
                  _CodePage(
                    onSubmit: () {},
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _EmailPage extends StatefulWidget {
  final Function onSubmit;

  _EmailPage({Key key, this.onSubmit}) : super(key: key);

  @override
  __EmailPageState createState() => __EmailPageState();
}

class __EmailPageState extends State<_EmailPage> {
  GlobalKey<FormState> _formKey = GlobalKey();
  String _email;
  String _emailError;
  bool _isLoading = false;
  bool _isUpdated = false;

  Future<void> _submit() async {
    setState(() {
      _isLoading = true;
    });

    final form = _formKey.currentState;
    form.save();
    final isValid = form.validate();

    if (!isValid) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text('Error'),
          content: Text(_emailError),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Ok'),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });

      return;
    }

    final error = await _send();

    if (error != null) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text('Error'),
          content: Text(error),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Ok'),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });
    } else if (widget.onSubmit != null) {
      setState(() {
        _isLoading = false;
        _isUpdated = true;
      });

      widget.onSubmit();
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<String> _send() async {
    final response = await Dio().get<String>(
        "http://www.washy-car.com/homemaids/api/forgetPassword/$_email");

    if (response == null ||
        response.statusCode != 200 ||
        response.data == null ||
        response.data.isEmpty) {
      return "An unexpected error happened. Please try again.";
    }

    final responseJson = jsonDecode(response.data);
    final status = responseJson['statue'] ?? 500;
    final code = responseJson['code'] ?? 2;
    if (status != 200 || code != 1) {
      return "There is no account registered with this email address.";
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.done,
                textDirection: TextDirection.ltr,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide(
                      color: Palette.cadetBlue,
                      width: 0.3,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide(
                      color: Palette.cadetBlue,
                      width: 0.3,
                    ),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide(
                      color: Palette.cadetBlue,
                      width: 0.3,
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide(
                      color: Palette.cadetBlue,
                      width: 0.3,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide(
                      color: Palette.cadetBlue,
                      width: 0.3,
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide(
                      color: Palette.cadetBlue,
                      width: 0.3,
                    ),
                  ),
                  contentPadding: EdgeInsets.only(
                    right: ScreenUtil().setWidth(27),
                    left: ScreenUtil().setWidth(27),
                    top: ScreenUtil().setHeight(23),
                    bottom: ScreenUtil().setHeight(20),
                  ),
                  errorStyle: Theme.of(context).textTheme.display3.copyWith(
                        fontSize: 0,
                        color: Palette.red,
                        height: 0,
                      ),
                  hintText: 'Email',
                  hintStyle: Theme.of(context).textTheme.display4.copyWith(
                        fontWeight: FontWeight.w300,
                        color: Palette.periwinkleGray,
                      ),
                ),
                validator: (String value) {
                  final pattern =
                      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
                  final regexp = new RegExp(pattern);
                  if (!regexp.hasMatch(value)) {
                    _emailError = 'Invalid email address.';
                    return _emailError;
                  } else {
                    _emailError = null;
                  }
                  return null;
                },
                onFieldSubmitted: (_) {
                  _submit();
                },
                onSaved: (String value) {
                  _email = value;
                },
              ),
            ],
          ),
        ),
        SizedBox(height: ScreenUtil().setHeight(31)),
        Align(
          alignment: Alignment.topRight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              HMButton(
                onTap: () {
                  _submit();
                },
                hasUpdateIcon: _isLoading,
                isLoading: _isLoading,
                done: _isUpdated,
                text: Text(
                  'SUBMIT',
                  style: Theme.of(context).textTheme.subtitle.copyWith(
                        fontSize: ScreenUtil().setSp(13),
                        letterSpacing: 0,
                        color: Palette.cornflowerBlue,
                      ),
                ),
                doneText: Text(
                  'SUBMITTED',
                  style: Theme.of(context).textTheme.subtitle.copyWith(
                        fontSize: ScreenUtil().setSp(13),
                        letterSpacing: 0,
                        color: Palette.cornflowerBlue,
                      ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _CodePage extends StatefulWidget {
  final Function onSubmit;

  _CodePage({Key key, this.onSubmit}) : super(key: key);

  @override
  __CodePageState createState() => __CodePageState();
}

class __CodePageState extends State<_CodePage> {
  GlobalKey<FormState> _formKey = GlobalKey();
  FocusNode _passwordFocusNode = FocusNode();
  FocusNode _confirmFocusNode = FocusNode();
  String _code;
  String _password;
  String _confirmPassword;
  bool _obscurePassword = true;
  bool _obscureConfirmPassword = true;
  bool _isLoading = false;
  bool _isUpdated = false;

  Future<void> _submit() async {
    final form = _formKey.currentState;
    form.save();
    final isValid = form.validate();
    if (!isValid) {
      // showCupertinoDialog(
      //   context: context,
      //   builder: (context) => CupertinoAlertDialog(
      //     title: Text('Error'),
      //     content: Text(
      //         'Invalid password. The password must contain at least 6 characters.'),
      //     actions: <Widget>[
      //       CupertinoDialogAction(
      //         onPressed: () => Navigator.of(context).pop(),
      //         child: Text('Ok'),
      //       ),
      //     ],
      //   ),
      // );

      return;
    }

    if (_password != _confirmPassword) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          content: Text("The passwords don't match"),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              isDefaultAction: true,
              child: Text('Ok'),
            ),
          ],
        ),
      );

      return;
    }

    setState(() {
      _isLoading = true;
    });

    final error = await _send();
    if (error != null) {
      setState(() {
        _isLoading = false;
      });

      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          content: Text(error),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              isDefaultAction: true,
              child: Text('Ok'),
            ),
          ],
        ),
      );

      return;
    }

    setState(() {
      _isLoading = false;
      _isUpdated = true;
    });

    await showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text('Success'),
        content: Text(
            'Your password was updated successfully ! You can now sign-in using your new password.'),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () => Navigator.of(context).pop(),
            isDefaultAction: true,
            child: Text('Ok'),
          ),
        ],
      ),
    );

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ),
    );
  }

  Future<String> _send() async {
    final formData = FormData.fromMap({
      'code': _code,
      'password': _password,
    });

    final response = await Dio().post<String>(
        'http://www.washy-car.com/homemaids/api/forgetPassword/change',
        data: formData);
    if (response == null ||
        response.statusCode != 200 ||
        response.data == null ||
        response.data.isEmpty) {
      return 'An unexpected error happened. Please try again';
    }

    final responseJson = jsonDecode(response.data);
    final status = responseJson['statue'];
    final code = responseJson['code'];
    if (status != 200 || code != 1) {
      return "Incorrect code.";
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Text(
              'You will soon receive a 4-digits code on your inbox. You can use it to change your password.',
              style: Theme.of(context).textTheme.display1,
            ),
            SizedBox(height: ScreenUtil().setHeight(40)),
            TextFormField(
              onChanged: (String value) {
                setState(() {
                  _isUpdated = false;
                });
              },
              textInputAction: TextInputAction.next,
              validator: (String value) {
                if (value == null ||
                    value.length != 4 ||
                    !RegExp(r'^\d{4}$').hasMatch(value)) {
                  return "The code must contain 4 digits.";
                }
                return null;
              },
              onEditingComplete: () {
                FocusScope.of(context).requestFocus(_passwordFocusNode);
              },
              onSaved: (String value) {
                _code = value;
              },
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(15),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.20,
                    color: Palette.cadetBlue,
                  ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    width: 0.3,
                    color: Palette.cadetBlue,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    width: 0.3,
                    color: Palette.cadetBlue,
                  ),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    width: 0.3,
                    color: Colors.red,
                  ),
                ),
                contentPadding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(22),
                  right: ScreenUtil().setWidth(29.5 + 17.12 + 30),
                  bottom: ScreenUtil().setHeight(22),
                  left: ScreenUtil().setWidth(26),
                ),
                hintText: 'Code',
                hintStyle: Theme.of(context).textTheme.display1.copyWith(
                      fontSize: ScreenUtil().setSp(12),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.20,
                      color: Palette.cadetBlue,
                    ),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(16)),
            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                TextFormField(
                  focusNode: _passwordFocusNode,
                  obscureText: _obscurePassword,
                  textInputAction: TextInputAction.next,
                  onChanged: (String value) {
                    setState(() {
                      _isUpdated = false;
                    });
                  },
                  validator: (String value) {
                    if (value == null || value.length < 6) {
                      return "The password must contain at least 6 characters.";
                    }
                    return null;
                  },
                  onEditingComplete: () {
                    FocusScope.of(context).requestFocus(_confirmFocusNode);
                  },
                  onSaved: (String value) {
                    _password = value;
                  },
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(15),
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.20,
                        color: Palette.cadetBlue,
                      ),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(
                        width: 0.3,
                        color: Palette.cadetBlue,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(
                        width: 0.3,
                        color: Palette.cadetBlue,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(
                        width: 0.3,
                        color: Colors.red,
                      ),
                    ),
                    contentPadding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(22),
                      right: ScreenUtil().setWidth(29.5 + 17.12 + 30),
                      bottom: ScreenUtil().setHeight(22),
                      left: ScreenUtil().setWidth(26),
                    ),
                    hintText: 'New password',
                    hintStyle: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.20,
                          color: Palette.cadetBlue,
                        ),
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  bottom: 0,
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        _obscurePassword = !_obscurePassword;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                        right: ScreenUtil().setWidth(29.5),
                        left: ScreenUtil().setWidth(20),
                      ),
                      child: Image(
                        image: AssetImage('assets/images/Component 9 – 2.png'),
                        width: ScreenUtil().setWidth(17.12),
                        height: ScreenUtil().setHeight(12.36),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(16)),
            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                TextFormField(
                  focusNode: _confirmFocusNode,
                  obscureText: _obscureConfirmPassword,
                  onChanged: (String value) {
                    setState(() {
                      _isUpdated = false;
                    });
                  },
                  onEditingComplete: () {
                    _submit();
                  },
                  onSaved: (String value) {
                    _confirmPassword = value;
                  },
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(15),
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.20,
                        color: Palette.cadetBlue,
                      ),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(
                        width: 0.3,
                        color: Palette.cadetBlue,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(
                        width: 0.3,
                        color: Palette.cadetBlue,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(
                        width: 0.3,
                        color: Colors.red,
                      ),
                    ),
                    contentPadding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(22),
                      right: ScreenUtil().setWidth(29.5 + 17.12 + 30),
                      bottom: ScreenUtil().setHeight(22),
                      left: ScreenUtil().setWidth(26),
                    ),
                    hintText: 'New password',
                    hintStyle: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.20,
                          color: Palette.cadetBlue,
                        ),
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  bottom: 0,
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        _obscureConfirmPassword = !_obscureConfirmPassword;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                        right: ScreenUtil().setWidth(29.5),
                        left: ScreenUtil().setWidth(20),
                      ),
                      child: Image(
                        image: AssetImage('assets/images/Component 9 – 2.png'),
                        width: ScreenUtil().setWidth(17.12),
                        height: ScreenUtil().setHeight(12.36),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(31)),
            Align(
              alignment: Alignment.topRight,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  HMButton(
                    onTap: () {
                      _submit();
                    },
                    isLoading: _isLoading,
                    done: _isUpdated,
                    text: Text(
                      'UPDATE',
                      style: Theme.of(context).textTheme.subtitle.copyWith(
                            fontSize: ScreenUtil().setSp(13),
                            letterSpacing: 0,
                            color: Palette.cornflowerBlue,
                          ),
                    ),
                    doneText: Text(
                      'UPDATED',
                      style: Theme.of(context).textTheme.subtitle.copyWith(
                            fontSize: ScreenUtil().setSp(13),
                            letterSpacing: 0,
                            color: Palette.cornflowerBlue,
                          ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
