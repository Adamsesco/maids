import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:hive/hive.dart';
import 'package:home_maids/app/screens/login_screen/login_screen.dart';
import 'package:home_maids/app/services/users_service.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/app/widgets/hmbutton/hmbutton.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/widgets/client_navigation_bar/client_navigation_bar.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';

import '../../types/address.dart';

class ClientProfileEditScreen extends StatefulWidget {
  @override
  _ClientProfileEditScreenState createState() =>
      _ClientProfileEditScreenState();
}

class _ClientProfileEditScreenState extends State<ClientProfileEditScreen> {
  GlobalKey<FormState> _formKey;

  Client _client;
  File _avatarFile;
  String _fullName;
  String _email;
  String _phoneNumber;
  String _password;
  bool _passwordChanged = false;
  String _address;
  bool _isLoading;
  bool _isUpdated;

  @override
  void initState() {
    super.initState();

    _formKey = GlobalKey();
    _client = Provider.of<Client>(context, listen: false);
    print(_client.token);
    _fullName = "${_client.firstName} ${_client.lastName}";
    _email = _client.email;
    _phoneNumber = _client.phoneNumber;
    _password = 'xxxxxx';
    _address = _client.addresses == null || _client.addresses.isEmpty
        ? ''
        : _client.addresses.first.address ?? '';

    _isLoading = false;
    _isUpdated = false;
  }

  Future<void> _pickAvatar() async {
    final image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        _avatarFile = image;
        _isUpdated = false;
      });
    }
  }

  Future<void> _update() async {
    setState(() {
      _isLoading = true;
    });

    String avatarBase64;
    if (_avatarFile != null) {
      final fileContent = await _avatarFile.readAsBytes();
      avatarBase64 = base64.encode(fileContent);
    }

    final firstName = _fullName?.split(RegExp(r'\s+'))?.first ?? '';
    final lastName = _fullName?.split(RegExp(r'\s+'))?.last ?? '';
    print("First name: $firstName");
    print("Last name: $lastName");
    print("Address: $_address");
    final formData = FormData.fromMap({
      'email': _email,
      'phone': _phoneNumber,
      'first_name': firstName,
      'last_name': lastName,
      'address': _address,
      if (_passwordChanged) 'password': _password,
      if (avatarBase64 != null && avatarBase64.isNotEmpty)
        'avatar': "data:image/png;base64,$avatarBase64",
    });

    final response = await Dio().post<String>(
        "http://washy-car.com/homemaids/api/edit/profile/${_client.token}",
        data: formData);
    if (response == null ||
        response.statusCode != 200 ||
        response.data == null ||
        response.data.isEmpty) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(S.of(context).Error),
          content:
              Text(S.of(context).An_unexpected_error_happened_Please_try_again),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });
      return;
    }

    print(response.data);
    final responseJson = jsonDecode(response.data);
    final status = responseJson['status'];
    final code = responseJson['code'];
    if (status != 200 || code != 1) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(S.of(context).Error),
          content:
              Text(S.of(context).An_unexpected_error_happened_Please_try_again),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });
      return;
    }

    final data = responseJson['data'];
    _client.setData(
      email: data['email'],
      avatarUrl: data['avatar'],
      phoneNumber: data['phone'],
      addresses: [Address(data['address'], '')],
      firstName: data['first_name'],
      lastName: data['last_name'],
    );

    final clientData = _client.toMap();
    final userDb = Hive.box<Map<dynamic, dynamic>>('user');
    await userDb.delete('user');
    userDb.put('user', clientData);

    setState(() {
      _isLoading = false;
      _isUpdated = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: Palette.royalBlue,
      brightness: Brightness.dark,
      centerTitle: true,
      leading: InkWell(
        onTap: () => Navigator.of(context).pop(),
        child: Image(
          image: AssetImage('assets/images/noun_Arrow_2335767.png'),
          height: ScreenUtil().setHeight(19.27),
          matchTextDirection: true,
        ),
      ),
      title: Text(
        S.of(context).PROFILE,
        style: Theme.of(context).textTheme.headline,
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar,
      bottomNavigationBar: ClientNavigationBar(currentIndex: 3),
      body: SingleChildScrollView(
        child: Container(
          height:
              MediaQuery.of(context).size.height - appBar.preferredSize.height,
          child: Consumer<Client>(
            builder: (BuildContext context, Client client, Widget child) =>
                Column(
              children: <Widget>[
                SizedBox(height: ScreenUtil().setHeight(24)),
                Container(
                  margin: EdgeInsetsDirectional.only(
                    end: ScreenUtil().setWidth(21),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(28),
                        ),
                        child: Text(
                          S.of(context).EDIT_PROFILE,
                          style: Theme.of(context).textTheme.subtitle.copyWith(
                                fontSize: ScreenUtil().setSp(17),
                              ),
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          await UsersService().signOut();
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) => LoginScreen(),
                            ),
                          );
                        },
                        child: Image(
                          image: AssetImage('assets/images/logout-icon.png'),
                          matchTextDirection: true,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(25)),
                Container(
                  margin: EdgeInsetsDirectional.only(
                    start: ScreenUtil().setWidth(46),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          _pickAvatar();
                        },
                        child: Stack(
                          children: <Widget>[
                            Avatar(
                              client.avatarUrl,
                              avatarFile: _avatarFile,
                              width: ScreenUtil().setWidth(79),
                              height: ScreenUtil().setHeight(76),
                              hasShadow: true,
                            ),
                            Positioned(
                              top: 0,
                              right: 0,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(9),
                                  vertical: ScreenUtil().setHeight(7),
                                ),
                                child: Image(
                                  image: AssetImage(
                                      'assets/images/update-icon.png'),
                                  width: ScreenUtil().setWidth(19.19),
                                  height: ScreenUtil().setHeight(15.85),
                                  color: Palette.grape,
                                ),
                              ),
                            ),
                          ],
                        ),
//                        child: Stack(
//                          children: <Widget>[
//                            Container(
//                              width: ScreenUtil().setWidth(79),
//                              height: ScreenUtil().setHeight(76),
//                              decoration: BoxDecoration(
//                                borderRadius: BorderRadius.circular(15),
//                                image: DecorationImage(
//                                  image: NetworkImage(client.avatarUrl),
//                                  fit: BoxFit.cover,
//                                ),
//                                boxShadow: [
//                                  BoxShadow(
//                                    color: Colors.black.withOpacity(0.13),
//                                    blurRadius: 18,
//                                  ),
//                                ],
//                              ),
//                            ),
//                            Positioned(
//                              top: 0,
//                              right: 0,
//                              child: Container(
//                                padding: EdgeInsets.symmetric(
//                                  horizontal: ScreenUtil().setWidth(9),
//                                  vertical: ScreenUtil().setHeight(7),
//                                ),
//                                child: Image(
//                                  image: AssetImage(
//                                      'assets/images/update-icon.png'),
//                                  width: ScreenUtil().setWidth(19.19),
//                                  height: ScreenUtil().setHeight(15.85),
//                                  color: Colors.white,
//                                ),
//                              ),
//                            ),
//                          ],
//                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(24),
                          vertical: ScreenUtil().setHeight(18),
                        ),
                        decoration: BoxDecoration(
                          color: Palette.blackHaze,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(16),
                            bottomLeft: Radius.circular(16),
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${client.firstName} ${client.lastName}",
                              style:
                                  Theme.of(context).textTheme.subtitle.copyWith(
                                        fontSize: ScreenUtil().setSp(15),
                                        letterSpacing: 0,
                                      ),
                            ),
                            Text(
                              "${S.of(context).Member_since} " +
                                  DateFormat('dd MMMM y')
                                      .format(client.dateCreated),
                              style:
                                  Theme.of(context).textTheme.display1.copyWith(
                                        fontSize: ScreenUtil().setSp(10),
                                        fontWeight: FontWeight.w300,
                                        color: Palette.tundora,
                                      ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(11)),
                            Container(
                              margin: EdgeInsets.symmetric(
                                horizontal: ScreenUtil().setWidth(42),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        client.completedOrdersCount.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle
                                            .copyWith(
                                              fontSize: ScreenUtil().setSp(14),
                                              color: Palette.fiord,
                                            ),
                                      ),
                                      Text(
                                        S.of(context).Completed_Orders,
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .display1
                                            .copyWith(
                                              fontSize: ScreenUtil().setSp(8),
                                              color: Palette.fiord,
                                            ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(28.3)),
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        client.canceledOrdersCount.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle
                                            .copyWith(
                                              fontSize: ScreenUtil().setSp(14),
                                              color: Palette.fiord,
                                            ),
                                      ),
                                      Text(
                                        S.of(context).Canceled_Orders,
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .display1
                                            .copyWith(
                                              fontSize: ScreenUtil().setSp(8),
                                              color: Palette.fiord,
                                            ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(32)),
                Container(
                  padding: EdgeInsetsDirectional.only(
                    start: ScreenUtil().setWidth(45),
                    end: ScreenUtil().setWidth(66.3),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        _TextField(
                          initialValue: _fullName,
                          hintText: S.of(context).Full_name,
                          onChanged: (String value) {
                            _fullName = value;
                            setState(() {
                              _isUpdated = false;
                            });
                          },
                        ),
                        SizedBox(height: ScreenUtil().setHeight(12)),
                        _TextField(
                          initialValue: _email,
                          hintText: S.of(context).Email_address,
                          onChanged: (String value) {
                            _email = value;
                            setState(() {
                              _isUpdated = false;
                            });
                          },
                        ),
                        SizedBox(height: ScreenUtil().setHeight(12)),
                        _TextField(
                          initialValue: _phoneNumber,
                          hintText: S.of(context).Phone_number,
                          onChanged: (String value) {
                            _phoneNumber = value;
                            setState(() {
                              _isUpdated = false;
                            });
                          },
                        ),
                        SizedBox(height: ScreenUtil().setHeight(12)),
                        _TextField(
                          obscureText: true,
                          initialValue: _password,
                          hintText: S.of(context).Password,
                          onChanged: (String value) {
                            _password = value;
                            _passwordChanged = true;
                            setState(() {
                              _isUpdated = false;
                            });
                          },
                        ),
                        SizedBox(height: ScreenUtil().setHeight(12)),
                        _TextField(
                          initialValue: _address,
                          hintText: S.of(context).Address,
                          maxLines: 3,
                          onChanged: (String value) {
                            _address = value;
                            setState(() {
                              _isUpdated = false;
                            });
                          },
                        ),
                        SizedBox(height: ScreenUtil().setHeight(40)),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Row(
                                children: <Widget>[
                                  HMButton(
                                    onTap: () {
                                      _update();
                                    },
                                    isLoading: _isLoading,
                                    done: _isUpdated,
                                    text: Text(
                                      S.of(context).UPDATE,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle
                                          .copyWith(
                                            fontSize: ScreenUtil().setSp(13),
                                            letterSpacing: 0,
                                            color: Palette.cornflowerBlue,
                                          ),
                                    ),
                                    doneText: Text(
                                      S.of(context).UPDATED,
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle
                                          .copyWith(
                                            fontSize: ScreenUtil().setSp(13),
                                            letterSpacing: 0,
                                            color: Palette.cornflowerBlue,
                                          ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(48)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

typedef void _OnChanged(String value);

class _TextField extends StatelessWidget {
  final String initialValue;
  final Widget prefix;
  final int maxLines;
  final String hintText;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final bool obscureText;
  final _OnChanged onChanged;

  _TextField({
    this.initialValue = '',
    this.prefix,
    this.maxLines = 1,
    this.hintText,
    this.keyboardType = TextInputType.text,
    this.textInputAction = TextInputAction.none,
    this.obscureText = false,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (prefix != null) ...[
          prefix,
          SizedBox(width: ScreenUtil().setWidth(14.6)),
        ],
        Expanded(
          child: TextFormField(
            onChanged: (String value) {
              if (onChanged != null) {
                onChanged(value);
              }
            },
            obscureText: obscureText,
            initialValue: initialValue,
            maxLines: maxLines,
            keyboardType: keyboardType,
            textInputAction: textInputAction,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(21),
                vertical: ScreenUtil().setHeight(maxLines > 1 ? 25 : 7),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Palette.cadetBlue,
                  width: 0.3,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Palette.cadetBlue,
                  width: 0.3,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Palette.cadetBlue,
                  width: 0.3,
                ),
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 0.3,
                ),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 0.3,
                ),
              ),
              hintText: hintText,
              hintStyle: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(11),
                    color: Palette.fiord.withOpacity(0.6),
                  ),
            ),
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontSize: ScreenUtil().setSp(11),
                  color: Palette.fiord,
                ),
          ),
        ),
      ],
    );
  }
}
