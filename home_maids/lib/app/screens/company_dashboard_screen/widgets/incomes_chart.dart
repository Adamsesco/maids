import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/types/income.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:intl/intl.dart' as intl;

typedef OnTap(double value, Offset position);
typedef OnScroll(double offset);

class IncomesChart extends StatefulWidget {
  final List<Income> data;

  IncomesChart({Key key, @required this.data}) : super(key: key);

  @override
  _IncomesChartState createState() => _IncomesChartState();
}

class _IncomesChartState extends State<IncomesChart> {
  double _maxValue;

  @override
  void initState() {
    super.initState();

    _maxValue = widget.data.fold(0.0, (previous, current) {
      return max(previous, current.amount);
    });
  }

  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Transform.translate(
                      offset: Offset(0, -ScreenUtil().setHeight(4)),
                      child: Image(
                        image: AssetImage('assets/images/rising-icon.png'),
                        height: ScreenUtil().setHeight(6.59),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(3.9)),
                    Text(
                      '+1.4%',
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(14),
                            color: Palette.fiord,
                          ),
                    ),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(2.9)),
                Text(
                  '\$45,100',
                  style: Theme.of(context).textTheme.headline.copyWith(
                        color: Palette.royalBlue,
                      ),
                ),
                Text(
                  S.of(context).TOTAL_EARN,
                  style: Theme.of(context).textTheme.subtitle.copyWith(
                        fontSize: ScreenUtil().setSp(14),
                        fontWeight: FontWeight.w300,
                        color: Palette.cornflowerBlue,
                      ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Text(
                  now.day.toString(),
                  style: Theme.of(context).textTheme.headline.copyWith(
                        fontSize: ScreenUtil().setSp(36),
                        color: Palette.royalBlue,
                        letterSpacing: -1,
                      ),
                ),
                Text(
                  intl.DateFormat('MMM').format(now).toUpperCase(),
                  style: Theme.of(context).textTheme.subtitle.copyWith(
                        fontSize: ScreenUtil().setSp(22),
                        fontWeight: FontWeight.w300,
                        height: 0.80,
                        color: Palette.cornflowerBlue,
                      ),
                ),
              ],
            ),
          ],
        ),
        Container(
          alignment: Alignment.bottomLeft,
          child: _ChartCurve(
            widget.data,
          ),
        ),
      ],
    );
  }
}

class _ChartCurve extends StatefulWidget {
  final List<Income> points;

  _ChartCurve(this.points);

  @override
  __ChartCurveState createState() => __ChartCurveState();
}

class __ChartCurveState extends State<_ChartCurve> {
  List<double> _values;
  Offset _tagOffset;
  _ValueTag _tag;
  double _maxValue;
  double _curveWidth;
  double _curveHeight;
  List<_ValueShort> _yLabels;
  GlobalKey _curveKey;

  @override
  void initState() {
    super.initState();

    _values = widget.points.map((point) => point.amount).toList();
    _maxValue = _values.fold(0.0, max);
    _yLabels = _getShortValues();
    _curveKey = GlobalKey();
  }

  @override
  Widget build(BuildContext context) {
    _curveWidth = widget.points.length * ScreenUtil().setWidth(50);
    _curveHeight = ScreenUtil().setHeight(109.2);

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          width: ScreenUtil().setWidth(40),
          height: ScreenUtil().setHeight(160),
          child: Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                child: Text(
                  '0',
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(9),
                        fontWeight: FontWeight.w500,
                        color: Palette.trendyPink,
                      ),
                ),
              ),
              for (var label in _yLabels)
                Builder(
                  builder: (context) {
                    final y =
                        _getY2(Size(0, _curveHeight), label.ceil, _maxValue);
                    final short =
                        label.short.toString().replaceAll(RegExp(r'\.0$'), '');

                    return Positioned(
                      bottom: y,
                      child: Text(
                        "$short${label.suffix}",
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(9),
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.11,
                              color: Palette.trendyPink,
                            ),
                      ),
                    );
                  },
                ),
            ],
          ),
        ),
        SizedBox(
          width: ScreenUtil().setWidth(290),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Column(
              children: <Widget>[
                Stack(
                  alignment: Alignment.bottomLeft,
                  children: <Widget>[
                    SizedBox(
                      height: ScreenUtil().setHeight(160),
                    ),
                    SizedBox(
                      width: _curveWidth,
                      height: _curveHeight,
                      child: Stack(
                        key: _curveKey,
                        alignment: Alignment.bottomLeft,
                        children: <Widget>[
                          CustomPaint(
                            size: Size(_curveWidth, _curveHeight),
                            painter: _CurvePainter(_values),
                          ),
                          ClipPath(
                            clipper: _CurveClipper(_values),
                            child: Container(
                              width: _curveWidth,
                              height: _curveHeight,
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color(0xFFDBBFFB),
                                    Colors.white,
                                  ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    for (var label in _yLabels)
                      Builder(
                        builder: (context) {
                          final y = _getY2(
                              Size(0, _curveHeight), label.ceil, _maxValue);
                          // print("${label.ceil} $y");

                          return Positioned(
                            bottom: y,
                            left: 0,
                            right: 0,
                            child: Container(
                              height: 0.85,
                              color: Palette.slateGrey.withOpacity(0.08),
                            ),
                          );
                        },
                      ),
                    Positioned(
                      left: -ScreenUtil().setWidth(15),
                      child: Row(
                        children: <Widget>[
                          for (var i = 0; i < _values.length; i++)
                            InkWell(
                              onTap: () {
                                final value = _values[i];
                                setState(() {
                                  _setTag(i, value);
                                });
                              },
                              highlightColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              child: SizedBox(
                                width: ScreenUtil().setWidth(50),
                                height: ScreenUtil().setHeight(149.4),
                              ),
                            ),
                        ],
                      ),
                    ),
                    if (_tag != null && _tagOffset != null)
                      Builder(
                        builder: (BuildContext context) {
                          final textStyle =
                              Theme.of(context).textTheme.display1.copyWith(
                                    fontSize: ScreenUtil().setSp(13),
                                    fontWeight: FontWeight.w800,
                                    letterSpacing: -0.68,
                                    color: Colors.white,
                                  );
                          final text = "${_tag.currency} ${_tag.value.toInt()}";
                          final textSize = _getTextSize(text, textStyle);
                          final tagSize =
                              Size(textSize.width + 21, textSize.height + 14);

                          double top;
                          double left;
                          bool reverseTag = false;

                          top = _tagOffset.dy -
                              tagSize.height +
                              ScreenUtil().setHeight(40.2);

                          if (_tagOffset.dx > (tagSize.width + 6)) {
                            left = _tagOffset.dx - tagSize.width - 6;
                          } else {
                            left = _tagOffset.dx + 6;
                            reverseTag = true;
                          }

                          return Positioned(
                            top: top,
                            left: left,
                            child: _ValueTag(
                              _tag.value,
                              _tag.currency,
                              reverse: reverseTag,
                            ),
                          );
                        },
                      ),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(6)),
                Row(
                  children: <Widget>[
                    for (var i = 0; i < widget.points.length; i++)
                      Builder(
                        builder: (context) {
                          final label = widget.points[i].label;

                          final textStyle =
                              Theme.of(context).textTheme.display1.copyWith(
                                    fontSize: ScreenUtil().setSp(8),
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: 1.11,
                                    color: Palette.trendyPink,
                                  );

                          return Transform.translate(
                            offset: Offset.zero,
                            child: SizedBox(
                              width: ScreenUtil().setWidth(50),
                              child: Text(
                                label,
                                style: textStyle,
                              ),
                            ),
                          );
                        },
                      ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void _setTag(int index, double value) {
    _tagOffset = Offset(index * ScreenUtil().setWidth(50),
        _getY(Size(0, _curveHeight), value, _maxValue));
    _tag = _ValueTag(value, '\$');
  }

  List<_ValueShort> _getShortValues() {
    if (_values == null || _values.isEmpty) {
      return [];
    }

    if (_values.length <= 2) {
      final _shortValue = _valueShort(_maxValue);
      return [_shortValue];
    }

    final shortValueMax = _valueShort(_maxValue);
    final shortValueMiddle = _ValueShort(
        shortValueMax.ceil / 2, shortValueMax.short / 2, shortValueMax.suffix);
    return [shortValueMax, shortValueMiddle];
  }

  _ValueShort _valueShort(double _value) {
    if (_value < 25) {
      return _ValueShort(10, 10, '');
    }

    if (_value < 75) {
      return _ValueShort(50, 50, '');
    }

    if (_value < 250) {
      return _ValueShort(100, 100, '');
    }

    if (_value < 750) {
      return _ValueShort(500, 500, '');
    }

    if (_value < 2500) {
      return _ValueShort(1000, 1, 'k');
    }

    if (_value < 7500) {
      return _ValueShort(5000, 5, 'k');
    }

    if (_value < 25000) {
      return _ValueShort(10000, 10, 'k');
    }

    if (_value < 75000) {
      return _ValueShort(50000, 50, 'k');
    }

    if (_value < 250000) {
      return _ValueShort(10000, 100, 'k');
    }

    if (_value < 750000) {
      return _ValueShort(500000, 500, 'k');
    }

    if (_value < 2500000) {
      return _ValueShort(1000000, 1, 'M');
    }

    if (_value < 7500000) {
      return _ValueShort(5000000, 5, 'M');
    }

    if (_value < 25000000) {
      return _ValueShort(10000000, 10, 'M');
    }

    return _ValueShort(0, 0, '');
  }
}

class _ValueShort {
  final double ceil;
  final double short;
  final String suffix;

  _ValueShort(this.ceil, this.short, this.suffix);
}

class _ValueTag extends StatelessWidget {
  final double value;
  final String currency;
  final bool reverse;

  _ValueTag(this.value, this.currency, {this.reverse = false});

  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context).textTheme.display1.copyWith(
          fontSize: ScreenUtil().setSp(13),
          fontWeight: FontWeight.w800,
          letterSpacing: -0.68,
          color: Colors.white,
        );

    return Transform.translate(
      offset: Offset.zero,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10.5,
          vertical: 7,
        ),
        decoration: BoxDecoration(
          color: Palette.royalBlue,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
            bottomLeft: reverse ? Radius.zero : Radius.circular(15),
            bottomRight: reverse ? Radius.circular(15) : Radius.zero,
          ),
          boxShadow: [
            BoxShadow(
              color: Palette.heliotrope2,
              blurRadius: 6,
            ),
          ],
        ),
        child: Text(
          "$currency ${value.toInt()}",
          style: textStyle,
        ),
      ),
    );
  }
}

class _CurvePainter extends CustomPainter {
  List<double> points;

  _CurvePainter(this.points);

  @override
  void paint(Canvas canvas, Size size) {
    if (points == null || points.isEmpty) {
      return null;
    }

    final paint = Paint()
      ..color = Palette.cornflowerBlue
      ..style = PaintingStyle.stroke
      ..strokeWidth = 6;

    final path = Path();
    _drawCurve(points, path, size, false);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(_CurvePainter oldDelegate) {
    return !listEquals(points, oldDelegate.points);
  }
}

class _Point {
  double x;
  double y;

  _Point(this.x, this.y);
}

class _CurveClipper extends CustomClipper<Path> {
  List<double> points;

  _CurveClipper(this.points);

  @override
  Path getClip(Size size) {
    if (points == null || points.isEmpty) {
      return null;
    }

    final path = Path();
    _drawCurve(points, path, size, true);

    return path;
  }

  @override
  bool shouldReclip(_CurveClipper oldClipper) {
    return !listEquals(points, oldClipper.points);
  }
}

void _drawCurve(List<double> points, Path path, Size size, bool shouldClose) {
  if (points == null || points.isEmpty) {
    return;
  }

  final highestPoint = points.fold(double.negativeInfinity, max);

  final xStep = ScreenUtil().setWidth(50);
  var xOffset = 0.0;
  path.moveTo(0, _getY(size, points[0], highestPoint));
  for (var i = 1; i < points.length; i++) {
    final lastPoint = _Point(xOffset, _getY(size, points[i - 1], highestPoint));
    final valueX = xOffset + xStep;
    final valueY = _getY(size, points[i], highestPoint);
    final controlPointX = lastPoint.x + (valueX - lastPoint.x) / 2;

    path.cubicTo(
        controlPointX, lastPoint.y, controlPointX, valueY, valueX, valueY);
    xOffset += xStep;
  }
  if (shouldClose) {
    path.lineTo(xOffset, size.height);
    path.lineTo(0, size.height);
  }
}

double _getY(Size size, double p, double highestPoint) {
  final y = size.height - size.height * (p / highestPoint);
  return y;
}

double _getY2(Size size, double p, double highestPoint) {
  final y = size.height * (p / highestPoint);
  return y;
}

Size _getTextSize(String text, TextStyle textStyle) {
  final textSpan = TextSpan(
    text: text,
    style: textStyle,
  );
  final textPainter = TextPainter(
    text: textSpan,
    textDirection: TextDirection.ltr,
  );
  textPainter.layout();
  final textWidth = textPainter.width;
  final textHeight = textPainter.height;

  return Size(textWidth, textHeight);
}
