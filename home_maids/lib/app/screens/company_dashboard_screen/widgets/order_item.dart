import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/order.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class OrderItem extends StatefulWidget {
  final Order order;

  OrderItem({Key key, this.order}) : super(key: key);

  @override
  _OrderItemState createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {
  User _me;

  @override
  void initState() {
    super.initState();

    final client = Provider.of<Client>(context, listen: false);
    final company = Provider.of<Company>(context, listen: false);
    if (client != null && client.id != null) {
      _me = client;
    } else {
      _me = company;
    }
  }

  @override
  Widget build(BuildContext context) {
    final client = widget.order.client;
    final company = widget.order.company;
    final address = widget.order.address.address;
    final orderNumber = widget.order.number ?? 0;
    final orderNumberStr =
        List.generate(6 - orderNumber.toString().length, (_) => '0').join() +
            orderNumber.toString();

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(16),
        vertical: ScreenUtil().setHeight(8),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Avatar(
            _me.role == UserRole.company ? client.avatarUrl : company.avatarUrl,
            width: ScreenUtil().setWidth(101),
            height: ScreenUtil().setWidth(99),
          ),
          SizedBox(width: ScreenUtil().setWidth(18)),
          SizedBox(
            width: ScreenUtil().setWidth(141),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "${client.firstName} ${client.lastName}",
                      style: Theme.of(context).textTheme.headline.copyWith(
                            fontSize: ScreenUtil().setSp(14),
                            color: Palette.royalBlue,
                          ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            address,
                            style:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(10),
                                      color: Palette.fiord,
                                    ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(13.5)),
                Text(
                  "${S.of(context).ORDER_N} $orderNumberStr",
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(9),
                        color: Palette.royalBlue,
                      ),
                ),
              ],
            ),
          ),
          Spacer(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                DateFormat('dd').format(widget.order.date),
                style: Theme.of(context).textTheme.display1.copyWith(
                      fontSize: ScreenUtil().setSp(24),
                      color: Palette.royalBlue,
                      fontWeight: FontWeight.w800,
                    ),
              ),
              Text(
                DateFormat('MMM').format(widget.order.date).toUpperCase(),
                style: Theme.of(context).textTheme.display1.copyWith(
                      fontSize: ScreenUtil().setSp(15),
                      color: Palette.royalBlue,
                      letterSpacing: -0.10,
                      fontWeight: FontWeight.w300,
                      height: 0.8,
                    ),
              ),
            ],
          ),
          SizedBox(width: ScreenUtil().setWidth(12)),
        ],
      ),
    );
  }
}
