import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/company_dashboard_screen/widgets/incomes_chart.dart';
import 'package:home_maids/app/screens/company_dashboard_screen/widgets/order_item.dart';
import 'package:home_maids/app/screens/company_orders_screen/company_orders_screen.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/incomes_store.dart';
import 'package:home_maids/app/stores/order.dart';
import 'package:home_maids/app/stores/orders_store.dart';
import 'package:home_maids/app/types/income.dart';
import 'package:home_maids/app/types/incomes_type.dart';
import 'package:home_maids/app/widgets/dropdown/dropdown.dart';
import 'package:home_maids/app/widgets/dropdown/dropdown_item.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class CompanyDashboardScreen extends StatefulWidget {
  @override
  _CompanyDashboardScreenState createState() => _CompanyDashboardScreenState();
}

class _CompanyDashboardScreenState extends State<CompanyDashboardScreen> {
  IncomesStore _incomes;
  IncomesType _incomesType;

  @override
  void initState() {
    super.initState();

    _incomes = Provider.of<IncomesStore>(context, listen: false);

    _incomesType = IncomesType.year;
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: Palette.royalBlue,
      brightness: Brightness.dark,
      centerTitle: true,
      leading: Container(),
      title: Text(
        S.of(context).DASHBOARD,
        style: Theme.of(context).textTheme.headline,
      ),
      bottom: PreferredSize(
        preferredSize: Size(MediaQuery.of(context).size.width,
            ScreenUtil().setHeight(27.4 + 14.6) + 3),
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsetsDirectional.only(
                start: ScreenUtil().setWidth(20.1),
                bottom: ScreenUtil().setHeight(14.6),
              ),
              child: Row(
                children: <Widget>[
                  Text(
                    S.of(context).ANALYTICS_FROM,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(13),
                          color: Colors.white.withOpacity(0.99),
                        ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(20)),
                  DropDown<IncomesType>(
                    onChanged: (IncomesType incomesType) {
                      setState(() {
                        _incomesType = incomesType;
                      });
                    },
                    button: Row(
                      children: <Widget>[
                        Text(
                          _incomesType == IncomesType.year
                              ? S.of(context).LAST_YEAR
                              : _incomesType == IncomesType.month
                                  ? S.of(context).LAST_MONTH
                                  : '',
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(13),
                                color: Colors.white.withOpacity(0.99),
                                fontWeight: FontWeight.w800,
                              ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(7.1)),
                        RotatedBox(
                          quarterTurns: 3,
                          child: Image(
                            image: AssetImage(
                                'assets/images/noun_Arrow_2335767.png'),
                            height: ScreenUtil().setHeight(9.95),
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    items: [
                      DropdownItem<IncomesType>(
                        value: IncomesType.year,
                        child: Text(
                          S.of(context).LAST_YEAR,
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(13),
                                color: Palette.fiord,
                                fontWeight: FontWeight.w800,
                              ),
                        ),
                      ),
                      DropdownItem<IncomesType>(
                        value: IncomesType.month,
                        child: Text(
                          S.of(context).LAST_MONTH,
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(13),
                                color: Palette.fiord,
                                fontWeight: FontWeight.w800,
                              ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Consumer<OrdersStore>(
              builder: (BuildContext context, OrdersStore ordersStore,
                      Widget child) =>
                  FutureBuilder(
                future: ordersStore.orders,
                builder: (BuildContext context,
                    AsyncSnapshot<List<Order>> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                    case ConnectionState.active:
                      return SizedBox(
                        height: 4,
                        child: LinearProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Palette.brightTurquoise),
                        ),
                      );
                    case ConnectionState.none:
                    case ConnectionState.done:
                      return SizedBox(height: 4);
                  }

                  return SizedBox(height: 4);
                },
              ),
            ),
          ],
        ),
      ),
    );

    return Scaffold(
      appBar: appBar,
      bottomNavigationBar: CompanyNavigationBar(currentIndex: 0),
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setHeight(11.5)),
          Container(
            width: ScreenUtil().setWidth(339.25),
            alignment: Alignment.center,
            child: FutureBuilder<List<List<Income>>>(
              future: Future.wait(
                  [_incomes.monthlyIncomes, _incomes.yearlyIncomes]),
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                  case ConnectionState.active:
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );
                  case ConnectionState.done:
                    if (snapshot.hasError || !snapshot.hasData) {
                      return Container();
                    }

                    List<Income> incomes;
                    switch (_incomesType) {
                      case IncomesType.month:
                        incomes = snapshot.data[0];
                        break;
                      case IncomesType.year:
                        incomes = snapshot.data[1];
                        break;
                    }

                    return IncomesChart(
                      key: UniqueKey(),
                      data: incomes,
                    );
                }

                return Container();
              },
            ),
          ),
          SizedBox(height: ScreenUtil().setHeight(15.5)),
          Center(
            child: Container(
              width: ScreenUtil().setWidth(339.25),
              height: 1,
              color: Palette.slateGrey.withOpacity(0.08),
            ),
          ),
          SizedBox(height: ScreenUtil().setHeight(25)),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(20),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  S.of(context).LAST_ORDERS,
                  style: Theme.of(context).textTheme.headline.copyWith(
                        color: Palette.fiord,
                      ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      PageRouteBuilder(
                        pageBuilder: (context, animation, secondaryAnimation) =>
                            CompanyOrdersScreen(),
                        transitionsBuilder:
                            (context, animation, secondaryAnimation, child) {
                          return child;
                        },
                      ),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(6),
                      vertical: ScreenUtil().setHeight(4),
                    ),
                    child: Row(
                      children: <Widget>[
                        Text(
                          S.of(context).VIEW_ALL,
                          style: Theme.of(context).textTheme.headline.copyWith(
                                fontSize: ScreenUtil().setSp(12),
                                color: Palette.royalBlue,
                              ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(8.4)),
                        RotatedBox(
                          quarterTurns: 2,
                          child: Image(
                            image: AssetImage(
                                'assets/images/noun_Arrow_2335767.png'),
                            height: ScreenUtil().setHeight(11.27),
                            fit: BoxFit.fitHeight,
                            color: Palette.royalBlue,
                            matchTextDirection: true,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Consumer<OrdersStore>(
              builder: (BuildContext context, OrdersStore ordersStore,
                      Widget child) =>
                  FutureBuilder(
                future: ordersStore.orders,
                builder: (BuildContext context,
                    AsyncSnapshot<List<Order>> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                    case ConnectionState.active:
                      return Center(child: CupertinoActivityIndicator());
                    case ConnectionState.done:
                      final orders = snapshot.data;
                      if (orders == null || orders.isEmpty) {
                        return Container();
                      }

                      return ListView.separated(
                        padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(23),
                        ),
                        separatorBuilder: (context, index) => Container(
                          width: MediaQuery.of(context).size.width,
                          height: 1,
                          color: Palette.slateGrey.withOpacity(0.08),
                        ),
                        itemCount: orders.length + 1,
                        itemBuilder: (BuildContext context, int index) {
                          final order = orders[0];

                          return OrderItem(order: order);
                        },
                      );
                  }

                  return Container(); // unreachable
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
