import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/review.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:provider/provider.dart';

class ReviewsScreen extends StatefulWidget {
  final Company company;

  ReviewsScreen({Key key, @required this.company}) : super(key: key);

  @override
  _ReviewsScreenState createState() => _ReviewsScreenState();
}

class _ReviewsScreenState extends State<ReviewsScreen> {
  User _me;
  Future<List<Review>> _reviewsFuture;

  @override
  void initState() {
    super.initState();

    final client = Provider.of<Client>(context, listen: false);
    final company = Provider.of<Company>(context, listen: false);

    if (client != null && client.id != null) {
      _me = client;
    } else if (company != null && company.id != null) {
      _me = company;
    }

    _reviewsFuture = _getReviews(widget.company);
  }

  Future<List<Review>> _getReviews(Company company) async {
    final response = await http
        .get("http://washy-car.com/homemaids/api/reviews/${company.id}");
    if (response == null ||
        response.statusCode != 200 ||
        response.body == null ||
        response.body.isEmpty) {
      return [];
    }

    final responseJson = jsonDecode(response.body);
    final data = responseJson['data'] as List<dynamic>;
    final reviews = data.map((r) {
      final client = Client()
        ..setData(
          id: r['client_id'],
          firstName: r['first_name'],
          lastName: r['last_name'],
          avatarUrl: r['avatar'],
        );

      final review = Review()
        ..setData(
          author: client,
          company: company,
          date: DateTime.parse(r['date']),
          rating: double.tryParse(r['rating']) ?? 0,
          content: r['review'],
          isNegative: r['rating'] == '0',
        );

      return review;
    })?.toList();

    return reviews ?? [];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
          ),
        ),
        title: Text(
          'REVIEWS',
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      body: FutureBuilder<List<Review>>(
        future: _reviewsFuture,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              return Center(
                child: CupertinoActivityIndicator(),
              );
            case ConnectionState.done:
              if (snapshot.hasError || !snapshot.hasData) {
                return Container();
              }

              final reviews = snapshot.data;

              return ListView.separated(
                padding: EdgeInsets.symmetric(
                  vertical: ScreenUtil().setHeight(18),
                  horizontal: ScreenUtil().setWidth(8),
                ),
                itemCount: reviews?.length ?? 0,
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(height: ScreenUtil().setHeight(11));
                },
                itemBuilder: (BuildContext context, int index) {
                  final review = reviews[index];
                  final fullName =
                      "${review.author.firstName} ${review.author.lastName}";
                  final date = DateFormat('dd MMMM y').format(review.date);
                  final rating = review.rating;
                  final content = review.content;

                  return Container(
                    width: MediaQuery.of(context).size.width -
                        ScreenUtil().setWidth(8.0 * 2),
                    padding: EdgeInsets.symmetric(
                      vertical: ScreenUtil().setHeight(21),
                      horizontal: ScreenUtil().setWidth(24),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.11),
                          blurRadius: 40,
                        ),
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              fullName,
                              style:
                                  Theme.of(context).textTheme.subtitle.copyWith(
                                        fontSize: ScreenUtil().setSp(14),
                                        letterSpacing: 0,
                                      ),
                            ),
                            Text(
                              date,
                              style:
                                  Theme.of(context).textTheme.display1.copyWith(
                                        fontSize: ScreenUtil().setSp(10),
                                      ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(6)),
                        RatingBar(
                          initialRating: rating,
                          ignoreGestures: true,
                          allowHalfRating: true,
                          onRatingUpdate: (_) {},
                          itemSize: ScreenUtil().setHeight(11.4),
                          itemPadding: EdgeInsets.symmetric(
                            horizontal: 2,
                          ),
                          ratingWidget: RatingWidget(
                            full: Image(
                              image: AssetImage('assets/images/star-full.png'),
                            ),
                            half: Image(
                              image: AssetImage('assets/images/star-half.png'),
                            ),
                            empty: Image(
                              image: AssetImage('assets/images/star-empty.png'),
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(22.4)),
                        Text(
                          content,
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(11),
                              ),
                        ),
                      ],
                    ),
                  );
                },
              );
          }

          return Container();
        },
      ),
    );
  }
}
