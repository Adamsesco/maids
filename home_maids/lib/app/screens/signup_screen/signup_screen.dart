import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/client_dashboard_screen/client_dashboard_screen.dart';
import 'package:home_maids/app/screens/signup_screen/widgets/signup_details.dart';
import 'package:home_maids/app/screens/signup_screen/widgets/signup_form.dart';
import 'package:home_maids/app/services/users_service.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/types/signup_data.dart';
import 'package:home_maids/app/types/signup_method.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  UsersService _usersService;
  PageController _pageController;
  int _page = 0;

  SignupData _signupData;
  bool _isLoading;

  @override
  void initState() {
    super.initState();

    _usersService = UsersService();
    _pageController = PageController();
    _signupData = SignupData();
    _isLoading = false;
  }

  Future<bool> _signup() async {
    setState(() {
      _isLoading = true;
    });

    final client = Client()
      ..setData(
        firstName: _signupData.firstName,
        lastName: _signupData.lastName,
        email: _signupData.email,
        phoneNumber: _signupData.phoneNumber,
        addresses: [_signupData.address],
        completedOrdersCount: 0,
        canceledOrdersCount: 0,
        signupMethod: SignupMethod.email,
        dateCreated: DateTime.now(),
      );
    final user =
        await _usersService.signUpWithEmail(client, _signupData.password);

    if (user == null) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(S.of(context).Signup_error),
          content:
              Text(S.of(context).An_unexpected_error_happened_Please_try_again),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );

      final client2 = Provider.of<Client>(context, listen: false);
      client2.copyFrom(client);

      setState(() {
        _isLoading = false;
      });

      return false;
    }

    setState(() {
      _isLoading = false;
    });

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        brightness: Brightness.light,
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            if (_page == 0) {
              Navigator.of(context).pop();
            } else {
              _pageController.previousPage(
                  duration: Duration(milliseconds: 200), curve: Curves.linear);
            }
          },
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            color: Palette.royalBlue,
            matchTextDirection: true,
          ),
        ),
        title: Image(
          image: AssetImage('assets/images/logo.png'),
          height: ScreenUtil().setHeight(28),
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Spacer(),
            Text(
              S.of(context).IN_ORDER_TO_COMPLETE_THIS_WE_WILL_NEED_SOME_DETAILS,
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(15),
                    fontWeight: FontWeight.w300,
                    color: Palette.royalBlue,
                  ),
            ),
            Spacer(),
            Expanded(
              flex: 14,
              child: PageView(
                controller: _pageController,
                onPageChanged: (int page) {
                  setState(() {
                    _page = page;
                  });
                },
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  SignupForm(
                    signupData: _signupData,
                    onUpdate: () {
                      setState(() {});
                    },
                    onComplete: () {
                      setState(() {
                        _pageController.nextPage(
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      });
                    },
                  ),
                  SignupDetails(
                    signupData: _signupData,
                  ),
                ],
              ),
            ),
            Spacer(),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: S.of(context).By_creating_an_account_you_agree_to_our,
                style: TextStyle(
                  fontFamily: 'SF UI Text',
                  fontSize: ScreenUtil().setSp(10),
                  fontWeight: FontWeight.w300,
                  color: Palette.cadetBlue,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: S.of(context).Terms_of_service,
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        print('Terms of service');
                      },
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: S.of(context).and,
                  ),
                  TextSpan(
                    text: S.of(context).Privacy_policy,
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        print('Privacy policy');
                      },
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            Builder(builder: (context) {
              bool canContinue = false;

              if (_page == 0) {
                canContinue = _signupData.firstName != null &&
                    _signupData.firstName.isNotEmpty &&
                    _signupData.lastName != null &&
                    _signupData.lastName.isNotEmpty &&
                    _signupData.email != null &&
                    _signupData.email.isNotEmpty &&
                    _signupData.password != null &&
                    _signupData.password.isNotEmpty;
              } else {
                canContinue = true;
              }

              print("Can continue: $canContinue");

              return InkWell(
                onTap: canContinue
                    ? () async {
                        if (_page == 0) {
                          _pageController.nextPage(
                              duration: Duration(milliseconds: 200),
                              curve: Curves.linear);
                        } else if (_page == 1) {
                          final signupResult = await _signup();
                          if (signupResult) {
                            Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    ClientDashboardScreen(),
                              ),
                            );
                          }
                        }
                      }
                    : null,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                child: Container(
                  width: ScreenUtil().setWidth(332),
                  height: ScreenUtil().setHeight(52),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Palette.athensGray,
                    gradient: canContinue
                        ? LinearGradient(
                            colors: <Color>[
                              Palette.heliotrope,
                              Palette.royalBlue2,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          )
                        : null,
                  ),
                  child: _isLoading
                      ? CupertinoActivityIndicator()
                      : Text(
                          S.of(context).CONTINUE,
                          style: Theme.of(context).textTheme.button.copyWith(
                                letterSpacing: 0.70,
                                color: canContinue
                                    ? Colors.white
                                    : Palette.cadetBlue,
                              ),
                        ),
                ),
              );
            }),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
