import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/signup_data.dart';
import 'package:home_maids/generated/i18n.dart';

class SignupDetails extends StatelessWidget {
  final SignupData signupData;
  final TextEditingController _firstNameController;
  final TextEditingController _lastNameController;
  final TextEditingController _emailNameController;

  final FocusNode _addressFocusNode = FocusNode();

  SignupDetails({@required this.signupData})
      : _firstNameController =
            TextEditingController(text: signupData.firstName),
        _lastNameController = TextEditingController(text: signupData.lastName),
        _emailNameController = TextEditingController(text: signupData.email),
        super();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(21),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: _firstNameController,
              enabled:
                  signupData.firstName == null || signupData.firstName.isEmpty,
              style: Theme.of(context).textTheme.display4.copyWith(
                    fontWeight: FontWeight.w300,
                    color: Palette.cadetBlue,
                  ),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(40),
                  vertical: ScreenUtil().setHeight(17),
                ),
                hintText: S.of(context).First_name,
                hintStyle: Theme.of(context).textTheme.display4.copyWith(
                      fontWeight: FontWeight.w300,
                      color: Palette.cadetBlue,
                    ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 1,
                  ),
                ),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(13)),
            TextField(
              controller: _lastNameController,
              enabled:
                  signupData.lastName == null || signupData.lastName.isEmpty,
              style: Theme.of(context).textTheme.display4.copyWith(
                    fontWeight: FontWeight.w300,
                    color: Palette.cadetBlue,
                  ),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(40),
                  vertical: ScreenUtil().setHeight(17),
                ),
                hintText: S.of(context).Last_name,
                hintStyle: Theme.of(context).textTheme.display4.copyWith(
                      fontWeight: FontWeight.w300,
                      color: Palette.cadetBlue,
                    ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 1,
                  ),
                ),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(13)),
            TextField(
              controller: _emailNameController,
              enabled: signupData.email == null || signupData.email.isEmpty,
              style: Theme.of(context).textTheme.display4.copyWith(
                    fontWeight: FontWeight.w300,
                    color: Palette.cadetBlue,
                  ),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(40),
                  vertical: ScreenUtil().setHeight(17),
                ),
                hintText: S.of(context).Email,
                hintStyle: Theme.of(context).textTheme.display4.copyWith(
                      fontWeight: FontWeight.w300,
                      color: Palette.cadetBlue,
                    ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 1,
                  ),
                ),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(13)),
            TextField(
              onChanged: (String value) {
                signupData.phoneNumber = value;
              },
              textInputAction: TextInputAction.next,
              onEditingComplete: () =>
                  FocusScope.of(context).requestFocus(_addressFocusNode),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(40),
                  vertical: ScreenUtil().setHeight(17),
                ),
                hintText: S.of(context).Phone_N,
                hintStyle: Theme.of(context).textTheme.display4.copyWith(
                      fontWeight: FontWeight.w300,
                      color: Palette.cadetBlue,
                    ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 1,
                  ),
                ),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(13)),
            TextField(
              focusNode: _addressFocusNode,
              onChanged: (String value) {
                signupData.address = Address(value, '');
              },
              textInputAction: TextInputAction.done,
              maxLines: 4,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(40),
                  vertical: ScreenUtil().setHeight(17),
                ),
                hintText: S.of(context).Address,
                hintStyle: Theme.of(context).textTheme.display4.copyWith(
                      fontWeight: FontWeight.w300,
                      color: Palette.cadetBlue,
                    ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 1,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
