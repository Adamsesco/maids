import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/services/users_service.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/types/signup_data.dart';
import 'package:home_maids/app/types/signup_method.dart';
import 'package:home_maids/generated/i18n.dart';

class SignupForm extends StatefulWidget {
  final SignupData signupData;
  final Function onComplete;
  final Function onUpdate;

  SignupForm(
      {@required this.signupData, @required this.onComplete, this.onUpdate})
      : super();

  @override
  _SignupFormState createState() => _SignupFormState();
}

class _SignupFormState extends State<SignupForm> {
  UsersService _usersService;
  FocusNode _lastNameFocusNode;
  FocusNode _emailFocusNode;
  FocusNode _passwordFocusNode;

  @override
  void initState() {
    super.initState();

    _usersService = UsersService();
    _lastNameFocusNode = FocusNode();
    _emailFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
  }

  @override
  void dispose() {
    _lastNameFocusNode.dispose();
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: () async {
              final user = await _usersService.getGoogleUser() as Client;

              if (user == null) {
                showCupertinoDialog(
                  context: context,
                  builder: (context) => CupertinoAlertDialog(
                    title: Text(S.of(context).An_error_occurred),
                    content: Text(S
                        .of(context)
                        .An_error_occurred_while_trying_to_authenticate_you_in_with_Google),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text(S.of(context).Ok),
                      ),
                    ],
                  ),
                );

                return;
              }

              widget.signupData.firstName = user.firstName;
              widget.signupData.lastName = user.lastName;
              widget.signupData.email = user.email;
              widget.signupData.signupMethod = SignupMethod.google;
              widget.signupData.idToken = user.token;
              print(widget.signupData);

              widget.onComplete();
            },
            child: Container(
              width: ScreenUtil().setWidth(332),
              height: ScreenUtil().setHeight(52),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Palette.alabaster,
              ),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    left: ScreenUtil().setWidth(48.2),
                    child: Image(
                      image: AssetImage('assets/images/google-logo-icon.png'),
                    ),
                  ),
                  Text(
                    S.of(context).SIGN_UP_WITH_GOOGLE,
                    style: Theme.of(context).textTheme.button.copyWith(
                          fontSize: ScreenUtil().setSp(10),
                          fontWeight: FontWeight.w800,
                          letterSpacing: 0.8,
                          color: Palette.royalBlue,
                        ),
                  ),
                ],
              ),
            ),
          ),
          Spacer(),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 1,
                  color: Colors.black.withOpacity(0.08),
                ),
              ),
              SizedBox(width: ScreenUtil().setWidth(23)),
              Text(
                S.of(context).OR_SIGN_UP_WITH_EMAIL,
                style: Theme.of(context).textTheme.button.copyWith(
                      fontSize: ScreenUtil().setSp(10),
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1.2,
                      color: Palette.fiord,
                    ),
              ),
              SizedBox(width: ScreenUtil().setWidth(23)),
              Expanded(
                child: Container(
                  height: 1,
                  color: Colors.black.withOpacity(0.08),
                ),
              ),
            ],
          ),
          Spacer(),
          Expanded(
            flex: 10,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(21),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextField(
                      onChanged: (String value) {
                        widget.signupData.firstName = value;
                        if (widget.onUpdate != null) {
                          widget.onUpdate();
                        }
                      },
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        FocusScope.of(context).requestFocus(_lastNameFocusNode);
                      },
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(40),
                          vertical: ScreenUtil().setHeight(17),
                        ),
                        hintText: S.of(context).First_name,
                        hintStyle:
                            Theme.of(context).textTheme.display4.copyWith(
                                  fontWeight: FontWeight.w300,
                                  color: Palette.cadetBlue,
                                ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 0.3,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 1,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(13)),
                    TextField(
                      focusNode: _lastNameFocusNode,
                      onChanged: (String value) {
                        widget.signupData.lastName = value;
                        if (widget.onUpdate != null) {
                          widget.onUpdate();
                        }
                      },
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () {
                        _lastNameFocusNode.unfocus();
                        FocusScope.of(context).requestFocus(_emailFocusNode);
                      },
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(40),
                          vertical: ScreenUtil().setHeight(17),
                        ),
                        hintText: S.of(context).Last_name,
                        hintStyle:
                            Theme.of(context).textTheme.display4.copyWith(
                                  fontWeight: FontWeight.w300,
                                  color: Palette.cadetBlue,
                                ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 0.3,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 1,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(13)),
                    TextField(
                      focusNode: _emailFocusNode,
                      onChanged: (String value) {
                        widget.signupData.email = value;
                        if (widget.onUpdate != null) {
                          widget.onUpdate();
                        }
                      },
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () {
                        _emailFocusNode.unfocus();
                        FocusScope.of(context).requestFocus(_passwordFocusNode);
                      },
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(40),
                          vertical: ScreenUtil().setHeight(17),
                        ),
                        hintText: S.of(context).Email,
                        hintStyle:
                            Theme.of(context).textTheme.display4.copyWith(
                                  fontWeight: FontWeight.w300,
                                  color: Palette.cadetBlue,
                                ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 0.3,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 1,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(13)),
                    TextField(
                      focusNode: _passwordFocusNode,
                      onChanged: (String value) {
                        widget.signupData.password = value;
                        if (widget.onUpdate != null) {
                          widget.onUpdate();
                        }
                      },
                      textInputAction: TextInputAction.done,
                      obscureText: true,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(40),
                          vertical: ScreenUtil().setHeight(17),
                        ),
                        hintText: S.of(context).Password,
                        hintStyle:
                            Theme.of(context).textTheme.display4.copyWith(
                                  fontWeight: FontWeight.w300,
                                  color: Palette.cadetBlue,
                                ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 0.3,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 1,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
