import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:hive/hive.dart';
import 'package:home_maids/app/screens/login_screen/login_screen.dart';
import 'package:home_maids/app/screens/reviews_screen/reviews_screen.dart';
import 'package:home_maids/app/services/users_service.dart';
import 'package:home_maids/app/stores/incomes_store.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/income.dart';
import 'package:home_maids/app/widgets/hmbutton/hmbutton.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:provider/provider.dart';

class CompanyProfileScreen extends StatefulWidget {
  @override
  _CompanyProfileScreenState createState() => _CompanyProfileScreenState();
}

class _CompanyProfileScreenState extends State<CompanyProfileScreen> {
  GlobalKey<FormState> _formKey;
  File _avatarFile;
  Company _company;
  String _companyName;
  String _email;
  String _phoneNumber;
  Address _address;
  String _description;
  double _rating;
  bool _isLoading;
  bool _isUpdated;

  @override
  void initState() {
    super.initState();

    _formKey = GlobalKey();
    _company = Provider.of<Company>(context, listen: false);
    _companyName = _company.name;
    _email = _company.email;
    _phoneNumber = _company.phoneNumber;
    _address = _company.address;
    _description = _company.description;
    _rating = _company.rating;

    _isLoading = false;
    _isUpdated = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        title: Text(
          S.of(context).PROFILE,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      bottomNavigationBar: CompanyNavigationBar(currentIndex: 2),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: ScreenUtil().setHeight(60),
                    color: Palette.royalBlue,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(11),
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(26),
                      vertical: ScreenUtil().setHeight(26),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(13),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.16),
                          blurRadius: 35,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                _pickAvatar();
                              },
                              child: Stack(
                                children: <Widget>[
                                  Avatar(
                                    _company.avatarUrl,
                                    avatarFile: _avatarFile,
                                    width: ScreenUtil().setWidth(95),
                                    height: ScreenUtil().setWidth(92),
                                    hasShadow: true,
                                  ),
                                  Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(9),
                                        vertical: ScreenUtil().setHeight(7),
                                      ),
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/update-icon.png'),
                                        width: ScreenUtil().setWidth(19.19),
                                        height: ScreenUtil().setHeight(15.85),
                                        color: Palette.grape,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: ScreenUtil().setWidth(30)),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _company.name,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle
                                      .copyWith(
                                        fontSize: ScreenUtil().setSp(15),
                                        letterSpacing: 0,
                                      ),
                                ),
                                Text(
                                  "${S.of(context).Since} ${DateFormat('dd MMMM y').format(_company.dateCreated)}",
                                  style: Theme.of(context)
                                      .textTheme
                                      .display1
                                      .copyWith(
                                        fontSize: ScreenUtil().setSp(11),
                                        fontWeight: FontWeight.w300,
                                      ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(10)),
                                InkWell(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      PageRouteBuilder(
                                        pageBuilder: (context, animation,
                                                secondaryAnimation) =>
                                            ReviewsScreen(company: _company),
                                        transitionsBuilder: (context, animation,
                                            secondaryAnimation, child) {
                                          return child;
                                        },
                                      ),
                                    );
                                  },
                                  child: Row(
                                    children: <Widget>[
                                      RatingBar(
                                        initialRating: _rating,
                                        ignoreGestures: true,
                                        allowHalfRating: true,
                                        onRatingUpdate: (_) {},
                                        itemSize: ScreenUtil().setHeight(11.4),
                                        itemPadding: EdgeInsets.symmetric(
                                          horizontal: 2,
                                        ),
                                        ratingWidget: RatingWidget(
                                          full: Image(
                                            image: AssetImage(
                                                'assets/images/star-full.png'),
                                          ),
                                          half: Image(
                                            image: AssetImage(
                                                'assets/images/star-half.png'),
                                          ),
                                          empty: Image(
                                            image: AssetImage(
                                                'assets/images/star-empty.png'),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                          width: ScreenUtil().setWidth(12)),
                                      Text(
                                        "${_company.reviews == null ? 0 : _company.reviews.length} ${S.of(context).Reviews}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .display1
                                            .copyWith(
                                              fontSize: ScreenUtil().setSp(10),
                                              color: Palette.orangePeel,
                                            ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Spacer(),
                            SizedBox(
                              height: ScreenUtil().setWidth(92),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  InkWell(
                                    onTap: () async {
                                      await UsersService().signOut();
                                      Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(
                                          builder: (context) => LoginScreen(),
                                        ),
                                      );
                                    },
                                    child: Image(
                                      image: AssetImage(
                                          'assets/images/logout-icon.png'),
                                      matchTextDirection: true,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(25)),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(11),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Text(
                                    _company.jobsDone.toString(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .copyWith(
                                          fontSize: ScreenUtil().setSp(14),
                                          letterSpacing: 0,
                                          color: Palette.fiord,
                                        ),
                                  ),
                                  Text(
                                    S.of(context).Jobs_Done,
                                    style: Theme.of(context)
                                        .textTheme
                                        .display1
                                        .copyWith(
                                          fontSize: ScreenUtil().setSp(11),
                                          fontWeight: FontWeight.w300,
                                          color: Palette.fiord,
                                        ),
                                  ),
                                ],
                              ),
                              Column(
                                children: <Widget>[
                                  Text(
                                    _rating.toStringAsFixed(1),
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .copyWith(
                                          fontSize: ScreenUtil().setSp(14),
                                          letterSpacing: 0,
                                          color: Palette.fiord,
                                        ),
                                  ),
                                  Text(
                                    S.of(context).Rating,
                                    style: Theme.of(context)
                                        .textTheme
                                        .display1
                                        .copyWith(
                                          fontSize: ScreenUtil().setSp(11),
                                          fontWeight: FontWeight.w300,
                                          color: Palette.fiord,
                                        ),
                                  ),
                                ],
                              ),
                              FutureBuilder<List<Income>>(
                                  future: Provider.of<IncomesStore>(context,
                                          listen: false)
                                      .yearlyIncomes,
                                  builder: (context, snapshot) {
                                    double balance = 0;

                                    switch (snapshot.connectionState) {
                                      case ConnectionState.none:
                                      case ConnectionState.waiting:
                                      case ConnectionState.active:
                                        balance = 0;
                                        break;
                                      case ConnectionState.done:
                                        if (snapshot.hasData &&
                                            !snapshot.hasError) {
                                          balance = snapshot.data.fold(
                                              0,
                                              (double acc, Income income) =>
                                                  acc + income.amount);
                                        }
                                        break;
                                    }

                                    return Column(
                                      children: <Widget>[
                                        Text(
                                          "${balance.toStringAsFixed(3)} ${_company.currency}",
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle
                                              .copyWith(
                                                fontSize:
                                                    ScreenUtil().setSp(14),
                                                letterSpacing: 0,
                                                color: Palette.fiord,
                                              ),
                                        ),
                                        Text(
                                          S.of(context).Available_Balance,
                                          style: Theme.of(context)
                                              .textTheme
                                              .display1
                                              .copyWith(
                                                fontSize:
                                                    ScreenUtil().setSp(11),
                                                fontWeight: FontWeight.w300,
                                                color: Palette.fiord,
                                              ),
                                        ),
                                      ],
                                    );
                                  }),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: ScreenUtil().setHeight(29)),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(29),
                ),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      _TextField(
                        onChanged: (String value) {
                          _companyName = value;
                          setState(() {
                            _isUpdated = false;
                          });
                        },
                        initialValue: _companyName,
                        hintText: S.of(context).Company_name,
                        prefix: Image(
                          image: AssetImage(
                              'assets/images/noun_building_1987214.png'),
                          width: ScreenUtil().setWidth(10.4),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(12)),
                      _TextField(
                        onChanged: (String value) {
                          _email = value;
                          setState(() {
                            _isUpdated = false;
                          });
                        },
                        initialValue: _email,
                        hintText: S.of(context).Email_address,
                        prefix: Image(
                          image: AssetImage('assets/images/email-icon.png'),
                          width: ScreenUtil().setWidth(10.18),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(12)),
                      _TextField(
                        onChanged: (String value) {
                          _phoneNumber = value;
                          setState(() {
                            _isUpdated = false;
                          });
                        },
                        initialValue: _phoneNumber,
                        hintText: S.of(context).Phone_number,
                        prefix: Image(
                          image:
                              AssetImage('assets/images/Component 12 – 2.png'),
                          width: ScreenUtil().setWidth(12.28),
                          matchTextDirection: true,
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(23)),
                      _TextField(
                        onChanged: (String value) {
                          _address = Address(value, '');
                          setState(() {
                            _isUpdated = false;
                          });
                        },
                        initialValue: _address.address,
                        hintText: S.of(context).Address,
                        maxLines: 3,
                        prefix: Image(
                          image:
                              AssetImage('assets/images/Component 11 – 2.png'),
                          width: ScreenUtil().setWidth(15.28),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(17)),
                      _TextField(
                        onChanged: (String value) {
                          _description = value;
                          setState(() {
                            _isUpdated = false;
                          });
                        },
                        initialValue: _description,
                        hintText: S.of(context).Description,
                        maxLines: 5,
                        prefix: Image(
                          image: AssetImage(
                              'assets/images/noun_description_2228899.png'),
                          width: ScreenUtil().setWidth(15.62),
                          matchTextDirection: true,
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(21)),
                      Row(
                        children: <Widget>[
                          SizedBox(width: ScreenUtil().setWidth(15.62 + 14.6)),
                          Expanded(
                            child: Row(
                              children: <Widget>[
                                HMButton(
                                  onTap: () {
                                    _update();
                                  },
                                  isLoading: _isLoading,
                                  done: _isUpdated,
                                  text: Text(
                                    S.of(context).UPDATE,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .copyWith(
                                          fontSize: ScreenUtil().setSp(13),
                                          letterSpacing: 0,
                                          color: Palette.cornflowerBlue,
                                        ),
                                  ),
                                  doneText: Text(
                                    S.of(context).UPDATED,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .copyWith(
                                          fontSize: ScreenUtil().setSp(13),
                                          letterSpacing: 0,
                                          color: Palette.cornflowerBlue,
                                        ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(18.3)),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _pickAvatar() async {
    final image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        _avatarFile = image;
        _isUpdated = false;
      });
    }
  }

  Future<void> _update() async {
    setState(() {
      _isLoading = true;
    });

    String avatarBase64;
    if (_avatarFile != null) {
      final fileContent = await _avatarFile.readAsBytes();
      avatarBase64 = base64.encode(fileContent);
    }

    final formData = FormData.fromMap({
      'email': _email,
      'phone': _phoneNumber,
      'company_name': _companyName,
      'address': _address.address,
      'description': _description,
      if (avatarBase64 != null && avatarBase64.isNotEmpty)
        'avatar': "data:image/png;base64,$avatarBase64",
    });
    print(formData);

    final response = await Dio().post<String>(
        "http://washy-car.com/homemaids/api/edit/profile/${_company.token}",
        data: formData);
    if (response == null ||
        response.statusCode != 200 ||
        response.data == null ||
        response.data.isEmpty) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(S.of(context).Error),
          content:
              Text(S.of(context).An_unexpected_error_happened_Please_try_again),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });
      return;
    }

    print(response.data);
    final responseJson = jsonDecode(response.data);
    final status = responseJson['status'];
    final code = responseJson['code'];
    if (status != 200 || code != 1) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(S.of(context).Error),
          content:
              Text(S.of(context).An_unexpected_error_happened_Please_try_again),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });
      return;
    }

    final data = responseJson['data'];
    print(data);
    _company.setData(
      email: data['email'],
      avatarUrl: data['avatar'],
      phoneNumber: data['phone'],
      address: Address(data['address'], ''),
      name: data['company_name'] ?? _companyName,
      description: data['description'] ?? _description,
    );

    final companyData = _company.toMap();
    final userDb = Hive.box<Map<dynamic, dynamic>>('user');
    await userDb.delete('user');
    userDb.put('user', companyData);

    setState(() {
      _isLoading = false;
      _isUpdated = true;
    });
  }
}

typedef void OnChanged(String value);
typedef void Validator(String value);

class _TextField extends StatelessWidget {
  final String initialValue;
  final Widget prefix;
  final int maxLines;
  final String hintText;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final OnChanged onChanged;

  _TextField({
    this.initialValue = '',
    this.prefix,
    this.maxLines = 1,
    this.hintText,
    this.keyboardType = TextInputType.text,
    this.textInputAction = TextInputAction.none,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (prefix != null) ...[
          prefix,
          SizedBox(width: ScreenUtil().setWidth(14.6)),
        ],
        Expanded(
          child: TextFormField(
            onChanged: onChanged,
            initialValue: initialValue,
            maxLines: maxLines,
            keyboardType: keyboardType,
            textInputAction: textInputAction,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(21),
                vertical: ScreenUtil().setHeight(maxLines > 1 ? 25 : 7),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Palette.cadetBlue,
                  width: 0.3,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Palette.cadetBlue,
                  width: 0.3,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Palette.cadetBlue,
                  width: 0.3,
                ),
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 0.3,
                ),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 0.3,
                ),
              ),
              hintText: hintText,
              hintStyle: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(11),
                    color: Palette.fiord.withOpacity(0.6),
                  ),
            ),
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontSize: ScreenUtil().setSp(11),
                  color: Palette.fiord,
                ),
          ),
        ),
      ],
    );
  }
}
