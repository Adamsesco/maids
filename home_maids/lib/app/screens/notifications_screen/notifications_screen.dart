import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/messages_screen/messages_screen.dart';
import 'package:home_maids/app/screens/notifications_list_screen/notifications_list_screen.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/messages_store.dart';
import 'package:home_maids/app/stores/notifications_store.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/app/widgets/client_navigation_bar/client_navigation_bar.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class NotificationsScreen extends StatefulWidget {
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  User _me;
  NotificationsStore _notificationsStore;
  MessagesStore _messagesStore;

  @override
  void initState() {
    super.initState();

    final client = Provider.of<Client>(context, listen: false);
    final company = Provider.of<Company>(context, listen: false);
    if (client != null && client.id != null) {
      _me = client;
    } else if (company != null && company.id != null) {
      _me = company;
    }

    _notificationsStore =
        Provider.of<NotificationsStore>(context, listen: false);
    _messagesStore = Provider.of<MessagesStore>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        leading: Container(),
        centerTitle: true,
        title: Text(
          S.of(context).NOTIFICATIONS,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      bottomNavigationBar: _me.role == UserRole.company
          ? CompanyNavigationBar(currentIndex: 1)
          : ClientNavigationBar(currentIndex: 2),
      body: ListView(
        padding: EdgeInsets.symmetric(
          vertical: ScreenUtil().setHeight(16),
        ),
        children: <Widget>[
          InkWell(
            onTap: () => Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    NotificationsListScreen(),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return child;
                },
              ),
            ),
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            child: Observer(
              builder: (BuildContext context) {
                _NotificationData data;

                if (_notificationsStore.notifications.isNotEmpty) {
                  final notification = _notificationsStore.notifications.last;
                  final avatarUrl = notification.avatarUrl;
                  final name =
                      "${notification.firstName} ${notification.lastName}";

                  data = _NotificationData(
                    avatarUrl: avatarUrl,
                    name: name,
                    content: notification.body,
                    date: notification.date ?? DateTime.now(),
                  );
                }

                return _NotificationsCard(
                  title: S.of(context).NOTIFICATIONS,
                  subtitle: "${_notificationsStore.unreadCount} ${S.of(context).Notifications}",
                  data: [data],
                );
              },
            ),
          ),
          SizedBox(height: ScreenUtil().setHeight(16)),
          InkWell(
            onTap: () => Navigator.of(context).push(
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    MessagesScreen(),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  return child;
                },
              ),
            ),
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            child: Observer(
              builder: (BuildContext context) {
                _NotificationData data;

                if (_messagesStore.messages.isNotEmpty) {
                  final message = _messagesStore.messages
                      .where((message) =>
                          message.sender.id != _me.id && !message.isArchived)
                      .last;
                  String avatarUrl;
                  String name;

                  final sender = message.sender;
                  switch (sender.role) {
                    case UserRole.anonymous:
                      // TODO: Handle this case.
                      break;

                    case UserRole.client:
                      final client = sender as Client;
                      avatarUrl = client.avatarUrl;
                      name = "${client.firstName} ${client.lastName}";
                      break;

                    case UserRole.company:
                      final company = sender as Company;
                      avatarUrl = company.avatarUrl;
                      name = company.name;
                      break;
                  }

                  data = _NotificationData(
                    avatarUrl: avatarUrl,
                    name: name,
                    content: message.content,
                    date: message.date,
                  );
                }

                return _NotificationsCard(
                  title: S.of(context).MESSAGES,
                  subtitle: "${_messagesStore.unreadCount} ${S.of(context).New_Messages}",
                  data: [data],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _NotificationData {
  final String avatarUrl;
  final String name;
  final String content;
  final DateTime date;

  _NotificationData({this.avatarUrl, this.name, this.content, this.date});
}

class _NotificationsCard extends StatefulWidget {
  final String title;
  final String subtitle;
  final List<_NotificationData> data;

  _NotificationsCard(
      {@required this.title, @required this.subtitle, @required this.data});

  @override
  __NotificationsCardState createState() => __NotificationsCardState();
}

class __NotificationsCardState extends State<_NotificationsCard> {
  @override
  Widget build(BuildContext context) {
    return LimitedBox(
      // maxHeight:
      //     (3 * ScreenUtil().setWidth(55)) + (2 * ScreenUtil().setHeight(18)),
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(8),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(30),
          vertical: ScreenUtil().setHeight(18),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.11),
              blurRadius: 40,
            ),
          ],
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  widget.title,
                  style: Theme.of(context).textTheme.subtitle,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      widget.subtitle,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(11),
                            fontWeight: FontWeight.w800,
                            color: Palette.cornflowerBlue,
                          ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(7)),
                  ],
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(26)),
            if (widget.data != null &&
                widget.data.length > 0 &&
                widget.data.first != null)
              ListView.separated(
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                separatorBuilder: (context, index) =>
                    SizedBox(height: ScreenUtil().setHeight(18)),
                itemCount: widget.data?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  final data = widget.data[index];

                  return Row(
                    children: <Widget>[
                      Avatar(
                        data.avatarUrl,
                        width: ScreenUtil().setWidth(55),
                        height: ScreenUtil().setWidth(55),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(16)),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  data.name,
                                  style: Theme.of(context)
                                      .textTheme
                                      .display1
                                      .copyWith(
                                        fontWeight: FontWeight.w800,
                                        color: Palette.royalBlue,
                                      ),
                                ),
                                Text(
                                  DateFormat('dd MMMM y').format(data.date),
                                  style: Theme.of(context)
                                      .textTheme
                                      .display1
                                      .copyWith(
                                        fontSize: ScreenUtil().setSp(9),
                                      ),
                                ),
                              ],
                            ),
                            SizedBox(height: ScreenUtil().setHeight(4)),
                            Text(
                              data.content,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style:
                                  Theme.of(context).textTheme.display1.copyWith(
                                        fontSize: ScreenUtil().setSp(11),
                                        color: Palette.fiord,
                                      ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              ),
          ],
        ),
      ),
    );
  }
}
