import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/data/orders_repository.dart';
import 'package:home_maids/app/screens/services_selection_screen/services_selection_screen.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/new_order_store.dart';
import 'package:home_maids/app/stores/order.dart';
import 'package:home_maids/app/stores/orders_store.dart';
import 'package:home_maids/app/types/order_status.dart';
import 'package:home_maids/app/types/sort.dart';
import 'package:home_maids/app/utils/order_utils.dart';
import 'package:home_maids/app/widgets/client_navigation_bar/client_navigation_bar.dart';
import 'package:home_maids/app/widgets/dropdown/dropdown_item.dart';
import 'package:home_maids/app/widgets/dropdown/dropdown_menu.dart';
import 'package:home_maids/app/widgets/expandible_order_item/expandible_order_item.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class ClientDashboardScreen extends StatefulWidget {
  @override
  _ClientDashboardScreenState createState() => _ClientDashboardScreenState();
}

class _ClientDashboardScreenState extends State<ClientDashboardScreen> {
  Client _client;
  OrdersStore _ordersStore;
  Future _newOrderFuture;
  int _page = 1;
  ScrollController _scrollController = ScrollController();
  bool _isLoading = false;
  OrderStatus _orderStatus = OrderStatus.all;

  @override
  void initState() {
    super.initState();

    _client = Provider.of<Client>(context, listen: false);
    _ordersStore = Provider.of<OrdersStore>(context, listen: false);
    _newOrderFuture = _addNewOrder();

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _getMoreOrders(_ordersStore);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();

    super.dispose();
  }

  Future<void> _getMoreOrders(OrdersStore ordersStore) async {
    final orders = await ordersStore.orders;

    if (_isLoading) {
      return;
    }

    setState(() {
      _isLoading = true;
    });

    _page += 1;

    final newOrders = await OrdersRepository()
        .get(_client, page: _page, status: orderStatusCode(_orderStatus));
    if (newOrders == null || newOrders.isEmpty) {
      setState(() {
        _isLoading = false;
      });

      return;
    }

    if (orders.isEmpty || _orderStatus == OrderStatus.all) {
      orders.addAll(newOrders);
    }

    setState(() {
      _isLoading = false;
      ordersStore.setOrders(ordersStore.orders);
    });
  }

  Future<void> _addNewOrder() async {
    final newOrder = Provider.of<NewOrderStore>(context, listen: false);
    final order = newOrder.order;
    if (order == null) {
      return;
    }
    newOrder.order = null;

    final services = jsonEncode(order.servicesData
        .map((sd) => {
              'service': int.tryParse(sd.service.id),
              'num_of_maids': sd.maidsCount,
              'hours': sd.hours,
            })
        .toList());

    final formData = FormData.fromMap({
      'user_id': _client.id,
      'address': _client.addresses.first.address,
      'services': services,
      'company_id': order.company.id,
      'is_material_required': order.requiresMaterials,
      'price': order.price,
      'date_request': order.date,
    });

    final request = await Dio().post<String>(
        'http://washy-car.com/homemaids/api/addorder',
        data: formData);
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return;
    }

    final responseJson = jsonDecode(request.data);
    final orderNumber = responseJson['order_number'] ?? 0;
    order.number = orderNumber;

    final orders = await _ordersStore.orders;
    orders.insert(0, order);
    _ordersStore.setOrders(_ordersStore.orders);
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: Palette.royalBlue,
      brightness: Brightness.dark,
      centerTitle: true,
      leading: Container(),
      title: Text(
        S.of(context).DASHBOARD,
        style: Theme.of(context).textTheme.headline,
      ),
      actions: <Widget>[
        DropDownMenu<OrderStatus>(
          onChanged: (status) async {
            final orders = await _ordersStore.orders;
            setState(() {
              orders.clear();
              _page = 0;
              _orderStatus = status;
              _getMoreOrders(_ordersStore);
            });
          },
          width: ScreenUtil().setWidth(203),
          button: Image(
            image: AssetImage('assets/images/Component 5 – 2.png'),
            height: ScreenUtil().setHeight(16.01),
            color: Colors.white,
            matchTextDirection: true,
          ),
          head: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Image(
                image: AssetImage('assets/images/Component 5 – 2.png'),
                height: ScreenUtil().setHeight(16.01),
                color: Palette.royalBlue,
              ),
            ],
          ),
          items: [
            DropdownItem<OrderStatus>(
              value: OrderStatus.all,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.royalBlue,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Latest_first,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.all) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
            DropdownItem<OrderStatus>(
              value: OrderStatus.unpaid,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.amber,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Unpaid,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.unpaid) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
            DropdownItem<OrderStatus>(
              value: OrderStatus.done,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.malachite2,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Complete,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.done) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
            DropdownItem<OrderStatus>(
              value: OrderStatus.cancelled,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.cadetBlue,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Canceled,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.cancelled) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
            DropdownItem<OrderStatus>(
              value: OrderStatus.declined,
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(8),
                    height: ScreenUtil().setWidth(8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.mercury,
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(11)),
                  Text(
                    S.of(context).Declined,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(12),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.40,
                          color: Palette.fiord,
                        ),
                  ),
                  if (_orderStatus == OrderStatus.declined) ...[
                    Spacer(),
                    Image(
                      image: AssetImage('assets/images/Component 3 – 2.png'),
                      height: ScreenUtil().setHeight(14),
                      matchTextDirection: true,
                    ),
                  ],
                ],
              ),
            ),
          ],
        ),
      ],
      bottom: PreferredSize(
        preferredSize: Size(MediaQuery.of(context).size.width, 4),
        child: Column(
          children: <Widget>[
            Consumer<OrdersStore>(
              builder: (BuildContext context, OrdersStore ordersStore,
                      Widget child) =>
                  FutureBuilder(
                future: _newOrderFuture,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                    case ConnectionState.active:
                      return SizedBox(
                        height: 4,
                        child: LinearProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Palette.brightTurquoise),
                        ),
                      );
                    case ConnectionState.none:
                    case ConnectionState.done:
                      return SizedBox(height: 4);
                  }

                  return SizedBox(height: 4);
                },
              ),
            ),
          ],
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar,
      bottomNavigationBar: ClientNavigationBar(currentIndex: 0),
      body: FutureBuilder<void>(
        future: _newOrderFuture,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              return Center(child: CupertinoActivityIndicator());
            case ConnectionState.done:
              return Container(
                child: Consumer<OrdersStore>(
                  builder: (BuildContext context, OrdersStore ordersStore,
                          Widget child) =>
                      FutureBuilder(
                    future: ordersStore.orders,
                    builder: (BuildContext context,
                        AsyncSnapshot<List<Order>> snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                        case ConnectionState.active:
                          return Center(child: CupertinoActivityIndicator());
                        case ConnectionState.none:
                          return Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(20),
                            ),
                            child: _NoOrderWidget(),
                          );
                        case ConnectionState.done:
                          if (snapshot.hasError || !snapshot.hasData) {
                            return Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.symmetric(
                                horizontal: ScreenUtil().setWidth(20),
                              ),
                              child: _NoOrderWidget(),
                            );
                          }

                          final orders = snapshot.data;

                          return ListView.builder(
                            controller: _scrollController,
                            padding: EdgeInsets.symmetric(
                              vertical: ScreenUtil().setHeight(7),
                            ),
                            itemCount: orders.length + (_isLoading ? 1 : 0),
                            itemBuilder: (BuildContext context, int index) {
                              if (index < orders.length) {
                                final order = orders[index];
                                return ExpandibleOrderItem(order: order);
                              }

                              return Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(8),
                                ),
                                child: CupertinoActivityIndicator(),
                              );
                            },
                          );
                      }

                      return Container();
                    },
                  ),
                ),
              );
          }

          return Container();
        },
      ),
    );
  }
}

class _NoOrderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            S.of(context).You_dont_have_any_order_yet,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontSize: ScreenUtil().setSp(14),
                ),
          ),
          SizedBox(height: ScreenUtil().setHeight(40)),
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                CupertinoPageRoute(
                  builder: (context) => ServicesSelectionScreen(),
                ),
              );
            },
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(38),
              ),
              height: ScreenUtil().setHeight(52),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Palette.athensGray,
                gradient: LinearGradient(
                  colors: <Color>[
                    Palette.heliotrope,
                    Palette.royalBlue2,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Text(
                S.of(context).NEW_ORDER,
                style: Theme.of(context).textTheme.button.copyWith(
                      letterSpacing: 0.70,
                      color: Colors.white,
                    ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
