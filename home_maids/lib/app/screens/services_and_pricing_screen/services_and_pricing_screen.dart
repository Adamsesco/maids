import 'package:dio/dio.dart';
import 'package:home_maids/app/stores/application_store.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/service.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:provider/provider.dart';

class ServicesAndPricingScreen extends StatefulWidget {
  final Service service;

  ServicesAndPricingScreen({Key key, @required this.service}) : super(key: key);

  @override
  _ServicesAndPricingScreenState createState() =>
      _ServicesAndPricingScreenState();
}

class _ServicesAndPricingScreenState extends State<ServicesAndPricingScreen> {
  Company _company;
  Service _service;
  bool _isEnabled;
  double _price;
  double _oldPrice;

  TextEditingController _priceController;
  TextEditingController _oldPriceController;

  @override
  void initState() {
    super.initState();

    _company = Provider.of<Company>(context, listen: false);

    final companyService = _company.services.firstWhere(
        (service) => service.name['en'] == widget.service.name['en'],
        orElse: () => null);
    _service = Service()
      ..setData(
        id: widget.service.id,
        name: widget.service.name,
        iconUrl: widget.service.iconUrl,
        price: companyService?.price ?? 0.0,
        oldPrice: companyService?.oldPrice,
        isEnabled: companyService?.isEnabled ?? false,
      );

    _isEnabled = _service.isEnabled;
    _price = _service.price;
    _oldPrice = _service.oldPrice;

    _priceController = TextEditingController(text: _price?.toStringAsFixed(2));
    _oldPriceController =
        TextEditingController(text: _oldPrice?.toStringAsFixed(2));
  }

  Future<void> _update() async {
    final service = _service;
    final serviceId = service.id;
    final price = _price;
    final oldPrice = _oldPrice;
    final isEnabled = _isEnabled ? 1 : 0;

    final services =
        '["$serviceId": { "price": $price, "oldprice": $oldPrice, "is_enabled": $isEnabled }]';

    final formData = FormData.fromMap({
      'token': _company.token,
      'services': services,
    });
    Dio().post<String>("http://washy-car.com/homemaids//api/updateServices",
        data: formData);

    service.price = price;
    service.oldPrice = oldPrice;
    service.isEnabled = _isEnabled;
  }

  @override
  Widget build(BuildContext context) {
    final appStore = Provider.of<ApplicationStore>(context, listen: false);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            _update();
            Navigator.of(context).pop();
          },
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        title: Text(
          S.of(context).SERVICES_PRICING,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      bottomNavigationBar: CompanyNavigationBar(currentIndex: 2),
      body: Container(
        child: Column(
          children: <Widget>[
            SizedBox(height: ScreenUtil().setHeight(19)),
            _ServiceItem(
              iconUrl: _service.iconUrl,
              text: _service.name[appStore.locale.languageCode],
              isEnabled: _isEnabled,
              onToggle: (bool value) {
                setState(() {
                  _isEnabled = value;
                });
              },
            ),
            SizedBox(height: ScreenUtil().setHeight(11)),
            Container(
              color: Palette.blackHaze,
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(4),
                right: ScreenUtil().setWidth(28.5),
                bottom: ScreenUtil().setHeight(4),
                left: ScreenUtil().setWidth(21.4),
              ),
              child: Row(
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/images/noun_Money_2449334.png'),
                    width: ScreenUtil().setWidth(17.1),
                    color: _isEnabled ? Palette.fiord : Palette.cadetBlue,
                  ),
                  SizedBox(width: ScreenUtil().setWidth(9.8)),
                  Text(
                    S.of(context).Price,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(14),
                          color: _isEnabled ? Palette.fiord : Palette.cadetBlue,
                        ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(9)),
                  Builder(builder: (context) {
                    final textSpan = TextSpan(
                      text: _priceController.text,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(14),
                            fontWeight: FontWeight.w800,
                            color:
                                _isEnabled ? Palette.salem : Palette.cadetBlue,
                            decoration: _oldPrice == null
                                ? TextDecoration.none
                                : TextDecoration.lineThrough,
                          ),
                    );
                    final textPainter = TextPainter(
                      text: textSpan,
                      textDirection: Directionality.of(context),
                    );
                    textPainter.layout();
                    final textWidth = textPainter.width;

                    return SizedBox(
                      width: textWidth,
                      child: TextField(
                        controller: _priceController,
                        onChanged: (String value) {
                          setState(() {});
                        },
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(14),
                              fontWeight: FontWeight.w800,
                              color: _isEnabled
                                  ? Palette.salem
                                  : Palette.cadetBlue,
                              decoration: _oldPrice == null
                                  ? TextDecoration.none
                                  : TextDecoration.lineThrough,
                            ),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.zero,
                          border: InputBorder.none,
                        ),
                      ),
                    );
                  }),
                  SizedBox(width: ScreenUtil().setWidth(6)),
                  Text(
                    S.of(context).Per_hour,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(11),
                          fontWeight: FontWeight.w300,
                          color: _isEnabled ? Palette.fiord : Palette.cadetBlue,
                        ),
                  ),
                  Spacer(),
                  Text(
                    S.of(context).Discount,
                    style: Theme.of(context).textTheme.display1.copyWith(
                          fontSize: ScreenUtil().setSp(14),
                          color: _isEnabled ? Palette.fiord : Palette.cadetBlue,
                        ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(10.2)),
                  Transform.scale(
                    scale: 0.7,
                    child: CupertinoSwitch(
                      value: _oldPrice != null && _isEnabled,
                      onChanged: (bool value) {
                        if (_isEnabled) {
                          setState(() {
                            if (value) {
                              _oldPrice = _oldPrice ?? _price;
                              _oldPriceController.text =
                                  _oldPrice.toStringAsFixed(2);
                            } else {
                              _oldPrice = null;
                            }
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
            if (_oldPrice != null) ...[
              SizedBox(height: ScreenUtil().setHeight(2)),
              Container(
                color: Palette.blackHaze,
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(13),
                  right: ScreenUtil().setWidth(28.5),
                  bottom: ScreenUtil().setHeight(13),
                  left: ScreenUtil().setWidth(21.4),
                ),
                child: Row(
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/images/noun_Money_2449334.png'),
                      width: ScreenUtil().setWidth(17.1),
                      color: _isEnabled ? Palette.fiord : Palette.cadetBlue,
                    ),
                    SizedBox(width: ScreenUtil().setWidth(9.8)),
                    Text(
                      S.of(context).New_Price,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(14),
                            color:
                                _isEnabled ? Palette.fiord : Palette.cadetBlue,
                          ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(9)),
                    Builder(builder: (context) {
                      final textSpan = TextSpan(
                        text: _oldPriceController.text,
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(14),
                              fontWeight: FontWeight.w800,
                              color: _isEnabled
                                  ? Palette.salem
                                  : Palette.cadetBlue,
                              decoration: _oldPrice == null
                                  ? TextDecoration.none
                                  : TextDecoration.lineThrough,
                            ),
                      );
                      final textPainter = TextPainter(
                        text: textSpan,
                        textDirection: Directionality.of(context),
                      );
                      textPainter.layout();
                      final textWidth = textPainter.width;

                      return SizedBox(
                        width: textWidth,
                        child: TextField(
                          controller: _oldPriceController,
                          onChanged: (String value) {
                            setState(() {});
                          },
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(14),
                                fontWeight: FontWeight.w800,
                                color: _isEnabled
                                    ? Palette.salem
                                    : Palette.cadetBlue,
                              ),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.zero,
                            border: InputBorder.none,
                          ),
                        ),
                      );
                    }),
                    SizedBox(width: ScreenUtil().setWidth(6)),
                    Text(
                      S.of(context).Per_hour,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(11),
                            fontWeight: FontWeight.w300,
                            color:
                                _isEnabled ? Palette.fiord : Palette.cadetBlue,
                          ),
                    ),
                  ],
                ),
              ),
            ],
          ],
        ),
      ),
    );
  }
}

class _ServiceItem extends StatelessWidget {
  final String iconUrl;
  final String text;
  final bool isEnabled;
  final Function(bool) onToggle;

  _ServiceItem(
      {@required this.iconUrl,
      @required this.text,
      @required this.onToggle,
      this.isEnabled = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Palette.blackHaze,
      padding: EdgeInsets.only(
        top: ScreenUtil().setHeight(11.3),
        right: ScreenUtil().setWidth(28.5),
        bottom: ScreenUtil().setHeight(11.3),
        left: ScreenUtil().setWidth(46.1),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: SvgPicture.network(
              iconUrl,
              color: isEnabled ? Palette.cornflowerBlue : Palette.cadetBlue,
              width: ScreenUtil().setWidth(32.84),
              height: ScreenUtil().setHeight(32.34),
            ),
          ),
          Spacer(),
          Expanded(
            flex: 4,
            child: Text(
              text,
              style: Theme.of(context).textTheme.subtitle.copyWith(
                    fontWeight: FontWeight.w300,
                    letterSpacing: -0.40,
                    color: isEnabled ? Palette.fiord : Palette.cadetBlue,
                  ),
            ),
          ),
          Spacer(),
          Transform.scale(
            scale: 0.7,
            child: CupertinoSwitch(
              value: isEnabled,
              onChanged: onToggle,
            ),
          ),
        ],
      ),
    );
  }
}
