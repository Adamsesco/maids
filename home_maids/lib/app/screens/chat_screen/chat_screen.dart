import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:home_maids/app/stores/messages_store.dart';
import 'package:home_maids/app/widgets/client_navigation_bar/client_navigation_bar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:intl/intl.dart' as intl;

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/message.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ChatScreen extends StatefulWidget {
  final Message message;

  ChatScreen({Key key, @required this.message}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  MessagesStore _messagesStore;
  User _me;
  String _myName;
  String _myFirstName;
  String _myLastName;
  String _myAvatarUrl;
  String _myPhoneNumber;
  User _other;
  String _otherName;
  String _otherFirstName;
  String _otherLastName;
  String _otherAvatarUrl;
  String _otherPhoneNumber;
  String _messageContent;
  ScrollController _scrollController;
  TextEditingController _messageTextController;

  @override
  void initState() {
    super.initState();

    _messagesStore = Provider.of<MessagesStore>(context, listen: false);
    _messageContent = '';
    _scrollController = ScrollController();
    _messageTextController = TextEditingController(text: '');

    final client = Provider.of<Client>(context, listen: false);
    final company = Provider.of<Company>(context, listen: false);
    if (client != null && client.id != null) {
      _me = client;
      _myFirstName = client.firstName;
      _myLastName = client.lastName;
      _myPhoneNumber = client.phoneNumber;
      _myAvatarUrl = client.avatarUrl;
    } else if (company != null && company.id != null) {
      _me = company;
      _myName = company.name;
      _myPhoneNumber = company.phoneNumber;
      _myAvatarUrl = company.avatarUrl;
    }

    if (widget.message.sender.id == _me.id) {
      _other = widget.message.recipient;
    } else {
      _other = widget.message.sender;
    }

    switch (_other.role) {
      case UserRole.anonymous:
        // TODO: Handle this case.
        break;
      case UserRole.client:
        final otherClient = _other as Client;
        _otherFirstName = otherClient.firstName;
        _otherLastName = otherClient.lastName;
        _otherPhoneNumber = otherClient.phoneNumber;
        _otherAvatarUrl = otherClient.avatarUrl;
        break;
      case UserRole.company:
        final otherCompany = _other as Company;
        _otherName = otherCompany.name;
        _otherPhoneNumber = otherCompany.phoneNumber;
        _otherAvatarUrl = otherCompany.avatarUrl;
        break;
    }

    _scrollToEnd();
  }

  // Future<void> _markAsRead() async {
  //   final participantsIds = [
  //     widget.message.sender.id,
  //     widget.message.recipient.id
  //   ];
  //   final discussion = _messagesStore.messages
  //           .where((message) =>
  //               participantsIds.contains(message.sender.id) &&
  //               participantsIds.contains(message.recipient.id))
  //           ?.toList() ??
  //       [];
  // }

  Future<void> _sendMessage(String content) async {
    final nowTimeStamp = Timestamp.fromDate(DateTime.now());

    await Firestore.instance.collection('messages').add({
      'content': content,
      'date': nowTimeStamp,
      'has_recipient_archived': false,
      'has_recipient_read': false,
      'has_sender_archived': false,
      'has_sender_read': true,
      'participants_ids': [_me.id, _other.id],
      'recipient_avatar_url': _otherAvatarUrl,
      'recipient_id': _other.id,
      'recipient_name': _otherName,
      'recipient_first_name': _otherFirstName,
      'recipient_last_name': _otherLastName,
      'recipient_phone_number': _otherPhoneNumber,
      'sender_avatar_url': _myAvatarUrl,
      'sender_name': _myName,
      'sender_first_name': _myFirstName,
      'sender_id': _me.id,
      'sender_last_name': _myLastName,
      'sender_phone_number': _myPhoneNumber,
    });
  }

  void _scrollToEnd() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 10),
        curve: Curves.easeOut,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        elevation: 4,
        brightness: Brightness.dark,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        centerTitle: true,
        title: Avatar(
          _otherAvatarUrl,
          width: ScreenUtil().setWidth(41),
          height: ScreenUtil().setWidth(41),
        ),
        actions: <Widget>[
          InkWell(
            onTap: () {
              print(_otherPhoneNumber);
              launch("tel://$_otherPhoneNumber");
            },
            child: Image(
              image: AssetImage('assets/images/Component 6 – 2.png'),
              color: Colors.white,
              matchTextDirection: true,
            ),
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: Text(
            _otherName ?? "$_otherFirstName $_otherLastName",
            style: Theme.of(context).textTheme.subtitle.copyWith(
                  color: Colors.white,
                ),
          ),
        ),
      ),
      bottomNavigationBar: _me.role == UserRole.company
          ? CompanyNavigationBar(currentIndex: 1)
          : ClientNavigationBar(currentIndex: 2),
      body: Observer(builder: (context) {
        final participantsIds = [
          widget.message.sender.id,
          widget.message.recipient.id
        ];
        final discussion = _messagesStore.messages
                .where((message) =>
                    participantsIds.contains(message.sender.id) &&
                    participantsIds.contains(message.recipient.id))
                ?.toList() ??
            [];

        return Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                controller: _scrollController,
                padding: EdgeInsets.zero,
                itemCount: discussion.length,
                itemBuilder: (BuildContext context, int index) {
                  final message = discussion[index];

                  if (message.content == null || message.content.isEmpty) {
                    return Container();
                  }

                  final sender = message.sender;
                  final isSentByMe = message.sender.id == _me.id;

                  String senderAvatarUrl;
                  switch (sender.role) {
                    case UserRole.anonymous:
                      break;
                    case UserRole.client:
                      final client = sender as Client;
                      senderAvatarUrl = client.avatarUrl;
                      break;
                    case UserRole.company:
                      final company = sender as Company;
                      senderAvatarUrl = company.avatarUrl;
                      break;
                  }

                  return Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(23),
                      vertical: ScreenUtil().setHeight(25.0 / 2.0),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        if (!isSentByMe) ...[
                          Avatar(
                            senderAvatarUrl,
                            width: ScreenUtil().setWidth(41),
                            height: ScreenUtil().setWidth(41),
                          ),
                          SizedBox(width: ScreenUtil().setWidth(8)),
                        ],
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(17),
                              vertical: ScreenUtil().setHeight(13),
                            ),
                            decoration: BoxDecoration(
                              color: isSentByMe
                                  ? Palette.blackHaze
                                  : Palette.cornflowerBlue,
                              borderRadius: BorderRadius.circular(14),
                              boxShadow: isSentByMe
                                  ? null
                                  : [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.11),
                                        blurRadius: 10,
                                      ),
                                    ],
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  message.content,
                                  style: Theme.of(context)
                                      .textTheme
                                      .display1
                                      .copyWith(
                                        color: isSentByMe
                                            ? Palette.tundora
                                            : Colors.white,
                                        height: 1.7,
                                      ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(10)),
                                Align(
                                  alignment: AlignmentDirectional.bottomEnd,
                                  child: Text(
                                    intl.DateFormat('HH:mm')
                                        .format(message.date),
                                    style: Theme.of(context)
                                        .textTheme
                                        .display1
                                        .copyWith(
                                          fontSize: ScreenUtil().setSp(10),
                                          color: isSentByMe
                                              ? Palette.cadetBlue
                                              : Colors.white,
                                        ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(17),
                vertical: ScreenUtil().setHeight(19),
              ),
              child: Stack(
                children: <Widget>[
                  Builder(builder: (context) {
                    final textSpan = TextSpan(
                      text: S.of(context).Send,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontWeight: FontWeight.w800,
                            color: Palette.cornflowerBlue,
                          ),
                    );
                    final textPainter = TextPainter(
                      text: textSpan,
                      textDirection: Directionality.of(context),
                    );
                    textPainter.layout();
                    final textWidth = textPainter.width;

                    return TextField(
                      controller: _messageTextController,
                      onChanged: (String value) => _messageContent = value,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(
                          top: ScreenUtil().setHeight(18),
                          right: textWidth + ScreenUtil().setWidth(21.6 * 2),
                          bottom: ScreenUtil().setHeight(18),
                          left: ScreenUtil().setWidth(25),
                        ),
                        hintText: S.of(context).Type_a_message,
                        hintStyle:
                            Theme.of(context).textTheme.display1.copyWith(
                                  fontWeight: FontWeight.w300,
                                  letterSpacing: 1.10,
                                  color: Palette.cadetBlue,
                                ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 0.3,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 0.3,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Palette.cadetBlue,
                            width: 0.3,
                          ),
                        ),
                      ),
                    );
                  }),
                  Positioned(
                    right: Directionality.of(context) == TextDirection.ltr
                        ? 0
                        : null,
                    left: Directionality.of(context) == TextDirection.ltr
                        ? null
                        : 0,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _messageTextController.text = '';
                          _sendMessage(_messageContent);
                          _scrollToEnd();
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(21.6),
                          vertical: ScreenUtil().setHeight(18),
                        ),
                        child: Text(
                          S.of(context).Send,
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontWeight: FontWeight.w800,
                                color: Palette.cornflowerBlue,
                              ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      }),
    );
  }
}
