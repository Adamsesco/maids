import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/client_dashboard_screen/client_dashboard_screen.dart';
import 'package:home_maids/app/screens/login_screen/login_screen.dart';
import 'package:home_maids/app/stores/application_store.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/new_order_store.dart';
import 'package:home_maids/app/stores/order.dart';
import 'package:home_maids/app/stores/service.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/order_status.dart';
import 'package:home_maids/app/types/service_data.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

const _kAlignments = [Alignment.topRight, Alignment.topLeft];

class CheckoutScreen extends StatefulWidget {
  final Company selectedCompany;
  final List<ServiceData> selectedServices;
  final DateTime selectedDateTime;

  CheckoutScreen(
      {Key key,
      @required this.selectedCompany,
      @required this.selectedServices,
      @required this.selectedDateTime})
      : super(key: key);

  @override
  _CheckoutScreenState createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  ApplicationStore _appStore;
  Client _client;

  @override
  void initState() {
    super.initState();

    _appStore = Provider.of<ApplicationStore>(context, listen: false);
    _client = Provider.of<Client>(context, listen: false);
  }

  Future<void> _checkout() async {
    final order = Order()
      ..setData(
        isNew: true,
        status: OrderStatus.pending,
        client: _client,
        company: widget.selectedCompany,
        date: widget.selectedDateTime,
        duration: widget.selectedServices.fold(
            Duration(),
            (Duration acc, ServiceData sd) =>
                Duration(hours: acc.inHours + sd.hours)),
        maidsCount: widget.selectedServices
            .fold(0, (int acc, ServiceData sd) => acc + sd.maidsCount),
        servicesData: widget.selectedServices,
        address: (_client?.addresses ?? [Address('', '')]).first,
        requiresMaterials: widget.selectedServices.first.requiresMaterials,
        price: widget.selectedCompany.services
            .fold(0.0, (double acc, Service service) => acc + service.price),
      );

    final newOrderStore = Provider.of<NewOrderStore>(context, listen: false);
    newOrderStore.setOrder(order);

    if (_client == null || _client.id == null) {
      Navigator.of(context).push(CupertinoPageRoute(
        builder: (context) => LoginScreen(),
      ));
    } else {
      Navigator.of(context).push(CupertinoPageRoute(
        builder: (context) => ClientDashboardScreen(),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    final company = widget.selectedCompany;
    final services = widget.selectedServices;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        title: Text(
          S.of(context).DETAILS,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Spacer(),
            Avatar(
              company.avatarUrl,
              width: ScreenUtil().setWidth(76.69),
              height: ScreenUtil().setWidth(76.69),
            ),
            SizedBox(height: ScreenUtil().setHeight(10)),
            Text(
              company.name,
              style: Theme.of(context).textTheme.subtitle.copyWith(
                    fontSize: ScreenUtil().setSp(15),
                    letterSpacing: 0,
                    color: Palette.fiord,
                  ),
            ),
            SizedBox(height: ScreenUtil().setHeight(6.5)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RatingBar(
                  initialRating: company.rating,
                  ignoreGestures: true,
                  allowHalfRating: true,
                  onRatingUpdate: (_) {},
                  itemSize: ScreenUtil().setHeight(11.4),
                  itemPadding: EdgeInsets.symmetric(
                    horizontal: 2,
                  ),
                  ratingWidget: RatingWidget(
                    full: Image(
                      image: AssetImage('assets/images/star-full.png'),
                    ),
                    half: Image(
                      image: AssetImage('assets/images/star-half.png'),
                    ),
                    empty: Image(
                      image: AssetImage('assets/images/star-empty.png'),
                    ),
                  ),
                ),
                SizedBox(width: ScreenUtil().setWidth(12)),
                Text(
                  "${company.reviews == null ? 0 : company.reviews.length} ${S.of(context).Reviews}",
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(10),
                        color: Palette.orangePeel,
                      ),
                ),
              ],
            ),
            Spacer(),
            Expanded(
              flex: 7,
              child: ListView.separated(
                padding: EdgeInsets.zero,
                itemCount: services.length,
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(height: ScreenUtil().setHeight(10));
                },
                itemBuilder: (BuildContext context, int index) {
                  final service = services[index];
                  final companyService = company.services.firstWhere(
                      (s) => s.name['en'] == service.service.name['en'],
                      orElse: () => null);
                  final alignment = _kAlignments[index % 2];

                  return Align(
                    alignment: alignment,
                    child: Container(
                      // height: ScreenUtil().setHeight(71),
                      margin: EdgeInsets.only(
                        right: alignment == Alignment.topLeft
                            ? ScreenUtil().setWidth(21)
                            : 0,
                        left: alignment == Alignment.topRight
                            ? ScreenUtil().setWidth(21)
                            : 0,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.horizontal(
                          right: alignment == Alignment.topLeft
                              ? Radius.circular(9)
                              : Radius.zero,
                          left: alignment == Alignment.topRight
                              ? Radius.circular(9)
                              : Radius.zero,
                        ),
                        color: Palette.athensGray,
                      ),
                      child: IntrinsicHeight(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          textDirection: alignment == Alignment.topRight
                              ? TextDirection.ltr
                              : TextDirection.rtl,
                          children: <Widget>[
                            Spacer(),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: ScreenUtil().setHeight(21),
                              ),
                              child: SizedBox(
                                width: ScreenUtil().setWidth(117),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      RichText(
                                        text: TextSpan(
                                          children: [
                                            TextSpan(
                                              text:
                                                  "${service.hours * service.maidsCount}x ",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .display1
                                                  .copyWith(
                                                    fontSize:
                                                        ScreenUtil().setSp(12),
                                                    fontWeight: FontWeight.w800,
                                                    color: Palette.fiord,
                                                  ),
                                            ),
                                            TextSpan(
                                              text: service.service.name[
                                                  _appStore
                                                      .locale.languageCode],
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .display1
                                                  .copyWith(
                                                    fontSize:
                                                        ScreenUtil().setSp(12),
                                                    fontWeight: FontWeight.w600,
                                                    color: Palette.fiord,
                                                  ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(3)),
                                      Text(
                                        "${S.of(context).price_per_hour_is} ${companyService?.price ?? 0.0}${company.currency}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .display1
                                            .copyWith(
                                              fontSize: ScreenUtil().setSp(12),
                                              fontWeight: FontWeight.w400,
                                              color: Palette.fiord,
                                            ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Spacer(),
                            ClipPath(
                              clipper: _Clipper(alignment: alignment),
                              child: SizedBox(
                                width: ScreenUtil().setWidth(186.11),
                                child: Image(
                                  image: NetworkImage(service.service.imageUrl),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            Spacer(),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(
                  right: ScreenUtil().setWidth(21),
                ),
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(24.3),
                  right: ScreenUtil().setWidth(32.1),
                  bottom: ScreenUtil().setHeight(27.9),
                  left: ScreenUtil().setWidth(35),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.horizontal(
                    right: Radius.circular(9),
                  ),
                  color: Palette.royalBlue,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          S.of(context).TOTAL,
                          style: Theme.of(context).textTheme.subtitle.copyWith(
                                fontSize: ScreenUtil().setSp(13),
                                letterSpacing: 0,
                                color: Colors.white,
                              ),
                        ),
                        Builder(builder: (context) {
                          final totalPrice = widget.selectedServices.fold(0.0,
                              (double acc, ServiceData serviceData) {
                            final service = company.services.firstWhere(
                                (s) =>
                                    s.name['en'] ==
                                    serviceData.service.name['en'],
                                orElse: () => null);
                            print(service?.name ?? 'Service not found');
                            if (service == null) {
                              return 0.0;
                            }

                            final servicePrice = service.price *
                                serviceData.hours *
                                serviceData.maidsCount;
                            return acc + servicePrice;
                          });

                          return Text(
                            "${totalPrice.toStringAsFixed(2)}${company.currency}",
                            style:
                                Theme.of(context).textTheme.subtitle.copyWith(
                                      fontSize: ScreenUtil().setSp(18),
                                      letterSpacing: -0.60,
                                      color: Colors.white,
                                    ),
                          );
                        }),
                      ],
                    ),
                    InkWell(
                      onTap: () {
                        _checkout();
                      },
                      child: Container(
                        padding: EdgeInsets.only(
                          top: ScreenUtil().setHeight(12.1),
                          right: ScreenUtil().setWidth(6.6),
                          bottom: ScreenUtil().setHeight(11.8),
                          left: ScreenUtil().setWidth(18),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: Colors.white,
                            width: 1,
                          ),
                        ),
                        child: Row(
                          children: <Widget>[
                            Text(
                              S.of(context).CHECKOUT,
                              style:
                                  Theme.of(context).textTheme.subtitle.copyWith(
                                        fontSize: ScreenUtil().setSp(12),
                                        letterSpacing: 0,
                                        color: Colors.white,
                                      ),
                            ),
                            SizedBox(width: ScreenUtil().setWidth(20.9)),
                            Image(
                              image: AssetImage('assets/images/buy-icon.png'),
                              width: ScreenUtil().setWidth(22.55),
                              height: ScreenUtil().setHeight(20.47),
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}

class _Clipper extends CustomClipper<Path> {
  final Alignment alignment;

  _Clipper({this.alignment}) : super();

  @override
  Path getClip(Size size) {
    final clipSize = ScreenUtil().setWidth(34);
    final path = Path();

    if (alignment == Alignment.topRight) {
      path.moveTo(clipSize, 0);
      path.lineTo(size.width, 0);
      path.lineTo(size.width, size.height);
      path.lineTo(0, size.height);
    } else {
      path.lineTo(size.width, 0);
      path.lineTo(size.width - clipSize, size.height);
      path.lineTo(0, size.height);
    }
    path.close();

    return path;
  }

  @override
  bool shouldReclip(_Clipper oldClipper) => false;
}
