import 'dart:convert';

import 'package:home_maids/app/screens/reviews_screen/reviews_screen.dart';
import 'package:home_maids/app/stores/application_store.dart';
import 'package:home_maids/app/stores/review.dart';
import 'package:home_maids/app/stores/service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/checkout_screen/checkout_screen.dart';
import 'package:home_maids/app/screens/zone_selection_screen/zone_selection_screen.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/types/address.dart';
import 'package:home_maids/app/types/service_data.dart';
import 'package:home_maids/app/types/sort.dart';
import 'package:home_maids/app/types/zone.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/app/widgets/dropdown/dropdown_item.dart';
import 'package:home_maids/app/widgets/dropdown/dropdown_menu.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class SearchResultsScreen extends StatefulWidget {
  final List<ServiceData> selectedServices;
  final DateTime selectedDateTime;
  final City selectedCity;
  final Zone selectedZone;

  SearchResultsScreen(
      {Key key,
      @required this.selectedServices,
      @required this.selectedDateTime,
      @required this.selectedCity,
      @required this.selectedZone})
      : super(key: key);

  @override
  SearchResultsScreenState createState() => SearchResultsScreenState();
}

class SearchResultsScreenState extends State<SearchResultsScreen> {
  ApplicationStore _appStore;
  Future<List<Company>> _companiesFuture;
  Sort _sort;

  @override
  void initState() {
    super.initState();

    _appStore = Provider.of<ApplicationStore>(context, listen: false);

    _sort = Sort.alphabetical;

    _companiesFuture = _search();
  }

  Future<List<Company>> _search() async {
    List<Company> companies = [];

//    String filter;
//    String sort;
//    switch (_sort) {
//      case Sort.unpaid:
//      case Sort.complete:
//      case Sort.canceled:
//      case Sort.declined:
//      case Sort.latest:
//      case Sort.delayed:
//        filter = '';
//        sort = '';
//        break;
//
//      case Sort.alphabetical:
//        filter = 'company_name';
//        sort = 'ASC';
//        break;
//      case Sort.lowestPrice:
//        filter = 'min_price';
//        sort = 'ASC';
//        break;
//      case Sort.highestPrice:
//        filter = 'min_price';
//        sort = 'DESC';
//        break;
//      case Sort.rating:
//        filter = 'rate';
//        sort = 'DESC';
//        break;
//    }

    final formData = FormData.fromMap({
      if (widget.selectedZone != null) 'zone_id': widget.selectedZone.id,
      'city_id': widget.selectedCity.id,
      'services': jsonEncode(widget.selectedServices
          .map((serviceData) => int.tryParse(serviceData.service.id))
          .toList()),
//      'filter': filter,
//      'sort': sort,
    });
    final companiesResponse = await Dio().post<String>(
        "http://washy-car.com/homemaids/api/search",
        data: formData);

    if (companiesResponse == null || companiesResponse.statusCode != 200) {
      return companies;
    }

    final companiesDecoded = jsonDecode(companiesResponse.data);
    if (companiesDecoded == null || companiesDecoded['data'] == null) {
      return companies;
    }

    final data = companiesDecoded['data'];
    companies = data
        .map((companyData) {
          final company = Company();

          final List<Service> services = companyData['services']
              ?.map((service) => Service()
                ..setData(
                  name: {
                    'en': service['name'],
                    'ar': service['name_arabic'],
                  },
                  price: double.tryParse(service['price']) ?? 0.0,
                ))
              ?.toList()
              ?.cast<Service>();

          final pricePerHour = services?.fold(
                  0.0, (double acc, Service service) => acc + service.price) ??
              0.0;

          company.setData(
            id: companyData['company_id'] as String,
            name: companyData['compamy_name'] as String,
            avatarUrl: companyData['avatar'] as String,
            currency: companyData['currency'] as String,
            rating: companyData['rates'] as double,
            reviews: List<Review>.generate(
                companyData['reveiws'] ?? 0, (_) => Review()),
            services: services,
            hourlyPrice: pricePerHour,
            address: Address(companyData['address'], ''),
          );
          print(company);

          return company;
        })
        .toList()
        .cast<Company>();

    return companies;
  }

  double _totalPrice(Company company) {
    final totalPrice = widget.selectedServices.fold(0.0,
        (double acc, ServiceData serviceData) {
      final service = company.services.firstWhere(
          (s) => s.name['en'] == serviceData.service.name['en'],
          orElse: () => null);
      print(service?.name ?? 'Service not found');
      if (service == null) {
        return 0.0;
      }

      final servicePrice =
          service.price * serviceData.hours * serviceData.maidsCount;
      return acc + servicePrice;
    });

    return totalPrice;
  }

  Future<List<Company>> _sortCompanies(Sort sort) async {
    final companies = await _companiesFuture;

    switch (sort) {
      case Sort.unpaid:
      case Sort.complete:
      case Sort.canceled:
      case Sort.declined:
      case Sort.latest:
      case Sort.delayed:
        return companies;
      case Sort.alphabetical:
        companies.sort((c1, c2) => c1.name.compareTo(c2.name));
        return companies;
      case Sort.lowestPrice:
        companies.sort((c1, c2) {
          final totalPrice1 = _totalPrice(c1);
          final totalPrice2 = _totalPrice(c2);
          return totalPrice1.compareTo(totalPrice2);
        });
        return companies;
      case Sort.highestPrice:
        companies.sort((c1, c2) {
          final totalPrice1 = _totalPrice(c1);
          final totalPrice2 = _totalPrice(c2);
          return totalPrice2.compareTo(totalPrice1);
        });
        return companies;
      case Sort.rating:
        companies.sort((c1, c2) => c2.rating.compareTo(c1.rating));
        return companies;
    }

    return companies;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFDFDFD),
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        title: Text(
          S.of(context).SELECT_A_COMPANY,
          style: Theme.of(context).textTheme.headline,
        ),
        actions: <Widget>[
          DropDownMenu<Sort>(
            onChanged: (sort) async {
              final companiesFuture = _sortCompanies(sort);
              setState(() {
                _sort = sort;
                _companiesFuture = companiesFuture;
              });
            },
            width: ScreenUtil().setWidth(203),
            button: Image(
              image: AssetImage('assets/images/Component 5 – 2.png'),
              height: ScreenUtil().setHeight(16.01),
              color: Colors.white,
              matchTextDirection: true,
            ),
            head: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Image(
                  image: AssetImage('assets/images/Component 5 – 2.png'),
                  height: ScreenUtil().setHeight(16.01),
                  color: Palette.royalBlue,
                ),
              ],
            ),
            items: [
              DropdownItem<Sort>(
                value: Sort.alphabetical,
                child: Row(
                  children: <Widget>[
                    Text(
                      S.of(context).Name_AZ,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w500,
                            letterSpacing: 0.40,
                            color: Palette.fiord,
                          ),
                    ),
                    if (_sort == Sort.alphabetical) ...[
                      Spacer(),
                      Image(
                        image: AssetImage('assets/images/Component 3 – 2.png'),
                        height: ScreenUtil().setHeight(14),
                        matchTextDirection: true,
                      ),
                    ],
                  ],
                ),
              ),
              DropdownItem<Sort>(
                value: Sort.lowestPrice,
                child: Row(
                  children: <Widget>[
                    Text(
                      S.of(context).Lowest_Price,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w500,
                            letterSpacing: 0.40,
                            color: Palette.fiord,
                          ),
                    ),
                    if (_sort == Sort.lowestPrice) ...[
                      Spacer(),
                      Image(
                        image: AssetImage('assets/images/Component 3 – 2.png'),
                        height: ScreenUtil().setHeight(14),
                        matchTextDirection: true,
                      ),
                    ],
                  ],
                ),
              ),
              DropdownItem<Sort>(
                value: Sort.highestPrice,
                child: Row(
                  children: <Widget>[
                    Text(
                      S.of(context).Highest_Price,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w500,
                            letterSpacing: 0.40,
                            color: Palette.fiord,
                          ),
                    ),
                    if (_sort == Sort.highestPrice) ...[
                      Spacer(),
                      Image(
                        image: AssetImage('assets/images/Component 3 – 2.png'),
                        height: ScreenUtil().setHeight(14),
                        matchTextDirection: true,
                      ),
                    ],
                  ],
                ),
              ),
              DropdownItem<Sort>(
                value: Sort.rating,
                child: Row(
                  children: <Widget>[
                    Text(
                      S.of(context).Rating,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w500,
                            letterSpacing: 0.40,
                            color: Palette.fiord,
                          ),
                    ),
                    if (_sort == Sort.rating) ...[
                      Spacer(),
                      Image(
                        image: AssetImage('assets/images/Component 3 – 2.png'),
                        height: ScreenUtil().setHeight(14),
                        matchTextDirection: true,
                      ),
                    ],
                  ],
                ),
              ),
            ],
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      body: Container(
        color: Color(0xFFFDFDFD),
        child: Column(
          children: <Widget>[
            SizedBox(height: ScreenUtil().setHeight(19)),
            InkWell(
              onTap: () {
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    builder: (context) => ZoneSelectionScreen(
                        selectedServices: widget.selectedServices,
                        selectedDateTime: widget.selectedDateTime),
                  ),
                );
              },
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(21),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(21),
                  vertical: ScreenUtil().setHeight(16),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/images/Component 11 – 2.png'),
                      width: ScreenUtil().setWidth(16),
                      height: ScreenUtil().setWidth(16),
                      color: Palette.royalBlue,
                    ),
                    SizedBox(width: ScreenUtil().setWidth(20.1)),
                    Builder(builder: (context) {
                      final zoneName = widget.selectedZone == null
                          ? ''
                          : widget.selectedZone.name[_appStore.locale.languageCode];
                      final separator = widget.selectedZone == null ? '' : ',';
                      final cityName = widget.selectedCity?.name[_appStore.locale.languageCode] ?? '';
                      return Text(
                        "$zoneName$separator $cityName",
                        style: Theme.of(context).textTheme.display1.copyWith(
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.w400,
                              letterSpacing: 0.80,
                              color: Palette.fiord,
                            ),
                      );
                    }),
                  ],
                ),
              ),
            ),
            Expanded(
              child: FutureBuilder<List<Company>>(
                future: _companiesFuture,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                    case ConnectionState.active:
                      return CupertinoActivityIndicator();
                    case ConnectionState.none:
                      return Center(
                        child: Text(
                          S.of(context).No_result_found,
                          style: Theme.of(context).textTheme.display1,
                        ),
                      );
                    case ConnectionState.done:
                      if (snapshot.hasError || !snapshot.hasData) {
                        return Center(
                          child: Text(
                            S.of(context).No_result_found,
                            style: Theme.of(context).textTheme.display1,
                          ),
                        );
                      }

                      final companies = snapshot.data;

                      return ListView.separated(
                        padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(19),
                        ),
                        itemCount: companies?.length ?? 0,
                        separatorBuilder: (BuildContext context, int index) {
                          return SizedBox(height: ScreenUtil().setHeight(6.2));
                        },
                        itemBuilder: (BuildContext context, int index) {
                          final company = companies[index];

                          final totalPrice = _totalPrice(company);
                          print("Total price: $totalPrice");

                          return InkWell(
                            onTap: () {
                              Navigator.of(context).push(
                                CupertinoPageRoute(
                                  builder: (context) => CheckoutScreen(
                                    selectedServices: widget.selectedServices,
                                    selectedDateTime: widget.selectedDateTime,
                                    selectedCompany: company,
                                  ),
                                ),
                              );
                            },
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: ScreenUtil().setWidth(8),
                              ),
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                      top: ScreenUtil().setHeight(29),
                                      right: ScreenUtil().setWidth(40),
                                      bottom: ScreenUtil().setHeight(20),
                                      left: ScreenUtil().setWidth(28),
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.11),
                                          blurRadius: 40,
                                        ),
                                      ],
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              company.name.toUpperCase(),
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline
                                                  .copyWith(
                                                    fontSize:
                                                        ScreenUtil().setSp(16),
                                                    letterSpacing: 0,
                                                    color: Palette.royalBlue,
                                                  ),
                                            ),
                                            SizedBox(
                                                height:
                                                    ScreenUtil().setHeight(8)),
                                            InkWell(
                                              onTap: () {
                                                Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        ReviewsScreen(
                                                            company: company),
                                                  ),
                                                );
                                              },
                                              child: Row(
                                                children: <Widget>[
                                                  RatingBar(
                                                    initialRating:
                                                        company.rating,
                                                    ignoreGestures: true,
                                                    allowHalfRating: true,
                                                    onRatingUpdate: (_) {},
                                                    itemSize: ScreenUtil()
                                                        .setHeight(11.4),
                                                    itemPadding:
                                                        EdgeInsets.symmetric(
                                                      horizontal: 2,
                                                    ),
                                                    ratingWidget: RatingWidget(
                                                      full: Image(
                                                        image: AssetImage(
                                                            'assets/images/star-full.png'),
                                                      ),
                                                      half: Image(
                                                        image: AssetImage(
                                                            'assets/images/star-half.png'),
                                                      ),
                                                      empty: Image(
                                                        image: AssetImage(
                                                            'assets/images/star-empty.png'),
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                      width: ScreenUtil()
                                                          .setWidth(12)),
                                                  Text(
                                                    "${company.reviews == null ? 0 : company.reviews.length} ${S.of(context).Reviews}",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .display1
                                                        .copyWith(
                                                          fontSize: ScreenUtil()
                                                              .setSp(10),
                                                          color: Palette
                                                              .orangePeel,
                                                        ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                                height: ScreenUtil()
                                                    .setHeight(6.6)),
                                            SizedBox(
                                              width: ScreenUtil().setWidth(151),
                                              child: Text(
                                                company.address.address,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .display1
                                                    .copyWith(
                                                      fontSize: ScreenUtil()
                                                          .setSp(10),
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      letterSpacing: 0.50,
                                                      color: Palette.fiord,
                                                    ),
                                              ),
                                            ),
                                            SizedBox(
                                                height: ScreenUtil()
                                                    .setHeight(15.8)),
                                            Row(
                                              children: <Widget>[
                                                Text(
                                                  S.of(context).PRICE,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .display1
                                                      .copyWith(
                                                        fontSize: ScreenUtil()
                                                            .setSp(11),
                                                        fontWeight:
                                                            FontWeight.w300,
                                                        letterSpacing: 0,
                                                        color: Palette.fiord,
                                                      ),
                                                ),
                                                SizedBox(
                                                    width: ScreenUtil()
                                                        .setWidth(8)),
                                                Text(
                                                  "$totalPrice ${company.currency}",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline
                                                      .copyWith(
                                                        fontSize: ScreenUtil()
                                                            .setSp(18),
                                                        letterSpacing: -0.60,
                                                        color:
                                                            Palette.royalBlue,
                                                      ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Spacer(),
                                        Avatar(
                                          company.avatarUrl,
                                          width: ScreenUtil().setWidth(100),
                                          height: ScreenUtil().setWidth(100),
                                          hasShadow: false,
                                        ),
                                      ],
                                    ),
                                  ),
//                                  Positioned(
//                                    top: ScreenUtil().setHeight(15.6),
//                                    right: ScreenUtil().setWidth(18.8),
//                                    child: Image(
//                                      image:
//                                          AssetImage('assets/images/dots.png'),
//                                      width: ScreenUtil().setWidth(30.2),
//                                    ),
//                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                  }

                  return Container();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
