import 'package:flutter/cupertino.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/widgets/hmbutton/hmbutton.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:provider/provider.dart';

class SecurityScreen extends StatefulWidget {
  @override
  _SecurityScreenState createState() => _SecurityScreenState();
}

class _SecurityScreenState extends State<SecurityScreen> {
  Company _company;
  GlobalKey<FormState> _formKey;
  String _password;
  String _confirmPassword;
  bool _obscurePassword;
  bool _obscureConfirmPassword;
  bool _isLoading;
  bool _isUpdated;

  @override
  void initState() {
    super.initState();

    _company = Provider.of<Company>(context, listen: false);

    _formKey = new GlobalKey();
    _obscurePassword = true;
    _obscureConfirmPassword = true;
    _isLoading = false;
    _isUpdated = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        title: Text(
          S.of(context).SECURITY,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      bottomNavigationBar: CompanyNavigationBar(currentIndex: 2),
      body: Container(
        child: Column(
          children: <Widget>[
            SizedBox(height: ScreenUtil().setHeight(24)),
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(28),
                  ),
                  child: Text(
                    S.of(context).PASSWORD,
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(50)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(31),
              ),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        TextFormField(
                          onChanged: (String value) {
                            _password = value;

                            setState(() {
                              _isUpdated = false;
                            });
                          },
                          obscureText: _obscurePassword,
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(15),
                                fontWeight: FontWeight.w500,
                                letterSpacing: 1.20,
                                color: Palette.cadetBlue,
                              ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(
                                width: 0.3,
                                color: Palette.cadetBlue,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(
                                width: 0.3,
                                color: Palette.cadetBlue,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(
                                width: 0.3,
                                color: Colors.red,
                              ),
                            ),
                            contentPadding: EdgeInsets.only(
                              top: ScreenUtil().setHeight(22),
                              right: ScreenUtil().setWidth(29.5 + 17.12 + 30),
                              bottom: ScreenUtil().setHeight(22),
                              left: ScreenUtil().setWidth(26),
                            ),
                            hintText: S.of(context).New_password,
                            hintStyle:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(12),
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.20,
                                      color: Palette.cadetBlue,
                                    ),
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 0,
                          bottom: 0,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                _obscurePassword = !_obscurePassword;
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.only(
                                right: ScreenUtil().setWidth(29.5),
                                left: ScreenUtil().setWidth(20),
                              ),
                              child: Image(
                                image: AssetImage(
                                    'assets/images/Component 9 – 2.png'),
                                width: ScreenUtil().setWidth(17.12),
                                height: ScreenUtil().setHeight(12.36),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(16)),
                    Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        TextFormField(
                          onChanged: (String value) {
                            _confirmPassword = value;

                            setState(() {
                              _isUpdated = false;
                            });
                          },
                          obscureText: _obscureConfirmPassword,
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(15),
                                fontWeight: FontWeight.w500,
                                letterSpacing: 1.20,
                                color: Palette.cadetBlue,
                              ),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(
                                width: 0.3,
                                color: Palette.cadetBlue,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(
                                width: 0.3,
                                color: Palette.cadetBlue,
                              ),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(
                                width: 0.3,
                                color: Colors.red,
                              ),
                            ),
                            contentPadding: EdgeInsets.only(
                              top: ScreenUtil().setHeight(22),
                              right: ScreenUtil().setWidth(29.5 + 17.12 + 30),
                              bottom: ScreenUtil().setHeight(22),
                              left: ScreenUtil().setWidth(26),
                            ),
                            hintText: S.of(context).New_password,
                            hintStyle:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(12),
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.20,
                                      color: Palette.cadetBlue,
                                    ),
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 0,
                          bottom: 0,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                _obscureConfirmPassword =
                                    !_obscureConfirmPassword;
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.only(
                                right: ScreenUtil().setWidth(29.5),
                                left: ScreenUtil().setWidth(20),
                              ),
                              child: Image(
                                image: AssetImage(
                                    'assets/images/Component 9 – 2.png'),
                                width: ScreenUtil().setWidth(17.12),
                                height: ScreenUtil().setHeight(12.36),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(31)),
                    Align(
                      alignment: Alignment.topRight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          HMButton(
                            onTap: () {
                              _update();
                            },
                            isLoading: _isLoading,
                            done: _isUpdated,
                            text: Text(
                              S.of(context).UPDATE,
                              style:
                                  Theme.of(context).textTheme.subtitle.copyWith(
                                        fontSize: ScreenUtil().setSp(13),
                                        letterSpacing: 0,
                                        color: Palette.cornflowerBlue,
                                      ),
                            ),
                            doneText: Text(
                              S.of(context).UPDATED,
                              style:
                                  Theme.of(context).textTheme.subtitle.copyWith(
                                        fontSize: ScreenUtil().setSp(13),
                                        letterSpacing: 0,
                                        color: Palette.cornflowerBlue,
                                      ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _update() async {
    if (_password != _confirmPassword) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          content: Text(S.of(context).The_passwords_dont_match),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              isDefaultAction: true,
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );

      return;
    }

    setState(() {
      _isLoading = true;
    });

    final password = Uri.encodeComponent(_password);
    final uri =
        "http://washy-car.com/homemaids/api/edit/profile/${_company.token}?password=$password";
    await http.post(uri);

    setState(() {
      _isLoading = false;
      _isUpdated = true;
    });
  }
}
