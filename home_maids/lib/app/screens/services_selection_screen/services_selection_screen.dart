import 'dart:convert';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_maids/app/stores/application_store.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/pick_date_time_screen/pick_date_time_screen.dart';
import 'package:home_maids/app/stores/service.dart';
import 'package:home_maids/app/types/service_data.dart';
import 'package:provider/provider.dart';

class ServicesSelectionScreen extends StatefulWidget {
  @override
  ServicesSelectionScreenState createState() => ServicesSelectionScreenState();
}

class ServicesSelectionScreenState extends State<ServicesSelectionScreen> {
  ApplicationStore _appStore;
  Future<List<ServiceData>> servicesFuture;
  ServiceData _selectedService;

  @override
  void initState() {
    super.initState();

    _appStore = Provider.of<ApplicationStore>(context, listen: false);

    servicesFuture = _getServices();
  }

  Future<List<ServiceData>> _getServices() async {
    List<ServiceData> serviceData = [];

    final response =
        await http.get('http://washy-car.com/homemaids/api/services');
    if (response != null &&
        response.statusCode == 200 &&
        response.body != null &&
        response.body.isNotEmpty) {
      final servicesDecoded = jsonDecode(response.body);

      final List<Service> services = servicesDecoded
          ?.map((service) => Service()
            ..setData(
              id: service['id'] as String,
              name: {
                'en': service['name'] as String,
                'ar': service['name_arabic'] as String,
              },
              iconUrl: service['icon'] as String,
              imageUrl: service['image'] as String,
              isEnabled: service['is_enabled'] == '0' ? false : true,
            ))
          ?.toList()
          ?.cast<Service>();

      serviceData = services
          ?.map(
            (Service service) => ServiceData(
              service: service,
              hours: 1,
              maidsCount: 1,
              requiresMaterials: false,
              selected: false,
            ),
          )
          ?.toList();
    }

    return serviceData;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        brightness: Brightness.light,
        centerTitle: true,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            color: Palette.royalBlue,
            matchTextDirection: true,
          ),
        ),
        title: Image(
          image: AssetImage('assets/images/logo.png'),
          height: ScreenUtil().setHeight(28),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(30),
        ),
        child: FutureBuilder<List<ServiceData>>(
          future: servicesFuture,
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
              case ConnectionState.active:
                return Center(
                  child: CupertinoActivityIndicator(),
                );
              case ConnectionState.none:
                return Container();
              case ConnectionState.done:
                if (snapshot.hasError || !snapshot.hasData) {
                  return Container();
                }

                final services = snapshot.data;

                return Column(
                  children: <Widget>[
                    Spacer(),
                    Column(
                      children: <Widget>[
                        for (var i = 0; i < services.length; i += 2) ...[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              _ServiceCard(
                                onTap: () {
                                  setState(() {
                                    if (_selectedService == null ||
                                        !services[i].selected ||
                                        (_selectedService == services[i] &&
                                            services[i].selected)) {
                                      services[i].selected =
                                          !services[i].selected;
                                    }
                                    _selectedService = services[i];
                                  });
                                },
                                selected: services[i].selected,
                                current: _selectedService == services[i],
                                serviceName: services[i].service.name[_appStore.locale.languageCode],
                                iconPath: services[i].service.iconUrl,
                              ),
                              SizedBox(width: ScreenUtil().setWidth(18)),
                              if (i + 1 < services.length)
                                _ServiceCard(
                                  onTap: () {
                                    setState(() {
                                      if (_selectedService == null ||
                                          !services[i + 1].selected ||
                                          (_selectedService ==
                                                  services[i + 1] &&
                                              services[i + 1].selected)) {
                                        services[i + 1].selected =
                                            !services[i + 1].selected;
                                      }
                                      _selectedService = services[i + 1];
                                    });
                                  },
                                  selected: services[i + 1].selected,
                                  current: _selectedService == services[i + 1],
                                  serviceName:
                                      services[i + 1].service.name[_appStore.locale.languageCode],
                                  iconPath: services[i + 1].service.iconUrl,
                                ),
                            ],
                          ),
                          if (i + 2 < services.length)
                            SizedBox(height: ScreenUtil().setHeight(15)),
                        ],
                      ],
                    ),
                    Spacer(),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            S.of(context).How_long_do_you_need_our_maid_to_stay,
                            style:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(13),
                                      fontWeight: FontWeight.w500,
                                      color: Palette.fiord,
                                    ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(12.9)),
                    SizedBox(
                      width: ScreenUtil().setWidth(223),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: SliderTheme(
                              data: SliderTheme.of(context),
                              child: Slider(
                                value: _selectedService == null
                                    ? 1
                                    : _selectedService.hours.toDouble(),
                                onChanged: (double value) {
                                  setState(() {
                                    if (_selectedService != null) {
                                      _selectedService.hours = value.toInt();
                                    }
                                  });
                                },
                                min: 1.0,
                                max: 10.0,
                                divisions: 10,
                              ),
                            ),
                          ),
                          Text(
                            _selectedService == null
                                ? '1'
                                : _selectedService.hours.toInt().toString(),
                            style:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(23),
                                      fontWeight: FontWeight.w800,
                                      letterSpacing: -0.64,
                                      color: Palette.cornflowerBlue,
                                    ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            S.of(context).How_many_maids_do_you_need,
                            style:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(13),
                                      fontWeight: FontWeight.w500,
                                      color: Palette.fiord,
                                    ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(12.9)),
                    SizedBox(
                      width: ScreenUtil().setWidth(223),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: SliderTheme(
                              data: SliderTheme.of(context),
                              child: Slider(
                                value: _selectedService == null
                                    ? 1
                                    : _selectedService.maidsCount.toDouble(),
                                onChanged: (double value) {
                                  setState(() {
                                    if (_selectedService != null) {
                                      _selectedService.maidsCount =
                                          value.toInt();
                                    }
                                  });
                                },
                                min: 1.0,
                                max: 10.0,
                                divisions: 10,
                              ),
                            ),
                          ),
                          Text(
                            _selectedService == null
                                ? '1'
                                : _selectedService.maidsCount
                                    .toInt()
                                    .toString(),
                            style:
                                Theme.of(context).textTheme.display1.copyWith(
                                      fontSize: ScreenUtil().setSp(23),
                                      fontWeight: FontWeight.w800,
                                      letterSpacing: -0.64,
                                      color: Palette.cornflowerBlue,
                                    ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          S.of(context).Do_you_require_cleaning_materials,
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(13),
                                fontWeight: FontWeight.w500,
                                color: Palette.fiord,
                              ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(22.4)),
                        Transform.scale(
                          scale: 0.8,
                          child: CupertinoSwitch(
                            value: _selectedService == null
                                ? false
                                : _selectedService.requiresMaterials,
                            onChanged: (bool value) {
                              setState(() {
                                if (_selectedService != null) {
                                  _selectedService.requiresMaterials = value;
                                }
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                    Spacer(),
                    Builder(builder: (context) {
                      final enabled = services
                          .any((ServiceData service) => service.selected);

                      return InkWell(
                        onTap: enabled
                            ? () {
                                Navigator.of(context).push(CupertinoPageRoute(
                                  builder: (context) => PickDateTimeScreen(
                                    selectedServices: services
                                        .where((service) => service.selected)
                                        .toList(),
                                  ),
                                ));
                              }
                            : null,
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        child: Container(
                          width: ScreenUtil().setWidth(332),
                          height: ScreenUtil().setHeight(52),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Palette.athensGray,
                            gradient: enabled
                                ? LinearGradient(
                                    colors: <Color>[
                                      Palette.heliotrope,
                                      Palette.royalBlue2,
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  )
                                : null,
                          ),
                          child: Text(
                            S.of(context).CHECK_AVAILABILITY,
                            style: Theme.of(context).textTheme.button.copyWith(
                                  letterSpacing: 0.70,
                                  color: enabled
                                      ? Colors.white
                                      : Palette.cadetBlue,
                                ),
                          ),
                        ),
                      );
                    }),
                    Spacer(),
                  ],
                );
            }

            return Container();
          },
        ),
      ),
    );
  }
}

class _ServiceCard extends StatelessWidget {
  final String serviceName;
  final String iconPath;
  final bool selected;
  final bool current;
  final Function onTap;

  _ServiceCard({
    @required this.serviceName,
    @required this.iconPath,
    this.onTap,
    this.selected = false,
    this.current = false,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        width: ScreenUtil().setWidth(118),
        height: ScreenUtil().setWidth(111),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Palette.blackHaze,
          border: Border.all(
            width: current ? 2 : 0.2,
            color: current ? Palette.brightTurquoise : Palette.silver,
          ),
          gradient: selected
              ? LinearGradient(
                  colors: [
                    Palette.heliotrope,
                    Palette.royalBlue2,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [0.35, 1],
                )
              : null,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SvgPicture.network(
              iconPath,
              color: selected ? Colors.white : Palette.cadetBlue,
            ),
            SizedBox(height: ScreenUtil().setHeight(9.6)),
            Text(
              serviceName,
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(12),
                    fontWeight: FontWeight.w300,
                    letterSpacing: 0.10,
                    color: selected ? Colors.white : Palette.cadetBlue,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
