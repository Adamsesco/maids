import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:intl/intl.dart';
import 'package:home_maids/app/stores/notification.dart' as notif;

class NotificationsPage extends StatefulWidget {
  final List<notif.Notification> notifications;

  NotificationsPage({Key key, @required this.notifications}) : super(key: key);

  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  void initState() {
    super.initState();

    // TODO: mark notifications as read.
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            top: ScreenUtil().setHeight(26),
            right: ScreenUtil().setWidth(50.06),
            left: ScreenUtil().setWidth(28),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                S.of(context).NOTIFICATIONS,
                style: Theme.of(context).textTheme.subtitle.copyWith(
                      fontSize: ScreenUtil().setSp(17),
                    ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(30),
              right: ScreenUtil().setWidth(8),
              left: ScreenUtil().setWidth(8),
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.11),
                  blurRadius: 40,
                ),
              ],
            ),
            child: ListView.builder(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(23),
              ),
              itemCount: widget.notifications == null
                  ? 0
                  : widget.notifications.length,
              itemBuilder: (BuildContext context, int index) {
                final notification = widget.notifications[index];

                return Column(
                  children: <Widget>[
                    if (index >= 1)
                      Container(
                        height: 1,
                        margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(8),
                        ),
                        color: Palette.royalBlue.withOpacity(0.08),
                      ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: ScreenUtil().setWidth(8),
                      ),
                      child: ClipRect(
                        child: _NotificationItem(notification),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}

class _NotificationItem extends StatelessWidget {
  final notif.Notification notification;

  _NotificationItem(this.notification) : assert(notification != null);

  @override
  Widget build(BuildContext context) {
    final name = "${notification.firstName} ${notification.lastName}";
    final avatarUrl = notification.avatarUrl;
    final content = notification.body;
    final date = notification.date;

    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10),
        right: ScreenUtil().setWidth(35),
        bottom: ScreenUtil().setHeight(10),
        left: ScreenUtil().setWidth(27),
      ),
      child: Row(
        children: <Widget>[
          Avatar(
            avatarUrl,
            width: ScreenUtil().setWidth(55),
            height: ScreenUtil().setWidth(55),
          ),
          SizedBox(width: ScreenUtil().setWidth(16)),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      name,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontWeight: FontWeight.w800,
                            color: Palette.royalBlue,
                          ),
                    ),
                    Text(
                      DateFormat('dd MMMM y').format(date),
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(9),
                          ),
                    ),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(4)),
                Text(
                  content,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(11),
                        color: Palette.fiord,
                      ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
