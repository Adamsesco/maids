import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/messages_screen/messages_page/message_page.dart';
import 'package:home_maids/app/screens/notifications_list_screen/notifications_page/notifications_page.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/notifications_store.dart';
import 'package:home_maids/app/stores/notification.dart' as notif;
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:home_maids/app/widgets/client_navigation_bar/client_navigation_bar.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class NotificationsListScreen extends StatefulWidget {
  @override
  _NotificationsListScreenState createState() =>
      _NotificationsListScreenState();
}

class _NotificationsListScreenState extends State<NotificationsListScreen> {
  User _me;
  NotificationsStore _notificationsStore;
  List<notif.Notification> _notifications;

  @override
  void initState() {
    super.initState();

    final client = Provider.of<Client>(context, listen: false);
    final company = Provider.of<Company>(context, listen: false);
    if (client != null && client.id != null) {
      _me = client;
    } else if (company != null && company.id != null) {
      _me = company;
    }

    _notificationsStore =
        Provider.of<NotificationsStore>(context, listen: false);
    _notifications = _notificationsStore.notifications;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        centerTitle: true,
        title: Text(
          S.of(context).NOTIFICATIONS,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      bottomNavigationBar: _me.role == UserRole.company
          ? CompanyNavigationBar(currentIndex: 1)
          : ClientNavigationBar(currentIndex: 2),
      body: NotificationsPage(
        notifications: _notifications,
      ),
    );
  }
}
