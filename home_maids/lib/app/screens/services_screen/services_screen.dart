import 'dart:convert';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_maids/app/stores/application_store.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/services_and_pricing_screen/services_and_pricing_screen.dart';
import 'package:home_maids/app/stores/service.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:provider/provider.dart';

class ServicesScreen extends StatefulWidget {
  @override
  _ServicesScreenState createState() => _ServicesScreenState();
}

class _ServicesScreenState extends State<ServicesScreen> {
  Future<List<Service>> _servicesFuture;

  @override
  void initState() {
    super.initState();

    _servicesFuture = _getServices();
  }

  Future<List<Service>> _getServices() async {
    List<Service> services = [];

    final response =
        await http.get('http://washy-car.com/homemaids/api/services');
    if (response != null &&
        response.statusCode == 200 &&
        response.body != null &&
        response.body.isNotEmpty) {
      final servicesDecoded = jsonDecode(response.body);

      services = servicesDecoded
          ?.map((service) => Service()
            ..setData(
              id: service['id'] as String,
              name: {
                'en': service['name'] as String,
                'ar': service['name_arabic'] as String,
              },
              iconUrl: service['icon'] as String,
              imageUrl: service['image'] as String,
              isEnabled: service['is_enabled'] == '0' ? false : true,
            ))
          ?.toList()
          ?.cast<Service>();
    }

    return services;
  }

  @override
  Widget build(BuildContext context) {
    final appStore = Provider.of<ApplicationStore>(context, listen: false);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        title: Text(
          S.of(context).SERVICES_PRICING,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      bottomNavigationBar: CompanyNavigationBar(currentIndex: 2),
      body: Container(
        child: Column(
          children: <Widget>[
            SizedBox(height: ScreenUtil().setHeight(26)),
            Text(
              S.of(context).PLEASE_SELECT_FROM_THESES_SERVICES_THOSE_YOU_PROVIDE,
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(10),
                    color: Palette.royalBlue,
                  ),
            ),
            SizedBox(height: ScreenUtil().setHeight(21)),
            FutureBuilder(
              future: _servicesFuture,
              builder: (BuildContext context,
                  AsyncSnapshot<List<Service>> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                  case ConnectionState.none:
                  case ConnectionState.active:
                    return Container();

                  case ConnectionState.done:
                    if (snapshot.hasError || !snapshot.hasData) {
                      return Container();
                    }

                    return Column(
                      children: <Widget>[
                        for (var service in snapshot.data) ...[
                          _ServiceItem(
                            icon: SvgPicture.network(
                              service.iconUrl,
                              color: Palette.royalBlue,
                              width: ScreenUtil().setWidth(32.84),
                              height: ScreenUtil().setHeight(32.34),
                            ),
                            text: service.name[appStore.locale.languageCode],
                            screen: ServicesAndPricingScreen(
                              service: service,
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(4)),
                        ],
                      ],
                    );
                }

                return Container();
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _ServiceItem extends StatelessWidget {
  final Widget icon;
  final String text;
  final Widget screen;

  _ServiceItem(
      {@required this.icon, @required this.text, @required this.screen});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) =>
                this.screen,
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              return child;
            },
          ),
        );
      },
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      child: Container(
        color: Palette.blackHaze,
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(46.1),
          vertical: ScreenUtil().setHeight(11.3),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: icon,
            ),
            Spacer(),
            Expanded(
              flex: 4,
              child: Text(
                text,
                style: Theme.of(context).textTheme.subtitle.copyWith(
                      fontWeight: FontWeight.w300,
                      letterSpacing: -0.40,
                      color: Palette.fiord,
                    ),
              ),
            ),
            Spacer(),
            RotatedBox(
              quarterTurns: 2,
              child: Image(
                image: AssetImage('assets/images/noun_Arrow_2335767.png'),
                height: ScreenUtil().setHeight(13.4),
                color: Palette.fiord,
                matchTextDirection: true,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
