import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/forgot_password_screen/forgot_password_screen.dart';
import 'package:home_maids/app/screens/services_selection_screen/services_selection_screen.dart';
import 'package:home_maids/app/screens/signup_screen/signup_screen.dart';
import 'package:home_maids/app/services/users_service.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/incomes_store.dart';
import 'package:home_maids/app/stores/messages_store.dart';
import 'package:home_maids/app/stores/orders_store.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  UsersService _usersService;
  GlobalKey<FormState> _formKey;
  FocusNode _passwordFocusNode;

  String _email;
  String _password;
  String _emailError;
  String _passwordError;

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    _usersService = new UsersService();
    _formKey = new GlobalKey();
    _passwordFocusNode = new FocusNode();

    final company = Provider.of<Company>(context, listen: false);
    final client = Provider.of<Client>(context, listen: false);
    company.id = null;
    client.id = null;
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    final appBar = AppBar(
      backgroundColor: Colors.white,
      brightness: Brightness.light,
      leading: InkWell(
        onTap: () {
          if (Navigator.of(context).canPop()) {
            Navigator.of(context).pop();
          }
        },
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(10),
            vertical: ScreenUtil().setHeight(10),
          ),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            color: Palette.royalBlue,
            matchTextDirection: true,
          ),
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar,
      body: SafeArea(
        top: false,
        child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.vertical -
                appBar.preferredSize.height,
            child: Column(
              children: <Widget>[
                Spacer(),
                Image(
                  image: AssetImage('assets/images/logo.png'),
                ),
                SizedBox(height: ScreenUtil().setHeight(74)),
                InkWell(
                  onTap: () {
                    _signinWithGoogle();
                  },
                  child: Container(
                    width: ScreenUtil().setWidth(332),
                    height: ScreenUtil().setHeight(52),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Palette.alabaster,
                    ),
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Positioned(
                          left: ScreenUtil().setWidth(48.2),
                          child: Image(
                            image: AssetImage(
                                'assets/images/google-logo-icon.png'),
                          ),
                        ),
                        Text(
                          S.of(context).SIGN_IN_WITH_GOOGLE,
                          style: Theme.of(context).textTheme.button.copyWith(
                                fontSize: ScreenUtil().setSp(10),
                                fontWeight: FontWeight.w800,
                                letterSpacing: 0.8,
                                color: Palette.royalBlue,
                              ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(30)),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        height: 1,
                        color: Colors.black.withOpacity(0.08),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(23)),
                    Text(
                      S.of(context).OR_SIGN_IN_WITH_EMAIL,
                      style: Theme.of(context).textTheme.button.copyWith(
                            fontSize: ScreenUtil().setSp(10),
                            fontWeight: FontWeight.w700,
                            letterSpacing: 1.2,
                            color: Palette.fiord,
                          ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(23)),
                    Expanded(
                      child: Container(
                        height: 1,
                        color: Colors.black.withOpacity(0.08),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(30)),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(21),
                    right: ScreenUtil().setWidth(21),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            TextFormField(
                              enabled: !_isLoading,
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              textDirection: TextDirection.ltr,
                              decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                disabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                contentPadding: EdgeInsets.only(
                                  right: ScreenUtil().setWidth(25),
                                  left:
                                      ScreenUtil().setWidth(25 + 15.11 + 20.9),
                                  top: ScreenUtil().setHeight(17),
                                  bottom: ScreenUtil().setHeight(17),
                                ),
                                errorStyle: Theme.of(context)
                                    .textTheme
                                    .display3
                                    .copyWith(
                                      fontSize: 0,
                                      color: Palette.red,
                                      height: 0,
                                    ),
                                hintText: S.of(context).Email,
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .display4
                                    .copyWith(
                                      fontWeight: FontWeight.w300,
                                      color: Palette.periwinkleGray,
                                    ),
                              ),
                              validator: (String value) {
                                final pattern =
                                    r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
                                final regexp = new RegExp(pattern);
                                if (!regexp.hasMatch(value)) {
                                  _emailError =
                                      S.of(context).Invalid_email_address;
                                  return _emailError;
                                } else {
                                  _emailError = null;
                                }
                                return null;
                              },
                              onFieldSubmitted: (_) {
                                FocusScope.of(context)
                                    .requestFocus(_passwordFocusNode);
                              },
                              onSaved: (String value) {
                                _email = value;
                              },
                            ),
                            Positioned(
                              left: ScreenUtil().setWidth(25),
                              child: SizedBox(
                                child: Image(
                                  image: AssetImage(
                                      'assets/images/email-icon.png'),
                                  color: Palette.cornflowerBlue,
                                  width: ScreenUtil().setWidth(15.11),
                                  fit: BoxFit.fitWidth,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(14)),
                        Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            TextFormField(
                              focusNode: _passwordFocusNode,
                              enabled: !_isLoading,
                              obscureText: true,
                              textInputAction: TextInputAction.done,
                              textDirection: TextDirection.ltr,
                              decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                disabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  borderSide: BorderSide(
                                    color: Palette.cadetBlue,
                                    width: 0.3,
                                  ),
                                ),
                                contentPadding: EdgeInsets.only(
                                  right: ScreenUtil().setWidth(25),
                                  left:
                                      ScreenUtil().setWidth(25 + 15.11 + 20.9),
                                  top: ScreenUtil().setHeight(17),
                                  bottom: ScreenUtil().setHeight(17),
                                ),
                                errorStyle: Theme.of(context)
                                    .textTheme
                                    .display3
                                    .copyWith(
                                      fontSize: 0,
                                      height: 0,
                                      color: Palette.red,
                                    ),
                                hintText: S.of(context).Password,
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .display4
                                    .copyWith(
                                      fontWeight: FontWeight.w300,
                                      color: Palette.periwinkleGray,
                                    ),
                              ),
                              validator: (String value) {
                                if (value == null || value.length < 6) {
                                  _passwordError = S
                                      .of(context)
                                      .The_password_must_contain_at_least_6_characters;
                                  return _passwordError;
                                } else {
                                  _passwordError = null;
                                }
                                return null;
                              },
                              onFieldSubmitted: (_) {
                                _login();
                              },
                              onSaved: (String value) {
                                _password = value;
                              },
                            ),
                            Positioned(
                              left: ScreenUtil().setWidth(25),
                              child: Image(
                                image: AssetImage(
                                    'assets/images/password-icon.png'),
                                width: ScreenUtil().setWidth(15.11),
                                fit: BoxFit.fitWidth,
                                color: Palette.cornflowerBlue,
                              ),
                            ),
                          ],
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => ForgotPasswordScreen(),
                              ),
                            );
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: ScreenUtil().setHeight(14),
                            ),
                            child: Align(
                              alignment: Directionality.of(context) ==
                                      TextDirection.ltr
                                  ? Alignment.topRight
                                  : Alignment.topLeft,
                              child: Text(
                                S.of(context).Forgot_password,
                                style: Theme.of(context).textTheme.display3,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(51.5 - 14)),
                        InkWell(
                          onTap: () {
                            _login();
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (context) => ClientDashboardScreen()));
                          },
                          child: Container(
                            width: ScreenUtil().setWidth(139),
                            height: ScreenUtil().setHeight(52),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                width: 1,
                                color: Palette.cornflowerBlue,
                              ),
                            ),
                            child: _isLoading
                                ? CupertinoActivityIndicator()
                                : Text(
                                    S.of(context).LOGIN,
                                    style: Theme.of(context)
                                        .textTheme
                                        .display3
                                        .copyWith(
                                          fontSize: ScreenUtil().setSp(13),
                                          fontWeight: FontWeight.w800,
                                          color: Palette.cornflowerBlue,
                                        ),
                                  ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => SignupScreen(),
                            ));
                          },
                          child: Padding(
                            padding: EdgeInsets.all(ScreenUtil().setHeight(14)),
                            child: RichText(
                              text: TextSpan(
                                style: Theme.of(context).textTheme.display3,
                                children: [
                                  TextSpan(
                                    text: S.of(context).Dont_have_an_account,
                                  ),
                                  TextSpan(
                                    text: S.of(context).SIGN_UP,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(51.5 - 14)),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => ServicesSelectionScreen(),
                              ),
                            );
                          },
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          child: Container(
                            width: ScreenUtil().setWidth(332),
                            height: ScreenUtil().setHeight(52),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              gradient: LinearGradient(
                                colors: <Color>[
                                  Palette.heliotrope,
                                  Palette.royalBlue2,
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                              ),
                            ),
                            child: Text(
                              S.of(context).QUICK_SEARCH,
                              style:
                                  Theme.of(context).textTheme.button.copyWith(
                                        letterSpacing: 0.70,
                                        color: Colors.white,
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      if (_emailError != null) {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(S.of(context).Invalid_email_address),
          ),
        );
      } else if (_passwordError != null) {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(
                S.of(context).The_password_must_contain_at_least_6_characters),
          ),
        );
      }
      return false;
    }
  }

  Future<void> _signinWithGoogle() async {
    setState(() {
      _isLoading = true;
    });

    final company = Provider.of<Company>(context, listen: false);
    final fcmToken = await company.fcmToken;
    final user = await _usersService.signinWithGoogle(fcmToken);
    _performLogin(user);

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _login() async {
    setState(() {
      _isLoading = true;
    });

    try {
      if (_saveForm()) {
        final company = Provider.of<Company>(context, listen: false);
        final fcmToken = await company.fcmToken;
        print(fcmToken);
        final user =
            await _usersService.signInWithEmail(_email, _password, fcmToken);
        _performLogin(user);
      }
    } on Exception {
      setState(() {
        _isLoading = false;
      });
    }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _performLogin(User user) async {
    if (user == null) {
      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(
            S.of(context).Signin_error,
            style: Theme.of(context).textTheme.subtitle,
          ),
          content: Text(
            S.of(context).Incorrect_email_or_password,
            style: Theme.of(context).textTheme.display1,
          ),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).Ok),
            ),
          ],
        ),
      );
    } else {
      final company = Provider.of<Company>(context, listen: false);
      final client = Provider.of<Client>(context, listen: false);
      final ordersStore = Provider.of<OrdersStore>(context, listen: false);
      final messagesStore = Provider.of<MessagesStore>(context, listen: false);
      final incomesStore = Provider.of<IncomesStore>(context, listen: false);

      final screen = _usersService.performLogin(
          user, company, client, ordersStore, messagesStore, incomesStore);

      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => screen,
        ),
      );
    }
  }
}
