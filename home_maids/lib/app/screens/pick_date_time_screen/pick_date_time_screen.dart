import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/screens/search_results_screen/search_results_screen.dart';
import 'package:home_maids/app/screens/zone_selection_screen/zone_selection_screen.dart';
import 'package:home_maids/app/types/service_data.dart';
import 'package:home_maids/app/widgets/hm_day_picker/hm_day_picker.dart';
import 'package:home_maids/app/widgets/hm_time_picker/hm_time_picker.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:intl/intl.dart';
import 'package:home_maids/app/const/colors.dart';

const _kmaxDays = 30;

class PickDateTimeScreen extends StatefulWidget {
  final List<ServiceData> selectedServices;

  PickDateTimeScreen({Key key, @required this.selectedServices})
      : super(key: key);

  @override
  _PickDateTimeScreenState createState() => _PickDateTimeScreenState();
}

class _PickDateTimeScreenState extends State<PickDateTimeScreen> {
  DateTime _selectedDateTime;
  DateTime _now;
  DateTime _firstSelectableDateTime;
  DateTime _lastSelectableDateTime;
  int _totalHours;

  @override
  void initState() {
    super.initState();

    _now = DateTime.now();
    _firstSelectableDateTime = _now;
    _lastSelectableDateTime = _now.add(Duration(days: _kmaxDays));
    _selectedDateTime = _now;
    _totalHours = widget.selectedServices
        .fold(0, (int sum, ServiceData serviceData) => sum + serviceData.hours);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        title: Text(
          S.of(context).PICK_DATE_TIME,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Spacer(),
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(38),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Builder(builder: (context) {
                        final enabled = (_selectedDateTime.month >
                                    _firstSelectableDateTime.month &&
                                _selectedDateTime.year ==
                                    _firstSelectableDateTime.year) ||
                            _selectedDateTime.year >
                                _firstSelectableDateTime.year;

                        return InkWell(
                          onTap: enabled
                              ? () {
                                  final year = _selectedDateTime.month == 1
                                      ? _selectedDateTime.year - 1
                                      : _selectedDateTime.year;
                                  final month = _selectedDateTime.month == 1
                                      ? 12
                                      : _selectedDateTime.month - 1;
                                  final day = month ==
                                              _firstSelectableDateTime.month &&
                                          year ==
                                              _firstSelectableDateTime.year &&
                                          _selectedDateTime.day <
                                              _firstSelectableDateTime.day
                                      ? _firstSelectableDateTime.day
                                      : _selectedDateTime.day;
                                  setState(() {
                                    _selectedDateTime = DateTime(
                                      year,
                                      month,
                                      day,
                                      _selectedDateTime.hour,
                                      _selectedDateTime.minute,
                                      _selectedDateTime.second,
                                      _selectedDateTime.millisecond,
                                      _selectedDateTime.microsecond,
                                    );
                                  });
                                }
                              : null,
                          child: Image(
                            image: AssetImage(
                                'assets/images/noun_Arrow_2335767.png'),
                            width: ScreenUtil().setWidth(11.68),
                            height: ScreenUtil().setHeight(19.27),
                            color:
                                enabled ? Palette.fiord : Palette.blackSqueeze,
                            matchTextDirection: true,
                          ),
                        );
                      }),
                      SizedBox(width: ScreenUtil().setWidth(26.2)),
                      Builder(builder: (BuildContext context) {
                        final enabled = (_selectedDateTime.year ==
                                    _lastSelectableDateTime.year &&
                                _selectedDateTime.month <
                                    _lastSelectableDateTime.month) ||
                            _selectedDateTime.year <
                                _lastSelectableDateTime.year;
                        return InkWell(
                          onTap: enabled
                              ? () {
                                  final year = _selectedDateTime.month == 12
                                      ? _selectedDateTime.year + 1
                                      : _selectedDateTime.year;
                                  final month = _selectedDateTime.month == 12
                                      ? 1
                                      : _selectedDateTime.month + 1;
                                  final day = month ==
                                              _lastSelectableDateTime.month &&
                                          year ==
                                              _lastSelectableDateTime.year &&
                                          _selectedDateTime.day >
                                              _lastSelectableDateTime.day
                                      ? _lastSelectableDateTime.day
                                      : _selectedDateTime.day;
                                  setState(() {
                                    _selectedDateTime = DateTime(
                                      year,
                                      month,
                                      day,
                                      _selectedDateTime.hour,
                                      _selectedDateTime.minute,
                                      _selectedDateTime.second,
                                      _selectedDateTime.millisecond,
                                      _selectedDateTime.microsecond,
                                    );
                                  });
                                }
                              : null,
                          child: RotatedBox(
                            quarterTurns: 2,
                            child: Image(
                              image: AssetImage(
                                  'assets/images/noun_Arrow_2335767.png'),
                              width: ScreenUtil().setWidth(11.68),
                              height: ScreenUtil().setHeight(19.27),
                              color: enabled
                                  ? Palette.fiord
                                  : Palette.blackSqueeze,
                              matchTextDirection: true,
                            ),
                          ),
                        );
                      }),
                    ],
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: DateFormat('MMMM').format(_selectedDateTime),
                          style: Theme.of(context).textTheme.subtitle.copyWith(
                                fontSize: ScreenUtil().setSp(23),
                                letterSpacing: -0.80,
                                color: Palette.fiord,
                              ),
                        ),
                        TextSpan(
                          text: ' ',
                        ),
                        TextSpan(
                          text: DateFormat('y').format(_selectedDateTime),
                          style: Theme.of(context).textTheme.subtitle.copyWith(
                                fontSize: ScreenUtil().setSp(23),
                                fontWeight: FontWeight.w400,
                                letterSpacing: -0.80,
                                color: Palette.fiord,
                              ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            Expanded(
              flex: 8,
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(38),
                ),
                child: Theme(
                  data: ThemeData(
                    accentColor: Palette.royalBlue,
                    textTheme: TextTheme(
                      caption: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(15),
                            fontWeight: FontWeight.w700,
                            color: Palette.fiord,
                          ),
                      body1: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(15),
                            fontWeight: FontWeight.w500,
                            color: Palette.royalBlue,
                          ),
                    ),
                    accentTextTheme: TextTheme(
                      body2: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(15),
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                    ),
                  ),
                  child: HMDayPicker(
                    selectedDate: _selectedDateTime,
                    currentDate: _now,
                    displayedMonth: _selectedDateTime,
                    firstDate: _firstSelectableDateTime,
                    lastDate: _lastSelectableDateTime,
                    onChanged: (DateTime dateTime) {
                      setState(() {
                        _selectedDateTime = DateTime(
                          dateTime.year,
                          dateTime.month,
                          dateTime.day,
                          _selectedDateTime.hour,
                          _selectedDateTime.minute,
                          _selectedDateTime.second,
                          _selectedDateTime.millisecond,
                          _selectedDateTime.microsecond,
                        );
                      });
                    },
                  ),
                ),
              ),
            ),
            Spacer(),
            Expanded(
              flex: 5,
              child: Align(
                alignment: Alignment.topLeft,
                child: SizedBox(
                  width: ScreenUtil().setWidth(354),
                  child: HMTimePicker(
                    initialDateTime: _selectedDateTime,
                    duration: Duration(hours: _totalHours),
                  ),
                ),
              ),
            ),
            Spacer(),
            InkWell(
              onTap: () {
                Navigator.of(context).push(CupertinoPageRoute(
                  builder: (context) => ZoneSelectionScreen(
                    selectedServices: widget.selectedServices,
                    selectedDateTime: _selectedDateTime,
                  ),
                ));
              },
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(38),
                ),
                height: ScreenUtil().setHeight(52),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Palette.athensGray,
                  gradient: LinearGradient(
                    colors: <Color>[
                      Palette.heliotrope,
                      Palette.royalBlue2,
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                ),
                child: Text(
                  S.of(context).BOOK_NOW,
                  style: Theme.of(context).textTheme.button.copyWith(
                        letterSpacing: 0.70,
                        color: Colors.white,
                      ),
                ),
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
