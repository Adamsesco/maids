import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/intro_screen/intro_screen.dart';
import 'package:home_maids/app/stores/application_store.dart';
import 'package:provider/provider.dart';

class LanguageScreen extends StatefulWidget {
  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  ApplicationStore _appStore;

  @override
  void initState() {
    super.initState();

    _appStore = Provider.of<ApplicationStore>(context, listen: false);
  }

  void _setLanguage(String lang) {
    Locale locale;
    switch (lang) {
      case 'ar':
        locale = Locale('ar');
        break;

      case 'en':
        locale = Locale('en');
        break;
    }

    _appStore.setLocale(locale);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/logo.png'),
          ),
          Positioned(
            right: 0,
            left: 0,
            bottom: ScreenUtil().setHeight(110),
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(21),
              ),
              child: Column(
                children: <Widget>[
                  _PrimaryButton(
                    text: 'العربية',
                    onTap: () {
                      _setLanguage('ar');

                      Navigator.of(context).push(
                        CupertinoPageRoute(
                          builder: (context) => IntroScreen(),
                        ),
                      );
                    },
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20)),
                  _PrimaryButton(
                    text: 'ENGLISH',
                    onTap: () {
                      _setLanguage('en');

                      Navigator.of(context).push(
                        CupertinoPageRoute(
                          builder: (context) => IntroScreen(),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _PrimaryButton extends StatelessWidget {
  final bool enabled;
  final Function onTap;
  final String text;

  _PrimaryButton({this.enabled = true, this.onTap, @required this.text})
      : super();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: enabled
          ? () {
              if (onTap != null) {
                onTap();
              }
            }
          : null,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        height: ScreenUtil().setHeight(52),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Palette.athensGray,
          gradient: LinearGradient(
            colors: <Color>[
              Palette.heliotrope,
              Palette.royalBlue2,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: Text(
          text,
          style: Theme.of(context).textTheme.button.copyWith(
                letterSpacing: 0.70,
                color: Colors.white,
              ),
        ),
      ),
    );
  }
}
