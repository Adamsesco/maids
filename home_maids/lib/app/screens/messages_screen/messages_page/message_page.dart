import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/chat_screen/chat_screen.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/message.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:home_maids/app/widgets/avatar/avatar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:intl/intl.dart';

typedef void OnArchive(Message message);

class MessagesPage extends StatefulWidget {
  final List<Message> messages;
  final bool isArchived;
  final OnArchive onArchive;
  final Function onIconButtonPressed;

  MessagesPage(
      {Key key,
      @required this.messages,
      @required this.isArchived,
      @required this.onArchive,
      this.onIconButtonPressed})
      : super(key: key);

  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            top: ScreenUtil().setHeight(26),
            right: ScreenUtil().setWidth(50.06),
            left: ScreenUtil().setWidth(28),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                widget.isArchived ? S.of(context).ARCHIVED_MESSAGES : S.of(context).MESSAGES,
                style: Theme.of(context).textTheme.subtitle.copyWith(
                      fontSize: ScreenUtil().setSp(17),
                    ),
              ),
              InkWell(
                onTap: () {
                  if (widget.onIconButtonPressed != null) {
                    widget.onIconButtonPressed();
                  }
                },
                child: Image(
                  image: AssetImage('assets/images/noun_archive_798231.png'),
                  height: ScreenUtil().setHeight(19.01),
                  color: widget.isArchived
                      ? Palette.cadetBlue
                      : Palette.cornflowerBlue,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(
              top: ScreenUtil().setHeight(30),
              right: ScreenUtil().setWidth(8),
              left: ScreenUtil().setWidth(8),
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.11),
                  blurRadius: 40,
                ),
              ],
            ),
            child: AnimatedList(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(23),
              ),
              initialItemCount:
                  widget.messages == null ? 0 : widget.messages.length,
              itemBuilder: (BuildContext context, int index,
                  Animation<double> animation) {
                final message = widget.messages[index];

                final key = GlobalKey();
                return Column(
                  children: <Widget>[
                    if (index >= 1)
                      Container(
                        height: 1,
                        margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(8),
                        ),
                        color: Palette.royalBlue.withOpacity(0.08),
                      ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: ScreenUtil().setWidth(8),
                      ),
                      child: ClipRect(
                        child: Slidable(
                          key: key,
                          actionPane: SlidableDrawerActionPane(),
                          actionExtentRatio: 0.25,
                          secondaryActions: <Widget>[
                            InkWell(
                              onTap: () {
                                setState(() {
                                  widget.onArchive(message);
                                  AnimatedList.of(context).removeItem(
                                    index,
                                    (context, animation) {
                                      final widget = key.currentWidget;
                                      return SizeTransition(
                                        axis: Axis.vertical,
                                        sizeFactor: animation,
                                        child: widget,
                                      );
                                    },
                                    duration: Duration(milliseconds: 200),
                                  );
                                });
                              },
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Container(
                                color: widget.isArchived
                                    ? Palette.fiord
                                    : Palette.cadetBlue,
                                width: ScreenUtil().setWidth(77),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(widget.isArchived
                                          ? 'assets/images/Component 8 – 2.png'
                                          : 'assets/images/Component 7 – 2.png'),
                                      width: ScreenUtil().setWidth(15.07),
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                        height: ScreenUtil().setHeight(6.1)),
                                    Text(
                                      widget.isArchived
                                          ? S.of(context).Unarchive
                                          : S.of(context).Archive,
                                      style: Theme.of(context)
                                          .textTheme
                                          .display1
                                          .copyWith(
                                            fontSize: ScreenUtil().setSp(9),
                                            color: Colors.white,
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(
                                PageRouteBuilder(
                                  pageBuilder: (context, animation,
                                          secondaryAnimation) =>
                                      ChatScreen(message: message),
                                  transitionsBuilder: (context, animation,
                                      secondaryAnimation, child) {
                                    return child;
                                  },
                                ),
                              );
                            },
                            child: _MessageItem(message),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}

class _MessageItem extends StatelessWidget {
  final Message message;

  _MessageItem(this.message) : assert(message != null);

  @override
  Widget build(BuildContext context) {
    String name;
    String avatarUrl;
    String content;
    DateTime date;

    final sender = message.sender;
    switch (sender.role) {
      case UserRole.anonymous:
        return Container();
        break;
      case UserRole.client:
        final client = sender as Client;
        name = "${client.firstName} ${client.lastName}";
        avatarUrl = client.avatarUrl;
        break;
      case UserRole.company:
        final company = sender as Company;
        name = company.name;
        avatarUrl = company.avatarUrl;
        break;
    }
    content = message.content;
    date = message.date;

    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(10),
        right: ScreenUtil().setWidth(35),
        bottom: ScreenUtil().setHeight(10),
        left: ScreenUtil().setWidth(27),
      ),
      child: Row(
        children: <Widget>[
          Avatar(
            avatarUrl,
            width: ScreenUtil().setWidth(55),
            height: ScreenUtil().setWidth(55),
          ),
          SizedBox(width: ScreenUtil().setWidth(16)),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      name,
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontWeight: FontWeight.w800,
                            color: Palette.royalBlue,
                          ),
                    ),
                    Text(
                      DateFormat('dd MMMM y').format(date),
                      style: Theme.of(context).textTheme.display1.copyWith(
                            fontSize: ScreenUtil().setSp(9),
                          ),
                    ),
                  ],
                ),
                SizedBox(height: ScreenUtil().setHeight(4)),
                Text(
                  content,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.display1.copyWith(
                        fontSize: ScreenUtil().setSp(11),
                        color: Palette.fiord,
                      ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
