import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/screens/messages_screen/messages_page/message_page.dart';
import 'package:home_maids/app/stores/client.dart';
import 'package:home_maids/app/stores/company.dart';
import 'package:home_maids/app/stores/message.dart';
import 'package:home_maids/app/stores/messages_store.dart';
import 'package:home_maids/app/types/user.dart';
import 'package:home_maids/app/types/user_role.dart';
import 'package:home_maids/app/widgets/client_navigation_bar/client_navigation_bar.dart';
import 'package:home_maids/app/widgets/navigation_bar/navigation_bar.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:provider/provider.dart';

class MessagesScreen extends StatefulWidget {
  @override
  _MessagesScreenState createState() => _MessagesScreenState();
}

class _MessagesScreenState extends State<MessagesScreen> {
  User _me;
  MessagesStore _messagesStore;
  List<Message> _messages = [];
  List<Message> _archivedMessages = [];
  PageController _pageController;

  @override
  void initState() {
    super.initState();

    final client = Provider.of<Client>(context, listen: false);
    final company = Provider.of<Company>(context, listen: false);
    if (client != null && client.id != null) {
      _me = client;
    } else if (company != null && company.id != null) {
      _me = company;
    }

    _messagesStore = Provider.of<MessagesStore>(context, listen: false);
    final messages = _messagesStore.messages
            .where(
                (message) => message.sender.id != _me.id && !message.isArchived)
            ?.toList() ??
        [];
    messages.reversed.forEach((message) {
      if (!_messages.any((m) => m.sender.id == message.sender.id)) {
        _messages.add(message);
      }
    });

    final archivedMessages = _messagesStore.messages
            .where(
                (message) => message.sender.id != _me.id && message.isArchived)
            ?.toList() ??
        [];
    archivedMessages.reversed.forEach((message) {
      if (!_archivedMessages.any((m) => m.sender.id == message.sender.id)) {
        _archivedMessages.add(message);
      }
    });

    _pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(),
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        centerTitle: true,
        title: Text(
          S.of(context).MESSAGES,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      bottomNavigationBar: _me.role == UserRole.company
          ? CompanyNavigationBar(currentIndex: 1)
          : ClientNavigationBar(currentIndex: 2),
      body: PageView(
        controller: _pageController,
        children: <Widget>[
          MessagesPage(
            messages: _messages,
            isArchived: false,
            onArchive: (Message message) {
              setState(() {
                _messages.removeWhere((Message m) => m.id == message.id);
                message.isArchived = true;
                _archivedMessages.add(message);
              });
            },
            onIconButtonPressed: () {
              _pageController.nextPage(
                duration: Duration(milliseconds: 200),
                curve: Curves.linear,
              );
            },
          ),
          MessagesPage(
            messages: _archivedMessages,
            isArchived: true,
            onArchive: (Message message) {
              setState(() {
                _archivedMessages
                    .removeWhere((Message m) => m.id == message.id);
                message.isArchived = false;
                _messages.add(message);
              });
            },
            onIconButtonPressed: () {
              _pageController.previousPage(
                duration: Duration(milliseconds: 200),
                curve: Curves.linear,
              );
            },
          ),
        ],
      ),
    );
  }
}
