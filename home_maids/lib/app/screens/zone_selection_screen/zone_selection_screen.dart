import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:home_maids/app/screens/search_results_screen/search_results_screen.dart';
import 'package:home_maids/app/stores/application_store.dart';
import 'package:home_maids/app/types/service_data.dart';
import 'package:home_maids/app/widgets/hmbutton/hmbutton.dart';
import 'package:home_maids/generated/i18n.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:home_maids/app/const/colors.dart';
import 'package:home_maids/app/types/zone.dart';
import 'package:provider/provider.dart';

class ZoneSelectionScreen extends StatefulWidget {
  final List<ServiceData> selectedServices;
  final DateTime selectedDateTime;

  ZoneSelectionScreen(
      {Key key,
      @required this.selectedServices,
      @required this.selectedDateTime})
      : super(key: key);

  @override
  _ZoneSelectionScreenState createState() => _ZoneSelectionScreenState();
}

class _ZoneSelectionScreenState extends State<ZoneSelectionScreen> {
  Box _prefsDb;
  Future<List<City>> _citiesFuture;
  PageController _pageController;
  City _selectedCity;
  Zone _selectedZone;

  @override
  void initState() {
    super.initState();

    _pageController = PageController();

    _citiesFuture = _getCities();
  }

  Future<List<City>> _getCities() async {
    List<City> cities;

    final citiesResponse =
        await http.get('http://washy-car.com/homemaids/api/zones');
    if (citiesResponse == null ||
        citiesResponse.statusCode != 200 ||
        citiesResponse.body == null ||
        citiesResponse.body.isEmpty) {
      return cities;
    }

    final citiesDecoded = jsonDecode(citiesResponse.body);
    cities = citiesDecoded
            ?.map((city) => City(
                  id: city['id'],
                  name: {
                    'en': city['name'],
                    'ar': city['name_ar'],
                  },
                  zones: [Zone()],
                ))
            ?.toList()
            ?.cast<City>() ??
        [];

    print(cities);

    for (var i = 0; i < cities.length; i++) {
      final city = cities[i];
      List<Zone> zones;

      final zonesResponse = await http
          .get("http://washy-car.com/homemaids/api/cities/${city.id}");
      if (zonesResponse == null ||
          zonesResponse.statusCode != 200 ||
          zonesResponse.body == null ||
          zonesResponse.body.isEmpty) {
        break;
      }

      final zonesDecoded = jsonDecode(zonesResponse.body);
      zones = zonesDecoded
              ?.map((zone) => Zone(
                    id: zone['id'],
                    name: {
                      'en': zone['name'],
                      'ar': zone['name_ar'],
                    },
                  ))
              ?.toList()
              ?.cast<Zone>() ??
          [Zone()];
      if (zones == null || zones.isEmpty) {
        zones = [Zone()];
      }

      final cityNew = City(
        id: city.id,
        name: city.name,
        zones: zones,
      );
      cities[i] = cityNew;
    }

    if (cities == null || cities.isEmpty) {
      return cities;
    }

    _prefsDb = await Hive.openBox('prefs');
    final defaultCityId = _prefsDb.get('city_id') as String;
    final defaultZoneId = _prefsDb.get('zone_id') as String;
    if (defaultCityId != null && defaultZoneId != null) {
      final city = cities.firstWhere((city) => city.id == defaultCityId,
          orElse: () => null);
      if (city != null) {
        _selectedCity = city;

        final zone = city.zones
            .firstWhere((zone) => zone.id == defaultZoneId, orElse: () => null);
        if (zone != null) {
          _selectedZone = zone;
        }

        return cities;
      }
    }

    return cities;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Palette.royalBlue,
        brightness: Brightness.dark,
        elevation: 0,
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            final currentPage = _pageController.page.toInt();

            if (currentPage == 0) {
              Navigator.of(context).pop();
            } else {
              _pageController.previousPage(
                  duration: Duration(milliseconds: 200), curve: Curves.linear);
            }
          },
          child: Image(
            image: AssetImage('assets/images/noun_Arrow_2335767.png'),
            height: ScreenUtil().setHeight(19.27),
            matchTextDirection: true,
          ),
        ),
        title: Text(
          S.of(context).SELECT_A_ZONE,
          style: Theme.of(context).textTheme.headline,
        ),
        bottom: PreferredSize(
          preferredSize: Size(
              MediaQuery.of(context).size.width, ScreenUtil().setHeight(10)),
          child: SizedBox(height: ScreenUtil().setHeight(10)),
        ),
      ),
      body: FutureBuilder<List<City>>(
        future: _citiesFuture,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:

            case ConnectionState.waiting:

            case ConnectionState.active:
              return Center(
                child: CupertinoActivityIndicator(),
              );
            case ConnectionState.done:
              if (snapshot.hasError || !snapshot.hasData) {
                return Container();
              }

              final cities = snapshot.data;

              return Container(
                child: PageView(
                  controller: _pageController,
                  physics: NeverScrollableScrollPhysics(),
                  children: <Widget>[
                    _FirstPage(
                      city: _selectedCity,
                      zone: _selectedZone,
                      onCityFieldPressed: () {
                        _pageController.animateToPage(1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);
                      },
                      onZoneFieldPressed: () {
                        if (_selectedCity == null) {
                          showCupertinoDialog(
                            context: context,
                            builder: (context) => CupertinoAlertDialog(
                              title: Text(S.of(context).Select_city),
                              content: Text(S.of(context).Please_select_a_city),
                              actions: <Widget>[
                                CupertinoDialogAction(
                                  onPressed: () => Navigator.of(context).pop(),
                                  child: Text(S.of(context).Ok),
                                ),
                              ],
                            ),
                          );
                        } else {
                          _pageController.animateToPage(2,
                              duration: Duration(milliseconds: 200),
                              curve: Curves.linear);
                        }
                      },
                      onValidate: (City city, Zone zone) {
                        Navigator.of(context).push(
                          CupertinoPageRoute(
                            builder: (context) => SearchResultsScreen(
                              selectedServices: widget.selectedServices,
                              selectedDateTime: widget.selectedDateTime,
                              selectedCity: city,
                              selectedZone: zone,
                            ),
                          ),
                        );
                      },
                    ),
                    _CitySelect(
                      cities: cities,
                      onSelect: (City city) {
                        if (city != _selectedCity) {
                          setState(() {
                            _selectedCity = city;
                            _selectedZone = null;
                          });
                        }

                        _pageController.animateToPage(0,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);

                        _prefsDb.put('city_id', city.id);
                      },
                    ),
                    _ZoneSelect(
                      city: _selectedCity,
                      onSelect: (Zone zone) {
                        setState(() {
                          _selectedZone = zone;
                        });

                        _pageController.animateToPage(0,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear);

                        _prefsDb.put('zone_id', zone.id);
                      },
                    ),
                  ],
                ),
              );
          }

          return Container();
        },
      ),
    );
  }
}

typedef void _OnValidate(City city, Zone zone);

class _FirstPage extends StatelessWidget {
  final City city;
  final Zone zone;
  final Function onCityFieldPressed;
  final Function onZoneFieldPressed;
  final _OnValidate onValidate;

  _FirstPage(
      {this.city,
      this.zone,
      this.onCityFieldPressed,
      this.onZoneFieldPressed,
      this.onValidate});

  @override
  Widget build(BuildContext context) {
    final appStore = Provider.of<ApplicationStore>(context, listen: false);

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(21),
        vertical: ScreenUtil().setHeight(19),
      ),
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: onCityFieldPressed,
            child: _ZoneField(
                name: city == null
                    ? S.of(context).City
                    : city.name[appStore.locale.languageCode]),
          ),
          SizedBox(height: ScreenUtil().setHeight(17)),
          InkWell(
            onTap: onZoneFieldPressed,
            child: _ZoneField(
                name: zone == null
                    ? S.of(context).Zone
                    : zone.name[appStore.locale.languageCode]),
          ),
          SizedBox(height: ScreenUtil().setHeight(40)),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              HMButton(
                onTap: () {
                  if (city == null) {
                    showCupertinoDialog(
                      context: context,
                      builder: (context) => CupertinoAlertDialog(
                        title: Text(S.of(context).Select_city),
                        content: Text(S.of(context).Please_select_a_city_first),
                        actions: <Widget>[
                          CupertinoDialogAction(
                            onPressed: () => Navigator.of(context).pop(),
                            child: Text(S.of(context).Ok),
                          ),
                        ],
                      ),
                    );
                  } else {
                    if (onValidate != null) {
                      onValidate(city, zone);
                    }
                  }
                },
                text: Text(
                  S.of(context).VALIDATE,
                  style: Theme.of(context).textTheme.subtitle.copyWith(
                        fontSize: ScreenUtil().setSp(13),
                        letterSpacing: 0,
                        color: Palette.cornflowerBlue,
                      ),
                ),
                doneText: Text(
                  S.of(context).VALIDATE,
                  style: Theme.of(context).textTheme.subtitle.copyWith(
                        fontSize: ScreenUtil().setSp(13),
                        letterSpacing: 0,
                        color: Palette.cornflowerBlue,
                      ),
                ),
                done: true,
                hasUpdateIcon: true,
                hasAnimations: false,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _ZoneField extends StatelessWidget {
  final String name;

  _ZoneField({this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: ScreenUtil().setWidth(21),
        vertical: ScreenUtil().setHeight(16),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: Palette.cadetBlue,
          width: 0.3,
        ),
      ),
      child: Row(
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/Component 11 – 2.png'),
            width: ScreenUtil().setWidth(16),
            height: ScreenUtil().setWidth(16),
            color: Palette.royalBlue,
          ),
          SizedBox(width: ScreenUtil().setWidth(20.1)),
          Text(
            name,
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontSize: ScreenUtil().setSp(12),
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.80,
                  color: Palette.fiord,
                ),
          ),
        ],
      ),
    );
  }
}

class _CitySelect extends StatefulWidget {
  final List<City> cities;
  final Function(City) onSelect;

  _CitySelect({Key key, @required this.cities, this.onSelect})
      : super(key: key);

  @override
  _CitySelectState createState() => _CitySelectState();
}

class _CitySelectState extends State<_CitySelect> {
  RegExp _cityNameFilter;

  @override
  void initState() {
    super.initState();

    _cityNameFilter = new RegExp('.*');
  }

  @override
  Widget build(BuildContext context) {
    final appStore = Provider.of<ApplicationStore>(context, listen: false);

    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(17),
              right: ScreenUtil().setWidth(21),
              bottom: ScreenUtil().setHeight(56.0 - 17.0),
              left: ScreenUtil().setWidth(21),
            ),
            child: TextField(
              onChanged: (String value) {
                setState(() {
                  _cityNameFilter = new RegExp(
                      value == null || value.length == 0 ? '.*' : value,
                      caseSensitive: false);
                });
              },
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(12),
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.80,
                    color: Palette.fiord,
                  ),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(36.1),
                  vertical: ScreenUtil().setHeight(17),
                ),
                hintText: S.of(context).Type_your_city,
                hintStyle: Theme.of(context).textTheme.display1.copyWith(
                      fontSize: ScreenUtil().setSp(12),
                      fontWeight: FontWeight.w400,
                      letterSpacing: 0.80,
                      color: Palette.cadetBlue,
                    ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Builder(
              builder: (context) {
                final cities = widget.cities
                    ?.where((City city) =>
                        _cityNameFilter.hasMatch(city.name[appStore.locale.languageCode]))
                    ?.toList();

                return ListView.separated(
                  padding: EdgeInsets.zero,
                  itemCount: cities?.length ?? 0,
                  separatorBuilder: (BuildContext context, int index) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: 1,
                      color: Palette.slateGrey.withOpacity(0.08),
                    );
                  },
                  itemBuilder: (BuildContext context, int index) {
                    final city = cities[index];

                    return InkWell(
                      onTap: () {
                        if (widget.onSelect != null) {
                          widget.onSelect(city);
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.only(
                          top: ScreenUtil().setHeight(17),
                          right: ScreenUtil().setWidth(21),
                          bottom: ScreenUtil().setHeight(17),
                          left: ScreenUtil().setWidth(59),
                        ),
                        child: Text(
                          city.name[appStore.locale.languageCode],
                          style: Theme.of(context).textTheme.display1.copyWith(
                                fontSize: ScreenUtil().setSp(17),
                                fontWeight: FontWeight.w300,
                                letterSpacing: -0.40,
                                color: Palette.fiord,
                              ),
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _ZoneSelect extends StatefulWidget {
  final City city;
  final Function(Zone) onSelect;

  _ZoneSelect({Key key, @required this.city, this.onSelect}) : super(key: key);

  @override
  _ZoneSelectState createState() => _ZoneSelectState();
}

class _ZoneSelectState extends State<_ZoneSelect> {
  ApplicationStore _appStore;
  List<Map<String, List<Zone>>> _zones;
  RegExp _zoneNameFilter;

  @override
  void initState() {
    super.initState();

    _appStore = Provider.of<ApplicationStore>(context, listen: false);

    final flattenedZones = <String, List<Zone>>{};
    widget.city?.zones?.forEach((Zone zone) {
      final initialLetter = zone.name[_appStore.locale.languageCode][0].toUpperCase();

      List<Zone> entry;
      if (flattenedZones.containsKey(initialLetter)) {
        entry = flattenedZones[initialLetter];
      } else {
        entry = [];
        flattenedZones[initialLetter] = entry;
      }
      entry.add(zone);
    });

    List<Map<String, List<Zone>>> zones = [];
    flattenedZones?.forEach((String initialLetter, List<Zone> zs) {
      zones.add({initialLetter: zs});
    });
    zones?.sort((a, b) => a.keys.first.compareTo(b.keys.first));

    _zones = zones;

    _zoneNameFilter = new RegExp('.*');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(21),
              vertical: ScreenUtil().setHeight(17),
            ),
            child: TextField(
              onChanged: (String value) {
                setState(() {
                  _zoneNameFilter = new RegExp(
                      value == null || value.length == 0 ? '.*' : value,
                      caseSensitive: false);
                });
              },
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontSize: ScreenUtil().setSp(12),
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.80,
                    color: Palette.fiord,
                  ),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(36.1),
                  vertical: ScreenUtil().setHeight(17),
                ),
                hintText: "${S.of(context).Type_your_zone_in} ${widget.city.name[_appStore.locale.languageCode]}",
                hintStyle: Theme.of(context).textTheme.display1.copyWith(
                      fontSize: ScreenUtil().setSp(12),
                      fontWeight: FontWeight.w400,
                      letterSpacing: 0.80,
                      color: Palette.cadetBlue,
                    ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: Palette.cadetBlue,
                    width: 0.3,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Builder(builder: (context) {
              List<Map<String, List<Zone>>> zones = [];
              _zones.forEach((e) {
                final zs = e.values.first
                    .where((Zone z) => _zoneNameFilter.hasMatch(z.name[_appStore.locale.languageCode]))
                    ?.toList();

                if (zs.isNotEmpty) {
                  zones.add({e.keys.first: zs});
                }
              });

              return ListView.separated(
                padding: EdgeInsets.zero,
                itemCount: zones?.length ?? 0,
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(height: ScreenUtil().setHeight(30));
                },
                itemBuilder: (BuildContext context, int index) {
                  final e = zones[index];
                  final initialLetter = e.keys.first;
                  final zs = e.values.first;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(27),
                        ),
                        child: Text(
                          initialLetter,
                          style: Theme.of(context).textTheme.subtitle.copyWith(
                                letterSpacing: -0.40,
                                color: Palette.cornflowerBlue,
                              ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(3)),
                      for (var zone in zs) ...[
                        InkWell(
                          onTap: () {
                            if (widget.onSelect != null) {
                              widget.onSelect(zone);
                            }
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(59),
                              vertical: ScreenUtil().setHeight(17),
                            ),
                            child: Text(
                              zone.name[_appStore.locale.languageCode],
                              style:
                                  Theme.of(context).textTheme.display1.copyWith(
                                        fontSize: ScreenUtil().setSp(17),
                                        fontWeight: FontWeight.w300,
                                        letterSpacing: -0.40,
                                        color: Palette.fiord,
                                      ),
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 1,
                          color: Palette.slateGrey.withOpacity(0.08),
                        ),
                      ],
                    ],
                  );
                },
              );
            }),
          ),
        ],
      ),
    );
  }
}
