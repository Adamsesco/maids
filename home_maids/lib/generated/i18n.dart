// DO NOT EDIT. This is code generated via package:gen_lang/generate.dart

import 'dart:async';

import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import 'messages_all.dart';

class S {
 
  static const GeneratedLocalizationsDelegate delegate = GeneratedLocalizationsDelegate();

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }
  
  static Future<S> load(Locale locale) {
    final String name = locale.countryCode == null ? locale.languageCode : locale.toString();

    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new S();
    });
  }
  
  String get locale {
    return Intl.message("English", name: 'locale');
  }

  String get SKIP {
    return Intl.message("SKIP", name: 'SKIP');
  }

  String get GET_STARTED {
    return Intl.message("GET STARTED", name: 'GET_STARTED');
  }

  String get Best_companies {
    return Intl.message("Best companies", name: 'Best_companies');
  }

  String get We_make_sure_to_thoroughly_study_the_demand {
    return Intl.message("We make sure to thoroughly study the demand of the company associated with", name: 'We_make_sure_to_thoroughly_study_the_demand');
  }

  String get TrustMaids {
    return Intl.message("TrustMaids", name: 'TrustMaids');
  }

  String get which_must_be_the_best_and_most_efficient_in_its_region {
    return Intl.message(", which must be the best and most efficient in its region.", name: 'which_must_be_the_best_and_most_efficient_in_its_region');
  }

  String get Trustworthy {
    return Intl.message("Trustworthy", name: 'Trustworthy');
  }

  String get We_all_know_when_it_comes_to_trust {
    return Intl.message("We all know when it comes to trust, it is a big issue. On ", name: 'We_all_know_when_it_comes_to_trust');
  }

  String get we_have_the_most_trustworthy_maids {
    return Intl.message(" we have the most trustworthy maids.", name: 'we_have_the_most_trustworthy_maids');
  }

  String get It_is_convenient {
    return Intl.message("It is convenient", name: 'It_is_convenient');
  }

  String get Choose_the_service_that_suits_your_budget {
    return Intl.message("Choose the service that suits your budget, the nearest, the cheapest or even the most popular on the market.", name: 'Choose_the_service_that_suits_your_budget');
  }

  String get JAN {
    return Intl.message("JAN", name: 'JAN');
  }

  String get FEB {
    return Intl.message("FEB", name: 'FEB');
  }

  String get MAR {
    return Intl.message("MAR", name: 'MAR');
  }

  String get APR {
    return Intl.message("APR", name: 'APR');
  }

  String get MAY {
    return Intl.message("MAY", name: 'MAY');
  }

  String get JUN {
    return Intl.message("JUN", name: 'JUN');
  }

  String get JUL {
    return Intl.message("JUL", name: 'JUL');
  }

  String get AUG {
    return Intl.message("AUG", name: 'AUG');
  }

  String get SEP {
    return Intl.message("SEP", name: 'SEP');
  }

  String get OCT {
    return Intl.message("OCT", name: 'OCT');
  }

  String get NOV {
    return Intl.message("NOV", name: 'NOV');
  }

  String get DEC {
    return Intl.message("DEC", name: 'DEC');
  }

  String get SUN {
    return Intl.message("SUN", name: 'SUN');
  }

  String get MON {
    return Intl.message("MON", name: 'MON');
  }

  String get TUE {
    return Intl.message("TUE", name: 'TUE');
  }

  String get WED {
    return Intl.message("WED", name: 'WED');
  }

  String get THU {
    return Intl.message("THU", name: 'THU');
  }

  String get FRI {
    return Intl.message("FRI", name: 'FRI');
  }

  String get SAT {
    return Intl.message("SAT", name: 'SAT');
  }

  String get DASHBOARD {
    return Intl.message("DASHBOARD", name: 'DASHBOARD');
  }

  String get ANALYTICS_FROM {
    return Intl.message("ANALYTICS FROM", name: 'ANALYTICS_FROM');
  }

  String get LAST_YEAR {
    return Intl.message("LAST YEAR", name: 'LAST_YEAR');
  }

  String get LAST_MONTH {
    return Intl.message("LAST MONTH", name: 'LAST_MONTH');
  }

  String get LAST_ORDERS {
    return Intl.message("LAST ORDERS", name: 'LAST_ORDERS');
  }

  String get VIEW_ALL {
    return Intl.message("VIEW ALL", name: 'VIEW_ALL');
  }

  String get ORDERS {
    return Intl.message("ORDERS", name: 'ORDERS');
  }

  String get Latest_first {
    return Intl.message("Latest first", name: 'Latest_first');
  }

  String get Delayed {
    return Intl.message("Delayed", name: 'Delayed');
  }

  String get Canceled {
    return Intl.message("Canceled", name: 'Canceled');
  }

  String get Declined {
    return Intl.message("Declined", name: 'Declined');
  }

  String get Complete {
    return Intl.message("Complete", name: 'Complete');
  }

  String get Reviews {
    return Intl.message("Reviews", name: 'Reviews');
  }

  String get ORDER_N {
    return Intl.message("ORDER Nº", name: 'ORDER_N');
  }

  String get ORDERED_SERVICES {
    return Intl.message("ORDERED SERVICES", name: 'ORDERED_SERVICES');
  }

  String get DATE {
    return Intl.message("DATE", name: 'DATE');
  }

  String get TIME {
    return Intl.message("TIME", name: 'TIME');
  }

  String get N_OF_MAIDS {
    return Intl.message("Nº OF MAIDS", name: 'N_OF_MAIDS');
  }

  String get DURATION {
    return Intl.message("DURATION", name: 'DURATION');
  }

  String get PAYMENT_METHOD {
    return Intl.message("PAYMENT METHOD", name: 'PAYMENT_METHOD');
  }

  String get PRICE {
    return Intl.message("PRICE", name: 'PRICE');
  }

  String get Leave_a_feedback {
    return Intl.message("Leave a feedback", name: 'Leave_a_feedback');
  }

  String get Decline_this_order {
    return Intl.message("Decline this order", name: 'Decline_this_order');
  }

  String get Talk_to {
    return Intl.message("Talk to", name: 'Talk_to');
  }

  String get An_error_happened {
    return Intl.message("An error happened", name: 'An_error_happened');
  }

  String get An_error_happened_when_trying_to_update_the_status_of_this_order {
    return Intl.message("An error happened when trying to update the status of this order. Please try again.", name: 'An_error_happened_when_trying_to_update_the_status_of_this_order');
  }

  String get Ok {
    return Intl.message("Ok", name: 'Ok');
  }

  String get TAKE_THIS_ORDER {
    return Intl.message("TAKE THIS\\nORDER", name: 'TAKE_THIS_ORDER');
  }

  String get ORDER_IN_PROGRESS {
    return Intl.message("ORDER IN\\nPROGRESS", name: 'ORDER_IN_PROGRESS');
  }

  String get ORDER_DELAYED {
    return Intl.message("ORDER\\nDELAYED", name: 'ORDER_DELAYED');
  }

  String get ORDER_COMPLETED {
    return Intl.message("ORDER\\nCOMPLETED", name: 'ORDER_COMPLETED');
  }

  String get ORDER_CANCELED {
    return Intl.message("ORDER\\nCANCELED", name: 'ORDER_CANCELED');
  }

  String get ORDER_DECLINED {
    return Intl.message("ORDER\\nDECLINED", name: 'ORDER_DECLINED');
  }

  String get DECLINE_REASON {
    return Intl.message("DECLINE REASON", name: 'DECLINE_REASON');
  }

  String get DUE_TO_VIOLATION_OF_A_POLICY {
    return Intl.message("DUE TO VIOLATION OF A POLICY", name: 'DUE_TO_VIOLATION_OF_A_POLICY');
  }

  String get Other_reason {
    return Intl.message("Other reason", name: 'Other_reason');
  }

  String get SUBMIT {
    return Intl.message("SUBMIT", name: 'SUBMIT');
  }

  String get SUBMITTED {
    return Intl.message("SUBMITTED", name: 'SUBMITTED');
  }

  String get NOTIFICATIONS {
    return Intl.message("NOTIFICATIONS", name: 'NOTIFICATIONS');
  }

  String get Notifications {
    return Intl.message("Notifications", name: 'Notifications');
  }

  String get MESSAGES {
    return Intl.message("MESSAGES", name: 'MESSAGES');
  }

  String get New_Messages {
    return Intl.message("New Messages", name: 'New_Messages');
  }

  String get ARCHIVED_MESSAGES {
    return Intl.message("ARCHIVED MESSAGES", name: 'ARCHIVED_MESSAGES');
  }

  String get Unarchive {
    return Intl.message("Unarchive", name: 'Unarchive');
  }

  String get Archive {
    return Intl.message("Archive", name: 'Archive');
  }

  String get Send {
    return Intl.message("Send", name: 'Send');
  }

  String get Type_a_message {
    return Intl.message("Type a message", name: 'Type_a_message');
  }

  String get SETTINGS {
    return Intl.message("SETTINGS", name: 'SETTINGS');
  }

  String get Profile {
    return Intl.message("Profile", name: 'Profile');
  }

  String get Service_pricing {
    return Intl.message("Service & pricing", name: 'Service_pricing');
  }

  String get Security {
    return Intl.message("Security", name: 'Security');
  }

  String get Please_make_sure_to_disable_your_availability {
    return Intl.message("Please make sure to disable your availability once you can no longer afford to take other orders", name: 'Please_make_sure_to_disable_your_availability');
  }

  String get Availability {
    return Intl.message("Availability", name: 'Availability');
  }

  String get REVIEWS {
    return Intl.message("REVIEWS", name: 'REVIEWS');
  }

  String get PROFILE {
    return Intl.message("PROFILE", name: 'PROFILE');
  }

  String get Since {
    return Intl.message("Since", name: 'Since');
  }

  String get Jobs_Done {
    return Intl.message("Jobs Done", name: 'Jobs_Done');
  }

  String get Rating {
    return Intl.message("Rating", name: 'Rating');
  }

  String get Available_Balance {
    return Intl.message("Available Balance", name: 'Available_Balance');
  }

  String get Company_name {
    return Intl.message("Company name", name: 'Company_name');
  }

  String get Email_address {
    return Intl.message("Email address", name: 'Email_address');
  }

  String get Phone_number {
    return Intl.message("Phone number", name: 'Phone_number');
  }

  String get Address {
    return Intl.message("Address", name: 'Address');
  }

  String get Description {
    return Intl.message("Description", name: 'Description');
  }

  String get UPDATE {
    return Intl.message("UPDATE", name: 'UPDATE');
  }

  String get UPDATED {
    return Intl.message("UPDATED", name: 'UPDATED');
  }

  String get SERVICES_PRICING {
    return Intl.message("SERVICES & PRICING", name: 'SERVICES_PRICING');
  }

  String get Price {
    return Intl.message("Price", name: 'Price');
  }

  String get Per_hour {
    return Intl.message("Per hour", name: 'Per_hour');
  }

  String get Discount {
    return Intl.message("Discount", name: 'Discount');
  }

  String get New_Price {
    return Intl.message("New Price", name: 'New_Price');
  }

  String get PLEASE_SELECT_FROM_THESES_SERVICES_THOSE_YOU_PROVIDE {
    return Intl.message("PLEASE SELECT FROM THESES SERVICES THOSE YOU PROVIDE", name: 'PLEASE_SELECT_FROM_THESES_SERVICES_THOSE_YOU_PROVIDE');
  }

  String get SECURITY {
    return Intl.message("SECURITY", name: 'SECURITY');
  }

  String get PASSWORD {
    return Intl.message("PASSWORD", name: 'PASSWORD');
  }

  String get New_password {
    return Intl.message("New password", name: 'New_password');
  }

  String get The_passwords_dont_match {
    return Intl.message("The passwords don't match", name: 'The_passwords_dont_match');
  }

  String get SIGN_IN_WITH_GOOGLE {
    return Intl.message("SIGN IN WITH GOOGLE", name: 'SIGN_IN_WITH_GOOGLE');
  }

  String get OR_SIGN_IN_WITH_EMAIL {
    return Intl.message("OR SIGN IN WITH E-MAIL", name: 'OR_SIGN_IN_WITH_EMAIL');
  }

  String get Email {
    return Intl.message("Email", name: 'Email');
  }

  String get Invalid_email_address {
    return Intl.message("Invalid email address.", name: 'Invalid_email_address');
  }

  String get Password {
    return Intl.message("Password", name: 'Password');
  }

  String get The_password_must_contain_at_least_6_characters {
    return Intl.message("The password must contain at least 6 characters.", name: 'The_password_must_contain_at_least_6_characters');
  }

  String get Forgot_password {
    return Intl.message("Forgot password ?", name: 'Forgot_password');
  }

  String get LOGIN {
    return Intl.message("LOGIN", name: 'LOGIN');
  }

  String get Dont_have_an_account {
    return Intl.message("Don't have an account ? ", name: 'Dont_have_an_account');
  }

  String get SIGN_UP {
    return Intl.message("SIGN UP", name: 'SIGN_UP');
  }

  String get QUICK_SEARCH {
    return Intl.message("QUICK SEARCH", name: 'QUICK_SEARCH');
  }

  String get Signin_error {
    return Intl.message("Sign-in error", name: 'Signin_error');
  }

  String get Incorrect_email_or_password {
    return Intl.message("Incorrect email or password.", name: 'Incorrect_email_or_password');
  }

  String get How_long_do_you_need_our_maid_to_stay {
    return Intl.message("How long do you need our maid to stay (in hours)?", name: 'How_long_do_you_need_our_maid_to_stay');
  }

  String get How_many_maids_do_you_need {
    return Intl.message("How many maids do you need ?", name: 'How_many_maids_do_you_need');
  }

  String get Do_you_require_cleaning_materials {
    return Intl.message("Do you require cleaning materials?", name: 'Do_you_require_cleaning_materials');
  }

  String get CHECK_AVAILABILITY {
    return Intl.message("CHECK AVAILABILITY", name: 'CHECK_AVAILABILITY');
  }

  String get Signup_error {
    return Intl.message("Signup error", name: 'Signup_error');
  }

  String get An_unexpected_error_happened_Please_try_again {
    return Intl.message("An unexpected error happened. Please try again.", name: 'An_unexpected_error_happened_Please_try_again');
  }

  String get IN_ORDER_TO_COMPLETE_THIS_WE_WILL_NEED_SOME_DETAILS {
    return Intl.message("IN ORDER TO COMPLETE THIS\\nWE WILL NEED SOME DETAILS", name: 'IN_ORDER_TO_COMPLETE_THIS_WE_WILL_NEED_SOME_DETAILS');
  }

  String get By_creating_an_account_you_agree_to_our {
    return Intl.message("By creating an account you agree to our\\n", name: 'By_creating_an_account_you_agree_to_our');
  }

  String get Terms_of_service {
    return Intl.message("Terms of service", name: 'Terms_of_service');
  }

  String get and {
    return Intl.message(" and ", name: 'and');
  }

  String get Privacy_policy {
    return Intl.message("Privacy policy", name: 'Privacy_policy');
  }

  String get CONTINUE {
    return Intl.message("CONTINUE", name: 'CONTINUE');
  }

  String get An_error_occurred {
    return Intl.message("An error occurred", name: 'An_error_occurred');
  }

  String get An_error_occurred_while_trying_to_authenticate_you_in_with_Google {
    return Intl.message("An error occurred while trying to authenticate you with Google. Please try again.", name: 'An_error_occurred_while_trying_to_authenticate_you_in_with_Google');
  }

  String get SIGN_UP_WITH_GOOGLE {
    return Intl.message("SIGN UP WITH GOOGLE", name: 'SIGN_UP_WITH_GOOGLE');
  }

  String get OR_SIGN_UP_WITH_EMAIL {
    return Intl.message("OR SIGN UP WITH E-MAIL", name: 'OR_SIGN_UP_WITH_EMAIL');
  }

  String get First_name {
    return Intl.message("First name", name: 'First_name');
  }

  String get Last_name {
    return Intl.message("Last name", name: 'Last_name');
  }

  String get Phone_N {
    return Intl.message("Phone Nº", name: 'Phone_N');
  }

  String get SELECT_A_ZONE {
    return Intl.message("SELECT A ZONE", name: 'SELECT_A_ZONE');
  }

  String get Select_city {
    return Intl.message("Select city", name: 'Select_city');
  }

  String get Please_select_a_city {
    return Intl.message("Please select a city.", name: 'Please_select_a_city');
  }

  String get City {
    return Intl.message("City", name: 'City');
  }

  String get Zone {
    return Intl.message("Zone", name: 'Zone');
  }

  String get Please_select_a_city_first {
    return Intl.message("Please select a city first.", name: 'Please_select_a_city_first');
  }

  String get VALIDATE {
    return Intl.message("VALIDATE", name: 'VALIDATE');
  }

  String get Type_your_city {
    return Intl.message("Type your city", name: 'Type_your_city');
  }

  String get Type_your_zone_in {
    return Intl.message("Type your zone in", name: 'Type_your_zone_in');
  }

  String get SELECT_A_COMPANY {
    return Intl.message("SELECT A COMPANY", name: 'SELECT_A_COMPANY');
  }

  String get Name_AZ {
    return Intl.message("Name A-Z", name: 'Name_AZ');
  }

  String get Lowest_Price {
    return Intl.message("Lowest Price", name: 'Lowest_Price');
  }

  String get Highest_Price {
    return Intl.message("Highest Price", name: 'Highest_Price');
  }

  String get No_result_found {
    return Intl.message("No result found.", name: 'No_result_found');
  }

  String get Error {
    return Intl.message("Error", name: 'Error');
  }

  String get You_should_write_a_review_if_you_want_to_leave_a_negative_feedback {
    return Intl.message("You should write a review if you want to leave a negative feedback.", name: 'You_should_write_a_review_if_you_want_to_leave_a_negative_feedback');
  }

  String get An_unexpected_error_happened_Please_try_submitting_your_review_again {
    return Intl.message("An unexpected error happened. Please try submitting your review again.", name: 'An_unexpected_error_happened_Please_try_submitting_your_review_again');
  }

  String get LEAVE_A_FEEDBACK {
    return Intl.message("LEAVE A FEEDBACK", name: 'LEAVE_A_FEEDBACK');
  }

  String get NEGATIVE_FEEDBACK {
    return Intl.message("NEGATIVE FEEDBACK", name: 'NEGATIVE_FEEDBACK');
  }

  String get Unpaid {
    return Intl.message("Unpaid", name: 'Unpaid');
  }

  String get EDIT_PROFILE {
    return Intl.message("EDIT PROFILE", name: 'EDIT_PROFILE');
  }

  String get Member_since {
    return Intl.message("Member since", name: 'Member_since');
  }

  String get Completed_Orders {
    return Intl.message("Completed\\nOrders", name: 'Completed_Orders');
  }

  String get Canceled_Orders {
    return Intl.message("Canceled\\nOrders", name: 'Canceled_Orders');
  }

  String get Full_name {
    return Intl.message("Full name", name: 'Full_name');
  }

  String get PICK_DATE_TIME {
    return Intl.message("PICK DATE & TIME", name: 'PICK_DATE_TIME');
  }

  String get BOOK_NOW {
    return Intl.message("BOOK NOW", name: 'BOOK_NOW');
  }

  String get Su {
    return Intl.message("Su", name: 'Su');
  }

  String get Mo {
    return Intl.message("Mo", name: 'Mo');
  }

  String get Tu {
    return Intl.message("Tu", name: 'Tu');
  }

  String get We {
    return Intl.message("We", name: 'We');
  }

  String get Th {
    return Intl.message("Th", name: 'Th');
  }

  String get Fr {
    return Intl.message("Fr", name: 'Fr');
  }

  String get Sa {
    return Intl.message("Sa", name: 'Sa');
  }

  String get DETAILS {
    return Intl.message("DETAILS", name: 'DETAILS');
  }

  String get price_per_hour_is {
    return Intl.message("price per hour is", name: 'price_per_hour_is');
  }

  String get TOTAL {
    return Intl.message("TOTAL", name: 'TOTAL');
  }

  String get CHECKOUT {
    return Intl.message("CHECKOUT", name: 'CHECKOUT');
  }

  String get HOURS {
    return Intl.message("HOURS", name: 'HOURS');
  }

  String get us {
    return Intl.message("us", name: 'us');
  }

  String get TOTAL_EARN {
    return Intl.message("TOTAL EARN", name: 'TOTAL_EARN');
  }

  String get You_dont_have_any_order_yet {
    return Intl.message("You don\\'t have any order yet. Do you want to make a new one ?", name: 'You_dont_have_any_order_yet');
  }

  String get NEW_ORDER {
    return Intl.message("NEW ORDER", name: 'NEW_ORDER');
  }


}

class GeneratedLocalizationsDelegate extends LocalizationsDelegate<S> {
  const GeneratedLocalizationsDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
			Locale("en", ""),
			Locale("ar", ""),

    ];
  }

  LocaleListResolutionCallback listResolution({Locale fallback}) {
    return (List<Locale> locales, Iterable<Locale> supported) {
      if (locales == null || locales.isEmpty) {
        return fallback ?? supported.first;
      } else {
        return _resolve(locales.first, fallback, supported);
      }
    };
  }

  LocaleResolutionCallback resolution({Locale fallback}) {
    return (Locale locale, Iterable<Locale> supported) {
      return _resolve(locale, fallback, supported);
    };
  }

  Locale _resolve(Locale locale, Locale fallback, Iterable<Locale> supported) {
    if (locale == null || !isSupported(locale)) {
      return fallback ?? supported.first;
    }

    final Locale languageLocale = Locale(locale.languageCode, "");
    if (supported.contains(locale)) {
      return locale;
    } else if (supported.contains(languageLocale)) {
      return languageLocale;
    } else {
      final Locale fallbackLocale = fallback ?? supported.first;
      return fallbackLocale;
    }
  }

  @override
  Future<S> load(Locale locale) {
    return S.load(locale);
  }

  @override
  bool isSupported(Locale locale) =>
    locale != null && supportedLocales.contains(locale);

  @override
  bool shouldReload(GeneratedLocalizationsDelegate old) => false;
}
